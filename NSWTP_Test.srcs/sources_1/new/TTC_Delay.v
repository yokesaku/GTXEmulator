`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2016/08/02 01:24:54
// Design Name: 
// Module Name: TTC_Delay
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module TTC_Delay(
    input wire CLK_in,
    input wire reset_in,
    input wire Delay_reset_in,

    input wire L1A_in,
    input wire BCR_in,
    input wire ECR_in,
    input wire TTC_RESET_in,
    input wire TEST_PULSE_in,

    input wire [7:0] L1A_delay_in,
    input wire [7:0] BCR_delay_in,
    input wire [7:0] trig_BCR_delay_in,
    input wire [7:0] ECR_delay_in,
    input wire [7:0] TTC_RESET_delay_in,
    input wire [7:0] TEST_PULSE_delay_in,

    output wire L1A_delayed_out,
    output wire BCR_delayed_out,
    output wire trig_BCR_delayed_out,
    output wire ECR_delayed_out,
    output wire TTC_RESET_delayed_out,   
    output wire TEST_PULSE_delayed_out
    );

wire RST_in = reset_in||Delay_reset_in;

Delay_single_bit Delay_L1A( .CLK_in(CLK_in), .data_in(L1A_in), .delaynum_in(L1A_delay_in), .reset_in(RST_in), .data_out(L1A_delayed_out));
Delay_single_bit Delay_BCR( .CLK_in(CLK_in), .data_in(BCR_in), .delaynum_in(BCR_delay_in), .reset_in(RST_in), .data_out(BCR_delayed_out));
Delay_single_bit Delay_trig_BCR( .CLK_in(CLK_in), .data_in(BCR_in), .delaynum_in(trig_BCR_delay_in), .reset_in(RST_in), .data_out(trig_BCR_delayed_out));
Delay_single_bit Delay_ECR( .CLK_in(CLK_in), .data_in(ECR_in), .delaynum_in(ECR_delay_in), .reset_in(RST_in), .data_out(ECR_delayed_out));
Delay_single_bit Delay_TTC_RESET( .CLK_in(CLK_in), .data_in(TTC_RESET_in), .delaynum_in(TTC_RESET_delay_in), .reset_in(RST_in), .data_out(TTC_RESET_delayed_out));
Delay_single_bit Delay_TEST_PULSE( .CLK_in(CLK_in), .data_in(TEST_PULSE_in), .delaynum_in(TEST_PULSE_delay_in), .reset_in(RST_in), .data_out(TEST_PULSE_delayed_out));

endmodule
