`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2016/07/11 00:23:43
// Design Name: 
// Module Name: GTX_RX
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module GTX_RX(
    input wire [31:0] gt0_rxdata_in,
    input wire [31:0] gt1_rxdata_in,
    input wire [31:0] gt2_rxdata_in,
    input wire [31:0] gt3_rxdata_in,
    input wire [31:0] gt4_rxdata_in,
    input wire [31:0] gt5_rxdata_in,
    input wire [31:0] gt6_rxdata_in,
    input wire [31:0] gt7_rxdata_in,
    input wire [31:0] gt8_rxdata_in,
    input wire [31:0] gt9_rxdata_in,
    input wire [15:0] gt10_rxdata_in,
    input wire [15:0] gt11_rxdata_in,
    
    input wire [3:0] gt0_rxcharisk_in,
    input wire [3:0] gt1_rxcharisk_in,
    input wire [3:0] gt2_rxcharisk_in,
    input wire [3:0] gt3_rxcharisk_in,
    input wire [3:0] gt4_rxcharisk_in,
    input wire [3:0] gt5_rxcharisk_in,
    input wire [3:0] gt6_rxcharisk_in,
    input wire [3:0] gt7_rxcharisk_in,
    input wire [3:0] gt8_rxcharisk_in,
    input wire [3:0] gt9_rxcharisk_in,
    input wire [1:0] gt10_rxcharisk_in,
    input wire [1:0] gt11_rxcharisk_in,

    input wire gt0_rxusrclk_in,
    input wire gt1_rxusrclk_in,
    input wire gt2_rxusrclk_in,
    input wire gt3_rxusrclk_in,
    input wire gt4_rxusrclk_in,
    input wire gt5_rxusrclk_in,
    input wire gt6_rxusrclk_in,
    input wire gt7_rxusrclk_in,
    input wire gt8_rxusrclk_in,
    input wire gt9_rxusrclk_in,
    input wire gt10_rxusrclk_in,
    input wire gt11_rxusrclk_in,

    input wire reset_in,
    input wire RX_reset_in,

    output reg  [111:0] rx0_data_out,
    output reg  [111:0] rx1_data_out,
    output reg  [111:0] rx2_data_out,
    output reg  [111:0] rx3_data_out,
    output reg  [111:0] rx4_data_out,
    output reg  [111:0] rx5_data_out,
    output reg  [111:0] rx6_data_out,
    output reg  [111:0] rx7_data_out,
    output reg  [111:0] rx8_data_out,
    output reg  [111:0] rx9_data_out,
    output reg  [111:0] rx10_data_out,
    output reg  [111:0] rx11_data_out
    //output wire [11:0] rx_BCID
    );
    
///// RX LOGIC ////////
    
    //Tihs RX Logic receives data and aligns into 128 bit data. (comment by akatsuka)
    wire RST_in = reset_in||RX_reset_in;
    
    //reg [11:0] rx_BCID_reg;
    reg [111:0] rx0_register;
    reg [111:0] rx1_register;
    reg [111:0] rx2_register;
    reg [111:0] rx3_register;
    reg [111:0] rx4_register;
    reg [111:0] rx5_register;
    reg [111:0] rx6_register;
    reg [111:0] rx7_register;
    reg [111:0] rx8_register;
    reg [111:0] rx9_register;
    reg [111:0] rx10_register;
    reg [111:0] rx11_register;

    reg [4:0] rx0_state = 5'b1;
    reg [4:0] rx1_state = 5'b1;
    reg [4:0] rx2_state = 5'b1;
    reg [4:0] rx3_state = 5'b1;
    reg [4:0] rx4_state = 5'b1;
    reg [4:0] rx5_state = 5'b1;
    reg [4:0] rx6_state = 5'b1;
    reg [4:0] rx7_state = 5'b1;
    reg [4:0] rx8_state = 5'b1;
    reg [4:0] rx9_state = 5'b1;
    reg [1:0] rx10_state = 2'b1;
    reg [1:0] rx11_state = 2'b1;

    parameter HEADER = 5'b10, DATA1 = 5'b100, DATA2 = 5'b1000, FOOTER = 5'b10000;
    parameter HEADER_16 = 2'b10, FOOTER_16 = 2'b11;

//assign rx_BCID = (gt0_rxcharisk_in == 4'b1) ? gt0_rxdata_in[15:8] : 8'hf;

always @(posedge gt0_rxusrclk_in) begin
    if (RST_in) begin
        rx0_state <= HEADER;
        rx0_register <= 112'b0;
    end
    else begin
        case (rx0_state)
            HEADER : begin
                rx0_data_out <= rx0_register;
                if (gt0_rxcharisk_in == 4'h3) begin      // detect comma (comment by akatsuka)
                    rx0_register[111:0]  <=    {rx0_register[95:0], gt0_rxdata_in[15:0], gt0_rxdata_in[31:16]};
                    rx0_state            <=    DATA1;
                end
            end
            DATA1 : begin
                rx0_register[111:0]  <=    {rx0_register[95:0], gt0_rxdata_in[15:0], gt0_rxdata_in[31:16]};
                rx0_state            <=    DATA2;
            end
            DATA2 : begin
                rx0_register[111:0]  <=    {rx0_register[95:0], gt0_rxdata_in[15:0], gt0_rxdata_in[31:16]};
                rx0_state            <=    FOOTER;
            end
            FOOTER : begin
                rx0_register[111:0]  <=    {rx0_register[95:0], gt0_rxdata_in[15:0], gt0_rxdata_in[31:16]};
                rx0_state            <=    HEADER;
            end
            default : begin
                rx0_register[111:0]  <=    112'hbbbb;
                rx0_state            <=    HEADER;
            end
        endcase
    end
end
        
always @(posedge gt1_rxusrclk_in) begin
    if (RST_in) begin
        rx1_state <= HEADER;
        rx1_register <= 112'b0;
    end
    else begin
        case (rx1_state)
            HEADER : begin
                rx1_data_out <= rx1_register;
                if (gt1_rxcharisk_in == 4'h3) begin      // detect comma (comment by akatsuka)
                    rx1_register[111:0]  <=    {rx1_register[95:0], gt1_rxdata_in[15:0], gt1_rxdata_in[31:16]};
                    rx1_state            <=    DATA1;
                end
            end
            DATA1 : begin
                 rx1_register[111:0]  <=    {rx1_register[95:0], gt1_rxdata_in[15:0], gt1_rxdata_in[31:16]};
                 rx1_state            <=    DATA2;
            end
            DATA2 : begin
                rx1_register[111:0]  <=    {rx1_register[95:0], gt1_rxdata_in[15:0], gt1_rxdata_in[31:16]};
                rx1_state            <=    FOOTER;
            end
            FOOTER : begin
                rx1_register[111:0]  <=    {rx1_register[95:0], gt1_rxdata_in[15:0], gt1_rxdata_in[31:16]};
                rx1_state            <=    HEADER;
            end
            default : begin
                rx1_register[111:0]  <=    112'hbbbb;
                rx1_state            <=    HEADER;
            end
        endcase
    end
end

always @(posedge gt2_rxusrclk_in) begin
    if (RST_in) begin
        rx2_state <= HEADER;
        rx2_register <= 112'b0;
    end
    else begin
        case (rx2_state)
            HEADER : begin
                rx2_data_out <= rx2_register;
                if (gt2_rxcharisk_in == 4'h3) begin      // detect comma (comment by akatsuka)
                    rx2_register[111:0]  <=    {rx2_register[95:0], gt2_rxdata_in[15:0], gt2_rxdata_in[31:16]};
                    rx2_state            <=    DATA1;
                end
            end
            DATA1 : begin
                    rx2_register[111:0]  <=    {rx2_register[95:0], gt2_rxdata_in[15:0], gt2_rxdata_in[31:16]};
                    rx2_state            <=    DATA2;
            end
            DATA2 : begin
                    rx2_register[111:0]  <=    {rx2_register[95:0], gt2_rxdata_in[15:0], gt2_rxdata_in[31:16]};
                    rx2_state            <=    FOOTER;
            end
            FOOTER : begin
                    rx2_register[111:0]  <=    {rx2_register[95:0], gt2_rxdata_in[15:0], gt2_rxdata_in[31:16]};
                    rx2_state            <=    HEADER;
            end
            default : begin
                rx2_register[111:0]  <=    112'hbbbb;
                rx2_state            <=    HEADER;
            end
        endcase
    end
end

always @(posedge gt3_rxusrclk_in) begin
    if (RST_in) begin
        rx3_state <= HEADER;
        rx3_register <= 112'b0;
    end
    else begin
        case (rx3_state)
            HEADER : begin
                rx3_data_out <= rx3_register;
                if (gt3_rxcharisk_in == 4'h3) begin      // detect comma (comment by akatsuka)
                    rx3_register[111:0]  <=    {rx3_register[95:0], gt3_rxdata_in[15:0], gt3_rxdata_in[31:16]};
                    rx3_state            <=    DATA1;
                end
            end
            DATA1 : begin
                    rx3_register[111:0]  <=    {rx3_register[95:0], gt3_rxdata_in[15:0], gt3_rxdata_in[31:16]};
                    rx3_state            <=    DATA2;
            end
            DATA2 : begin
                    rx3_register[111:0]  <=    {rx3_register[95:0], gt3_rxdata_in[15:0], gt3_rxdata_in[31:16]};
                    rx3_state            <=    FOOTER;
            end
            FOOTER : begin
                    rx3_register[111:0]  <=    {rx3_register[95:0], gt3_rxdata_in[15:0], gt3_rxdata_in[31:16]};
                    rx3_state            <=    HEADER;
            end
            default : begin
                rx3_register[111:0]  <=    112'hbbbb;
                rx3_state            <=    HEADER;
            end
        endcase
    end
end

always @(posedge gt4_rxusrclk_in) begin
    if (RST_in) begin
        rx4_state <= HEADER;
        rx4_register <= 112'b0;
    end
    else begin
        case (rx4_state)
            HEADER : begin
                rx4_data_out <= rx4_register;
                if (gt4_rxcharisk_in == 4'h3) begin      // detect comma (comment by akatsuka)
                    rx4_register[111:0]  <=    {rx4_register[95:0], gt4_rxdata_in[15:0], gt4_rxdata_in[31:16]};
                    rx4_state            <=    DATA1;
                end
            end
            DATA1 : begin
                    rx4_register[111:0]  <=    {rx4_register[95:0], gt4_rxdata_in[15:0], gt4_rxdata_in[31:16]};
                    rx4_state            <=    DATA2;
            end
            DATA2 : begin
                    rx4_register[111:0]  <=    {rx4_register[95:0], gt4_rxdata_in[15:0], gt4_rxdata_in[31:16]};
                    rx4_state            <=    FOOTER;
            end
            FOOTER : begin
                    rx4_register[111:0]  <=    {rx4_register[95:0], gt4_rxdata_in[15:0], gt4_rxdata_in[31:16]};
                    rx4_state            <=    HEADER;
            end
            default : begin
                rx4_register[111:0]  <=    112'hbbbb;
                rx4_state            <=    HEADER;
            end
        endcase
    end
end

always @(posedge gt5_rxusrclk_in) begin
    if (RST_in) begin
        rx5_state <= HEADER;
        rx5_register <= 112'b0;
    end
    else begin
        case (rx5_state)
            HEADER : begin
                rx5_data_out <= rx5_register;
                if (gt5_rxcharisk_in == 4'h3) begin      // detect comma (comment by akatsuka)
                    rx5_register[111:0]  <=    {rx5_register[95:0], gt5_rxdata_in[15:0], gt5_rxdata_in[31:16]};
                    rx5_state            <=    DATA1;
                end
            end
            DATA1 : begin
                    rx5_register[111:0]  <=    {rx5_register[95:0], gt5_rxdata_in[15:0], gt5_rxdata_in[31:16]};
                    rx5_state            <=    DATA2;
            end
            DATA2 : begin
                    rx5_register[111:0]  <=    {rx5_register[95:0], gt5_rxdata_in[15:0], gt5_rxdata_in[31:16]};
                    rx5_state            <=    FOOTER;
            end
            FOOTER : begin
                    rx5_register[111:0]  <=    {rx5_register[95:0], gt5_rxdata_in[15:0], gt5_rxdata_in[31:16]};
                    rx5_state            <=    HEADER;
            end
            default : begin
                rx5_register[111:0]  <=    112'hbbbb;
                rx5_state            <=    HEADER;
            end
        endcase
    end
end

always @(posedge gt6_rxusrclk_in) begin
    if (RST_in) begin
        rx6_state <= HEADER;
        rx6_register <= 112'b0;
    end
    else begin
        case (rx6_state)
            HEADER : begin
                rx6_data_out <= rx6_register;
                if (gt6_rxcharisk_in == 4'h3) begin      // detect comma (comment by akatsuka)
                    rx6_register[111:0]  <=    {rx6_register[95:0], gt6_rxdata_in[15:0], gt6_rxdata_in[31:16]};
                    rx6_state            <=    DATA1;
                end
            end
            DATA1 : begin
                    rx6_register[111:0]  <=    {rx6_register[95:0], gt6_rxdata_in[15:0], gt6_rxdata_in[31:16]};
                    rx6_state            <=    DATA2;
            end
            DATA2 : begin
                    rx6_register[111:0]  <=    {rx6_register[95:0], gt6_rxdata_in[15:0], gt6_rxdata_in[31:16]};
                    rx6_state            <=    FOOTER;
            end
            FOOTER : begin
                    rx6_register[111:0]  <=    {rx6_register[95:0], gt6_rxdata_in[15:0], gt6_rxdata_in[31:16]};
                    rx6_state            <=    HEADER;
            end
            default : begin
                rx6_register[111:0]  <=    112'hbbbb;
                rx6_state            <=    HEADER;
            end
        endcase
    end
end

always @(posedge gt7_rxusrclk_in) begin
    if (RST_in) begin
        rx7_state <= HEADER;
        rx7_register <= 112'b0;
    end
    else begin
        case (rx7_state)
            HEADER : begin
                rx7_data_out <= rx7_register;
                if (gt7_rxcharisk_in == 4'h3) begin      // detect comma (comment by akatsuka)
                    rx7_register[111:0]  <=    {rx7_register[95:0], gt7_rxdata_in[15:0], gt7_rxdata_in[31:16]};
                    rx7_state            <=    DATA1;
                end
            end
            DATA1 : begin
                    rx7_register[111:0]  <=    {rx7_register[95:0], gt7_rxdata_in[15:0], gt7_rxdata_in[31:16]};
                    rx7_state            <=    DATA2;
            end
            DATA2 : begin
                    rx7_register[111:0]  <=    {rx7_register[95:0], gt7_rxdata_in[15:0], gt7_rxdata_in[31:16]};
                    rx7_state            <=    FOOTER;
            end
            FOOTER : begin
                    rx7_register[111:0]  <=    {rx7_register[95:0], gt7_rxdata_in[15:0], gt7_rxdata_in[31:16]};
                    rx7_state            <=    HEADER;
            end
            default : begin
                rx7_register[111:0]  <=    112'hbbbb;
                rx7_state            <=    HEADER;
            end
        endcase
    end
end

always @(posedge gt8_rxusrclk_in) begin
    if (RST_in) begin
        rx8_state <= HEADER;
        rx8_register <= 128'b0;
    end
    else begin
        case (rx8_state)
            HEADER : begin
                rx8_data_out <= rx8_register;
                if (gt8_rxcharisk_in == 4'h3) begin      // detect comma (comment by akatsuka)
                    rx8_register[111:0]  <=    {rx8_register[95:0], gt8_rxdata_in[15:0], gt8_rxdata_in[31:16]};
                    rx8_state            <=    DATA1;
                end
            end
            DATA1 : begin
                    rx8_register[111:0]  <=    {rx8_register[95:0], gt8_rxdata_in[15:0], gt8_rxdata_in[31:16]};
                    rx8_state            <=    DATA2;
            end
            DATA2 : begin
                    rx8_register[111:0]  <=    {rx8_register[95:0], gt8_rxdata_in[15:0], gt8_rxdata_in[31:16]};
                    rx8_state            <=    FOOTER;
            end
            FOOTER : begin
                    rx8_register[111:0]  <=    {rx8_register[95:0], gt8_rxdata_in[15:0], gt8_rxdata_in[31:16]};
                    rx8_state            <=    HEADER;
            end
            default : begin
                rx8_register[111:0]  <=    112'hbbbb;
                rx8_state            <=    HEADER;
            end
        endcase
    end
end

always @(posedge gt9_rxusrclk_in) begin
    if (RST_in) begin
        rx9_state <= HEADER;
        rx9_register <= 112'b0;
    end
    else begin
        case (rx9_state)
            HEADER : begin
                rx9_data_out <= rx9_register;
                if (gt9_rxcharisk_in == 4'h3) begin      // detect comma (comment by akatsuka)
                    rx9_register[111:0]  <=    {rx9_register[95:0], gt9_rxdata_in[15:0], gt9_rxdata_in[31:16]};
                    rx9_state            <=    DATA1;
                end
            end
            DATA1 : begin
                    rx9_register[111:0]  <=    {rx9_register[95:0], gt9_rxdata_in[15:0], gt9_rxdata_in[31:16]};
                    rx9_state            <=    DATA2;
            end
            DATA2 : begin
                    rx9_register[111:0]  <=    {rx9_register[95:0], gt9_rxdata_in[15:0], gt9_rxdata_in[31:16]};
                    rx9_state            <=    FOOTER;
            end
            FOOTER : begin
                    rx9_register[111:0]  <=    {rx9_register[95:0], gt9_rxdata_in[15:0], gt9_rxdata_in[31:16]};
                    rx9_state            <=    HEADER;
            end
            default : begin
                rx9_register[111:0]  <=    112'hbbbb;
                rx9_state            <=    HEADER;
            end
        endcase
    end
end

always @(posedge gt10_rxusrclk_in) begin
    if (RST_in) begin
        rx10_state <= HEADER_16;
        rx10_register <= 112'b0;
    end
    else begin
        case (rx10_state)
            HEADER_16 : begin
                rx10_data_out <= rx10_register;
                if (gt10_rxcharisk_in == 2'b1) begin      // detect comma (comment by akatsuka)
                    rx10_register[111:0]  <=    {rx10_register[95:0], gt10_rxdata_in[15:0]};
                    //rx10_state            <=    HEADER_162;  //for 1.6 Gbps
                    rx10_state            <=    FOOTER_16;
                end
            end
 /*           HEADER_162 : begin
                    rx10_register[111:0]  <=    {rx10_register[95:0], gt10_rxdata_in[15:0]};
                    rx10_state            <=    DATA1;
            end
            DATA1 : begin
                    rx10_register[111:0]  <=    {rx10_register[95:0], gt10_rxdata_in[15:0]};
                    rx10_state            <=    DATA2;
            end
            DATA2 : begin
                    rx10_register[111:0]  <=    {rx10_register[95:0], gt10_rxdata_in[15:0]};
                    rx10_state            <=    DATA3;
            end
            DATA3 : begin
                    rx10_register[111:0]  <=    {rx10_register[95:0], gt10_rxdata_in[15:0]};
                    rx10_state            <=    DATA4;
            end
            DATA4 : begin
                    rx10_register[111:0]  <=    {rx10_register[95:0], gt10_rxdata_in[15:0]};
                    rx10_state            <=    FOOTER_161;
            end
            FOOTER_161 : begin
                    rx10_register[111:0]  <=    {rx10_register[95:0], gt10_rxdata_in[15:0]};
                    rx10_state            <=    FOOTER_162;
            end*/  //for 1.6 Gbps
            FOOTER_16 : begin
                    rx10_register[111:0]  <=    {rx10_register[95:0], gt10_rxdata_in[15:0]};
                    rx10_state            <=    HEADER_16;
            end
            default : begin
                rx10_register[111:0]  <=    112'hbbbb;
                rx10_state            <=    HEADER_16;
            end
        endcase
    end
end

always @(posedge gt11_rxusrclk_in) begin
    if (RST_in) begin
        rx11_state <= HEADER_16;
        rx11_register <= 112'b0;
    end
    else begin
        case (rx11_state)
            HEADER_16 : begin
                rx11_data_out <= rx11_register;
                if (gt11_rxcharisk_in == 2'b1) begin      // detect comma (comment by akatsuka)
                    rx11_register[111:0]  <=    {rx11_register[95:0], gt11_rxdata_in[15:0]};
                    //rx11_state            <=    HEADER_162; // for 1.6 Gbps
                    rx11_state            <=    FOOTER_16;
                end
            end
            /*HEADER_162 : begin
                    rx11_register[111:0]  <=    {rx11_register[95:0], gt11_rxdata_in[15:0]};
                    rx11_state            <=    DATA1;
            end
            DATA1 : begin
                    rx11_register[111:0]  <=    {rx11_register[95:0], gt11_rxdata_in[15:0]};
                    rx11_state            <=    DATA2;
            end
            DATA2 : begin
                    rx11_register[111:0]  <=    {rx11_register[95:0], gt11_rxdata_in[15:0]};
                    rx11_state            <=    DATA3;
            end
            DATA3 : begin
                    rx11_register[111:0]  <=    {rx11_register[95:0], gt11_rxdata_in[15:0]};
                    rx11_state            <=    DATA4;
            end
            DATA4 : begin
                    rx11_register[111:0]  <=    {rx11_register[95:0], gt11_rxdata_in[15:0]};
                    rx11_state            <=    FOOTER_161;
            end
            FOOTER_161 : begin
                    rx11_register[111:0]  <=    {rx11_register[95:0], gt11_rxdata_in[15:0]};
                    rx11_state            <=    FOOTER_162;
            end*/ // for 1.6 Gbps
            FOOTER_16 : begin
                    rx11_register[111:0]  <=    {rx11_register[95:0], gt11_rxdata_in[15:0]};
                    rx11_state            <=    HEADER_16;
            end
            default : begin
                rx11_register[111:0]  <=    112'hbbbb;
                rx11_state            <=    HEADER_16;
            end
        endcase
    end
end



endmodule
