`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2016/07/17 17:21:56
// Design Name: 
// Module Name: Delay
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Delay(
    input wire CLK_in,
    input wire CLK_in_0,
    input wire CLK_in_1,
    input wire CLK_in_2,
    input wire CLK_in_3,
    input wire [111:0] data_in,
    input wire [6:0] delaynum_in,
    input wire [3:0] phase_in,
    input wire reset_in,
    output reg [5:0] phase_out_0,
    output reg [5:0] phase_out_1,
    output reg [5:0] phase_out_2,
    output reg [5:0] phase_out_3,
    output wire [111:0] data_out
    );


wire wr_enb=1'b1;
reg [6:0] wr_addr = 7'b0;
reg [111:0] dina_pos;
reg [111:0] dina_neg;

reg [111:0] dina_CLK_0;
reg [111:0] dina_CLK_1;
reg [111:0] dina_CLK_2;
reg [111:0] dina_CLK_3;

wire [111:0] dina;

reg [6:0] rd_addr = 7'b0;
wire [111:0] doutb;
//assign dina = (delaynum_in[0]) ? dina_pos : dina_neg;
assign dina = (phase_in == 3'h0) ? dina_CLK_0 :
	      (phase_in == 3'h1) ? dina_CLK_1 :
	      (phase_in == 3'h2) ? dina_CLK_2 :
	      (phase_in == 3'h3) ? dina_CLK_3 : 112'h_FFFF_FFFF_FFFF_FFFF_FFFF_FFFF_FFFF;

assign data_out = doutb;
//assign data_out = (phase_in == 3'h0) ? dina_CLK_0 :
//    (phase_in == 3'h1) ? dina_CLK_1 :
//    (phase_in == 3'h2) ? dina_CLK_2 :
//    (phase_in == 3'h3) ? dina_CLK_3 : 112'h_FFFF_FFFF_FFFF_FFFF_FFFF_FFFF_FFFF;

always @ (posedge CLK_in) begin
    if (reset_in) begin
	phase_out_0 <= 6'h0;
	phase_out_1 <= 6'h0;
	phase_out_2 <= 6'h0;
	phase_out_3 <= 6'h0;
    end else begin
	if      (dina_CLK_0 != dina_CLK_1) phase_out_0 <= phase_out_0 + 6'h1;
	else if (dina_CLK_1 != dina_CLK_2) phase_out_1 <= phase_out_1 + 6'h1;
	else if (dina_CLK_2 != dina_CLK_3) phase_out_2 <= phase_out_2 + 6'h1;
	else if (dina_CLK_3 != dina_CLK_0) phase_out_3 <= phase_out_3 + 6'h1; // This case cannot happen??
	else begin
	end
    end
end

always @ (posedge CLK_in) begin
    if (reset_in) begin
        wr_addr <= 7'b0 + delaynum_in[6:0];
        rd_addr <= 7'b0;        
    end
    else begin
        wr_addr <= wr_addr + 7'b1;
        rd_addr <= rd_addr + 7'b1;    
    end
end

always @ (posedge CLK_in_0) begin
    if (reset_in) begin
    end
    else begin
	dina_CLK_0 <= data_in;
    end
end

always @ (posedge CLK_in_1) begin
    if (reset_in) begin
    end
    else begin
	dina_CLK_1 <= data_in;
    end
end

always @ (posedge CLK_in_2) begin
    if (reset_in) begin
    end
    else begin
	dina_CLK_2 <= data_in;
    end
end

always @ (posedge CLK_in_3) begin
    if (reset_in) begin
    end
    else begin
	dina_CLK_3 <= data_in;
    end
end

delay_mem delay_mem (
  .clka(CLK_in),    // input wire clka
  .wea(wr_enb),      // input wire [0 : 0] wea
  .addra(wr_addr),  // input wire [6 : 0] addra
  .dina(dina),    // input wire [127 : 0] dina
  .clkb(CLK_in),    // input wire clkb
  .rstb(reset_in),    // input wire rstb
  .addrb(rd_addr),  // input wire [6 : 0] addrb
  .doutb(doutb)  // output wire [127 : 0] doutb
);

endmodule
