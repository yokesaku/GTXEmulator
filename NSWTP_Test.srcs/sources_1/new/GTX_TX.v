`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2016/07/11 00:23:43
// Design Name: 
// Module Name: GTX_RX
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module GTX_TX(
    input wire          CLK_in,    
    input wire          reset_in,
    input wire          TX_reset_in,
    input wire          TX_Logic_reset_in,

    input wire          gt_txusrclk_in,
    input wire          gt_txusrclk_in_80,
    input wire [11:0]   BCID_in,

    input wire [15:0]    gtx_header_in,

    input wire [15:0]    gt0_word1_in,
    input wire [15:0]    gt0_word2_in,
    input wire [15:0]    gt0_word3_in,
    input wire [15:0]    gt0_word4_in,
    input wire [15:0]    gt0_word5_in,
    input wire [15:0]    gt0_word6_in,
    input wire [15:0]    gt0_word7_in,

    input wire [15:0]    gt1_word1_in,
    input wire [15:0]    gt1_word2_in,
    input wire [15:0]    gt1_word3_in,
    input wire [15:0]    gt1_word4_in,
    input wire [15:0]    gt1_word5_in,
    input wire [15:0]    gt1_word6_in,
    input wire [15:0]    gt1_word7_in,

    input wire [15:0]    gt2_word1_in,
    input wire [15:0]    gt2_word2_in,
    input wire [15:0]    gt2_word3_in,
    input wire [15:0]    gt2_word4_in,
    input wire [15:0]    gt2_word5_in,
    input wire [15:0]    gt2_word6_in,
    input wire [15:0]    gt2_word7_in,

    input wire [15:0]    gt3_word1_in,
    input wire [15:0]    gt3_word2_in,
    input wire [15:0]    gt3_word3_in,
    input wire [15:0]    gt3_word4_in,
    input wire [15:0]    gt3_word5_in,
    input wire [15:0]    gt3_word6_in,
    input wire [15:0]    gt3_word7_in,

    input wire [15:0]    gt4_word1_in,
    input wire [15:0]    gt4_word2_in,
    input wire [15:0]    gt4_word3_in,
    input wire [15:0]    gt4_word4_in,
    input wire [15:0]    gt4_word5_in,
    input wire [15:0]    gt4_word6_in,
    input wire [15:0]    gt4_word7_in,

    input wire [15:0]    gt5_word1_in,
    input wire [15:0]    gt5_word2_in,
    input wire [15:0]    gt5_word3_in,
    input wire [15:0]    gt5_word4_in,
    input wire [15:0]    gt5_word5_in,
    input wire [15:0]    gt5_word6_in,
    input wire [15:0]    gt5_word7_in,

    input wire [15:0]    gt6_word1_in,
    input wire [15:0]    gt6_word2_in,
    input wire [15:0]    gt6_word3_in,
    input wire [15:0]    gt6_word4_in,
    input wire [15:0]    gt6_word5_in,
    input wire [15:0]    gt6_word6_in,
    input wire [15:0]    gt6_word7_in,

    input wire [15:0]    gt7_word1_in,
    input wire [15:0]    gt7_word2_in,
    input wire [15:0]    gt7_word3_in,
    input wire [15:0]    gt7_word4_in,
    input wire [15:0]    gt7_word5_in,
    input wire [15:0]    gt7_word6_in,
    input wire [15:0]    gt7_word7_in,

    input wire [15:0]    gt8_word1_in,
    input wire [15:0]    gt8_word2_in,
    input wire [15:0]    gt8_word3_in,
    input wire [15:0]    gt8_word4_in,
    input wire [15:0]    gt8_word5_in,
    input wire [15:0]    gt8_word6_in,
    input wire [15:0]    gt8_word7_in,

    input wire [15:0]    gt9_word1_in,
    input wire [15:0]    gt9_word2_in,
    input wire [15:0]    gt9_word3_in,
    input wire [15:0]    gt9_word4_in,
    input wire [15:0]    gt9_word5_in,
    input wire [15:0]    gt9_word6_in,
    input wire [15:0]    gt9_word7_in,

    input wire [15:0]    gt10_word0_in,
    input wire [15:0]    gt10_word1_in,

    input wire [15:0]    gt11_word0_in,
    input wire [15:0]    gt11_word1_in,

    input wire [15:0]   track0_S0_in,
    input wire [15:0]   track1_S0_in,
    input wire [15:0]   track2_S0_in,
    input wire [15:0]   track3_S0_in,
    input wire [3:0]    flag_S0_in,
    input wire [15:0]   track0_S1_in,
    input wire [15:0]   track1_S1_in,
    input wire [15:0]   track2_S1_in,
    input wire [15:0]   track3_S1_in,
    input wire [3:0]    flag_S1_in,

    output reg [31:0]   gt0_txdata_out,
    output reg [31:0]   gt1_txdata_out,
    output reg [31:0]   gt2_txdata_out,
    output reg [31:0]   gt3_txdata_out,
    output reg [31:0]   gt4_txdata_out,
    output reg [31:0]   gt5_txdata_out,
    output reg [31:0]   gt6_txdata_out,
    output reg [31:0]   gt7_txdata_out,
    output reg [31:0]   gt8_txdata_out,
    output reg [31:0]   gt9_txdata_out,
    output reg [15:0]   gt10_txdata_out,
    output reg [15:0]   gt11_txdata_out,

    output reg [3:0]    gt0_txcharisk_out,
    output reg [3:0]    gt1_txcharisk_out,
    output reg [3:0]    gt2_txcharisk_out,
    output reg [3:0]    gt3_txcharisk_out,
    output reg [3:0]    gt4_txcharisk_out,
    output reg [3:0]    gt5_txcharisk_out,
    output reg [3:0]    gt6_txcharisk_out,
    output reg [3:0]    gt7_txcharisk_out,
    output reg [3:0]    gt8_txcharisk_out,
    output reg [3:0]    gt9_txcharisk_out,
    output reg [1:0]    gt10_txcharisk_out,
    output reg [1:0]    gt11_txcharisk_out
    );
    
wire RST_in = reset_in||TX_reset_in||TX_Logic_reset_in;
    
parameter PAUSE = 5'b1;
parameter HEADER = 5'b10;
parameter TRACK1 = 5'b100;
parameter TRACK2 = 5'b1000;
parameter FOOTER = 5'b10000;
    
// tmp
wire [7:0] CRC0;
wire [7:0] CRC1;
    
///// TX LOGIC ////////
       
reg [4:0] TX0_state = 5'b1;
reg [4:0] TX10_state = 5'b1;
reg [15:0] counter = 16'b0;
reg TX0_CLK_is_high = 1'b1;
reg TX10_CLK_is_high = 1'b1;
    
//reg [15:0] TXcounterall = 16'h0;
//wire [15:0] TXcounterall_w;
//reg [15:0] TXcounter = 16'h0;
//reg [15:0] TXcounter80 = 16'h0;

//assign TXcounterall_w = TXcounterall;
    
    CRC8_D80    CRC_0(
        .Data   ({track0_S0_in, track1_S0_in, track2_S0_in, track3_S0_in, flag_S0_in[3:0], BCID_in[11:0]}),
        .crc    (8'b0),
        .new_crc(CRC0)
    );

CRC8_D80    CRC_1(
        .Data   ({track0_S1_in, track1_S1_in, track2_S1_in, track3_S1_in, flag_S1_in[3:0], BCID_in[11:0]}),
        .crc    (8'b0),
        .new_crc(CRC1)
    );



    always @(posedge gt_txusrclk_in or posedge RST_in) begin
       //TXcounterall <= TXcounterall + 16'b1; //for loopback test
       //TXcounter <= TXcounterall_w; // for loopback test
        if (RST_in) begin
            TX0_state <= PAUSE;
            gt0_txdata_out <= 32'hbcbc;
            gt0_txcharisk_out <= 4'h1;
            gt1_txdata_out <= 32'hbcbc;
            gt1_txcharisk_out <= 4'h1;
            gt2_txdata_out <= 32'hbcbc;
            gt2_txcharisk_out <= 4'h1;
            gt3_txdata_out <= 32'hbcbc;
            gt3_txcharisk_out <= 4'h1;
            gt4_txdata_out <= 32'hbcbc;
            gt4_txcharisk_out <= 4'h1;
            gt5_txdata_out <= 32'hbcbc;
            gt5_txcharisk_out <= 4'h1;
            gt6_txdata_out <= 32'hbcbc;
            gt6_txcharisk_out <= 4'h1;
            gt7_txdata_out <= 32'hbcbc;
            gt7_txcharisk_out <= 4'h1;
            gt8_txdata_out <= 32'hbcbc;
            gt8_txcharisk_out <= 4'h1;
            gt9_txdata_out <= 32'hbcbc;
            gt9_txcharisk_out <= 4'h1;
            
            counter[15:0] <= 16'b0;
            TX0_CLK_is_high <= 1'b1;
        end
        else begin
            case (TX0_state)
                PAUSE : begin
                    if(!CLK_in) begin
                        TX0_CLK_is_high <= 1'b0;
                        TX0_state <= PAUSE;
                    end
                    else if(CLK_in && !TX0_CLK_is_high) begin
                        TX0_CLK_is_high <= 1'b1;                        
                        TX0_state <= HEADER;
                    end
                    else begin
                        TX0_state <= PAUSE;                        
                    end
                end                
                HEADER : begin
                    gt0_txdata_out <= {gt0_word1_in, gtx_header_in};
                    gt0_txcharisk_out <= 4'h3;
                    gt1_txdata_out <= {gt1_word1_in, gtx_header_in};
                    gt1_txcharisk_out <= 4'h3;

                    gt2_txdata_out <= {gt2_word1_in, gtx_header_in};
                    gt2_txcharisk_out <= 4'h3;

                    gt3_txdata_out <= {gt3_word1_in, gtx_header_in};
                    gt3_txcharisk_out <= 4'h3;

                    gt4_txdata_out <= {gt4_word1_in, gtx_header_in};
                    gt4_txcharisk_out <= 4'h3;

                    gt5_txdata_out <= {gt5_word1_in, gtx_header_in};
                    gt5_txcharisk_out <= 4'h3;

                    gt6_txdata_out <= {gt6_word1_in, gtx_header_in};
                    gt6_txcharisk_out <= 4'h3;

                    gt7_txdata_out <= {gt7_word1_in, gtx_header_in};
                    gt7_txcharisk_out <= 4'h3;

                    //gt8_txdata_out <= {gt8_word1_in, gtx_header_in};
                    gt8_txdata_out <= {16'h8, gtx_header_in};
                    gt8_txcharisk_out <= 4'h3;

                    //gt9_txdata_out <= {gt9_word1_in, gtx_header_in};
                    gt9_txdata_out <= {16'h9, gtx_header_in};
                    gt9_txcharisk_out <= 4'h3;
                    TX0_state <= TRACK1;
                end                
                TRACK1 : begin
                    gt0_txdata_out <= {gt0_word3_in, gt0_word2_in};
                    gt0_txcharisk_out <= 4'h0;

                    gt1_txdata_out <= {gt1_word3_in, gt1_word2_in};
                    gt1_txcharisk_out <= 4'h0;

                    gt2_txdata_out <= {gt2_word3_in, gt2_word2_in};
                    gt2_txcharisk_out <= 4'h0;

                    gt3_txdata_out <= {gt3_word3_in, gt3_word2_in};
                    gt3_txcharisk_out <= 4'h0;

                    gt4_txdata_out <= {gt4_word3_in, gt4_word2_in};
                    gt4_txcharisk_out <= 4'h0;

                    gt5_txdata_out <= {gt5_word3_in, gt5_word2_in};
                    gt5_txcharisk_out <= 4'h0;

                    gt6_txdata_out <= {gt6_word3_in, gt6_word2_in};
                    gt6_txcharisk_out <= 4'h0;

                    gt7_txdata_out <= {gt7_word3_in, gt7_word2_in};
                    gt7_txcharisk_out <= 4'h0;

                    //gt8_txdata_out <= {gt8_word3_in, gt8_word2_in};
                    gt8_txdata_out <= {32'h_1234};
                    gt8_txcharisk_out <= 4'h0;

                    //gt9_txdata_out <= {gt9_word3_in, gt9_word2_in};
                    gt8_txdata_out <= {32'h_5678};
                    gt9_txcharisk_out <= 4'h0;
                    TX0_state <= TRACK2;
                end
                TRACK2 : begin
                    gt0_txdata_out <= {gt0_word5_in, gt0_word4_in};
                    gt0_txcharisk_out <= 4'b0;
                    gt1_txdata_out <= {gt1_word5_in, gt1_word4_in};
                    gt1_txcharisk_out <= 4'b0;
                    gt2_txdata_out <= {gt2_word5_in, gt2_word4_in};
                    gt2_txcharisk_out <= 4'b0;
                    gt3_txdata_out <= {gt3_word5_in, gt3_word4_in};
                    gt3_txcharisk_out <= 4'b0;
                    gt4_txdata_out <= {gt4_word5_in, gt4_word4_in};
                    gt4_txcharisk_out <= 4'b0;
                    gt5_txdata_out <= {gt5_word5_in, gt5_word4_in};
                    gt5_txcharisk_out <= 4'b0;
                    gt6_txdata_out <= {gt6_word5_in, gt6_word4_in};
                    gt6_txcharisk_out <= 4'b0;
                    gt7_txdata_out <= {gt7_word5_in, gt7_word4_in};
                    gt7_txcharisk_out <= 4'b0;
                    //gt8_txdata_out <= {gt8_word5_in, gt8_word4_in};
                    gt8_txdata_out <= {32'h_abcd};
                    gt8_txcharisk_out <= 4'b0;
                    //gt9_txdata_out <= {gt9_word5_in, gt9_word4_in};
                    gt9_txdata_out <= {32'h_0e0f};
                    gt9_txcharisk_out <= 4'b0;
                    TX0_state <= FOOTER;
                end
                FOOTER : begin
                    gt0_txdata_out <= {gt0_word7_in, gt0_word6_in};
                    gt0_txcharisk_out <= 4'b0;                
                    gt1_txdata_out <= {gt1_word7_in, gt1_word6_in};
                    gt1_txcharisk_out <= 4'b0;                
                    gt2_txdata_out <= {gt2_word7_in, gt2_word6_in};
                    gt2_txcharisk_out <= 4'b0;                
                    gt3_txdata_out <= {gt3_word7_in, gt3_word6_in};
                    gt3_txcharisk_out <= 4'b0;                
                    gt4_txdata_out <= {gt4_word7_in, gt4_word6_in};
                    gt4_txcharisk_out <= 4'b0;                
                    gt5_txdata_out <= {gt5_word7_in, gt5_word6_in};
                    gt5_txcharisk_out <= 4'b0;                
                    gt6_txdata_out <= {gt6_word7_in, gt6_word6_in};
                    gt6_txcharisk_out <= 4'b0;                
                    gt7_txdata_out <= {gt7_word7_in, gt7_word6_in};
                    gt7_txcharisk_out <= 4'b0;                
                    //gt8_txdata_out <= {gt8_word7_in, gt8_word6_in};
                    gt8_txdata_out <= {32'h0};
                    gt8_txcharisk_out <= 4'b0;                
                    //gt9_txdata_out <= {gt9_word7_in, gt9_word6_in};
                    gt9_txdata_out <= {32'h0};
                    gt9_txcharisk_out <= 4'b0;                
                    counter <= counter + 16'h1;
                    TX0_state <= HEADER;
                end
                default : begin
                    gt0_txdata_out <= 32'heeee;
                    gt0_txcharisk_out <= 4'h0;
                    gt1_txdata_out <= 32'heeee;
                    gt1_txcharisk_out <= 4'h0;
                    counter <= counter + 16'h1;
                    TX0_state <= PAUSE;
                end                        
            endcase
        end
    end
    
    
    always @(posedge gt_txusrclk_in_80 or posedge RST_in) begin
        //TXcounter80 <= TXcounterall_w; //for loopback test
        if (RST_in) begin
            TX10_state <= PAUSE;
            gt10_txdata_out <= 16'hbcbc;
            gt10_txcharisk_out <= 2'h1;
            gt11_txdata_out <= 16'hbcbc;
            gt11_txcharisk_out <= 2'h1;
            TX10_CLK_is_high <= 1'b1;
        end
        else begin
            case (TX10_state)
                PAUSE : begin
                    if(!CLK_in) begin
                        TX10_CLK_is_high <= 1'b0;
                        TX10_state <= PAUSE;
                    end
                    else if(CLK_in && !TX10_CLK_is_high) begin
                        TX10_CLK_is_high <= 1'b1;                        
                        TX10_state <= HEADER;
                    end
                    else begin
                        TX10_state <= PAUSE;                        
                    end
                end                
                HEADER : begin
                    //gt10_txdata_out <= {8'hbc, 8'hbc};
		    gt10_txdata_out <= gt10_word0_in;
                    gt10_txcharisk_out <= 2'h1;
                    //gt11_txdata_out <= {8'hbc, 8'hbc};
		    gt11_txdata_out <= gt11_word0_in;
                    gt11_txcharisk_out <= 2'h1;
                    TX10_state <= FOOTER;
                end                
                FOOTER : begin
                    //gt10_txdata_out <= {flag_S0_in[3:0], BCID_in[11:0]};
		    gt10_txdata_out <= gt10_word1_in;
                    gt10_txcharisk_out <= 2'b00;
                    //gt11_txdata_out <= {flag_S1_in[3:0], BCID_in[11:0]};
		    gt11_txdata_out <= gt11_word1_in;
                    gt11_txcharisk_out <= 2'b00;                 
                    TX10_state <= HEADER;
                end
                default : begin
                    gt10_txdata_out <= 16'heeee;
                    gt10_txcharisk_out <= 2'b00;
                    gt11_txdata_out <= 16'heeee;
                    gt11_txcharisk_out <= 2'b00;
                    TX10_state <= PAUSE;
                end                        
            endcase
        end
    end

endmodule
