`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2016/06/06 14:46:46
// Design Name: 
// Module Name: vme
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

`define    Addr_TEST                         12'h0
`define    Addr_BITFILE_VERSION              12'hb

`define    Addr_RESET                        12'h101
`define    Addr_SCALER_RESET                 12'h102
`define    Addr_GTX_TX_RESET                 12'h103
`define    Addr_GTX_RX_RESET                 12'h104
`define    Addr_DELAY_RESET                  12'h105
`define    Addr_L1BUFFER_RESET               12'h106
`define    Addr_DERANDOMIZER_RESET           12'h107
`define    Addr_ZERO_SUPP_RESET              12'h108
`define    Addr_CLK_RESET                    12'h109
`define    Addr_FIFO_RESET                   12'h10a
`define    Addr_SITCP_RESET                  12'h10b
`define    Addr_SITCP_FIFO_RESET             12'h10c
`define    Addr_TX_LOGIC_RESET               12'h10d
`define    Addr_MONITORING_RESET             12'h10e
`define    Addr_LUT_INIT_RESET               12'h10f

`define    Addr_GT0_RX_RESET               12'h110
`define    Addr_GT1_RX_RESET               12'h111
`define    Addr_GT2_RX_RESET               12'h112
`define    Addr_GT3_RX_RESET               12'h113
`define    Addr_GT4_RX_RESET               12'h114
`define    Addr_GT5_RX_RESET               12'h115
`define    Addr_GT6_RX_RESET               12'h116
`define    Addr_GT7_RX_RESET               12'h117
`define    Addr_GT8_RX_RESET               12'h118
`define    Addr_GT9_RX_RESET               12'h119
`define    Addr_GT10_RX_RESET               12'h11a
`define    Addr_GT11_RX_RESET               12'h11b

`define    Addr_ERROR_SCALER_GT0             12'h200
`define    Addr_ERROR_SCALER_GT1             12'h201
`define    Addr_ERROR_SCALER_GT2             12'h202
`define    Addr_ERROR_SCALER_GT3             12'h203
`define    Addr_ERROR_SCALER_GT4             12'h204
`define    Addr_ERROR_SCALER_GT5             12'h205
`define    Addr_ERROR_SCALER_GT6             12'h206
`define    Addr_ERROR_SCALER_GT7             12'h207
`define    Addr_ERROR_SCALER_GT8             12'h208
`define    Addr_ERROR_SCALER_GT9             12'h209
`define    Addr_ERROR_SCALER_GT10            12'h20a
`define    Addr_ERROR_SCALER_GT11            12'h20b

`define    Addr_STATUS_GT0                   12'h210
`define    Addr_STATUS_GT1                   12'h211
`define    Addr_STATUS_GT2                   12'h212
`define    Addr_STATUS_GT3                   12'h213
`define    Addr_STATUS_GT4                   12'h214
`define    Addr_STATUS_GT5                   12'h215
`define    Addr_STATUS_GT6                   12'h216
`define    Addr_STATUS_GT7                   12'h217
`define    Addr_STATUS_GT8                   12'h218
`define    Addr_STATUS_GT9                   12'h219
`define    Addr_STATUS_GT10                  12'h21a
`define    Addr_STATUS_GT11                  12'h21b


`define    Addr_DELAY_GTX0                   12'h300
`define    Addr_DELAY_GTX1                   12'h301
`define    Addr_DELAY_GTX2                   12'h302
`define    Addr_DELAY_GTX3                   12'h303
`define    Addr_DELAY_GTX4                   12'h304
`define    Addr_DELAY_GTX5                   12'h305
`define    Addr_DELAY_GTX6                   12'h306
`define    Addr_DELAY_GTX7                   12'h307
`define    Addr_DELAY_GTX8                   12'h308
`define    Addr_DELAY_GTX9                   12'h309
`define    Addr_DELAY_GTX10                  12'h30a
`define    Addr_DELAY_GTX11                  12'h30b
`define    Addr_DELAY_GLINK0                 12'h310
`define    Addr_DELAY_GLINK1                 12'h311
`define    Addr_DELAY_GLINK2                 12'h312
`define    Addr_DELAY_GLINK3                 12'h313
`define    Addr_DELAY_GLINK4                 12'h314
`define    Addr_DELAY_GLINK5                 12'h315
`define    Addr_DELAY_GLINK6                 12'h316
`define    Addr_DELAY_GLINK7                 12'h317
`define    Addr_DELAY_GLINK8                 12'h318
`define    Addr_DELAY_GLINK9                 12'h319
`define    Addr_DELAY_GLINK10                12'h31a
`define    Addr_DELAY_GLINK11                12'h31b
`define    Addr_DELAY_GLINK12                12'h31c
`define    Addr_DELAY_GLINK13                12'h31d

`define    Addr_DELAY_L1A                    12'h320
`define    Addr_DELAY_BCR                    12'h321
`define    Addr_DELAY_ECR                    12'h322
`define    Addr_DELAY_TTC_RESET              12'h323
`define    Addr_DELAY_TEST_PULSE             12'h324
`define    Addr_DELAY_TRIG_BCR               12'h325

`define    Addr_MASK_RX0                    12'h330
`define    Addr_MASK_RX1                    12'h331
`define    Addr_MASK_RX2                    12'h332
`define    Addr_MASK_RX3                    12'h333
`define    Addr_MASK_RX4                    12'h334
`define    Addr_MASK_RX5                    12'h335
`define    Addr_MASK_RX6                    12'h336
`define    Addr_MASK_RX7                    12'h337
`define    Addr_MASK_RX8                    12'h338
`define    Addr_MASK_RX9                    12'h339
`define    Addr_MASK_RX10                   12'h33a
`define    Addr_MASK_RX11                   12'h33b

`define    Addr_MASK_L1A                    12'h340
`define    Addr_MASK_BCR                    12'h341
`define    Addr_MASK_ECR                    12'h342
`define    Addr_MASK_TTC_RESET              12'h343
`define    Addr_MASK_TEST_PULSE             12'h344
`define    Addr_MASK_TRIG_BCR               12'h345

`define    Addr_MASK_GLINK0                 12'h350
`define    Addr_MASK_GLINK1                 12'h351
`define    Addr_MASK_GLINK2                 12'h352
`define    Addr_MASK_GLINK3                 12'h353
`define    Addr_MASK_GLINK4                 12'h354
`define    Addr_MASK_GLINK5                 12'h355
`define    Addr_MASK_GLINK6                 12'h356
`define    Addr_MASK_GLINK7                 12'h357
`define    Addr_MASK_GLINK8                 12'h358
`define    Addr_MASK_GLINK9                 12'h359
`define    Addr_MASK_GLINK10                12'h35a
`define    Addr_MASK_GLINK11                12'h35b
`define    Addr_MASK_GLINK12                12'h35c
`define    Addr_MASK_GLINK13                12'h35d


`define    Addr_TEST_PULSE_LENGTH           12'h360
`define    Addr_TEST_PULSE_ENABLE           12'h361
`define    Addr_TEST_PULSE_WAIT_LENGTH      12'h362

`define    Addr_GTX0_PHASE_MONITOR0        12'h370
`define    Addr_GTX1_PHASE_MONITOR0        12'h371
`define    Addr_GTX2_PHASE_MONITOR0        12'h372
`define    Addr_GTX3_PHASE_MONITOR0        12'h373
`define    Addr_GTX4_PHASE_MONITOR0        12'h374
`define    Addr_GTX5_PHASE_MONITOR0        12'h375
`define    Addr_GTX6_PHASE_MONITOR0        12'h376
`define    Addr_GTX7_PHASE_MONITOR0        12'h377
`define    Addr_GTX8_PHASE_MONITOR0        12'h378
`define    Addr_GTX9_PHASE_MONITOR0        12'h379
`define    Addr_GTX10_PHASE_MONITOR0       12'h37a
`define    Addr_GTX11_PHASE_MONITOR0       12'h37b

`define    Addr_GTX0_PHASE_MONITOR1        12'h380
`define    Addr_GTX1_PHASE_MONITOR1        12'h381
`define    Addr_GTX2_PHASE_MONITOR1        12'h382
`define    Addr_GTX3_PHASE_MONITOR1        12'h383
`define    Addr_GTX4_PHASE_MONITOR1        12'h384
`define    Addr_GTX5_PHASE_MONITOR1        12'h385
`define    Addr_GTX6_PHASE_MONITOR1        12'h386
`define    Addr_GTX7_PHASE_MONITOR1        12'h387
`define    Addr_GTX8_PHASE_MONITOR1        12'h388
`define    Addr_GTX9_PHASE_MONITOR1        12'h389
`define    Addr_GTX10_PHASE_MONITOR1       12'h38a
`define    Addr_GTX11_PHASE_MONITOR1       12'h38b

`define    Addr_GTX0_PHASE_MONITOR2        12'h390
`define    Addr_GTX1_PHASE_MONITOR2        12'h391
`define    Addr_GTX2_PHASE_MONITOR2        12'h392
`define    Addr_GTX3_PHASE_MONITOR2        12'h393
`define    Addr_GTX4_PHASE_MONITOR2        12'h394
`define    Addr_GTX5_PHASE_MONITOR2        12'h395
`define    Addr_GTX6_PHASE_MONITOR2        12'h396
`define    Addr_GTX7_PHASE_MONITOR2        12'h397
`define    Addr_GTX8_PHASE_MONITOR2        12'h398
`define    Addr_GTX9_PHASE_MONITOR2        12'h399
`define    Addr_GTX10_PHASE_MONITOR2       12'h39a
`define    Addr_GTX11_PHASE_MONITOR2       12'h39b

`define    Addr_GTX0_PHASE_MONITOR3        12'h3a0
`define    Addr_GTX1_PHASE_MONITOR3        12'h3a1
`define    Addr_GTX2_PHASE_MONITOR3        12'h3a2
`define    Addr_GTX3_PHASE_MONITOR3        12'h3a3
`define    Addr_GTX4_PHASE_MONITOR3        12'h3a4
`define    Addr_GTX5_PHASE_MONITOR3        12'h3a5
`define    Addr_GTX6_PHASE_MONITOR3        12'h3a6
`define    Addr_GTX7_PHASE_MONITOR3        12'h3a7
`define    Addr_GTX8_PHASE_MONITOR3        12'h3a8
`define    Addr_GTX9_PHASE_MONITOR3        12'h3a9
`define    Addr_GTX10_PHASE_MONITOR3       12'h3aa
`define    Addr_GTX11_PHASE_MONITOR3       12'h3ab

`define    Addr_GTX0_PHASE_SELECT          12'h3b0
`define    Addr_GTX1_PHASE_SELECT          12'h3b1
`define    Addr_GTX2_PHASE_SELECT          12'h3b2
`define    Addr_GTX3_PHASE_SELECT          12'h3b3
`define    Addr_GTX4_PHASE_SELECT          12'h3b4
`define    Addr_GTX5_PHASE_SELECT          12'h3b5
`define    Addr_GTX6_PHASE_SELECT          12'h3b6
`define    Addr_GTX7_PHASE_SELECT          12'h3b7
`define    Addr_GTX8_PHASE_SELECT          12'h3b8
`define    Addr_GTX9_PHASE_SELECT          12'h3b9
`define    Addr_GTX10_PHASE_SELECT          12'h3ba
`define    Addr_GTX11_PHASE_SELECT          12'h3bb

`define    Addr_L1A_COUNTER0                12'h400
`define    Addr_L1A_COUNTER1                12'h401
`define    Addr_L1ID                        12'h402
`define    Addr_BCID                        12'h403
`define    Addr_SLID                        12'h404
`define    Addr_READOUT_BC                  12'h405
`define    Addr_L1BUFFER_DEPTH              12'h406
`define    Addr_TRIGL1BUFFER_DEPTH          12'h407
`define    Addr_L1BUFFER_BW_DEPTH           12'h408

`define    Addr_L1BUFFER_STATUS             12'h500

`define    Addr_DERANDOMIZER_STATUS         12'h510
`define    Addr_DERANDOMIZER_COUNT1_1       12'h511
`define    Addr_DERANDOMIZER_COUNT1_2       12'h512
`define    Addr_DERANDOMIZER_COUNT2         12'h513
`define    Addr_DERANDOMIZER_COUNT3         12'h514

`define    Addr_ZERO_SUPPRESS_STATUS        12'h520
`define    Addr_ZERO_SUPPRESS_COUNT         12'h521
`define    Addr_DATA_SIZE                   12'h522

`define    Addr_SITCP_COUNT                 12'h530

`define    Addr_BUSY_COUNT                  12'h540


`define    Addr_FULLBOARD_ID                12'h680

`define    Addr_MONITORING_FIFO_OUT_0        12'h700
`define    Addr_MONITORING_FIFO_OUT_1        12'h701
`define    Addr_MONITORING_FIFO_OUT_2        12'h702
`define    Addr_MONITORING_FIFO_OUT_3        12'h703
`define    Addr_MONITORING_FIFO_OUT_4        12'h704
`define    Addr_MONITORING_FIFO_OUT_5        12'h705
`define    Addr_MONITORING_FIFO_OUT_6        12'h706
`define    Addr_MONITORING_FIFO_OUT_7        12'h707
`define    Addr_MONITORING_FIFO_OUT_8        12'h708
`define    Addr_MONITORING_FIFO_OUT_9        12'h709
`define    Addr_MONITORING_FIFO_OUT_10       12'h70a
`define    Addr_MONITORING_FIFO_OUT_11       12'h70b
`define    Addr_MONITORING_FIFO_OUT_12       12'h70c
`define    Addr_MONITORING_FIFO_OUT_13       12'h70d
`define    Addr_MONITORING_FIFO_OUT_14       12'h70e
`define    Addr_MONITORING_FIFO_OUT_15       12'h70f

`define    Addr_MONITORING_FIFO_wr_en        12'h710
`define    Addr_MONITORING_FIFO_rd_en        12'h711
`define    Addr_LANE_SELECTOR                12'h712
`define    Addr_XADC_MON_ENABLE              12'h713
`define    Addr_XADC_MONITOR_OUT             12'h714
`define    Addr_XADC_MON_ADDR                12'h715

`define    Addr_L1A_MANUAL_PARAMETER         12'h720
`define    Addr_BUSY_PROPAGATION             12'h721

`define    Addr_GTX0_TEST_DATA1             12'h801
`define    Addr_GTX0_TEST_DATA2             12'h802
`define    Addr_GTX0_TEST_DATA3             12'h803
`define    Addr_GTX0_TEST_DATA4             12'h804
`define    Addr_GTX0_TEST_DATA5             12'h805
`define    Addr_GTX0_TEST_DATA6             12'h806
`define    Addr_GTX0_TEST_DATA7             12'h807
`define    Addr_GTX1_TEST_DATA1             12'h811
`define    Addr_GTX1_TEST_DATA2             12'h812
`define    Addr_GTX1_TEST_DATA3             12'h813
`define    Addr_GTX1_TEST_DATA4             12'h814
`define    Addr_GTX1_TEST_DATA5             12'h815
`define    Addr_GTX1_TEST_DATA6             12'h816
`define    Addr_GTX1_TEST_DATA7             12'h817
`define    Addr_GTX2_TEST_DATA1             12'h821
`define    Addr_GTX2_TEST_DATA2             12'h822
`define    Addr_GTX2_TEST_DATA3             12'h823
`define    Addr_GTX2_TEST_DATA4             12'h824
`define    Addr_GTX2_TEST_DATA5             12'h825
`define    Addr_GTX2_TEST_DATA6             12'h826
`define    Addr_GTX2_TEST_DATA7             12'h827
`define    Addr_GTX3_TEST_DATA1             12'h831
`define    Addr_GTX3_TEST_DATA2             12'h832
`define    Addr_GTX3_TEST_DATA3             12'h833
`define    Addr_GTX3_TEST_DATA4             12'h834
`define    Addr_GTX3_TEST_DATA5             12'h835
`define    Addr_GTX3_TEST_DATA6             12'h836
`define    Addr_GTX3_TEST_DATA7             12'h837
`define    Addr_GTX4_TEST_DATA1             12'h841
`define    Addr_GTX4_TEST_DATA2             12'h842
`define    Addr_GTX4_TEST_DATA3             12'h843
`define    Addr_GTX4_TEST_DATA4             12'h844
`define    Addr_GTX4_TEST_DATA5             12'h845
`define    Addr_GTX4_TEST_DATA6             12'h846
`define    Addr_GTX4_TEST_DATA7             12'h847
`define    Addr_GTX5_TEST_DATA1             12'h851
`define    Addr_GTX5_TEST_DATA2             12'h852
`define    Addr_GTX5_TEST_DATA3             12'h853
`define    Addr_GTX5_TEST_DATA4             12'h854
`define    Addr_GTX5_TEST_DATA5             12'h855
`define    Addr_GTX5_TEST_DATA6             12'h856
`define    Addr_GTX5_TEST_DATA7             12'h857
`define    Addr_GTX6_TEST_DATA1             12'h861
`define    Addr_GTX6_TEST_DATA2             12'h862
`define    Addr_GTX6_TEST_DATA3             12'h863
`define    Addr_GTX6_TEST_DATA4             12'h864
`define    Addr_GTX6_TEST_DATA5             12'h865
`define    Addr_GTX6_TEST_DATA6             12'h866
`define    Addr_GTX6_TEST_DATA7             12'h867
`define    Addr_GTX7_TEST_DATA1             12'h871
`define    Addr_GTX7_TEST_DATA2             12'h872
`define    Addr_GTX7_TEST_DATA3             12'h873
`define    Addr_GTX7_TEST_DATA4             12'h874
`define    Addr_GTX7_TEST_DATA5             12'h875
`define    Addr_GTX7_TEST_DATA6             12'h876
`define    Addr_GTX7_TEST_DATA7             12'h877
`define    Addr_GTX8_TEST_DATA1             12'h881
`define    Addr_GTX8_TEST_DATA2             12'h882
`define    Addr_GTX8_TEST_DATA3             12'h883
`define    Addr_GTX8_TEST_DATA4             12'h884
`define    Addr_GTX8_TEST_DATA5             12'h885
`define    Addr_GTX8_TEST_DATA6             12'h886
`define    Addr_GTX8_TEST_DATA7             12'h887
`define    Addr_GTX9_TEST_DATA1             12'h891
`define    Addr_GTX9_TEST_DATA2             12'h892
`define    Addr_GTX9_TEST_DATA3             12'h893
`define    Addr_GTX9_TEST_DATA4             12'h894
`define    Addr_GTX9_TEST_DATA5             12'h895
`define    Addr_GTX9_TEST_DATA6             12'h896
`define    Addr_GTX9_TEST_DATA7             12'h897
`define    Addr_GTX10_TEST_DATA1            12'h8a1
`define    Addr_GTX10_TEST_DATA2            12'h8a2
`define    Addr_GTX10_TEST_DATA3            12'h8a3
`define    Addr_GTX10_TEST_DATA4            12'h8a4
`define    Addr_GTX10_TEST_DATA5            12'h8a5
`define    Addr_GTX10_TEST_DATA6            12'h8a6
`define    Addr_GTX10_TEST_DATA7            12'h8a7
`define    Addr_GTX11_TEST_DATA1            12'h8b1
`define    Addr_GTX11_TEST_DATA2            12'h8b2
`define    Addr_GTX11_TEST_DATA3            12'h8b3
`define    Addr_GTX11_TEST_DATA4            12'h8b4
`define    Addr_GTX11_TEST_DATA5            12'h8b5
`define    Addr_GTX11_TEST_DATA6            12'h8b6
`define    Addr_GTX11_TEST_DATA7            12'h8b7

// emulator
`define    Addr_EMULATOR_ENABLLE            12'h8d0
`define    Addr_EMULATOR_DATA_LENGTH        12'h8d1
`define    Addr_EMULATOR_GTX_HEADER         12'h8d2
`define    Addr_EMULATOR_RESET              12'h8d3

`define    Addr_TRACK0_ETA                  12'h8e0
`define    Addr_TRACK0_PHI                  12'h8e1
`define    Addr_TRACK0_DELTATHETA           12'h8e2
`define    Addr_TRACK0_MM                   12'h8e3
`define    Addr_TRACK0_sTGC                 12'h8e4
`define    Addr_TRACK0_SPARE                12'h8e5

`define    Addr_TRACK1_ETA                  12'h8e6
`define    Addr_TRACK1_PHI                  12'h8e7
`define    Addr_TRACK1_DELTATHETA           12'h8e8
`define    Addr_TRACK1_MM                   12'h8e9
`define    Addr_TRACK1_sTGC                 12'h8ea
`define    Addr_TRACK1_SPARE                12'h8eb

`define    Addr_TRACK2_ETA                  12'h8ec
`define    Addr_TRACK2_PHI                  12'h8ed
`define    Addr_TRACK2_DELTATHETA           12'h8ee
`define    Addr_TRACK2_MM                   12'h8ef
`define    Addr_TRACK2_sTGC                 12'h8f0
`define    Addr_TRACK2_SPARE                12'h8f1

`define    Addr_TRACK3_ETA                  12'h8f2
`define    Addr_TRACK3_PHI                  12'h8f3
`define    Addr_TRACK3_DELTATHETA           12'h8f4
`define    Addr_TRACK3_MM                   12'h8f5
`define    Addr_TRACK3_sTGC                 12'h8f6
`define    Addr_TRACK3_SPARE                12'h8f7

`define    Addr_GLINK0_TEST_DATA1          12'h901
`define    Addr_GLINK0_TEST_DATA2          12'h902
`define    Addr_GLINK1_TEST_DATA1          12'h911
`define    Addr_GLINK1_TEST_DATA2          12'h912
`define    Addr_GLINK2_TEST_DATA1          12'h921
`define    Addr_GLINK2_TEST_DATA2          12'h922
`define    Addr_GLINK3_TEST_DATA1          12'h931
`define    Addr_GLINK3_TEST_DATA2          12'h932
`define    Addr_GLINK4_TEST_DATA1          12'h941
`define    Addr_GLINK4_TEST_DATA2          12'h942
`define    Addr_GLINK5_TEST_DATA1          12'h951
`define    Addr_GLINK5_TEST_DATA2          12'h952
`define    Addr_GLINK6_TEST_DATA1          12'h961
`define    Addr_GLINK6_TEST_DATA2          12'h962
`define    Addr_GLINK7_TEST_DATA1          12'h971
`define    Addr_GLINK7_TEST_DATA2          12'h972
`define    Addr_GLINK8_TEST_DATA1          12'h981
`define    Addr_GLINK8_TEST_DATA2          12'h982
`define    Addr_GLINK9_TEST_DATA1          12'h991
`define    Addr_GLINK9_TEST_DATA2          12'h992
`define    Addr_GLINK10_TEST_DATA1         12'h9a1
`define    Addr_GLINK10_TEST_DATA2         12'h9a2
`define    Addr_GLINK11_TEST_DATA1         12'h9b1
`define    Addr_GLINK11_TEST_DATA2         12'h9b2
`define    Addr_GLINK12_TEST_DATA1         12'h9c1
`define    Addr_GLINK12_TEST_DATA2         12'h9c2
`define    Addr_GLINK13_TEST_DATA1         12'h9d1
`define    Addr_GLINK13_TEST_DATA2         12'h9d2

// LUT initialization
`define    Addr_LUT_INIT_MODE               12'ha00
`define    Addr_LUT_INIT_DATA               12'ha01
`define    Addr_LUT_INIT_ROI                12'ha02
`define    Addr_LUT_INIT_ADDRESS            12'ha03
`define    Addr_LUT_INIT_OUTPUT             12'ha04
`define    Addr_LUT_INIT_STATE              12'ha05
`define    Addr_LUT_RD_DATA                 12'ha06
`define    Addr_LUT_RD_ADDRESS              12'ha07

// Decoder
`define    Addr_ALIGN_ETA_NSW_0             12'hb00
`define    Addr_ALIGN_ETA_NSW_1             12'hb01
`define    Addr_ALIGN_ETA_NSW_2             12'hb02
`define    Addr_ALIGN_ETA_NSW_3             12'hb03
`define    Addr_ALIGN_ETA_NSW_4             12'hb04
`define    Addr_ALIGN_ETA_NSW_5             12'hb05
`define    Addr_ALIGN_ETA_RPC               12'hb06
`define    Addr_ALIGN_PHI_NSW_0             12'hb10
`define    Addr_ALIGN_PHI_NSW_1             12'hb11
`define    Addr_ALIGN_PHI_NSW_2             12'hb12
`define    Addr_ALIGN_PHI_NSW_3             12'hb13
`define    Addr_ALIGN_PHI_NSW_4             12'hb14
`define    Addr_ALIGN_PHI_NSW_5             12'hb15
`define    Addr_ALIGN_PHI_RPC               12'hb16

`define	   Addr_tmp0			    12'hf00
`define	   Addr_tmp1			    12'hf01
`define	   Addr_tmp2			    12'hf02
`define	   Addr_tmp3			    12'hf03
`define	   Addr_tmp4			    12'hf04
`define	   Addr_tmp5			    12'hf05
`define	   Addr_tmp6			    12'hf06

`define Addr_TRACK0_SPARE_lane0_0      12'hc00
`define Addr_TRACK0_SPARE_lane0_1      12'hc01
`define Addr_TRACK0_SPARE_lane0_2      12'hc02
`define Addr_TRACK0_SPARE_lane0_3      12'hc03

`define Addr_TRACK0_SPARE_lane1_0      12'hc04
`define Addr_TRACK0_SPARE_lane1_1      12'hc05
`define Addr_TRACK0_SPARE_lane1_2      12'hc06
`define Addr_TRACK0_SPARE_lane1_3      12'hc07

`define Addr_TRACK0_SPARE_lane2_0      12'hc08
`define Addr_TRACK0_SPARE_lane2_1      12'hc09
`define Addr_TRACK0_SPARE_lane2_2      12'hc0a
`define Addr_TRACK0_SPARE_lane2_3      12'hc0b

`define Addr_TRACK0_SPARE_lane3_0      12'hc0c
`define Addr_TRACK0_SPARE_lane3_1      12'hc0d
`define Addr_TRACK0_SPARE_lane3_2      12'hc0e
`define Addr_TRACK0_SPARE_lane3_3      12'hc0f

`define Addr_TRACK0_SPARE_lane4_0      12'hc10
`define Addr_TRACK0_SPARE_lane4_1      12'hc11
`define Addr_TRACK0_SPARE_lane4_2      12'hc12
`define Addr_TRACK0_SPARE_lane4_3      12'hc13

`define Addr_TRACK0_SPARE_lane5_0      12'hc14
`define Addr_TRACK0_SPARE_lane5_1      12'hc15
`define Addr_TRACK0_SPARE_lane5_2      12'hc16
`define Addr_TRACK0_SPARE_lane5_3      12'hc17

`define Addr_TRACK0_SPARE_lane6_0      12'hc18
`define Addr_TRACK0_SPARE_lane6_1      12'hc19
`define Addr_TRACK0_SPARE_lane6_2      12'hc1a
`define Addr_TRACK0_SPARE_lane6_3      12'hc1b

`define Addr_TRACK0_sTGC_lane0_0      12'hc1c
`define Addr_TRACK0_sTGC_lane0_1      12'hc1d
`define Addr_TRACK0_sTGC_lane0_2      12'hc1e
`define Addr_TRACK0_sTGC_lane0_3      12'hc1f

`define Addr_TRACK0_sTGC_lane1_0      12'hc20
`define Addr_TRACK0_sTGC_lane1_1      12'hc21
`define Addr_TRACK0_sTGC_lane1_2      12'hc22
`define Addr_TRACK0_sTGC_lane1_3      12'hc23

`define Addr_TRACK0_sTGC_lane2_0      12'hc24
`define Addr_TRACK0_sTGC_lane2_1      12'hc25
`define Addr_TRACK0_sTGC_lane2_2      12'hc26
`define Addr_TRACK0_sTGC_lane2_3      12'hc27

`define Addr_TRACK0_sTGC_lane3_0      12'hc28
`define Addr_TRACK0_sTGC_lane3_1      12'hc29
`define Addr_TRACK0_sTGC_lane3_2      12'hc2a
`define Addr_TRACK0_sTGC_lane3_3      12'hc2b

`define Addr_TRACK0_sTGC_lane4_0      12'hc2c
`define Addr_TRACK0_sTGC_lane4_1      12'hc2d
`define Addr_TRACK0_sTGC_lane4_2      12'hc2e
`define Addr_TRACK0_sTGC_lane4_3      12'hc2f

`define Addr_TRACK0_sTGC_lane5_0      12'hc30
`define Addr_TRACK0_sTGC_lane5_1      12'hc31
`define Addr_TRACK0_sTGC_lane5_2      12'hc32
`define Addr_TRACK0_sTGC_lane5_3      12'hc33

`define Addr_TRACK0_sTGC_lane6_0      12'hc34
`define Addr_TRACK0_sTGC_lane6_1      12'hc35
`define Addr_TRACK0_sTGC_lane6_2      12'hc36
`define Addr_TRACK0_sTGC_lane6_3      12'hc37

`define Addr_TRACK0_MM_lane0_0      12'hc38
`define Addr_TRACK0_MM_lane0_1      12'hc39
`define Addr_TRACK0_MM_lane0_2      12'hc3a
`define Addr_TRACK0_MM_lane0_3      12'hc3b

`define Addr_TRACK0_MM_lane1_0      12'hc3c
`define Addr_TRACK0_MM_lane1_1      12'hc3d
`define Addr_TRACK0_MM_lane1_2      12'hc3e
`define Addr_TRACK0_MM_lane1_3      12'hc3f

`define Addr_TRACK0_MM_lane2_0      12'hc40
`define Addr_TRACK0_MM_lane2_1      12'hc41
`define Addr_TRACK0_MM_lane2_2      12'hc42
`define Addr_TRACK0_MM_lane2_3      12'hc43

`define Addr_TRACK0_MM_lane3_0      12'hc44
`define Addr_TRACK0_MM_lane3_1      12'hc45
`define Addr_TRACK0_MM_lane3_2      12'hc46
`define Addr_TRACK0_MM_lane3_3      12'hc47

`define Addr_TRACK0_MM_lane4_0      12'hc48
`define Addr_TRACK0_MM_lane4_1      12'hc49
`define Addr_TRACK0_MM_lane4_2      12'hc4a
`define Addr_TRACK0_MM_lane4_3      12'hc4b

`define Addr_TRACK0_MM_lane5_0      12'hc4c
`define Addr_TRACK0_MM_lane5_1      12'hc4d
`define Addr_TRACK0_MM_lane5_2      12'hc4e
`define Addr_TRACK0_MM_lane5_3      12'hc4f

`define Addr_TRACK0_MM_lane6_0      12'hc50
`define Addr_TRACK0_MM_lane6_1      12'hc51
`define Addr_TRACK0_MM_lane6_2      12'hc52
`define Addr_TRACK0_MM_lane6_3      12'hc53

`define Addr_TRACK0_DTHETA_lane0_0      12'hc54
`define Addr_TRACK0_DTHETA_lane0_1      12'hc55
`define Addr_TRACK0_DTHETA_lane0_2      12'hc56
`define Addr_TRACK0_DTHETA_lane0_3      12'hc57

`define Addr_TRACK0_DTHETA_lane1_0      12'hc58
`define Addr_TRACK0_DTHETA_lane1_1      12'hc59
`define Addr_TRACK0_DTHETA_lane1_2      12'hc5a
`define Addr_TRACK0_DTHETA_lane1_3      12'hc5b

`define Addr_TRACK0_DTHETA_lane2_0      12'hc5c
`define Addr_TRACK0_DTHETA_lane2_1      12'hc5d
`define Addr_TRACK0_DTHETA_lane2_2      12'hc5e
`define Addr_TRACK0_DTHETA_lane2_3      12'hc5f

`define Addr_TRACK0_DTHETA_lane3_0      12'hc60
`define Addr_TRACK0_DTHETA_lane3_1      12'hc61
`define Addr_TRACK0_DTHETA_lane3_2      12'hc62
`define Addr_TRACK0_DTHETA_lane3_3      12'hc63

`define Addr_TRACK0_DTHETA_lane4_0      12'hc64
`define Addr_TRACK0_DTHETA_lane4_1      12'hc65
`define Addr_TRACK0_DTHETA_lane4_2      12'hc66
`define Addr_TRACK0_DTHETA_lane4_3      12'hc67

`define Addr_TRACK0_DTHETA_lane5_0      12'hc68
`define Addr_TRACK0_DTHETA_lane5_1      12'hc69
`define Addr_TRACK0_DTHETA_lane5_2      12'hc6a
`define Addr_TRACK0_DTHETA_lane5_3      12'hc6b

`define Addr_TRACK0_DTHETA_lane6_0      12'hc6c
`define Addr_TRACK0_DTHETA_lane6_1      12'hc6d
`define Addr_TRACK0_DTHETA_lane6_2      12'hc6e
`define Addr_TRACK0_DTHETA_lane6_3      12'hc6f

`define Addr_TRACK0_PHI_lane0_0      12'hc70
`define Addr_TRACK0_PHI_lane0_1      12'hc71
`define Addr_TRACK0_PHI_lane0_2      12'hc72
`define Addr_TRACK0_PHI_lane0_3      12'hc73

`define Addr_TRACK0_PHI_lane1_0      12'hc74
`define Addr_TRACK0_PHI_lane1_1      12'hc75
`define Addr_TRACK0_PHI_lane1_2      12'hc76
`define Addr_TRACK0_PHI_lane1_3      12'hc77

`define Addr_TRACK0_PHI_lane2_0      12'hc78
`define Addr_TRACK0_PHI_lane2_1      12'hc79
`define Addr_TRACK0_PHI_lane2_2      12'hc7a
`define Addr_TRACK0_PHI_lane2_3      12'hc7b

`define Addr_TRACK0_PHI_lane3_0      12'hc7c
`define Addr_TRACK0_PHI_lane3_1      12'hc7d
`define Addr_TRACK0_PHI_lane3_2      12'hc7e
`define Addr_TRACK0_PHI_lane3_3      12'hc7f

`define Addr_TRACK0_PHI_lane4_0      12'hc80
`define Addr_TRACK0_PHI_lane4_1      12'hc81
`define Addr_TRACK0_PHI_lane4_2      12'hc82
`define Addr_TRACK0_PHI_lane4_3      12'hc83

`define Addr_TRACK0_PHI_lane5_0      12'hc84
`define Addr_TRACK0_PHI_lane5_1      12'hc85
`define Addr_TRACK0_PHI_lane5_2      12'hc86
`define Addr_TRACK0_PHI_lane5_3      12'hc87

`define Addr_TRACK0_PHI_lane6_0      12'hc88
`define Addr_TRACK0_PHI_lane6_1      12'hc89
`define Addr_TRACK0_PHI_lane6_2      12'hc8a
`define Addr_TRACK0_PHI_lane6_3      12'hc8b

`define Addr_TRACK0_ETA_lane0_0      12'hc8c
`define Addr_TRACK0_ETA_lane0_1      12'hc8d
`define Addr_TRACK0_ETA_lane0_2      12'hc8e
`define Addr_TRACK0_ETA_lane0_3      12'hc8f

`define Addr_TRACK0_ETA_lane1_0      12'hc90
`define Addr_TRACK0_ETA_lane1_1      12'hc91
`define Addr_TRACK0_ETA_lane1_2      12'hc92
`define Addr_TRACK0_ETA_lane1_3      12'hc93

`define Addr_TRACK0_ETA_lane2_0      12'hc94
`define Addr_TRACK0_ETA_lane2_1      12'hc95
`define Addr_TRACK0_ETA_lane2_2      12'hc96
`define Addr_TRACK0_ETA_lane2_3      12'hc97

`define Addr_TRACK0_ETA_lane3_0      12'hc98
`define Addr_TRACK0_ETA_lane3_1      12'hc99
`define Addr_TRACK0_ETA_lane3_2      12'hc9a
`define Addr_TRACK0_ETA_lane3_3      12'hc9b

`define Addr_TRACK0_ETA_lane4_0      12'hc9c
`define Addr_TRACK0_ETA_lane4_1      12'hc9d
`define Addr_TRACK0_ETA_lane4_2      12'hc9e
`define Addr_TRACK0_ETA_lane4_3      12'hc9f

`define Addr_TRACK0_ETA_lane5_0      12'hca0
`define Addr_TRACK0_ETA_lane5_1      12'hca1
`define Addr_TRACK0_ETA_lane5_2      12'hca2
`define Addr_TRACK0_ETA_lane5_3      12'hca3

`define Addr_TRACK0_ETA_lane6_0      12'hca4
`define Addr_TRACK0_ETA_lane6_1      12'hca5
`define Addr_TRACK0_ETA_lane6_2      12'hca6
`define Addr_TRACK0_ETA_lane6_3      12'hca7

`define Addr_TRACK1_SPARE_lane0_0      12'hca8
`define Addr_TRACK1_SPARE_lane0_1      12'hca9
`define Addr_TRACK1_SPARE_lane0_2      12'hcaa
`define Addr_TRACK1_SPARE_lane0_3      12'hcab

`define Addr_TRACK1_SPARE_lane1_0      12'hcac
`define Addr_TRACK1_SPARE_lane1_1      12'hcad
`define Addr_TRACK1_SPARE_lane1_2      12'hcae
`define Addr_TRACK1_SPARE_lane1_3      12'hcaf

`define Addr_TRACK1_SPARE_lane2_0      12'hcb0
`define Addr_TRACK1_SPARE_lane2_1      12'hcb1
`define Addr_TRACK1_SPARE_lane2_2      12'hcb2
`define Addr_TRACK1_SPARE_lane2_3      12'hcb3

`define Addr_TRACK1_SPARE_lane3_0      12'hcb4
`define Addr_TRACK1_SPARE_lane3_1      12'hcb5
`define Addr_TRACK1_SPARE_lane3_2      12'hcb6
`define Addr_TRACK1_SPARE_lane3_3      12'hcb7

`define Addr_TRACK1_SPARE_lane4_0      12'hcb8
`define Addr_TRACK1_SPARE_lane4_1      12'hcb9
`define Addr_TRACK1_SPARE_lane4_2      12'hcba
`define Addr_TRACK1_SPARE_lane4_3      12'hcbb

`define Addr_TRACK1_SPARE_lane5_0      12'hcbc
`define Addr_TRACK1_SPARE_lane5_1      12'hcbd
`define Addr_TRACK1_SPARE_lane5_2      12'hcbe
`define Addr_TRACK1_SPARE_lane5_3      12'hcbf

`define Addr_TRACK1_SPARE_lane6_0      12'hcc0
`define Addr_TRACK1_SPARE_lane6_1      12'hcc1
`define Addr_TRACK1_SPARE_lane6_2      12'hcc2
`define Addr_TRACK1_SPARE_lane6_3      12'hcc3

`define Addr_TRACK1_sTGC_lane0_0      12'hcc4
`define Addr_TRACK1_sTGC_lane0_1      12'hcc5
`define Addr_TRACK1_sTGC_lane0_2      12'hcc6
`define Addr_TRACK1_sTGC_lane0_3      12'hcc7

`define Addr_TRACK1_sTGC_lane1_0      12'hcc8
`define Addr_TRACK1_sTGC_lane1_1      12'hcc9
`define Addr_TRACK1_sTGC_lane1_2      12'hcca
`define Addr_TRACK1_sTGC_lane1_3      12'hccb

`define Addr_TRACK1_sTGC_lane2_0      12'hccc
`define Addr_TRACK1_sTGC_lane2_1      12'hccd
`define Addr_TRACK1_sTGC_lane2_2      12'hcce
`define Addr_TRACK1_sTGC_lane2_3      12'hccf

`define Addr_TRACK1_sTGC_lane3_0      12'hcd0
`define Addr_TRACK1_sTGC_lane3_1      12'hcd1
`define Addr_TRACK1_sTGC_lane3_2      12'hcd2
`define Addr_TRACK1_sTGC_lane3_3      12'hcd3

`define Addr_TRACK1_sTGC_lane4_0      12'hcd4
`define Addr_TRACK1_sTGC_lane4_1      12'hcd5
`define Addr_TRACK1_sTGC_lane4_2      12'hcd6
`define Addr_TRACK1_sTGC_lane4_3      12'hcd7

`define Addr_TRACK1_sTGC_lane5_0      12'hcd8
`define Addr_TRACK1_sTGC_lane5_1      12'hcd9
`define Addr_TRACK1_sTGC_lane5_2      12'hcda
`define Addr_TRACK1_sTGC_lane5_3      12'hcdb

`define Addr_TRACK1_sTGC_lane6_0      12'hcdc
`define Addr_TRACK1_sTGC_lane6_1      12'hcdd
`define Addr_TRACK1_sTGC_lane6_2      12'hcde
`define Addr_TRACK1_sTGC_lane6_3      12'hcdf

`define Addr_TRACK1_MM_lane0_0      12'hce0
`define Addr_TRACK1_MM_lane0_1      12'hce1
`define Addr_TRACK1_MM_lane0_2      12'hce2
`define Addr_TRACK1_MM_lane0_3      12'hce3

`define Addr_TRACK1_MM_lane1_0      12'hce4
`define Addr_TRACK1_MM_lane1_1      12'hce5
`define Addr_TRACK1_MM_lane1_2      12'hce6
`define Addr_TRACK1_MM_lane1_3      12'hce7

`define Addr_TRACK1_MM_lane2_0      12'hce8
`define Addr_TRACK1_MM_lane2_1      12'hce9
`define Addr_TRACK1_MM_lane2_2      12'hcea
`define Addr_TRACK1_MM_lane2_3      12'hceb

`define Addr_TRACK1_MM_lane3_0      12'hcec
`define Addr_TRACK1_MM_lane3_1      12'hced
`define Addr_TRACK1_MM_lane3_2      12'hcee
`define Addr_TRACK1_MM_lane3_3      12'hcef

`define Addr_TRACK1_MM_lane4_0      12'hcf0
`define Addr_TRACK1_MM_lane4_1      12'hcf1
`define Addr_TRACK1_MM_lane4_2      12'hcf2
`define Addr_TRACK1_MM_lane4_3      12'hcf3

`define Addr_TRACK1_MM_lane5_0      12'hcf4
`define Addr_TRACK1_MM_lane5_1      12'hcf5
`define Addr_TRACK1_MM_lane5_2      12'hcf6
`define Addr_TRACK1_MM_lane5_3      12'hcf7

`define Addr_TRACK1_MM_lane6_0      12'hcf8
`define Addr_TRACK1_MM_lane6_1      12'hcf9
`define Addr_TRACK1_MM_lane6_2      12'hcfa
`define Addr_TRACK1_MM_lane6_3      12'hcfb

`define Addr_TRACK1_DTHETA_lane0_0      12'hcfc
`define Addr_TRACK1_DTHETA_lane0_1      12'hcfd
`define Addr_TRACK1_DTHETA_lane0_2      12'hcfe
`define Addr_TRACK1_DTHETA_lane0_3      12'hcff

`define Addr_TRACK1_DTHETA_lane1_0      12'hd00
`define Addr_TRACK1_DTHETA_lane1_1      12'hd01
`define Addr_TRACK1_DTHETA_lane1_2      12'hd02
`define Addr_TRACK1_DTHETA_lane1_3      12'hd03

`define Addr_TRACK1_DTHETA_lane2_0      12'hd04
`define Addr_TRACK1_DTHETA_lane2_1      12'hd05
`define Addr_TRACK1_DTHETA_lane2_2      12'hd06
`define Addr_TRACK1_DTHETA_lane2_3      12'hd07

`define Addr_TRACK1_DTHETA_lane3_0      12'hd08
`define Addr_TRACK1_DTHETA_lane3_1      12'hd09
`define Addr_TRACK1_DTHETA_lane3_2      12'hd0a
`define Addr_TRACK1_DTHETA_lane3_3      12'hd0b

`define Addr_TRACK1_DTHETA_lane4_0      12'hd0c
`define Addr_TRACK1_DTHETA_lane4_1      12'hd0d
`define Addr_TRACK1_DTHETA_lane4_2      12'hd0e
`define Addr_TRACK1_DTHETA_lane4_3      12'hd0f

`define Addr_TRACK1_DTHETA_lane5_0      12'hd10
`define Addr_TRACK1_DTHETA_lane5_1      12'hd11
`define Addr_TRACK1_DTHETA_lane5_2      12'hd12
`define Addr_TRACK1_DTHETA_lane5_3      12'hd13

`define Addr_TRACK1_DTHETA_lane6_0      12'hd14
`define Addr_TRACK1_DTHETA_lane6_1      12'hd15
`define Addr_TRACK1_DTHETA_lane6_2      12'hd16
`define Addr_TRACK1_DTHETA_lane6_3      12'hd17

`define Addr_TRACK1_PHI_lane0_0      12'hd18
`define Addr_TRACK1_PHI_lane0_1      12'hd19
`define Addr_TRACK1_PHI_lane0_2      12'hd1a
`define Addr_TRACK1_PHI_lane0_3      12'hd1b

`define Addr_TRACK1_PHI_lane1_0      12'hd1c
`define Addr_TRACK1_PHI_lane1_1      12'hd1d
`define Addr_TRACK1_PHI_lane1_2      12'hd1e
`define Addr_TRACK1_PHI_lane1_3      12'hd1f

`define Addr_TRACK1_PHI_lane2_0      12'hd20
`define Addr_TRACK1_PHI_lane2_1      12'hd21
`define Addr_TRACK1_PHI_lane2_2      12'hd22
`define Addr_TRACK1_PHI_lane2_3      12'hd23

`define Addr_TRACK1_PHI_lane3_0      12'hd24
`define Addr_TRACK1_PHI_lane3_1      12'hd25
`define Addr_TRACK1_PHI_lane3_2      12'hd26
`define Addr_TRACK1_PHI_lane3_3      12'hd27

`define Addr_TRACK1_PHI_lane4_0      12'hd28
`define Addr_TRACK1_PHI_lane4_1      12'hd29
`define Addr_TRACK1_PHI_lane4_2      12'hd2a
`define Addr_TRACK1_PHI_lane4_3      12'hd2b

`define Addr_TRACK1_PHI_lane5_0      12'hd2c
`define Addr_TRACK1_PHI_lane5_1      12'hd2d
`define Addr_TRACK1_PHI_lane5_2      12'hd2e
`define Addr_TRACK1_PHI_lane5_3      12'hd2f

`define Addr_TRACK1_PHI_lane6_0      12'hd30
`define Addr_TRACK1_PHI_lane6_1      12'hd31
`define Addr_TRACK1_PHI_lane6_2      12'hd32
`define Addr_TRACK1_PHI_lane6_3      12'hd33

`define Addr_TRACK1_ETA_lane0_0      12'hd34
`define Addr_TRACK1_ETA_lane0_1      12'hd35
`define Addr_TRACK1_ETA_lane0_2      12'hd36
`define Addr_TRACK1_ETA_lane0_3      12'hd37

`define Addr_TRACK1_ETA_lane1_0      12'hd38
`define Addr_TRACK1_ETA_lane1_1      12'hd39
`define Addr_TRACK1_ETA_lane1_2      12'hd3a
`define Addr_TRACK1_ETA_lane1_3      12'hd3b

`define Addr_TRACK1_ETA_lane2_0      12'hd3c
`define Addr_TRACK1_ETA_lane2_1      12'hd3d
`define Addr_TRACK1_ETA_lane2_2      12'hd3e
`define Addr_TRACK1_ETA_lane2_3      12'hd3f

`define Addr_TRACK1_ETA_lane3_0      12'hd40
`define Addr_TRACK1_ETA_lane3_1      12'hd41
`define Addr_TRACK1_ETA_lane3_2      12'hd42
`define Addr_TRACK1_ETA_lane3_3      12'hd43

`define Addr_TRACK1_ETA_lane4_0      12'hd44
`define Addr_TRACK1_ETA_lane4_1      12'hd45
`define Addr_TRACK1_ETA_lane4_2      12'hd46
`define Addr_TRACK1_ETA_lane4_3      12'hd47

`define Addr_TRACK1_ETA_lane5_0      12'hd48
`define Addr_TRACK1_ETA_lane5_1      12'hd49
`define Addr_TRACK1_ETA_lane5_2      12'hd4a
`define Addr_TRACK1_ETA_lane5_3      12'hd4b

`define Addr_TRACK1_ETA_lane6_0      12'hd4c
`define Addr_TRACK1_ETA_lane6_1      12'hd4d
`define Addr_TRACK1_ETA_lane6_2      12'hd4e
`define Addr_TRACK1_ETA_lane6_3      12'hd4f

`define Addr_TRACK2_SPARE_lane0_0      12'hd50
`define Addr_TRACK2_SPARE_lane0_1      12'hd51
`define Addr_TRACK2_SPARE_lane0_2      12'hd52
`define Addr_TRACK2_SPARE_lane0_3      12'hd53

`define Addr_TRACK2_SPARE_lane1_0      12'hd54
`define Addr_TRACK2_SPARE_lane1_1      12'hd55
`define Addr_TRACK2_SPARE_lane1_2      12'hd56
`define Addr_TRACK2_SPARE_lane1_3      12'hd57

`define Addr_TRACK2_SPARE_lane2_0      12'hd58
`define Addr_TRACK2_SPARE_lane2_1      12'hd59
`define Addr_TRACK2_SPARE_lane2_2      12'hd5a
`define Addr_TRACK2_SPARE_lane2_3      12'hd5b

`define Addr_TRACK2_SPARE_lane3_0      12'hd5c
`define Addr_TRACK2_SPARE_lane3_1      12'hd5d
`define Addr_TRACK2_SPARE_lane3_2      12'hd5e
`define Addr_TRACK2_SPARE_lane3_3      12'hd5f

`define Addr_TRACK2_SPARE_lane4_0      12'hd60
`define Addr_TRACK2_SPARE_lane4_1      12'hd61
`define Addr_TRACK2_SPARE_lane4_2      12'hd62
`define Addr_TRACK2_SPARE_lane4_3      12'hd63

`define Addr_TRACK2_SPARE_lane5_0      12'hd64
`define Addr_TRACK2_SPARE_lane5_1      12'hd65
`define Addr_TRACK2_SPARE_lane5_2      12'hd66
`define Addr_TRACK2_SPARE_lane5_3      12'hd67

`define Addr_TRACK2_SPARE_lane6_0      12'hd68
`define Addr_TRACK2_SPARE_lane6_1      12'hd69
`define Addr_TRACK2_SPARE_lane6_2      12'hd6a
`define Addr_TRACK2_SPARE_lane6_3      12'hd6b

`define Addr_TRACK2_sTGC_lane0_0      12'hd6c
`define Addr_TRACK2_sTGC_lane0_1      12'hd6d
`define Addr_TRACK2_sTGC_lane0_2      12'hd6e
`define Addr_TRACK2_sTGC_lane0_3      12'hd6f

`define Addr_TRACK2_sTGC_lane1_0      12'hd70
`define Addr_TRACK2_sTGC_lane1_1      12'hd71
`define Addr_TRACK2_sTGC_lane1_2      12'hd72
`define Addr_TRACK2_sTGC_lane1_3      12'hd73

`define Addr_TRACK2_sTGC_lane2_0      12'hd74
`define Addr_TRACK2_sTGC_lane2_1      12'hd75
`define Addr_TRACK2_sTGC_lane2_2      12'hd76
`define Addr_TRACK2_sTGC_lane2_3      12'hd77

`define Addr_TRACK2_sTGC_lane3_0      12'hd78
`define Addr_TRACK2_sTGC_lane3_1      12'hd79
`define Addr_TRACK2_sTGC_lane3_2      12'hd7a
`define Addr_TRACK2_sTGC_lane3_3      12'hd7b

`define Addr_TRACK2_sTGC_lane4_0      12'hd7c
`define Addr_TRACK2_sTGC_lane4_1      12'hd7d
`define Addr_TRACK2_sTGC_lane4_2      12'hd7e
`define Addr_TRACK2_sTGC_lane4_3      12'hd7f

`define Addr_TRACK2_sTGC_lane5_0      12'hd80
`define Addr_TRACK2_sTGC_lane5_1      12'hd81
`define Addr_TRACK2_sTGC_lane5_2      12'hd82
`define Addr_TRACK2_sTGC_lane5_3      12'hd83

`define Addr_TRACK2_sTGC_lane6_0      12'hd84
`define Addr_TRACK2_sTGC_lane6_1      12'hd85
`define Addr_TRACK2_sTGC_lane6_2      12'hd86
`define Addr_TRACK2_sTGC_lane6_3      12'hd87

`define Addr_TRACK2_MM_lane0_0      12'hd88
`define Addr_TRACK2_MM_lane0_1      12'hd89
`define Addr_TRACK2_MM_lane0_2      12'hd8a
`define Addr_TRACK2_MM_lane0_3      12'hd8b

`define Addr_TRACK2_MM_lane1_0      12'hd8c
`define Addr_TRACK2_MM_lane1_1      12'hd8d
`define Addr_TRACK2_MM_lane1_2      12'hd8e
`define Addr_TRACK2_MM_lane1_3      12'hd8f

`define Addr_TRACK2_MM_lane2_0      12'hd90
`define Addr_TRACK2_MM_lane2_1      12'hd91
`define Addr_TRACK2_MM_lane2_2      12'hd92
`define Addr_TRACK2_MM_lane2_3      12'hd93

`define Addr_TRACK2_MM_lane3_0      12'hd94
`define Addr_TRACK2_MM_lane3_1      12'hd95
`define Addr_TRACK2_MM_lane3_2      12'hd96
`define Addr_TRACK2_MM_lane3_3      12'hd97

`define Addr_TRACK2_MM_lane4_0      12'hd98
`define Addr_TRACK2_MM_lane4_1      12'hd99
`define Addr_TRACK2_MM_lane4_2      12'hd9a
`define Addr_TRACK2_MM_lane4_3      12'hd9b

`define Addr_TRACK2_MM_lane5_0      12'hd9c
`define Addr_TRACK2_MM_lane5_1      12'hd9d
`define Addr_TRACK2_MM_lane5_2      12'hd9e
`define Addr_TRACK2_MM_lane5_3      12'hd9f

`define Addr_TRACK2_MM_lane6_0      12'hda0
`define Addr_TRACK2_MM_lane6_1      12'hda1
`define Addr_TRACK2_MM_lane6_2      12'hda2
`define Addr_TRACK2_MM_lane6_3      12'hda3

`define Addr_TRACK2_DTHETA_lane0_0      12'hda4
`define Addr_TRACK2_DTHETA_lane0_1      12'hda5
`define Addr_TRACK2_DTHETA_lane0_2      12'hda6
`define Addr_TRACK2_DTHETA_lane0_3      12'hda7

`define Addr_TRACK2_DTHETA_lane1_0      12'hda8
`define Addr_TRACK2_DTHETA_lane1_1      12'hda9
`define Addr_TRACK2_DTHETA_lane1_2      12'hdaa
`define Addr_TRACK2_DTHETA_lane1_3      12'hdab

`define Addr_TRACK2_DTHETA_lane2_0      12'hdac
`define Addr_TRACK2_DTHETA_lane2_1      12'hdad
`define Addr_TRACK2_DTHETA_lane2_2      12'hdae
`define Addr_TRACK2_DTHETA_lane2_3      12'hdaf

`define Addr_TRACK2_DTHETA_lane3_0      12'hdb0
`define Addr_TRACK2_DTHETA_lane3_1      12'hdb1
`define Addr_TRACK2_DTHETA_lane3_2      12'hdb2
`define Addr_TRACK2_DTHETA_lane3_3      12'hdb3

`define Addr_TRACK2_DTHETA_lane4_0      12'hdb4
`define Addr_TRACK2_DTHETA_lane4_1      12'hdb5
`define Addr_TRACK2_DTHETA_lane4_2      12'hdb6
`define Addr_TRACK2_DTHETA_lane4_3      12'hdb7

`define Addr_TRACK2_DTHETA_lane5_0      12'hdb8
`define Addr_TRACK2_DTHETA_lane5_1      12'hdb9
`define Addr_TRACK2_DTHETA_lane5_2      12'hdba
`define Addr_TRACK2_DTHETA_lane5_3      12'hdbb

`define Addr_TRACK2_DTHETA_lane6_0      12'hdbc
`define Addr_TRACK2_DTHETA_lane6_1      12'hdbd
`define Addr_TRACK2_DTHETA_lane6_2      12'hdbe
`define Addr_TRACK2_DTHETA_lane6_3      12'hdbf

`define Addr_TRACK2_PHI_lane0_0      12'hdc0
`define Addr_TRACK2_PHI_lane0_1      12'hdc1
`define Addr_TRACK2_PHI_lane0_2      12'hdc2
`define Addr_TRACK2_PHI_lane0_3      12'hdc3

`define Addr_TRACK2_PHI_lane1_0      12'hdc4
`define Addr_TRACK2_PHI_lane1_1      12'hdc5
`define Addr_TRACK2_PHI_lane1_2      12'hdc6
`define Addr_TRACK2_PHI_lane1_3      12'hdc7

`define Addr_TRACK2_PHI_lane2_0      12'hdc8
`define Addr_TRACK2_PHI_lane2_1      12'hdc9
`define Addr_TRACK2_PHI_lane2_2      12'hdca
`define Addr_TRACK2_PHI_lane2_3      12'hdcb

`define Addr_TRACK2_PHI_lane3_0      12'hdcc
`define Addr_TRACK2_PHI_lane3_1      12'hdcd
`define Addr_TRACK2_PHI_lane3_2      12'hdce
`define Addr_TRACK2_PHI_lane3_3      12'hdcf

`define Addr_TRACK2_PHI_lane4_0      12'hdd0
`define Addr_TRACK2_PHI_lane4_1      12'hdd1
`define Addr_TRACK2_PHI_lane4_2      12'hdd2
`define Addr_TRACK2_PHI_lane4_3      12'hdd3

`define Addr_TRACK2_PHI_lane5_0      12'hdd4
`define Addr_TRACK2_PHI_lane5_1      12'hdd5
`define Addr_TRACK2_PHI_lane5_2      12'hdd6
`define Addr_TRACK2_PHI_lane5_3      12'hdd7

`define Addr_TRACK2_PHI_lane6_0      12'hdd8
`define Addr_TRACK2_PHI_lane6_1      12'hdd9
`define Addr_TRACK2_PHI_lane6_2      12'hdda
`define Addr_TRACK2_PHI_lane6_3      12'hddb

`define Addr_TRACK2_ETA_lane0_0      12'hddc
`define Addr_TRACK2_ETA_lane0_1      12'hddd
`define Addr_TRACK2_ETA_lane0_2      12'hdde
`define Addr_TRACK2_ETA_lane0_3      12'hddf

`define Addr_TRACK2_ETA_lane1_0      12'hde0
`define Addr_TRACK2_ETA_lane1_1      12'hde1
`define Addr_TRACK2_ETA_lane1_2      12'hde2
`define Addr_TRACK2_ETA_lane1_3      12'hde3

`define Addr_TRACK2_ETA_lane2_0      12'hde4
`define Addr_TRACK2_ETA_lane2_1      12'hde5
`define Addr_TRACK2_ETA_lane2_2      12'hde6
`define Addr_TRACK2_ETA_lane2_3      12'hde7

`define Addr_TRACK2_ETA_lane3_0      12'hde8
`define Addr_TRACK2_ETA_lane3_1      12'hde9
`define Addr_TRACK2_ETA_lane3_2      12'hdea
`define Addr_TRACK2_ETA_lane3_3      12'hdeb

`define Addr_TRACK2_ETA_lane4_0      12'hdec
`define Addr_TRACK2_ETA_lane4_1      12'hded
`define Addr_TRACK2_ETA_lane4_2      12'hdee
`define Addr_TRACK2_ETA_lane4_3      12'hdef

`define Addr_TRACK2_ETA_lane5_0      12'hdf0
`define Addr_TRACK2_ETA_lane5_1      12'hdf1
`define Addr_TRACK2_ETA_lane5_2      12'hdf2
`define Addr_TRACK2_ETA_lane5_3      12'hdf3

`define Addr_TRACK2_ETA_lane6_0      12'hdf4
`define Addr_TRACK2_ETA_lane6_1      12'hdf5
`define Addr_TRACK2_ETA_lane6_2      12'hdf6
`define Addr_TRACK2_ETA_lane6_3      12'hdf7

`define Addr_TRACK3_SPARE_lane0_0      12'hdf8
`define Addr_TRACK3_SPARE_lane0_1      12'hdf9
`define Addr_TRACK3_SPARE_lane0_2      12'hdfa
`define Addr_TRACK3_SPARE_lane0_3      12'hdfb

`define Addr_TRACK3_SPARE_lane1_0      12'hdfc
`define Addr_TRACK3_SPARE_lane1_1      12'hdfd
`define Addr_TRACK3_SPARE_lane1_2      12'hdfe
`define Addr_TRACK3_SPARE_lane1_3      12'hdff

`define Addr_TRACK3_SPARE_lane2_0      12'he00
`define Addr_TRACK3_SPARE_lane2_1      12'he01
`define Addr_TRACK3_SPARE_lane2_2      12'he02
`define Addr_TRACK3_SPARE_lane2_3      12'he03

`define Addr_TRACK3_SPARE_lane3_0      12'he04
`define Addr_TRACK3_SPARE_lane3_1      12'he05
`define Addr_TRACK3_SPARE_lane3_2      12'he06
`define Addr_TRACK3_SPARE_lane3_3      12'he07

`define Addr_TRACK3_SPARE_lane4_0      12'he08
`define Addr_TRACK3_SPARE_lane4_1      12'he09
`define Addr_TRACK3_SPARE_lane4_2      12'he0a
`define Addr_TRACK3_SPARE_lane4_3      12'he0b

`define Addr_TRACK3_SPARE_lane5_0      12'he0c
`define Addr_TRACK3_SPARE_lane5_1      12'he0d
`define Addr_TRACK3_SPARE_lane5_2      12'he0e
`define Addr_TRACK3_SPARE_lane5_3      12'he0f

`define Addr_TRACK3_SPARE_lane6_0      12'he10
`define Addr_TRACK3_SPARE_lane6_1      12'he11
`define Addr_TRACK3_SPARE_lane6_2      12'he12
`define Addr_TRACK3_SPARE_lane6_3      12'he13

`define Addr_TRACK3_sTGC_lane0_0      12'he14
`define Addr_TRACK3_sTGC_lane0_1      12'he15
`define Addr_TRACK3_sTGC_lane0_2      12'he16
`define Addr_TRACK3_sTGC_lane0_3      12'he17

`define Addr_TRACK3_sTGC_lane1_0      12'he18
`define Addr_TRACK3_sTGC_lane1_1      12'he19
`define Addr_TRACK3_sTGC_lane1_2      12'he1a
`define Addr_TRACK3_sTGC_lane1_3      12'he1b

`define Addr_TRACK3_sTGC_lane2_0      12'he1c
`define Addr_TRACK3_sTGC_lane2_1      12'he1d
`define Addr_TRACK3_sTGC_lane2_2      12'he1e
`define Addr_TRACK3_sTGC_lane2_3      12'he1f

`define Addr_TRACK3_sTGC_lane3_0      12'he20
`define Addr_TRACK3_sTGC_lane3_1      12'he21
`define Addr_TRACK3_sTGC_lane3_2      12'he22
`define Addr_TRACK3_sTGC_lane3_3      12'he23

`define Addr_TRACK3_sTGC_lane4_0      12'he24
`define Addr_TRACK3_sTGC_lane4_1      12'he25
`define Addr_TRACK3_sTGC_lane4_2      12'he26
`define Addr_TRACK3_sTGC_lane4_3      12'he27

`define Addr_TRACK3_sTGC_lane5_0      12'he28
`define Addr_TRACK3_sTGC_lane5_1      12'he29
`define Addr_TRACK3_sTGC_lane5_2      12'he2a
`define Addr_TRACK3_sTGC_lane5_3      12'he2b

`define Addr_TRACK3_sTGC_lane6_0      12'he2c
`define Addr_TRACK3_sTGC_lane6_1      12'he2d
`define Addr_TRACK3_sTGC_lane6_2      12'he2e
`define Addr_TRACK3_sTGC_lane6_3      12'he2f

`define Addr_TRACK3_MM_lane0_0      12'he30
`define Addr_TRACK3_MM_lane0_1      12'he31
`define Addr_TRACK3_MM_lane0_2      12'he32
`define Addr_TRACK3_MM_lane0_3      12'he33

`define Addr_TRACK3_MM_lane1_0      12'he34
`define Addr_TRACK3_MM_lane1_1      12'he35
`define Addr_TRACK3_MM_lane1_2      12'he36
`define Addr_TRACK3_MM_lane1_3      12'he37

`define Addr_TRACK3_MM_lane2_0      12'he38
`define Addr_TRACK3_MM_lane2_1      12'he39
`define Addr_TRACK3_MM_lane2_2      12'he3a
`define Addr_TRACK3_MM_lane2_3      12'he3b

`define Addr_TRACK3_MM_lane3_0      12'he3c
`define Addr_TRACK3_MM_lane3_1      12'he3d
`define Addr_TRACK3_MM_lane3_2      12'he3e
`define Addr_TRACK3_MM_lane3_3      12'he3f

`define Addr_TRACK3_MM_lane4_0      12'he40
`define Addr_TRACK3_MM_lane4_1      12'he41
`define Addr_TRACK3_MM_lane4_2      12'he42
`define Addr_TRACK3_MM_lane4_3      12'he43

`define Addr_TRACK3_MM_lane5_0      12'he44
`define Addr_TRACK3_MM_lane5_1      12'he45
`define Addr_TRACK3_MM_lane5_2      12'he46
`define Addr_TRACK3_MM_lane5_3      12'he47

`define Addr_TRACK3_MM_lane6_0      12'he48
`define Addr_TRACK3_MM_lane6_1      12'he49
`define Addr_TRACK3_MM_lane6_2      12'he4a
`define Addr_TRACK3_MM_lane6_3      12'he4b

`define Addr_TRACK3_DTHETA_lane0_0      12'he4c
`define Addr_TRACK3_DTHETA_lane0_1      12'he4d
`define Addr_TRACK3_DTHETA_lane0_2      12'he4e
`define Addr_TRACK3_DTHETA_lane0_3      12'he4f

`define Addr_TRACK3_DTHETA_lane1_0      12'he50
`define Addr_TRACK3_DTHETA_lane1_1      12'he51
`define Addr_TRACK3_DTHETA_lane1_2      12'he52
`define Addr_TRACK3_DTHETA_lane1_3      12'he53

`define Addr_TRACK3_DTHETA_lane2_0      12'he54
`define Addr_TRACK3_DTHETA_lane2_1      12'he55
`define Addr_TRACK3_DTHETA_lane2_2      12'he56
`define Addr_TRACK3_DTHETA_lane2_3      12'he57

`define Addr_TRACK3_DTHETA_lane3_0      12'he58
`define Addr_TRACK3_DTHETA_lane3_1      12'he59
`define Addr_TRACK3_DTHETA_lane3_2      12'he5a
`define Addr_TRACK3_DTHETA_lane3_3      12'he5b

`define Addr_TRACK3_DTHETA_lane4_0      12'he5c
`define Addr_TRACK3_DTHETA_lane4_1      12'he5d
`define Addr_TRACK3_DTHETA_lane4_2      12'he5e
`define Addr_TRACK3_DTHETA_lane4_3      12'he5f

`define Addr_TRACK3_DTHETA_lane5_0      12'he60
`define Addr_TRACK3_DTHETA_lane5_1      12'he61
`define Addr_TRACK3_DTHETA_lane5_2      12'he62
`define Addr_TRACK3_DTHETA_lane5_3      12'he63

`define Addr_TRACK3_DTHETA_lane6_0      12'he64
`define Addr_TRACK3_DTHETA_lane6_1      12'he65
`define Addr_TRACK3_DTHETA_lane6_2      12'he66
`define Addr_TRACK3_DTHETA_lane6_3      12'he67

`define Addr_TRACK3_PHI_lane0_0      12'he68
`define Addr_TRACK3_PHI_lane0_1      12'he69
`define Addr_TRACK3_PHI_lane0_2      12'he6a
`define Addr_TRACK3_PHI_lane0_3      12'he6b

`define Addr_TRACK3_PHI_lane1_0      12'he6c
`define Addr_TRACK3_PHI_lane1_1      12'he6d
`define Addr_TRACK3_PHI_lane1_2      12'he6e
`define Addr_TRACK3_PHI_lane1_3      12'he6f

`define Addr_TRACK3_PHI_lane2_0      12'he70
`define Addr_TRACK3_PHI_lane2_1      12'he71
`define Addr_TRACK3_PHI_lane2_2      12'he72
`define Addr_TRACK3_PHI_lane2_3      12'he73

`define Addr_TRACK3_PHI_lane3_0      12'he74
`define Addr_TRACK3_PHI_lane3_1      12'he75
`define Addr_TRACK3_PHI_lane3_2      12'he76
`define Addr_TRACK3_PHI_lane3_3      12'he77

`define Addr_TRACK3_PHI_lane4_0      12'he78
`define Addr_TRACK3_PHI_lane4_1      12'he79
`define Addr_TRACK3_PHI_lane4_2      12'he7a
`define Addr_TRACK3_PHI_lane4_3      12'he7b

`define Addr_TRACK3_PHI_lane5_0      12'he7c
`define Addr_TRACK3_PHI_lane5_1      12'he7d
`define Addr_TRACK3_PHI_lane5_2      12'he7e
`define Addr_TRACK3_PHI_lane5_3      12'he7f

`define Addr_TRACK3_PHI_lane6_0      12'he80
`define Addr_TRACK3_PHI_lane6_1      12'he81
`define Addr_TRACK3_PHI_lane6_2      12'he82
`define Addr_TRACK3_PHI_lane6_3      12'he83

`define Addr_TRACK3_ETA_lane0_0      12'he84
`define Addr_TRACK3_ETA_lane0_1      12'he85
`define Addr_TRACK3_ETA_lane0_2      12'he86
`define Addr_TRACK3_ETA_lane0_3      12'he87

`define Addr_TRACK3_ETA_lane1_0      12'he88
`define Addr_TRACK3_ETA_lane1_1      12'he89
`define Addr_TRACK3_ETA_lane1_2      12'he8a
`define Addr_TRACK3_ETA_lane1_3      12'he8b

`define Addr_TRACK3_ETA_lane2_0      12'he8c
`define Addr_TRACK3_ETA_lane2_1      12'he8d
`define Addr_TRACK3_ETA_lane2_2      12'he8e
`define Addr_TRACK3_ETA_lane2_3      12'he8f

`define Addr_TRACK3_ETA_lane3_0      12'he90
`define Addr_TRACK3_ETA_lane3_1      12'he91
`define Addr_TRACK3_ETA_lane3_2      12'he92
`define Addr_TRACK3_ETA_lane3_3      12'he93

`define Addr_TRACK3_ETA_lane4_0      12'he94
`define Addr_TRACK3_ETA_lane4_1      12'he95
`define Addr_TRACK3_ETA_lane4_2      12'he96
`define Addr_TRACK3_ETA_lane4_3      12'he97

`define Addr_TRACK3_ETA_lane5_0      12'he98
`define Addr_TRACK3_ETA_lane5_1      12'he99
`define Addr_TRACK3_ETA_lane5_2      12'he9a
`define Addr_TRACK3_ETA_lane5_3      12'he9b

`define Addr_TRACK3_ETA_lane6_0      12'he9c
`define Addr_TRACK3_ETA_lane6_1      12'he9d
`define Addr_TRACK3_ETA_lane6_2      12'he9e
`define Addr_TRACK3_ETA_lane6_3      12'he9f

module vme  (
    input wire TTC_CLK,
    input wire CLK_160,
    input wire TXUSRCLK_in,
    input wire OE,
    input wire CE,
    input wire WE,
    input wire [11:0] VME_A,
    inout wire [15:0] VME_D,

    output reg Reset_TTC_out, // Global resets
    output reg Reset_160_out, // Global resets
    output reg Reset_TX_out,  // Global resets

    output reg Scaler_reset_out,
    output reg GTX_TX_reset_out,
    output reg GTX_RX_reset_out,
    output reg Delay_reset_out,
    output reg L1Buffer_reset_out,
    output reg Derandomizer_reset_out,
    output reg ZeroSupp_reset_out,
    output reg CLK_reset_out,
    output reg FIFO_reset_out,
    output reg SiTCP_reset_out,
    output reg SiTCP_FIFO_reset_out,
    output reg TX_Logic_reset_out,
    output reg Monitoring_reset_out,
    output reg LUT_init_reset_out,

    output reg EMULATOR_reset_out,
    output reg EMULATOR_enable,        // emulator
    output reg [15:0] EMULATOR_data_length, 
        
    output reg [15:0] EMULATOR_GTX_header,

    output reg [7:0] TRACK0_eta,        // emulator
    output reg [5:0] TRACK0_phi,        // emulator
    output reg [4:0] TRACK0_deltatheta, // emulator
    output reg [1:0] TRACK0_mm,        // emulator
    output reg [1:0] TRACK0_stgc,        // emulator
    output reg       TRACK0_spare,        // emulator
    
    output reg [7:0] TRACK1_eta,        // emulator
    output reg [5:0] TRACK1_phi,        // emulator
    output reg [4:0] TRACK1_deltatheta, // emulator
    output reg [1:0] TRACK1_mm,        // emulator
    output reg [1:0] TRACK1_stgc,        // emulator
    output reg       TRACK1_spare,        // emulator

    output reg [7:0] TRACK2_eta,        // emulator
    output reg [5:0] TRACK2_phi,        // emulator
    output reg [4:0] TRACK2_deltatheta, // emulator
    output reg [1:0] TRACK2_mm,        // emulator
    output reg [1:0] TRACK2_stgc,        // emulator
    output reg       TRACK2_spare,        // emulator
    
    output reg [7:0] TRACK3_eta,        // emulator
    output reg [5:0] TRACK3_phi,        // emulator
    output reg [4:0] TRACK3_deltatheta, // emulator
    output reg [1:0] TRACK3_mm,        // emulator
    output reg [1:0] TRACK3_stgc,        // emulator
    output reg       TRACK3_spare,        // emulator    


    output reg gt0_rx_reset_out,
    output reg gt1_rx_reset_out,
    output reg gt2_rx_reset_out,
    output reg gt3_rx_reset_out,
    output reg gt4_rx_reset_out,
    output reg gt5_rx_reset_out,
    output reg gt6_rx_reset_out,
    output reg gt7_rx_reset_out,
    output reg gt8_rx_reset_out,
    output reg gt9_rx_reset_out,
    output reg gt10_rx_reset_out,
    output reg gt11_rx_reset_out,

    input wire [15:0] Error_Scaler_gt_0,
    input wire [15:0] Error_Scaler_gt_1,
    input wire [15:0] Error_Scaler_gt_2,
    input wire [15:0] Error_Scaler_gt_3,
    input wire [15:0] Error_Scaler_gt_4,
    input wire [15:0] Error_Scaler_gt_5,
    input wire [15:0] Error_Scaler_gt_6,
    input wire [15:0] Error_Scaler_gt_7,
    input wire [15:0] Error_Scaler_gt_8,
    input wire [15:0] Error_Scaler_gt_9,
    input wire [15:0] Error_Scaler_gt_10,
    input wire [15:0] Error_Scaler_gt_11,

    input wire [15:0] Status_gt_0,
    input wire [15:0] Status_gt_1,
    input wire [15:0] Status_gt_2,
    input wire [15:0] Status_gt_3,
    input wire [15:0] Status_gt_4,
    input wire [15:0] Status_gt_5,
    input wire [15:0] Status_gt_6,
    input wire [15:0] Status_gt_7,
    input wire [15:0] Status_gt_8,
    input wire [15:0] Status_gt_9,
    input wire [15:0] Status_gt_10,
    input wire [15:0] Status_gt_11,

    
    output reg [7:0] Delay_glink0,
    output reg [7:0] Delay_glink1,
    output reg [7:0] Delay_glink2,
    output reg [7:0] Delay_glink3,
    output reg [7:0] Delay_glink4,
    output reg [7:0] Delay_glink5,
    output reg [7:0] Delay_glink6,
    output reg [7:0] Delay_glink7,
    output reg [7:0] Delay_glink8,
    output reg [7:0] Delay_glink9,
    output reg [7:0] Delay_glink10,
    output reg [7:0] Delay_glink11,
    output reg [7:0] Delay_glink12,
    output reg [7:0] Delay_glink13,

    output reg [7:0] Delay_gtx0,
    output reg [7:0] Delay_gtx1,
    output reg [7:0] Delay_gtx2,
    output reg [7:0] Delay_gtx3,
    output reg [7:0] Delay_gtx4,
    output reg [7:0] Delay_gtx5,
    output reg [7:0] Delay_gtx6,
    output reg [7:0] Delay_gtx7,
    output reg [7:0] Delay_gtx8,
    output reg [7:0] Delay_gtx9,
    output reg [7:0] Delay_gtx10,
    output reg [7:0] Delay_gtx11,

    output reg [7:0] Delay_L1A,
    output reg [7:0] Delay_BCR,
    output reg [7:0] Delay_trig_BCR,
    output reg [7:0] Delay_ECR,
    output reg [7:0] Delay_TTC_RESET,
    output reg [7:0] Delay_TEST_PULSE,
        
    output reg [1:0] Mask_gtx0,
    output reg [1:0] Mask_gtx1,
    output reg [1:0] Mask_gtx2,
    output reg [1:0] Mask_gtx3,
    output reg [1:0] Mask_gtx4,
    output reg [1:0] Mask_gtx5,
    output reg [1:0] Mask_gtx6,
    output reg [1:0] Mask_gtx7,
    output reg [1:0] Mask_gtx8,
    output reg [1:0] Mask_gtx9,
    output reg [1:0] Mask_gtx10,
    output reg [1:0] Mask_gtx11,

    output reg Mask_L1A,
    output reg Mask_BCR,
    output reg Mask_trig_BCR,
    output reg Mask_ECR,
    output reg Mask_TTC_RESET,
    output reg Mask_TEST_PULSE,

    output reg [1:0] Mask_glink0,
    output reg [1:0] Mask_glink1,
    output reg [1:0] Mask_glink2,
    output reg [1:0] Mask_glink3,
    output reg [1:0] Mask_glink4,
    output reg [1:0] Mask_glink5,
    output reg [1:0] Mask_glink6,
    output reg [1:0] Mask_glink7,
    output reg [1:0] Mask_glink8,
    output reg [1:0] Mask_glink9,
    output reg [1:0] Mask_glink10,
    output reg [1:0] Mask_glink11,
    output reg [1:0] Mask_glink12,
    output reg [1:0] Mask_glink13,
    
    output reg [15:0] Test_Pulse_Length,
    output reg [15:0] Test_Pulse_Wait_Length,
    output reg Test_Pulse_Enable,
    
    input wire [15:0] L1A_Counter0,
    input wire [15:0] L1A_Counter1,
    input wire [11:0] L1ID,
    input wire [11:0] BCID,
    output reg [11:0] SLID,
    output reg [15:0] Readout_BC,
    output reg [6:0] L1Buffer_depth,
    output reg [6:0] trigL1Buffer_depth,
    output reg [6:0] L1Buffer_BW_depth,
    
    input wire [15:0] L1Buffer_status,        
    input wire [15:0] Derandomizer_status,        
    input wire [15:0] Derandomizer_Count1_1,
    input wire [15:0] Derandomizer_Count1_2,
    input wire [15:0] Derandomizer_Count2,
    input wire [15:0] Derandomizer_Count3,

    input wire [15:0] ZeroSuppress_status,        
    input wire [15:0] ZeroSuppress_Count,        
    input wire [11:0] data_size,        
    
    input wire [15:0] SiTCP_Count,

    input wire [15:0] BUSY_Count,
    
    output reg [15:0] FullBoard_ID,

    input wire [15:0] monitoring_FIFO_out_0,
    input wire [15:0] monitoring_FIFO_out_1,
    input wire [15:0] monitoring_FIFO_out_2,
    input wire [15:0] monitoring_FIFO_out_3,
    input wire [15:0] monitoring_FIFO_out_4,
    input wire [15:0] monitoring_FIFO_out_5,
    input wire [15:0] monitoring_FIFO_out_6,
    input wire [15:0] monitoring_FIFO_out_7,
    input wire [15:0] monitoring_FIFO_out_8,
    input wire [15:0] monitoring_FIFO_out_9,
    input wire [15:0] monitoring_FIFO_out_10,
    input wire [15:0] monitoring_FIFO_out_11,
    input wire [15:0] monitoring_FIFO_out_12,
    input wire [15:0] monitoring_FIFO_out_13,
    input wire [15:0] monitoring_FIFO_out_14,
    input wire [15:0] monitoring_FIFO_out_15,

    output reg monitoring_FIFO_wr_en,
    output reg monitoring_FIFO_rd_en,
    output reg [6:0] lane_selector_out,  
    output reg xadc_mon_enable_out,
    output reg [6:0] xadc_mon_addr_out,
    input wire [15:0] xadc_monitor_in,

    output reg [15:0] L1A_manual_parameter,
    output reg Busy_propagation,
    
    output reg [15:0] gtx0_test_data1,
    output reg [15:0] gtx0_test_data2,
    output reg [15:0] gtx0_test_data3,
    output reg [15:0] gtx0_test_data4,
    output reg [15:0] gtx0_test_data5,
    output reg [15:0] gtx0_test_data6,
    output reg [15:0] gtx0_test_data7,
    output reg [15:0] gtx1_test_data1,
    output reg [15:0] gtx1_test_data2,
    output reg [15:0] gtx1_test_data3,
    output reg [15:0] gtx1_test_data4,
    output reg [15:0] gtx1_test_data5,
    output reg [15:0] gtx1_test_data6,
    output reg [15:0] gtx1_test_data7,
    output reg [15:0] gtx2_test_data1,
    output reg [15:0] gtx2_test_data2,
    output reg [15:0] gtx2_test_data3,
    output reg [15:0] gtx2_test_data4,
    output reg [15:0] gtx2_test_data5,
    output reg [15:0] gtx2_test_data6,
    output reg [15:0] gtx2_test_data7,
    output reg [15:0] gtx3_test_data1,
    output reg [15:0] gtx3_test_data2,
    output reg [15:0] gtx3_test_data3,
    output reg [15:0] gtx3_test_data4,
    output reg [15:0] gtx3_test_data5,
    output reg [15:0] gtx3_test_data6,
    output reg [15:0] gtx3_test_data7,
    output reg [15:0] gtx4_test_data1,
    output reg [15:0] gtx4_test_data2,
    output reg [15:0] gtx4_test_data3,
    output reg [15:0] gtx4_test_data4,
    output reg [15:0] gtx4_test_data5,
    output reg [15:0] gtx4_test_data6,
    output reg [15:0] gtx4_test_data7,
    output reg [15:0] gtx5_test_data1,
    output reg [15:0] gtx5_test_data2,
    output reg [15:0] gtx5_test_data3,
    output reg [15:0] gtx5_test_data4,
    output reg [15:0] gtx5_test_data5,
    output reg [15:0] gtx5_test_data6,
    output reg [15:0] gtx5_test_data7,
    output reg [15:0] gtx6_test_data1,
    output reg [15:0] gtx6_test_data2,
    output reg [15:0] gtx6_test_data3,
    output reg [15:0] gtx6_test_data4,
    output reg [15:0] gtx6_test_data5,
    output reg [15:0] gtx6_test_data6,
    output reg [15:0] gtx6_test_data7,
    output reg [15:0] gtx7_test_data1,
    output reg [15:0] gtx7_test_data2,
    output reg [15:0] gtx7_test_data3,
    output reg [15:0] gtx7_test_data4,
    output reg [15:0] gtx7_test_data5,
    output reg [15:0] gtx7_test_data6,
    output reg [15:0] gtx7_test_data7,
    output reg [15:0] gtx8_test_data1,
    output reg [15:0] gtx8_test_data2,
    output reg [15:0] gtx8_test_data3,
    output reg [15:0] gtx8_test_data4,
    output reg [15:0] gtx8_test_data5,
    output reg [15:0] gtx8_test_data6,
    output reg [15:0] gtx8_test_data7,
    output reg [15:0] gtx9_test_data1,
    output reg [15:0] gtx9_test_data2,
    output reg [15:0] gtx9_test_data3,
    output reg [15:0] gtx9_test_data4,
    output reg [15:0] gtx9_test_data5,
    output reg [15:0] gtx9_test_data6,
    output reg [15:0] gtx9_test_data7,
    output reg [15:0] gtx10_test_data1,
    output reg [15:0] gtx10_test_data2,
    output reg [15:0] gtx10_test_data3,
    output reg [15:0] gtx10_test_data4,
    output reg [15:0] gtx10_test_data5,
    output reg [15:0] gtx10_test_data6,
    output reg [15:0] gtx10_test_data7,
    output reg [15:0] gtx11_test_data1,
    output reg [15:0] gtx11_test_data2,
    output reg [15:0] gtx11_test_data3,
    output reg [15:0] gtx11_test_data4,
    output reg [15:0] gtx11_test_data5,
    output reg [15:0] gtx11_test_data6,
    output reg [15:0] gtx11_test_data7,

    output reg [15:0] glink0_test_data1,
    output reg [3:0] glink0_test_data2,
    output reg [15:0] glink1_test_data1,
    output reg [3:0] glink1_test_data2,
    output reg [15:0] glink2_test_data1,
    output reg [3:0] glink2_test_data2,
    output reg [15:0] glink3_test_data1,
    output reg [3:0] glink3_test_data2,
    output reg [15:0] glink4_test_data1,
    output reg [3:0] glink4_test_data2,
    output reg [15:0] glink5_test_data1,
    output reg [3:0] glink5_test_data2,
    output reg [15:0] glink6_test_data1,
    output reg [3:0] glink6_test_data2,
    output reg [15:0] glink7_test_data1,
    output reg [3:0] glink7_test_data2,
    output reg [15:0] glink8_test_data1,
    output reg [3:0] glink8_test_data2,
    output reg [15:0] glink9_test_data1,
    output reg [3:0] glink9_test_data2,
    output reg [15:0] glink10_test_data1,
    output reg [3:0] glink10_test_data2,
    output reg [15:0] glink11_test_data1,
    output reg [3:0] glink11_test_data2,
    output reg [15:0] glink12_test_data1,
    output reg [3:0] glink12_test_data2,
    output reg [15:0] glink13_test_data1,
    output reg [3:0] glink13_test_data2,

// gtx phase monitor
    output reg [3:0]  gtx0_phase_select,
    output reg [3:0]  gtx1_phase_select,
    output reg [3:0]  gtx2_phase_select,
    output reg [3:0]  gtx3_phase_select,
    output reg [3:0]  gtx4_phase_select,
    output reg [3:0]  gtx5_phase_select,
    output reg [3:0]  gtx6_phase_select,
    output reg [3:0]  gtx7_phase_select,
    output reg [3:0]  gtx8_phase_select,
    output reg [3:0]  gtx9_phase_select,
    output reg [3:0]  gtx10_phase_select,
    output reg [3:0]  gtx11_phase_select,

    input  wire [5:0] gtx0_phase_monitor_0,
    input  wire [5:0] gtx1_phase_monitor_0,
    input  wire [5:0] gtx2_phase_monitor_0,
    input  wire [5:0] gtx3_phase_monitor_0,
    input  wire [5:0] gtx4_phase_monitor_0,
    input  wire [5:0] gtx5_phase_monitor_0,
    input  wire [5:0] gtx6_phase_monitor_0,
    input  wire [5:0] gtx7_phase_monitor_0,
    input  wire [5:0] gtx8_phase_monitor_0,
    input  wire [5:0] gtx9_phase_monitor_0,
    input  wire [5:0] gtx10_phase_monitor_0,
    input  wire [5:0] gtx11_phase_monitor_0,

    input  wire [5:0] gtx0_phase_monitor_1,
    input  wire [5:0] gtx1_phase_monitor_1,
    input  wire [5:0] gtx2_phase_monitor_1,
    input  wire [5:0] gtx3_phase_monitor_1,
    input  wire [5:0] gtx4_phase_monitor_1,
    input  wire [5:0] gtx5_phase_monitor_1,
    input  wire [5:0] gtx6_phase_monitor_1,
    input  wire [5:0] gtx7_phase_monitor_1,
    input  wire [5:0] gtx8_phase_monitor_1,
    input  wire [5:0] gtx9_phase_monitor_1,
    input  wire [5:0] gtx10_phase_monitor_1,
    input  wire [5:0] gtx11_phase_monitor_1,

    input  wire [5:0] gtx0_phase_monitor_2,
    input  wire [5:0] gtx1_phase_monitor_2,
    input  wire [5:0] gtx2_phase_monitor_2,
    input  wire [5:0] gtx3_phase_monitor_2,
    input  wire [5:0] gtx4_phase_monitor_2,
    input  wire [5:0] gtx5_phase_monitor_2,
    input  wire [5:0] gtx6_phase_monitor_2,
    input  wire [5:0] gtx7_phase_monitor_2,
    input  wire [5:0] gtx8_phase_monitor_2,
    input  wire [5:0] gtx9_phase_monitor_2,
    input  wire [5:0] gtx10_phase_monitor_2,
    input  wire [5:0] gtx11_phase_monitor_2,

    input  wire [5:0] gtx0_phase_monitor_3,
    input  wire [5:0] gtx1_phase_monitor_3,
    input  wire [5:0] gtx2_phase_monitor_3,
    input  wire [5:0] gtx3_phase_monitor_3,
    input  wire [5:0] gtx4_phase_monitor_3,
    input  wire [5:0] gtx5_phase_monitor_3,
    input  wire [5:0] gtx6_phase_monitor_3,
    input  wire [5:0] gtx7_phase_monitor_3,
    input  wire [5:0] gtx8_phase_monitor_3,
    input  wire [5:0] gtx9_phase_monitor_3,
    input  wire [5:0] gtx10_phase_monitor_3,
    input  wire [5:0] gtx11_phase_monitor_3,
    
// LUT initialization
    output reg [1:0] LUT_init_mode,
    output reg [15:0] LUT_init_data,
    output reg LUT_init_wr_en,
    input wire SectorID,
    input wire [2:0] LUT_init_type,
    input wire [7:0] LUT_init_RoI,
    input wire [14:0] LUT_init_Address,
    input wire [4:0] LUT_init_data_out,
    input wire LUT_init_data_flg,
    input wire LUT_init_full,
    input wire [3:0] LUT_init_state,
    input wire LUT_init_valid,
    input wire [4:0] LUT_rd_data,
    output reg [14:0] LUT_rd_address,
    
    output reg [7:0] Align_eta_NSW_0,
    output reg [7:0] Align_eta_NSW_1,
    output reg [7:0] Align_eta_NSW_2,
    output reg [7:0] Align_eta_NSW_3,
    output reg [7:0] Align_eta_NSW_4,
    output reg [7:0] Align_eta_NSW_5,
    output reg [7:0] Align_eta_RPC,
    output reg [5:0] Align_phi_NSW_0,
    output reg [5:0] Align_phi_NSW_1,
    output reg [5:0] Align_phi_NSW_2,
    output reg [5:0] Align_phi_NSW_3,
    output reg [5:0] Align_phi_NSW_4,
    output reg [5:0] Align_phi_NSW_5,
    output reg [7:0] Align_phi_RPC,

    output reg TRACK0_SPARE_lane0_data0,
    output reg TRACK0_SPARE_lane0_data1,
    output reg TRACK0_SPARE_lane0_data2,
    output reg TRACK0_SPARE_lane0_data3,
    
    output reg TRACK0_SPARE_lane1_data0,
    output reg TRACK0_SPARE_lane1_data1,
    output reg TRACK0_SPARE_lane1_data2,
    output reg TRACK0_SPARE_lane1_data3,
    
    output reg TRACK0_SPARE_lane2_data0,
    output reg TRACK0_SPARE_lane2_data1,
    output reg TRACK0_SPARE_lane2_data2,
    output reg TRACK0_SPARE_lane2_data3,
    
    output reg TRACK0_SPARE_lane3_data0,
    output reg TRACK0_SPARE_lane3_data1,
    output reg TRACK0_SPARE_lane3_data2,
    output reg TRACK0_SPARE_lane3_data3,
    
    output reg TRACK0_SPARE_lane4_data0,
    output reg TRACK0_SPARE_lane4_data1,
    output reg TRACK0_SPARE_lane4_data2,
    output reg TRACK0_SPARE_lane4_data3,
    
    output reg TRACK0_SPARE_lane5_data0,
    output reg TRACK0_SPARE_lane5_data1,
    output reg TRACK0_SPARE_lane5_data2,
    output reg TRACK0_SPARE_lane5_data3,
    
    output reg TRACK0_SPARE_lane6_data0,
    output reg TRACK0_SPARE_lane6_data1,
    output reg TRACK0_SPARE_lane6_data2,
    output reg TRACK0_SPARE_lane6_data3,
    
    output reg [1:0] TRACK0_sTGC_lane0_data0,
    output reg [1:0] TRACK0_sTGC_lane0_data1,
    output reg [1:0] TRACK0_sTGC_lane0_data2,
    output reg [1:0] TRACK0_sTGC_lane0_data3,
    
    output reg [1:0] TRACK0_sTGC_lane1_data0,
    output reg [1:0] TRACK0_sTGC_lane1_data1,
    output reg [1:0] TRACK0_sTGC_lane1_data2,
    output reg [1:0] TRACK0_sTGC_lane1_data3,
    
    output reg [1:0] TRACK0_sTGC_lane2_data0,
    output reg [1:0] TRACK0_sTGC_lane2_data1,
    output reg [1:0] TRACK0_sTGC_lane2_data2,
    output reg [1:0] TRACK0_sTGC_lane2_data3,
    
    output reg [1:0] TRACK0_sTGC_lane3_data0,
    output reg [1:0] TRACK0_sTGC_lane3_data1,
    output reg [1:0] TRACK0_sTGC_lane3_data2,
    output reg [1:0] TRACK0_sTGC_lane3_data3,
    
    output reg [1:0] TRACK0_sTGC_lane4_data0,
    output reg [1:0] TRACK0_sTGC_lane4_data1,
    output reg [1:0] TRACK0_sTGC_lane4_data2,
    output reg [1:0] TRACK0_sTGC_lane4_data3,
    
    output reg [1:0] TRACK0_sTGC_lane5_data0,
    output reg [1:0] TRACK0_sTGC_lane5_data1,
    output reg [1:0] TRACK0_sTGC_lane5_data2,
    output reg [1:0] TRACK0_sTGC_lane5_data3,
    
    output reg [1:0] TRACK0_sTGC_lane6_data0,
    output reg [1:0] TRACK0_sTGC_lane6_data1,
    output reg [1:0] TRACK0_sTGC_lane6_data2,
    output reg [1:0] TRACK0_sTGC_lane6_data3,
    
    output reg [1:0] TRACK0_MM_lane0_data0,
    output reg [1:0] TRACK0_MM_lane0_data1,
    output reg [1:0] TRACK0_MM_lane0_data2,
    output reg [1:0] TRACK0_MM_lane0_data3,
    
    output reg [1:0] TRACK0_MM_lane1_data0,
    output reg [1:0] TRACK0_MM_lane1_data1,
    output reg [1:0] TRACK0_MM_lane1_data2,
    output reg [1:0] TRACK0_MM_lane1_data3,
    
    output reg [1:0] TRACK0_MM_lane2_data0,
    output reg [1:0] TRACK0_MM_lane2_data1,
    output reg [1:0] TRACK0_MM_lane2_data2,
    output reg [1:0] TRACK0_MM_lane2_data3,
    
    output reg [1:0] TRACK0_MM_lane3_data0,
    output reg [1:0] TRACK0_MM_lane3_data1,
    output reg [1:0] TRACK0_MM_lane3_data2,
    output reg [1:0] TRACK0_MM_lane3_data3,
    
    output reg [1:0] TRACK0_MM_lane4_data0,
    output reg [1:0] TRACK0_MM_lane4_data1,
    output reg [1:0] TRACK0_MM_lane4_data2,
    output reg [1:0] TRACK0_MM_lane4_data3,
    
    output reg [1:0] TRACK0_MM_lane5_data0,
    output reg [1:0] TRACK0_MM_lane5_data1,
    output reg [1:0] TRACK0_MM_lane5_data2,
    output reg [1:0] TRACK0_MM_lane5_data3,
    
    output reg [1:0] TRACK0_MM_lane6_data0,
    output reg [1:0] TRACK0_MM_lane6_data1,
    output reg [1:0] TRACK0_MM_lane6_data2,
    output reg [1:0] TRACK0_MM_lane6_data3,
    
    output reg [4:0] TRACK0_DTHETA_lane0_data0,
    output reg [4:0] TRACK0_DTHETA_lane0_data1,
    output reg [4:0] TRACK0_DTHETA_lane0_data2,
    output reg [4:0] TRACK0_DTHETA_lane0_data3,
    
    output reg [4:0] TRACK0_DTHETA_lane1_data0,
    output reg [4:0] TRACK0_DTHETA_lane1_data1,
    output reg [4:0] TRACK0_DTHETA_lane1_data2,
    output reg [4:0] TRACK0_DTHETA_lane1_data3,
    
    output reg [4:0] TRACK0_DTHETA_lane2_data0,
    output reg [4:0] TRACK0_DTHETA_lane2_data1,
    output reg [4:0] TRACK0_DTHETA_lane2_data2,
    output reg [4:0] TRACK0_DTHETA_lane2_data3,
    
    output reg [4:0] TRACK0_DTHETA_lane3_data0,
    output reg [4:0] TRACK0_DTHETA_lane3_data1,
    output reg [4:0] TRACK0_DTHETA_lane3_data2,
    output reg [4:0] TRACK0_DTHETA_lane3_data3,
    
    output reg [4:0] TRACK0_DTHETA_lane4_data0,
    output reg [4:0] TRACK0_DTHETA_lane4_data1,
    output reg [4:0] TRACK0_DTHETA_lane4_data2,
    output reg [4:0] TRACK0_DTHETA_lane4_data3,
    
    output reg [4:0] TRACK0_DTHETA_lane5_data0,
    output reg [4:0] TRACK0_DTHETA_lane5_data1,
    output reg [4:0] TRACK0_DTHETA_lane5_data2,
    output reg [4:0] TRACK0_DTHETA_lane5_data3,
    
    output reg [4:0] TRACK0_DTHETA_lane6_data0,
    output reg [4:0] TRACK0_DTHETA_lane6_data1,
    output reg [4:0] TRACK0_DTHETA_lane6_data2,
    output reg [4:0] TRACK0_DTHETA_lane6_data3,
    
    output reg [5:0] TRACK0_PHI_lane0_data0,
    output reg [5:0] TRACK0_PHI_lane0_data1,
    output reg [5:0] TRACK0_PHI_lane0_data2,
    output reg [5:0] TRACK0_PHI_lane0_data3,
    
    output reg [5:0] TRACK0_PHI_lane1_data0,
    output reg [5:0] TRACK0_PHI_lane1_data1,
    output reg [5:0] TRACK0_PHI_lane1_data2,
    output reg [5:0] TRACK0_PHI_lane1_data3,
    
    output reg [5:0] TRACK0_PHI_lane2_data0,
    output reg [5:0] TRACK0_PHI_lane2_data1,
    output reg [5:0] TRACK0_PHI_lane2_data2,
    output reg [5:0] TRACK0_PHI_lane2_data3,
    
    output reg [5:0] TRACK0_PHI_lane3_data0,
    output reg [5:0] TRACK0_PHI_lane3_data1,
    output reg [5:0] TRACK0_PHI_lane3_data2,
    output reg [5:0] TRACK0_PHI_lane3_data3,
    
    output reg [5:0] TRACK0_PHI_lane4_data0,
    output reg [5:0] TRACK0_PHI_lane4_data1,
    output reg [5:0] TRACK0_PHI_lane4_data2,
    output reg [5:0] TRACK0_PHI_lane4_data3,
    
    output reg [5:0] TRACK0_PHI_lane5_data0,
    output reg [5:0] TRACK0_PHI_lane5_data1,
    output reg [5:0] TRACK0_PHI_lane5_data2,
    output reg [5:0] TRACK0_PHI_lane5_data3,
    
    output reg [5:0] TRACK0_PHI_lane6_data0,
    output reg [5:0] TRACK0_PHI_lane6_data1,
    output reg [5:0] TRACK0_PHI_lane6_data2,
    output reg [5:0] TRACK0_PHI_lane6_data3,
    
    output reg [7:0] TRACK0_ETA_lane0_data0,
    output reg [7:0] TRACK0_ETA_lane0_data1,
    output reg [7:0] TRACK0_ETA_lane0_data2,
    output reg [7:0] TRACK0_ETA_lane0_data3,
    
    output reg [7:0] TRACK0_ETA_lane1_data0,
    output reg [7:0] TRACK0_ETA_lane1_data1,
    output reg [7:0] TRACK0_ETA_lane1_data2,
    output reg [7:0] TRACK0_ETA_lane1_data3,
    
    output reg [7:0] TRACK0_ETA_lane2_data0,
    output reg [7:0] TRACK0_ETA_lane2_data1,
    output reg [7:0] TRACK0_ETA_lane2_data2,
    output reg [7:0] TRACK0_ETA_lane2_data3,
    
    output reg [7:0] TRACK0_ETA_lane3_data0,
    output reg [7:0] TRACK0_ETA_lane3_data1,
    output reg [7:0] TRACK0_ETA_lane3_data2,
    output reg [7:0] TRACK0_ETA_lane3_data3,
    
    output reg [7:0] TRACK0_ETA_lane4_data0,
    output reg [7:0] TRACK0_ETA_lane4_data1,
    output reg [7:0] TRACK0_ETA_lane4_data2,
    output reg [7:0] TRACK0_ETA_lane4_data3,
    
    output reg [7:0] TRACK0_ETA_lane5_data0,
    output reg [7:0] TRACK0_ETA_lane5_data1,
    output reg [7:0] TRACK0_ETA_lane5_data2,
    output reg [7:0] TRACK0_ETA_lane5_data3,
    
    output reg [7:0] TRACK0_ETA_lane6_data0,
    output reg [7:0] TRACK0_ETA_lane6_data1,
    output reg [7:0] TRACK0_ETA_lane6_data2,
    output reg [7:0] TRACK0_ETA_lane6_data3,
    
    output reg TRACK1_SPARE_lane0_data0,
    output reg TRACK1_SPARE_lane0_data1,
    output reg TRACK1_SPARE_lane0_data2,
    output reg TRACK1_SPARE_lane0_data3,
    
    output reg TRACK1_SPARE_lane1_data0,
    output reg TRACK1_SPARE_lane1_data1,
    output reg TRACK1_SPARE_lane1_data2,
    output reg TRACK1_SPARE_lane1_data3,
    
    output reg TRACK1_SPARE_lane2_data0,
    output reg TRACK1_SPARE_lane2_data1,
    output reg TRACK1_SPARE_lane2_data2,
    output reg TRACK1_SPARE_lane2_data3,
    
    output reg TRACK1_SPARE_lane3_data0,
    output reg TRACK1_SPARE_lane3_data1,
    output reg TRACK1_SPARE_lane3_data2,
    output reg TRACK1_SPARE_lane3_data3,
    
    output reg TRACK1_SPARE_lane4_data0,
    output reg TRACK1_SPARE_lane4_data1,
    output reg TRACK1_SPARE_lane4_data2,
    output reg TRACK1_SPARE_lane4_data3,
    
    output reg TRACK1_SPARE_lane5_data0,
    output reg TRACK1_SPARE_lane5_data1,
    output reg TRACK1_SPARE_lane5_data2,
    output reg TRACK1_SPARE_lane5_data3,
    
    output reg TRACK1_SPARE_lane6_data0,
    output reg TRACK1_SPARE_lane6_data1,
    output reg TRACK1_SPARE_lane6_data2,
    output reg TRACK1_SPARE_lane6_data3,
    
    output reg [1:0] TRACK1_sTGC_lane0_data0,
    output reg [1:0] TRACK1_sTGC_lane0_data1,
    output reg [1:0] TRACK1_sTGC_lane0_data2,
    output reg [1:0] TRACK1_sTGC_lane0_data3,
    
    output reg [1:0] TRACK1_sTGC_lane1_data0,
    output reg [1:0] TRACK1_sTGC_lane1_data1,
    output reg [1:0] TRACK1_sTGC_lane1_data2,
    output reg [1:0] TRACK1_sTGC_lane1_data3,
    
    output reg [1:0] TRACK1_sTGC_lane2_data0,
    output reg [1:0] TRACK1_sTGC_lane2_data1,
    output reg [1:0] TRACK1_sTGC_lane2_data2,
    output reg [1:0] TRACK1_sTGC_lane2_data3,
    
    output reg [1:0] TRACK1_sTGC_lane3_data0,
    output reg [1:0] TRACK1_sTGC_lane3_data1,
    output reg [1:0] TRACK1_sTGC_lane3_data2,
    output reg [1:0] TRACK1_sTGC_lane3_data3,
    
    output reg [1:0] TRACK1_sTGC_lane4_data0,
    output reg [1:0] TRACK1_sTGC_lane4_data1,
    output reg [1:0] TRACK1_sTGC_lane4_data2,
    output reg [1:0] TRACK1_sTGC_lane4_data3,
    
    output reg [1:0] TRACK1_sTGC_lane5_data0,
    output reg [1:0] TRACK1_sTGC_lane5_data1,
    output reg [1:0] TRACK1_sTGC_lane5_data2,
    output reg [1:0] TRACK1_sTGC_lane5_data3,
    
    output reg [1:0] TRACK1_sTGC_lane6_data0,
    output reg [1:0] TRACK1_sTGC_lane6_data1,
    output reg [1:0] TRACK1_sTGC_lane6_data2,
    output reg [1:0] TRACK1_sTGC_lane6_data3,
    
    output reg [1:0] TRACK1_MM_lane0_data0,
    output reg [1:0] TRACK1_MM_lane0_data1,
    output reg [1:0] TRACK1_MM_lane0_data2,
    output reg [1:0] TRACK1_MM_lane0_data3,
    
    output reg [1:0] TRACK1_MM_lane1_data0,
    output reg [1:0] TRACK1_MM_lane1_data1,
    output reg [1:0] TRACK1_MM_lane1_data2,
    output reg [1:0] TRACK1_MM_lane1_data3,
    
    output reg [1:0] TRACK1_MM_lane2_data0,
    output reg [1:0] TRACK1_MM_lane2_data1,
    output reg [1:0] TRACK1_MM_lane2_data2,
    output reg [1:0] TRACK1_MM_lane2_data3,
    
    output reg [1:0] TRACK1_MM_lane3_data0,
    output reg [1:0] TRACK1_MM_lane3_data1,
    output reg [1:0] TRACK1_MM_lane3_data2,
    output reg [1:0] TRACK1_MM_lane3_data3,
    
    output reg [1:0] TRACK1_MM_lane4_data0,
    output reg [1:0] TRACK1_MM_lane4_data1,
    output reg [1:0] TRACK1_MM_lane4_data2,
    output reg [1:0] TRACK1_MM_lane4_data3,
    
    output reg [1:0] TRACK1_MM_lane5_data0,
    output reg [1:0] TRACK1_MM_lane5_data1,
    output reg [1:0] TRACK1_MM_lane5_data2,
    output reg [1:0] TRACK1_MM_lane5_data3,
    
    output reg [1:0] TRACK1_MM_lane6_data0,
    output reg [1:0] TRACK1_MM_lane6_data1,
    output reg [1:0] TRACK1_MM_lane6_data2,
    output reg [1:0] TRACK1_MM_lane6_data3,
    
    output reg [4:0] TRACK1_DTHETA_lane0_data0,
    output reg [4:0] TRACK1_DTHETA_lane0_data1,
    output reg [4:0] TRACK1_DTHETA_lane0_data2,
    output reg [4:0] TRACK1_DTHETA_lane0_data3,
    
    output reg [4:0] TRACK1_DTHETA_lane1_data0,
    output reg [4:0] TRACK1_DTHETA_lane1_data1,
    output reg [4:0] TRACK1_DTHETA_lane1_data2,
    output reg [4:0] TRACK1_DTHETA_lane1_data3,
    
    output reg [4:0] TRACK1_DTHETA_lane2_data0,
    output reg [4:0] TRACK1_DTHETA_lane2_data1,
    output reg [4:0] TRACK1_DTHETA_lane2_data2,
    output reg [4:0] TRACK1_DTHETA_lane2_data3,
    
    output reg [4:0] TRACK1_DTHETA_lane3_data0,
    output reg [4:0] TRACK1_DTHETA_lane3_data1,
    output reg [4:0] TRACK1_DTHETA_lane3_data2,
    output reg [4:0] TRACK1_DTHETA_lane3_data3,
    
    output reg [4:0] TRACK1_DTHETA_lane4_data0,
    output reg [4:0] TRACK1_DTHETA_lane4_data1,
    output reg [4:0] TRACK1_DTHETA_lane4_data2,
    output reg [4:0] TRACK1_DTHETA_lane4_data3,
    
    output reg [4:0] TRACK1_DTHETA_lane5_data0,
    output reg [4:0] TRACK1_DTHETA_lane5_data1,
    output reg [4:0] TRACK1_DTHETA_lane5_data2,
    output reg [4:0] TRACK1_DTHETA_lane5_data3,
    
    output reg [4:0] TRACK1_DTHETA_lane6_data0,
    output reg [4:0] TRACK1_DTHETA_lane6_data1,
    output reg [4:0] TRACK1_DTHETA_lane6_data2,
    output reg [4:0] TRACK1_DTHETA_lane6_data3,
    
    output reg [5:0] TRACK1_PHI_lane0_data0,
    output reg [5:0] TRACK1_PHI_lane0_data1,
    output reg [5:0] TRACK1_PHI_lane0_data2,
    output reg [5:0] TRACK1_PHI_lane0_data3,
    
    output reg [5:0] TRACK1_PHI_lane1_data0,
    output reg [5:0] TRACK1_PHI_lane1_data1,
    output reg [5:0] TRACK1_PHI_lane1_data2,
    output reg [5:0] TRACK1_PHI_lane1_data3,
    
    output reg [5:0] TRACK1_PHI_lane2_data0,
    output reg [5:0] TRACK1_PHI_lane2_data1,
    output reg [5:0] TRACK1_PHI_lane2_data2,
    output reg [5:0] TRACK1_PHI_lane2_data3,
    
    output reg [5:0] TRACK1_PHI_lane3_data0,
    output reg [5:0] TRACK1_PHI_lane3_data1,
    output reg [5:0] TRACK1_PHI_lane3_data2,
    output reg [5:0] TRACK1_PHI_lane3_data3,
    
    output reg [5:0] TRACK1_PHI_lane4_data0,
    output reg [5:0] TRACK1_PHI_lane4_data1,
    output reg [5:0] TRACK1_PHI_lane4_data2,
    output reg [5:0] TRACK1_PHI_lane4_data3,
    
    output reg [5:0] TRACK1_PHI_lane5_data0,
    output reg [5:0] TRACK1_PHI_lane5_data1,
    output reg [5:0] TRACK1_PHI_lane5_data2,
    output reg [5:0] TRACK1_PHI_lane5_data3,
    
    output reg [5:0] TRACK1_PHI_lane6_data0,
    output reg [5:0] TRACK1_PHI_lane6_data1,
    output reg [5:0] TRACK1_PHI_lane6_data2,
    output reg [5:0] TRACK1_PHI_lane6_data3,
    
    output reg [7:0] TRACK1_ETA_lane0_data0,
    output reg [7:0] TRACK1_ETA_lane0_data1,
    output reg [7:0] TRACK1_ETA_lane0_data2,
    output reg [7:0] TRACK1_ETA_lane0_data3,
    
    output reg [7:0] TRACK1_ETA_lane1_data0,
    output reg [7:0] TRACK1_ETA_lane1_data1,
    output reg [7:0] TRACK1_ETA_lane1_data2,
    output reg [7:0] TRACK1_ETA_lane1_data3,
    
    output reg [7:0] TRACK1_ETA_lane2_data0,
    output reg [7:0] TRACK1_ETA_lane2_data1,
    output reg [7:0] TRACK1_ETA_lane2_data2,
    output reg [7:0] TRACK1_ETA_lane2_data3,
    
    output reg [7:0] TRACK1_ETA_lane3_data0,
    output reg [7:0] TRACK1_ETA_lane3_data1,
    output reg [7:0] TRACK1_ETA_lane3_data2,
    output reg [7:0] TRACK1_ETA_lane3_data3,
    
    output reg [7:0] TRACK1_ETA_lane4_data0,
    output reg [7:0] TRACK1_ETA_lane4_data1,
    output reg [7:0] TRACK1_ETA_lane4_data2,
    output reg [7:0] TRACK1_ETA_lane4_data3,
    
    output reg [7:0] TRACK1_ETA_lane5_data0,
    output reg [7:0] TRACK1_ETA_lane5_data1,
    output reg [7:0] TRACK1_ETA_lane5_data2,
    output reg [7:0] TRACK1_ETA_lane5_data3,
    
    output reg [7:0] TRACK1_ETA_lane6_data0,
    output reg [7:0] TRACK1_ETA_lane6_data1,
    output reg [7:0] TRACK1_ETA_lane6_data2,
    output reg [7:0] TRACK1_ETA_lane6_data3,
    
    output reg TRACK2_SPARE_lane0_data0,
    output reg TRACK2_SPARE_lane0_data1,
    output reg TRACK2_SPARE_lane0_data2,
    output reg TRACK2_SPARE_lane0_data3,
    
    output reg TRACK2_SPARE_lane1_data0,
    output reg TRACK2_SPARE_lane1_data1,
    output reg TRACK2_SPARE_lane1_data2,
    output reg TRACK2_SPARE_lane1_data3,
    
    output reg TRACK2_SPARE_lane2_data0,
    output reg TRACK2_SPARE_lane2_data1,
    output reg TRACK2_SPARE_lane2_data2,
    output reg TRACK2_SPARE_lane2_data3,
    
    output reg TRACK2_SPARE_lane3_data0,
    output reg TRACK2_SPARE_lane3_data1,
    output reg TRACK2_SPARE_lane3_data2,
    output reg TRACK2_SPARE_lane3_data3,
    
    output reg TRACK2_SPARE_lane4_data0,
    output reg TRACK2_SPARE_lane4_data1,
    output reg TRACK2_SPARE_lane4_data2,
    output reg TRACK2_SPARE_lane4_data3,
    
    output reg TRACK2_SPARE_lane5_data0,
    output reg TRACK2_SPARE_lane5_data1,
    output reg TRACK2_SPARE_lane5_data2,
    output reg TRACK2_SPARE_lane5_data3,
    
    output reg TRACK2_SPARE_lane6_data0,
    output reg TRACK2_SPARE_lane6_data1,
    output reg TRACK2_SPARE_lane6_data2,
    output reg TRACK2_SPARE_lane6_data3,
    
    output reg [1:0] TRACK2_sTGC_lane0_data0,
    output reg [1:0] TRACK2_sTGC_lane0_data1,
    output reg [1:0] TRACK2_sTGC_lane0_data2,
    output reg [1:0] TRACK2_sTGC_lane0_data3,
    
    output reg [1:0] TRACK2_sTGC_lane1_data0,
    output reg [1:0] TRACK2_sTGC_lane1_data1,
    output reg [1:0] TRACK2_sTGC_lane1_data2,
    output reg [1:0] TRACK2_sTGC_lane1_data3,
    
    output reg [1:0] TRACK2_sTGC_lane2_data0,
    output reg [1:0] TRACK2_sTGC_lane2_data1,
    output reg [1:0] TRACK2_sTGC_lane2_data2,
    output reg [1:0] TRACK2_sTGC_lane2_data3,
    
    output reg [1:0] TRACK2_sTGC_lane3_data0,
    output reg [1:0] TRACK2_sTGC_lane3_data1,
    output reg [1:0] TRACK2_sTGC_lane3_data2,
    output reg [1:0] TRACK2_sTGC_lane3_data3,
    
    output reg [1:0] TRACK2_sTGC_lane4_data0,
    output reg [1:0] TRACK2_sTGC_lane4_data1,
    output reg [1:0] TRACK2_sTGC_lane4_data2,
    output reg [1:0] TRACK2_sTGC_lane4_data3,
    
    output reg [1:0] TRACK2_sTGC_lane5_data0,
    output reg [1:0] TRACK2_sTGC_lane5_data1,
    output reg [1:0] TRACK2_sTGC_lane5_data2,
    output reg [1:0] TRACK2_sTGC_lane5_data3,
    
    output reg [1:0] TRACK2_sTGC_lane6_data0,
    output reg [1:0] TRACK2_sTGC_lane6_data1,
    output reg [1:0] TRACK2_sTGC_lane6_data2,
    output reg [1:0] TRACK2_sTGC_lane6_data3,
    
    output reg [1:0] TRACK2_MM_lane0_data0,
    output reg [1:0] TRACK2_MM_lane0_data1,
    output reg [1:0] TRACK2_MM_lane0_data2,
    output reg [1:0] TRACK2_MM_lane0_data3,
    
    output reg [1:0] TRACK2_MM_lane1_data0,
    output reg [1:0] TRACK2_MM_lane1_data1,
    output reg [1:0] TRACK2_MM_lane1_data2,
    output reg [1:0] TRACK2_MM_lane1_data3,
    
    output reg [1:0] TRACK2_MM_lane2_data0,
    output reg [1:0] TRACK2_MM_lane2_data1,
    output reg [1:0] TRACK2_MM_lane2_data2,
    output reg [1:0] TRACK2_MM_lane2_data3,
    
    output reg [1:0] TRACK2_MM_lane3_data0,
    output reg [1:0] TRACK2_MM_lane3_data1,
    output reg [1:0] TRACK2_MM_lane3_data2,
    output reg [1:0] TRACK2_MM_lane3_data3,
    
    output reg [1:0] TRACK2_MM_lane4_data0,
    output reg [1:0] TRACK2_MM_lane4_data1,
    output reg [1:0] TRACK2_MM_lane4_data2,
    output reg [1:0] TRACK2_MM_lane4_data3,
    
    output reg [1:0] TRACK2_MM_lane5_data0,
    output reg [1:0] TRACK2_MM_lane5_data1,
    output reg [1:0] TRACK2_MM_lane5_data2,
    output reg [1:0] TRACK2_MM_lane5_data3,
    
    output reg [1:0] TRACK2_MM_lane6_data0,
    output reg [1:0] TRACK2_MM_lane6_data1,
    output reg [1:0] TRACK2_MM_lane6_data2,
    output reg [1:0] TRACK2_MM_lane6_data3,
    
    output reg [4:0] TRACK2_DTHETA_lane0_data0,
    output reg [4:0] TRACK2_DTHETA_lane0_data1,
    output reg [4:0] TRACK2_DTHETA_lane0_data2,
    output reg [4:0] TRACK2_DTHETA_lane0_data3,
    
    output reg [4:0] TRACK2_DTHETA_lane1_data0,
    output reg [4:0] TRACK2_DTHETA_lane1_data1,
    output reg [4:0] TRACK2_DTHETA_lane1_data2,
    output reg [4:0] TRACK2_DTHETA_lane1_data3,
    
    output reg [4:0] TRACK2_DTHETA_lane2_data0,
    output reg [4:0] TRACK2_DTHETA_lane2_data1,
    output reg [4:0] TRACK2_DTHETA_lane2_data2,
    output reg [4:0] TRACK2_DTHETA_lane2_data3,
    
    output reg [4:0] TRACK2_DTHETA_lane3_data0,
    output reg [4:0] TRACK2_DTHETA_lane3_data1,
    output reg [4:0] TRACK2_DTHETA_lane3_data2,
    output reg [4:0] TRACK2_DTHETA_lane3_data3,
    
    output reg [4:0] TRACK2_DTHETA_lane4_data0,
    output reg [4:0] TRACK2_DTHETA_lane4_data1,
    output reg [4:0] TRACK2_DTHETA_lane4_data2,
    output reg [4:0] TRACK2_DTHETA_lane4_data3,
    
    output reg [4:0] TRACK2_DTHETA_lane5_data0,
    output reg [4:0] TRACK2_DTHETA_lane5_data1,
    output reg [4:0] TRACK2_DTHETA_lane5_data2,
    output reg [4:0] TRACK2_DTHETA_lane5_data3,
    
    output reg [4:0] TRACK2_DTHETA_lane6_data0,
    output reg [4:0] TRACK2_DTHETA_lane6_data1,
    output reg [4:0] TRACK2_DTHETA_lane6_data2,
    output reg [4:0] TRACK2_DTHETA_lane6_data3,
    
    output reg [5:0] TRACK2_PHI_lane0_data0,
    output reg [5:0] TRACK2_PHI_lane0_data1,
    output reg [5:0] TRACK2_PHI_lane0_data2,
    output reg [5:0] TRACK2_PHI_lane0_data3,
    
    output reg [5:0] TRACK2_PHI_lane1_data0,
    output reg [5:0] TRACK2_PHI_lane1_data1,
    output reg [5:0] TRACK2_PHI_lane1_data2,
    output reg [5:0] TRACK2_PHI_lane1_data3,
    
    output reg [5:0] TRACK2_PHI_lane2_data0,
    output reg [5:0] TRACK2_PHI_lane2_data1,
    output reg [5:0] TRACK2_PHI_lane2_data2,
    output reg [5:0] TRACK2_PHI_lane2_data3,
    
    output reg [5:0] TRACK2_PHI_lane3_data0,
    output reg [5:0] TRACK2_PHI_lane3_data1,
    output reg [5:0] TRACK2_PHI_lane3_data2,
    output reg [5:0] TRACK2_PHI_lane3_data3,
    
    output reg [5:0] TRACK2_PHI_lane4_data0,
    output reg [5:0] TRACK2_PHI_lane4_data1,
    output reg [5:0] TRACK2_PHI_lane4_data2,
    output reg [5:0] TRACK2_PHI_lane4_data3,
    
    output reg [5:0] TRACK2_PHI_lane5_data0,
    output reg [5:0] TRACK2_PHI_lane5_data1,
    output reg [5:0] TRACK2_PHI_lane5_data2,
    output reg [5:0] TRACK2_PHI_lane5_data3,
    
    output reg [5:0] TRACK2_PHI_lane6_data0,
    output reg [5:0] TRACK2_PHI_lane6_data1,
    output reg [5:0] TRACK2_PHI_lane6_data2,
    output reg [5:0] TRACK2_PHI_lane6_data3,
    
    output reg [7:0] TRACK2_ETA_lane0_data0,
    output reg [7:0] TRACK2_ETA_lane0_data1,
    output reg [7:0] TRACK2_ETA_lane0_data2,
    output reg [7:0] TRACK2_ETA_lane0_data3,
    
    output reg [7:0] TRACK2_ETA_lane1_data0,
    output reg [7:0] TRACK2_ETA_lane1_data1,
    output reg [7:0] TRACK2_ETA_lane1_data2,
    output reg [7:0] TRACK2_ETA_lane1_data3,
    
    output reg [7:0] TRACK2_ETA_lane2_data0,
    output reg [7:0] TRACK2_ETA_lane2_data1,
    output reg [7:0] TRACK2_ETA_lane2_data2,
    output reg [7:0] TRACK2_ETA_lane2_data3,
    
    output reg [7:0] TRACK2_ETA_lane3_data0,
    output reg [7:0] TRACK2_ETA_lane3_data1,
    output reg [7:0] TRACK2_ETA_lane3_data2,
    output reg [7:0] TRACK2_ETA_lane3_data3,
    
    output reg [7:0] TRACK2_ETA_lane4_data0,
    output reg [7:0] TRACK2_ETA_lane4_data1,
    output reg [7:0] TRACK2_ETA_lane4_data2,
    output reg [7:0] TRACK2_ETA_lane4_data3,
    
    output reg [7:0] TRACK2_ETA_lane5_data0,
    output reg [7:0] TRACK2_ETA_lane5_data1,
    output reg [7:0] TRACK2_ETA_lane5_data2,
    output reg [7:0] TRACK2_ETA_lane5_data3,
    
    output reg [7:0] TRACK2_ETA_lane6_data0,
    output reg [7:0] TRACK2_ETA_lane6_data1,
    output reg [7:0] TRACK2_ETA_lane6_data2,
    output reg [7:0] TRACK2_ETA_lane6_data3,
    
    output reg TRACK3_SPARE_lane0_data0,
    output reg TRACK3_SPARE_lane0_data1,
    output reg TRACK3_SPARE_lane0_data2,
    output reg TRACK3_SPARE_lane0_data3,
    
    output reg TRACK3_SPARE_lane1_data0,
    output reg TRACK3_SPARE_lane1_data1,
    output reg TRACK3_SPARE_lane1_data2,
    output reg TRACK3_SPARE_lane1_data3,
    
    output reg TRACK3_SPARE_lane2_data0,
    output reg TRACK3_SPARE_lane2_data1,
    output reg TRACK3_SPARE_lane2_data2,
    output reg TRACK3_SPARE_lane2_data3,
    
    output reg TRACK3_SPARE_lane3_data0,
    output reg TRACK3_SPARE_lane3_data1,
    output reg TRACK3_SPARE_lane3_data2,
    output reg TRACK3_SPARE_lane3_data3,
    
    output reg TRACK3_SPARE_lane4_data0,
    output reg TRACK3_SPARE_lane4_data1,
    output reg TRACK3_SPARE_lane4_data2,
    output reg TRACK3_SPARE_lane4_data3,
    
    output reg TRACK3_SPARE_lane5_data0,
    output reg TRACK3_SPARE_lane5_data1,
    output reg TRACK3_SPARE_lane5_data2,
    output reg TRACK3_SPARE_lane5_data3,
    
    output reg TRACK3_SPARE_lane6_data0,
    output reg TRACK3_SPARE_lane6_data1,
    output reg TRACK3_SPARE_lane6_data2,
    output reg TRACK3_SPARE_lane6_data3,
    
    output reg [1:0] TRACK3_sTGC_lane0_data0,
    output reg [1:0] TRACK3_sTGC_lane0_data1,
    output reg [1:0] TRACK3_sTGC_lane0_data2,
    output reg [1:0] TRACK3_sTGC_lane0_data3,
    
    output reg [1:0] TRACK3_sTGC_lane1_data0,
    output reg [1:0] TRACK3_sTGC_lane1_data1,
    output reg [1:0] TRACK3_sTGC_lane1_data2,
    output reg [1:0] TRACK3_sTGC_lane1_data3,
    
    output reg [1:0] TRACK3_sTGC_lane2_data0,
    output reg [1:0] TRACK3_sTGC_lane2_data1,
    output reg [1:0] TRACK3_sTGC_lane2_data2,
    output reg [1:0] TRACK3_sTGC_lane2_data3,
    
    output reg [1:0] TRACK3_sTGC_lane3_data0,
    output reg [1:0] TRACK3_sTGC_lane3_data1,
    output reg [1:0] TRACK3_sTGC_lane3_data2,
    output reg [1:0] TRACK3_sTGC_lane3_data3,
    
    output reg [1:0] TRACK3_sTGC_lane4_data0,
    output reg [1:0] TRACK3_sTGC_lane4_data1,
    output reg [1:0] TRACK3_sTGC_lane4_data2,
    output reg [1:0] TRACK3_sTGC_lane4_data3,
    
    output reg [1:0] TRACK3_sTGC_lane5_data0,
    output reg [1:0] TRACK3_sTGC_lane5_data1,
    output reg [1:0] TRACK3_sTGC_lane5_data2,
    output reg [1:0] TRACK3_sTGC_lane5_data3,
    
    output reg [1:0] TRACK3_sTGC_lane6_data0,
    output reg [1:0] TRACK3_sTGC_lane6_data1,
    output reg [1:0] TRACK3_sTGC_lane6_data2,
    output reg [1:0] TRACK3_sTGC_lane6_data3,
    
    output reg [1:0] TRACK3_MM_lane0_data0,
    output reg [1:0] TRACK3_MM_lane0_data1,
    output reg [1:0] TRACK3_MM_lane0_data2,
    output reg [1:0] TRACK3_MM_lane0_data3,
    
    output reg [1:0] TRACK3_MM_lane1_data0,
    output reg [1:0] TRACK3_MM_lane1_data1,
    output reg [1:0] TRACK3_MM_lane1_data2,
    output reg [1:0] TRACK3_MM_lane1_data3,
    
    output reg [1:0] TRACK3_MM_lane2_data0,
    output reg [1:0] TRACK3_MM_lane2_data1,
    output reg [1:0] TRACK3_MM_lane2_data2,
    output reg [1:0] TRACK3_MM_lane2_data3,
    
    output reg [1:0] TRACK3_MM_lane3_data0,
    output reg [1:0] TRACK3_MM_lane3_data1,
    output reg [1:0] TRACK3_MM_lane3_data2,
    output reg [1:0] TRACK3_MM_lane3_data3,
    
    output reg [1:0] TRACK3_MM_lane4_data0,
    output reg [1:0] TRACK3_MM_lane4_data1,
    output reg [1:0] TRACK3_MM_lane4_data2,
    output reg [1:0] TRACK3_MM_lane4_data3,
    
    output reg [1:0] TRACK3_MM_lane5_data0,
    output reg [1:0] TRACK3_MM_lane5_data1,
    output reg [1:0] TRACK3_MM_lane5_data2,
    output reg [1:0] TRACK3_MM_lane5_data3,
    
    output reg [1:0] TRACK3_MM_lane6_data0,
    output reg [1:0] TRACK3_MM_lane6_data1,
    output reg [1:0] TRACK3_MM_lane6_data2,
    output reg [1:0] TRACK3_MM_lane6_data3,
    
    output reg [4:0] TRACK3_DTHETA_lane0_data0,
    output reg [4:0] TRACK3_DTHETA_lane0_data1,
    output reg [4:0] TRACK3_DTHETA_lane0_data2,
    output reg [4:0] TRACK3_DTHETA_lane0_data3,
    
    output reg [4:0] TRACK3_DTHETA_lane1_data0,
    output reg [4:0] TRACK3_DTHETA_lane1_data1,
    output reg [4:0] TRACK3_DTHETA_lane1_data2,
    output reg [4:0] TRACK3_DTHETA_lane1_data3,
    
    output reg [4:0] TRACK3_DTHETA_lane2_data0,
    output reg [4:0] TRACK3_DTHETA_lane2_data1,
    output reg [4:0] TRACK3_DTHETA_lane2_data2,
    output reg [4:0] TRACK3_DTHETA_lane2_data3,
    
    output reg [4:0] TRACK3_DTHETA_lane3_data0,
    output reg [4:0] TRACK3_DTHETA_lane3_data1,
    output reg [4:0] TRACK3_DTHETA_lane3_data2,
    output reg [4:0] TRACK3_DTHETA_lane3_data3,
    
    output reg [4:0] TRACK3_DTHETA_lane4_data0,
    output reg [4:0] TRACK3_DTHETA_lane4_data1,
    output reg [4:0] TRACK3_DTHETA_lane4_data2,
    output reg [4:0] TRACK3_DTHETA_lane4_data3,
    
    output reg [4:0] TRACK3_DTHETA_lane5_data0,
    output reg [4:0] TRACK3_DTHETA_lane5_data1,
    output reg [4:0] TRACK3_DTHETA_lane5_data2,
    output reg [4:0] TRACK3_DTHETA_lane5_data3,
    
    output reg [4:0] TRACK3_DTHETA_lane6_data0,
    output reg [4:0] TRACK3_DTHETA_lane6_data1,
    output reg [4:0] TRACK3_DTHETA_lane6_data2,
    output reg [4:0] TRACK3_DTHETA_lane6_data3,
    
    output reg [5:0] TRACK3_PHI_lane0_data0,
    output reg [5:0] TRACK3_PHI_lane0_data1,
    output reg [5:0] TRACK3_PHI_lane0_data2,
    output reg [5:0] TRACK3_PHI_lane0_data3,
    
    output reg [5:0] TRACK3_PHI_lane1_data0,
    output reg [5:0] TRACK3_PHI_lane1_data1,
    output reg [5:0] TRACK3_PHI_lane1_data2,
    output reg [5:0] TRACK3_PHI_lane1_data3,
    
    output reg [5:0] TRACK3_PHI_lane2_data0,
    output reg [5:0] TRACK3_PHI_lane2_data1,
    output reg [5:0] TRACK3_PHI_lane2_data2,
    output reg [5:0] TRACK3_PHI_lane2_data3,
    
    output reg [5:0] TRACK3_PHI_lane3_data0,
    output reg [5:0] TRACK3_PHI_lane3_data1,
    output reg [5:0] TRACK3_PHI_lane3_data2,
    output reg [5:0] TRACK3_PHI_lane3_data3,
    
    output reg [5:0] TRACK3_PHI_lane4_data0,
    output reg [5:0] TRACK3_PHI_lane4_data1,
    output reg [5:0] TRACK3_PHI_lane4_data2,
    output reg [5:0] TRACK3_PHI_lane4_data3,
    
    output reg [5:0] TRACK3_PHI_lane5_data0,
    output reg [5:0] TRACK3_PHI_lane5_data1,
    output reg [5:0] TRACK3_PHI_lane5_data2,
    output reg [5:0] TRACK3_PHI_lane5_data3,
    
    output reg [5:0] TRACK3_PHI_lane6_data0,
    output reg [5:0] TRACK3_PHI_lane6_data1,
    output reg [5:0] TRACK3_PHI_lane6_data2,
    output reg [5:0] TRACK3_PHI_lane6_data3,
    
    output reg [7:0] TRACK3_ETA_lane0_data0,
    output reg [7:0] TRACK3_ETA_lane0_data1,
    output reg [7:0] TRACK3_ETA_lane0_data2,
    output reg [7:0] TRACK3_ETA_lane0_data3,
    
    output reg [7:0] TRACK3_ETA_lane1_data0,
    output reg [7:0] TRACK3_ETA_lane1_data1,
    output reg [7:0] TRACK3_ETA_lane1_data2,
    output reg [7:0] TRACK3_ETA_lane1_data3,
    
    output reg [7:0] TRACK3_ETA_lane2_data0,
    output reg [7:0] TRACK3_ETA_lane2_data1,
    output reg [7:0] TRACK3_ETA_lane2_data2,
    output reg [7:0] TRACK3_ETA_lane2_data3,
    
    output reg [7:0] TRACK3_ETA_lane3_data0,
    output reg [7:0] TRACK3_ETA_lane3_data1,
    output reg [7:0] TRACK3_ETA_lane3_data2,
    output reg [7:0] TRACK3_ETA_lane3_data3,
    
    output reg [7:0] TRACK3_ETA_lane4_data0,
    output reg [7:0] TRACK3_ETA_lane4_data1,
    output reg [7:0] TRACK3_ETA_lane4_data2,
    output reg [7:0] TRACK3_ETA_lane4_data3,
    
    output reg [7:0] TRACK3_ETA_lane5_data0,
    output reg [7:0] TRACK3_ETA_lane5_data1,
    output reg [7:0] TRACK3_ETA_lane5_data2,
    output reg [7:0] TRACK3_ETA_lane5_data3,
    
    output reg [7:0] TRACK3_ETA_lane6_data0,
    output reg [7:0] TRACK3_ETA_lane6_data1,
    output reg [7:0] TRACK3_ETA_lane6_data2,
    output reg [7:0] TRACK3_ETA_lane6_data3,

    input wire [15:0] tmp0,
    input wire [15:0] tmp1,
    input wire [15:0] tmp2,
    input wire [15:0] tmp3,
    input wire [15:0] tmp4,
    input wire [15:0] tmp5,
    input wire [15:0] tmp6
    );
    
    
      
    //VME protocol
    wire rs,ws;
    assign rs = !(!OE & !CE);
    assign ws = !(!WE & !CE);
    wire [15:0] VME_DIN;     //VME data input
    (* ASYNC_REG = "TRUE" *) reg [15:0] VME_DOUT;    //VME data output
    (* ASYNC_REG = "TRUE" *) reg [11:0] VME_Address;
    (* ASYNC_REG = "TRUE" *) reg [15:0] VME_Data;
    assign VME_DIN = VME_D;
    assign VME_D = ((!OE & !CE)) ? VME_DOUT : 16'hzzzz;

    reg [15:0] Test_reg;    
    reg [15:0] bitfile_version;
   
initial begin
    Test_reg <= 16'h135;
    bitfile_version <= 16'h110;

    Reset_TTC_out <= 1'b0;
    Reset_TX_out <= 1'b0;
    Reset_160_out <= 1'b0;
    Scaler_reset_out <= 1'b0;
    GTX_TX_reset_out <= 1'b0;
    GTX_RX_reset_out <= 1'b0;
    Delay_reset_out <= 1'b0;
    L1Buffer_reset_out <= 1'b0;
    Derandomizer_reset_out <= 1'b0;
    ZeroSupp_reset_out <= 1'b0;
    CLK_reset_out <= 1'b0;
    FIFO_reset_out <= 1'b0;
    SiTCP_reset_out <= 1'b0;
    TX_Logic_reset_out <= 1'b0;
    Monitoring_reset_out <= 1'b0;
    LUT_init_reset_out <= 1'b0;
    
    gt0_rx_reset_out <= 1'b0;
    gt1_rx_reset_out <= 1'b0;
    gt2_rx_reset_out <= 1'b0;
    gt3_rx_reset_out <= 1'b0;
    gt4_rx_reset_out <= 1'b0;
    gt5_rx_reset_out <= 1'b0;
    gt6_rx_reset_out <= 1'b0;
    gt7_rx_reset_out <= 1'b0;
    gt8_rx_reset_out <= 1'b0;
    gt9_rx_reset_out <= 1'b0;
    gt10_rx_reset_out <= 1'b0;
    gt11_rx_reset_out <= 1'b0;
    
    Delay_gtx0 <= 8'h4;
    Delay_gtx1 <= 8'h4;
    Delay_gtx2 <= 8'h4;
    Delay_gtx3 <= 8'h4;
    Delay_gtx4 <= 8'h4;
    Delay_gtx5 <= 8'h4;
    Delay_gtx6 <= 8'h4;
    Delay_gtx7 <= 8'h4;
    Delay_gtx8 <= 8'h4;
    Delay_gtx9 <= 8'h4;
    Delay_gtx10 <= 8'h4;
    Delay_gtx11 <= 8'h4;
    
    Delay_glink0 <= 8'h4;
    Delay_glink1 <= 8'h4;
    Delay_glink2 <= 8'h4;
    Delay_glink3 <= 8'h4;
    Delay_glink4 <= 8'h4;
    Delay_glink5 <= 8'h4;
    Delay_glink6 <= 8'h4;
    Delay_glink7 <= 8'h4;
    Delay_glink8 <= 8'h4;
    Delay_glink9 <= 8'h4;
    Delay_glink10 <= 8'h4;
    Delay_glink11 <= 8'h4;
    Delay_glink12 <= 8'h4;
    Delay_glink13 <= 8'h4;

    Delay_L1A <= 8'h2;
    Delay_BCR <= 8'h2;
    Delay_trig_BCR <= 8'h2;
    Delay_ECR <= 8'h2;
    Delay_TTC_RESET <= 8'h2;
    Delay_TEST_PULSE <= 8'h2;
    
    Mask_gtx0 <= 2'b0;
    Mask_gtx1 <= 2'b0;
    Mask_gtx2 <= 2'b0;
    Mask_gtx3 <= 2'b0;
    Mask_gtx4 <= 2'b0;
    Mask_gtx5 <= 2'b0;
    Mask_gtx6 <= 2'b0;
    Mask_gtx7 <= 2'b0;
    Mask_gtx8 <= 2'b0;
    Mask_gtx9 <= 2'b0;
    Mask_gtx10 <= 2'b0;
    Mask_gtx11 <= 2'b0;
    
    Mask_L1A <= 1'b0;
    Mask_BCR <= 1'b0;
    Mask_trig_BCR <= 1'b0;
    Mask_ECR <= 1'b0;
    Mask_TTC_RESET <= 1'b0;
    Mask_TEST_PULSE <= 1'b0;


    Mask_glink0 <= 2'b0;
    Mask_glink1 <= 2'b0;
    Mask_glink2 <= 2'b0;
    Mask_glink3 <= 2'b0;
    Mask_glink4 <= 2'b0;
    Mask_glink5 <= 2'b0;
    Mask_glink6 <= 2'b0;
    Mask_glink7 <= 2'b0;
    Mask_glink8 <= 2'b0;
    Mask_glink9 <= 2'b0;
    Mask_glink10 <= 2'b0;
    Mask_glink11 <= 2'b0;
    Mask_glink12 <= 2'b0;
    Mask_glink13 <= 2'b0;

    Test_Pulse_Length <= 16'h7f;
    Test_Pulse_Wait_Length <= 16'h7f;
    Test_Pulse_Enable <= 1'b0;
    
    SLID <= 12'h777;
    Readout_BC <= 16'd64;
    L1Buffer_depth <= 7'd100; 
    trigL1Buffer_depth <= 7'd96; 
    L1Buffer_BW_depth <= 7'd102; 

    monitoring_FIFO_wr_en <= 1'b0;
    monitoring_FIFO_rd_en <= 1'b0;
    lane_selector_out <= 7'h0;
    xadc_mon_enable_out <= 1'b0;
    xadc_mon_addr_out <= 7'h0;

    FullBoard_ID <= 16'b0;

    L1A_manual_parameter <= 16'h6ff1;
    Busy_propagation <= 1'b0;    
        
    gtx0_test_data1 <= 16'h0;
    gtx0_test_data2 <= 16'h0;
    gtx0_test_data3 <= 16'h0;
    gtx0_test_data4 <= 16'h0;
    gtx0_test_data5 <= 16'h0;
    gtx0_test_data6 <= 16'h0;
    gtx0_test_data7 <= 16'h0;
    gtx1_test_data1 <= 16'h0;
    gtx1_test_data2 <= 16'h0;
    gtx1_test_data3 <= 16'h0;
    gtx1_test_data4 <= 16'h0;
    gtx1_test_data5 <= 16'h0;
    gtx1_test_data6 <= 16'h0;
    gtx1_test_data7 <= 16'h0;
    gtx2_test_data1 <= 16'h0;
    gtx2_test_data2 <= 16'h0;
    gtx2_test_data3 <= 16'h0;
    gtx2_test_data4 <= 16'h0;
    gtx2_test_data5 <= 16'h0;
    gtx2_test_data6 <= 16'h0;
    gtx2_test_data7 <= 16'h0;
    gtx3_test_data1 <= 16'h0;
    gtx3_test_data2 <= 16'h0;
    gtx3_test_data3 <= 16'h0;
    gtx3_test_data4 <= 16'h0;
    gtx3_test_data5 <= 16'h0;
    gtx3_test_data6 <= 16'h0;
    gtx3_test_data7 <= 16'h0;
    gtx4_test_data1 <= 16'h0;
    gtx4_test_data2 <= 16'h0;
    gtx4_test_data3 <= 16'h0;
    gtx4_test_data4 <= 16'h0;
    gtx4_test_data5 <= 16'h0;
    gtx4_test_data6 <= 16'h0;
    gtx4_test_data7 <= 16'h0;
    gtx5_test_data1 <= 16'h0;
    gtx5_test_data2 <= 16'h0;
    gtx5_test_data3 <= 16'h0;
    gtx5_test_data4 <= 16'h0;
    gtx5_test_data5 <= 16'h0;
    gtx5_test_data6 <= 16'h0;
    gtx5_test_data7 <= 16'h0;
    gtx6_test_data1 <= 16'h0;
    gtx6_test_data2 <= 16'h0;
    gtx6_test_data3 <= 16'h0;
    gtx6_test_data4 <= 16'h0;
    gtx6_test_data5 <= 16'h0;
    gtx6_test_data6 <= 16'h0;
    gtx6_test_data7 <= 16'h0;
    gtx7_test_data1 <= 16'h0;
    gtx7_test_data2 <= 16'h0;
    gtx7_test_data3 <= 16'h0;
    gtx7_test_data4 <= 16'h0;
    gtx7_test_data5 <= 16'h0;
    gtx7_test_data6 <= 16'h0;
    gtx7_test_data7 <= 16'h0;
    gtx8_test_data1 <= 16'h0;
    gtx8_test_data2 <= 16'h0;
    gtx8_test_data3 <= 16'h0;
    gtx8_test_data4 <= 16'h0;
    gtx8_test_data5 <= 16'h0;
    gtx8_test_data6 <= 16'h0;
    gtx8_test_data7 <= 16'h0;
    gtx9_test_data1 <= 16'h0;
    gtx9_test_data2 <= 16'h0;
    gtx9_test_data3 <= 16'h0;
    gtx9_test_data4 <= 16'h0;
    gtx9_test_data5 <= 16'h0;
    gtx9_test_data6 <= 16'h0;
    gtx9_test_data7 <= 16'h0;
    gtx10_test_data1 <= 16'h0;
    gtx10_test_data2 <= 16'h0;
    gtx10_test_data3 <= 16'h0;
    gtx10_test_data4 <= 16'h0;
    gtx10_test_data5 <= 16'h0;
    gtx10_test_data6 <= 16'h0;
    gtx10_test_data7 <= 16'h0;
    gtx11_test_data1 <= 16'h0;
    gtx11_test_data2 <= 16'h0;
    gtx11_test_data3 <= 16'h0;
    gtx11_test_data4 <= 16'h0;
    gtx11_test_data5 <= 16'h0;
    gtx11_test_data6 <= 16'h0;
    gtx11_test_data7 <= 16'h0;

    glink0_test_data1 <= 16'h0;
    glink0_test_data2 <= 4'h0;
    glink1_test_data1 <= 16'h0;
    glink1_test_data2 <= 4'h0;
    glink2_test_data1 <= 16'h0;
    glink2_test_data2 <= 4'h0;
    glink3_test_data1 <= 16'h0;
    glink3_test_data2 <= 4'h0;
    glink4_test_data1 <= 16'h0;
    glink4_test_data2 <= 4'h0;
    glink5_test_data1 <= 16'h0;
    glink5_test_data2 <= 4'h0;
    glink6_test_data1 <= 16'h0;
    glink6_test_data2 <= 4'h0;
    glink7_test_data1 <= 16'h0;
    glink7_test_data2 <= 4'h0;
    glink8_test_data1 <= 16'h0;
    glink8_test_data2 <= 4'h0;
    glink9_test_data1 <= 16'h0;
    glink9_test_data2 <= 4'h0;
    glink10_test_data1 <= 16'h0;
    glink10_test_data2 <= 4'h0;
    glink11_test_data1 <= 16'h0;
    glink11_test_data2 <= 4'h0;
    glink12_test_data1 <= 16'h0;
    glink12_test_data2 <= 4'h0;
    glink13_test_data1 <= 16'h0;
    glink13_test_data2 <= 4'h0;

    LUT_init_mode <= 2'h0;
    LUT_init_data <= 16'h0;
    LUT_init_wr_en <= 1'b1;
    LUT_rd_address <= 15'h0;
    
    Align_eta_NSW_0 <= 8'b0;
    Align_eta_NSW_1 <= 8'b0;
    Align_eta_NSW_2 <= 8'b0;
    Align_eta_NSW_3 <= 8'b0;
    Align_eta_NSW_4 <= 8'b0;
    Align_eta_NSW_5 <= 8'b0;
    Align_eta_RPC   <= 8'b0;
    Align_phi_NSW_0 <= 6'b0;
    Align_phi_NSW_1 <= 6'b0;
    Align_phi_NSW_2 <= 6'b0;
    Align_phi_NSW_3 <= 6'b0;
    Align_phi_NSW_4 <= 6'b0;
    Align_phi_NSW_5 <= 6'b0;
    Align_phi_RPC   <= 8'b0;

// gtx phase
    gtx0_phase_select <= 4'h0;
    gtx1_phase_select <= 4'h0;
    gtx2_phase_select <= 4'h0;
    gtx3_phase_select <= 4'h0;
    gtx4_phase_select <= 4'h0;
    gtx5_phase_select <= 4'h0;
    gtx6_phase_select <= 4'h0;
    gtx7_phase_select <= 4'h0;
    gtx8_phase_select <= 4'h0;
    gtx9_phase_select <= 4'h0;
    gtx10_phase_select <= 4'h0;
    gtx11_phase_select <= 4'h0;

    // emulator
    EMULATOR_reset_out <= 1'b0;
    EMULATOR_enable <= 1'b0;        // emulator
    EMULATOR_data_length <= 16'h_FFFF; 
    
    EMULATOR_GTX_header <=  16'h_bcbc;

    TRACK0_eta <= 8'b0;        // emulator
    TRACK0_phi <= 6'b0;        // emulator
    TRACK0_deltatheta <= 5'b0; // emulator
    TRACK0_mm <= 2'b0;        // emulator
    TRACK0_stgc <= 2'b0;        // emulator
    TRACK0_spare <= 1'b0;        // emulator
    
    TRACK1_eta <= 8'b0;        // emulator
    TRACK1_phi <= 6'b0;       // emulator
    TRACK1_deltatheta <= 5'b0; // emulator
    TRACK1_mm <= 2'b0;        // emulator
    TRACK1_stgc <= 2'b0;        // emulator
    TRACK1_spare <= 1'b0;        // emulator

    TRACK2_eta <= 8'b0;        // emulator
    TRACK2_phi <= 6'b0;        // emulator
    TRACK2_deltatheta <= 5'b0; // emulator
    TRACK2_mm <= 2'b0;        // emulator
    TRACK2_stgc <= 2'b0;        // emulator
    TRACK2_spare <= 1'b0;        // emulator

    TRACK3_eta <= 8'b0;        // emulator
    TRACK3_phi <= 6'b0;        // emulator
    TRACK3_deltatheta <= 5'b0; // emulator
    TRACK3_mm <= 2'b0;        // emulator
    TRACK3_stgc <= 2'b0;        // emulator
    TRACK3_spare <= 1'b0;        // emulator

    TRACK0_SPARE_lane0_data0 <= 1'b0;
    TRACK0_SPARE_lane0_data1 <= 1'b0;
    TRACK0_SPARE_lane0_data2 <= 1'b0;
    TRACK0_SPARE_lane0_data3 <= 1'b0;
    
    TRACK0_SPARE_lane1_data0 <= 1'b0;
    TRACK0_SPARE_lane1_data1 <= 1'b0;
    TRACK0_SPARE_lane1_data2 <= 1'b0;
    TRACK0_SPARE_lane1_data3 <= 1'b0;
    
    TRACK0_SPARE_lane2_data0 <= 1'b0;
    TRACK0_SPARE_lane2_data1 <= 1'b0;
    TRACK0_SPARE_lane2_data2 <= 1'b0;
    TRACK0_SPARE_lane2_data3 <= 1'b0;
    
    TRACK0_SPARE_lane3_data0 <= 1'b0;
    TRACK0_SPARE_lane3_data1 <= 1'b0;
    TRACK0_SPARE_lane3_data2 <= 1'b0;
    TRACK0_SPARE_lane3_data3 <= 1'b0;
    
    TRACK0_SPARE_lane4_data0 <= 1'b0;
    TRACK0_SPARE_lane4_data1 <= 1'b0;
    TRACK0_SPARE_lane4_data2 <= 1'b0;
    TRACK0_SPARE_lane4_data3 <= 1'b0;
    
    TRACK0_SPARE_lane5_data0 <= 1'b0;
    TRACK0_SPARE_lane5_data1 <= 1'b0;
    TRACK0_SPARE_lane5_data2 <= 1'b0;
    TRACK0_SPARE_lane5_data3 <= 1'b0;
    
    TRACK0_SPARE_lane6_data0 <= 1'b0;
    TRACK0_SPARE_lane6_data1 <= 1'b0;
    TRACK0_SPARE_lane6_data2 <= 1'b0;
    TRACK0_SPARE_lane6_data3 <= 1'b0;
    
    TRACK0_sTGC_lane0_data0 <= 2'b0;
    TRACK0_sTGC_lane0_data1 <= 2'b0;
    TRACK0_sTGC_lane0_data2 <= 2'b0;
    TRACK0_sTGC_lane0_data3 <= 2'b0;
    
    TRACK0_sTGC_lane1_data0 <= 2'b0;
    TRACK0_sTGC_lane1_data1 <= 2'b0;
    TRACK0_sTGC_lane1_data2 <= 2'b0;
    TRACK0_sTGC_lane1_data3 <= 2'b0;
    
    TRACK0_sTGC_lane2_data0 <= 2'b0;
    TRACK0_sTGC_lane2_data1 <= 2'b0;
    TRACK0_sTGC_lane2_data2 <= 2'b0;
    TRACK0_sTGC_lane2_data3 <= 2'b0;
    
    TRACK0_sTGC_lane3_data0 <= 2'b0;
    TRACK0_sTGC_lane3_data1 <= 2'b0;
    TRACK0_sTGC_lane3_data2 <= 2'b0;
    TRACK0_sTGC_lane3_data3 <= 2'b0;
    
    TRACK0_sTGC_lane4_data0 <= 2'b0;
    TRACK0_sTGC_lane4_data1 <= 2'b0;
    TRACK0_sTGC_lane4_data2 <= 2'b0;
    TRACK0_sTGC_lane4_data3 <= 2'b0;
    
    TRACK0_sTGC_lane5_data0 <= 2'b0;
    TRACK0_sTGC_lane5_data1 <= 2'b0;
    TRACK0_sTGC_lane5_data2 <= 2'b0;
    TRACK0_sTGC_lane5_data3 <= 2'b0;
    
    TRACK0_sTGC_lane6_data0 <= 2'b0;
    TRACK0_sTGC_lane6_data1 <= 2'b0;
    TRACK0_sTGC_lane6_data2 <= 2'b0;
    TRACK0_sTGC_lane6_data3 <= 2'b0;
    
    TRACK0_MM_lane0_data0 <= 2'b0;
    TRACK0_MM_lane0_data1 <= 2'b0;
    TRACK0_MM_lane0_data2 <= 2'b0;
    TRACK0_MM_lane0_data3 <= 2'b0;
    
    TRACK0_MM_lane1_data0 <= 2'b0;
    TRACK0_MM_lane1_data1 <= 2'b0;
    TRACK0_MM_lane1_data2 <= 2'b0;
    TRACK0_MM_lane1_data3 <= 2'b0;
    
    TRACK0_MM_lane2_data0 <= 2'b0;
    TRACK0_MM_lane2_data1 <= 2'b0;
    TRACK0_MM_lane2_data2 <= 2'b0;
    TRACK0_MM_lane2_data3 <= 2'b0;
    
    TRACK0_MM_lane3_data0 <= 2'b0;
    TRACK0_MM_lane3_data1 <= 2'b0;
    TRACK0_MM_lane3_data2 <= 2'b0;
    TRACK0_MM_lane3_data3 <= 2'b0;
    
    TRACK0_MM_lane4_data0 <= 2'b0;
    TRACK0_MM_lane4_data1 <= 2'b0;
    TRACK0_MM_lane4_data2 <= 2'b0;
    TRACK0_MM_lane4_data3 <= 2'b0;
    
    TRACK0_MM_lane5_data0 <= 2'b0;
    TRACK0_MM_lane5_data1 <= 2'b0;
    TRACK0_MM_lane5_data2 <= 2'b0;
    TRACK0_MM_lane5_data3 <= 2'b0;
    
    TRACK0_MM_lane6_data0 <= 2'b0;
    TRACK0_MM_lane6_data1 <= 2'b0;
    TRACK0_MM_lane6_data2 <= 2'b0;
    TRACK0_MM_lane6_data3 <= 2'b0;
    
    TRACK0_DTHETA_lane0_data0 <= 5'b0;
    TRACK0_DTHETA_lane0_data1 <= 5'b0;
    TRACK0_DTHETA_lane0_data2 <= 5'b0;
    TRACK0_DTHETA_lane0_data3 <= 5'b0;
    
    TRACK0_DTHETA_lane1_data0 <= 5'b0;
    TRACK0_DTHETA_lane1_data1 <= 5'b0;
    TRACK0_DTHETA_lane1_data2 <= 5'b0;
    TRACK0_DTHETA_lane1_data3 <= 5'b0;
    
    TRACK0_DTHETA_lane2_data0 <= 5'b0;
    TRACK0_DTHETA_lane2_data1 <= 5'b0;
    TRACK0_DTHETA_lane2_data2 <= 5'b0;
    TRACK0_DTHETA_lane2_data3 <= 5'b0;
    
    TRACK0_DTHETA_lane3_data0 <= 5'b0;
    TRACK0_DTHETA_lane3_data1 <= 5'b0;
    TRACK0_DTHETA_lane3_data2 <= 5'b0;
    TRACK0_DTHETA_lane3_data3 <= 5'b0;
    
    TRACK0_DTHETA_lane4_data0 <= 5'b0;
    TRACK0_DTHETA_lane4_data1 <= 5'b0;
    TRACK0_DTHETA_lane4_data2 <= 5'b0;
    TRACK0_DTHETA_lane4_data3 <= 5'b0;
    
    TRACK0_DTHETA_lane5_data0 <= 5'b0;
    TRACK0_DTHETA_lane5_data1 <= 5'b0;
    TRACK0_DTHETA_lane5_data2 <= 5'b0;
    TRACK0_DTHETA_lane5_data3 <= 5'b0;
    
    TRACK0_DTHETA_lane6_data0 <= 5'b0;
    TRACK0_DTHETA_lane6_data1 <= 5'b0;
    TRACK0_DTHETA_lane6_data2 <= 5'b0;
    TRACK0_DTHETA_lane6_data3 <= 5'b0;
    
    TRACK0_PHI_lane0_data0 <= 6'b0;
    TRACK0_PHI_lane0_data1 <= 6'b0;
    TRACK0_PHI_lane0_data2 <= 6'b0;
    TRACK0_PHI_lane0_data3 <= 6'b0;
    
    TRACK0_PHI_lane1_data0 <= 6'b0;
    TRACK0_PHI_lane1_data1 <= 6'b0;
    TRACK0_PHI_lane1_data2 <= 6'b0;
    TRACK0_PHI_lane1_data3 <= 6'b0;
    
    TRACK0_PHI_lane2_data0 <= 6'b0;
    TRACK0_PHI_lane2_data1 <= 6'b0;
    TRACK0_PHI_lane2_data2 <= 6'b0;
    TRACK0_PHI_lane2_data3 <= 6'b0;
    
    TRACK0_PHI_lane3_data0 <= 6'b0;
    TRACK0_PHI_lane3_data1 <= 6'b0;
    TRACK0_PHI_lane3_data2 <= 6'b0;
    TRACK0_PHI_lane3_data3 <= 6'b0;
    
    TRACK0_PHI_lane4_data0 <= 6'b0;
    TRACK0_PHI_lane4_data1 <= 6'b0;
    TRACK0_PHI_lane4_data2 <= 6'b0;
    TRACK0_PHI_lane4_data3 <= 6'b0;
    
    TRACK0_PHI_lane5_data0 <= 6'b0;
    TRACK0_PHI_lane5_data1 <= 6'b0;
    TRACK0_PHI_lane5_data2 <= 6'b0;
    TRACK0_PHI_lane5_data3 <= 6'b0;
    
    TRACK0_PHI_lane6_data0 <= 6'b0;
    TRACK0_PHI_lane6_data1 <= 6'b0;
    TRACK0_PHI_lane6_data2 <= 6'b0;
    TRACK0_PHI_lane6_data3 <= 6'b0;
    
    TRACK0_ETA_lane0_data0 <= 8'b0;
    TRACK0_ETA_lane0_data1 <= 8'b0;
    TRACK0_ETA_lane0_data2 <= 8'b0;
    TRACK0_ETA_lane0_data3 <= 8'b0;
    
    TRACK0_ETA_lane1_data0 <= 8'b0;
    TRACK0_ETA_lane1_data1 <= 8'b0;
    TRACK0_ETA_lane1_data2 <= 8'b0;
    TRACK0_ETA_lane1_data3 <= 8'b0;
    
    TRACK0_ETA_lane2_data0 <= 8'b0;
    TRACK0_ETA_lane2_data1 <= 8'b0;
    TRACK0_ETA_lane2_data2 <= 8'b0;
    TRACK0_ETA_lane2_data3 <= 8'b0;
    
    TRACK0_ETA_lane3_data0 <= 8'b0;
    TRACK0_ETA_lane3_data1 <= 8'b0;
    TRACK0_ETA_lane3_data2 <= 8'b0;
    TRACK0_ETA_lane3_data3 <= 8'b0;
    
    TRACK0_ETA_lane4_data0 <= 8'b0;
    TRACK0_ETA_lane4_data1 <= 8'b0;
    TRACK0_ETA_lane4_data2 <= 8'b0;
    TRACK0_ETA_lane4_data3 <= 8'b0;
    
    TRACK0_ETA_lane5_data0 <= 8'b0;
    TRACK0_ETA_lane5_data1 <= 8'b0;
    TRACK0_ETA_lane5_data2 <= 8'b0;
    TRACK0_ETA_lane5_data3 <= 8'b0;
    
    TRACK0_ETA_lane6_data0 <= 8'b0;
    TRACK0_ETA_lane6_data1 <= 8'b0;
    TRACK0_ETA_lane6_data2 <= 8'b0;
    TRACK0_ETA_lane6_data3 <= 8'b0;
    
    TRACK1_SPARE_lane0_data0 <= 1'b0;
    TRACK1_SPARE_lane0_data1 <= 1'b0;
    TRACK1_SPARE_lane0_data2 <= 1'b0;
    TRACK1_SPARE_lane0_data3 <= 1'b0;
    
    TRACK1_SPARE_lane1_data0 <= 1'b0;
    TRACK1_SPARE_lane1_data1 <= 1'b0;
    TRACK1_SPARE_lane1_data2 <= 1'b0;
    TRACK1_SPARE_lane1_data3 <= 1'b0;
    
    TRACK1_SPARE_lane2_data0 <= 1'b0;
    TRACK1_SPARE_lane2_data1 <= 1'b0;
    TRACK1_SPARE_lane2_data2 <= 1'b0;
    TRACK1_SPARE_lane2_data3 <= 1'b0;
    
    TRACK1_SPARE_lane3_data0 <= 1'b0;
    TRACK1_SPARE_lane3_data1 <= 1'b0;
    TRACK1_SPARE_lane3_data2 <= 1'b0;
    TRACK1_SPARE_lane3_data3 <= 1'b0;
    
    TRACK1_SPARE_lane4_data0 <= 1'b0;
    TRACK1_SPARE_lane4_data1 <= 1'b0;
    TRACK1_SPARE_lane4_data2 <= 1'b0;
    TRACK1_SPARE_lane4_data3 <= 1'b0;
    
    TRACK1_SPARE_lane5_data0 <= 1'b0;
    TRACK1_SPARE_lane5_data1 <= 1'b0;
    TRACK1_SPARE_lane5_data2 <= 1'b0;
    TRACK1_SPARE_lane5_data3 <= 1'b0;
    
    TRACK1_SPARE_lane6_data0 <= 1'b0;
    TRACK1_SPARE_lane6_data1 <= 1'b0;
    TRACK1_SPARE_lane6_data2 <= 1'b0;
    TRACK1_SPARE_lane6_data3 <= 1'b0;
    
    TRACK1_sTGC_lane0_data0 <= 2'b0;
    TRACK1_sTGC_lane0_data1 <= 2'b0;
    TRACK1_sTGC_lane0_data2 <= 2'b0;
    TRACK1_sTGC_lane0_data3 <= 2'b0;
    
    TRACK1_sTGC_lane1_data0 <= 2'b0;
    TRACK1_sTGC_lane1_data1 <= 2'b0;
    TRACK1_sTGC_lane1_data2 <= 2'b0;
    TRACK1_sTGC_lane1_data3 <= 2'b0;
    
    TRACK1_sTGC_lane2_data0 <= 2'b0;
    TRACK1_sTGC_lane2_data1 <= 2'b0;
    TRACK1_sTGC_lane2_data2 <= 2'b0;
    TRACK1_sTGC_lane2_data3 <= 2'b0;
    
    TRACK1_sTGC_lane3_data0 <= 2'b0;
    TRACK1_sTGC_lane3_data1 <= 2'b0;
    TRACK1_sTGC_lane3_data2 <= 2'b0;
    TRACK1_sTGC_lane3_data3 <= 2'b0;
    
    TRACK1_sTGC_lane4_data0 <= 2'b0;
    TRACK1_sTGC_lane4_data1 <= 2'b0;
    TRACK1_sTGC_lane4_data2 <= 2'b0;
    TRACK1_sTGC_lane4_data3 <= 2'b0;
    
    TRACK1_sTGC_lane5_data0 <= 2'b0;
    TRACK1_sTGC_lane5_data1 <= 2'b0;
    TRACK1_sTGC_lane5_data2 <= 2'b0;
    TRACK1_sTGC_lane5_data3 <= 2'b0;
    
    TRACK1_sTGC_lane6_data0 <= 2'b0;
    TRACK1_sTGC_lane6_data1 <= 2'b0;
    TRACK1_sTGC_lane6_data2 <= 2'b0;
    TRACK1_sTGC_lane6_data3 <= 2'b0;
    
    TRACK1_MM_lane0_data0 <= 2'b0;
    TRACK1_MM_lane0_data1 <= 2'b0;
    TRACK1_MM_lane0_data2 <= 2'b0;
    TRACK1_MM_lane0_data3 <= 2'b0;
    
    TRACK1_MM_lane1_data0 <= 2'b0;
    TRACK1_MM_lane1_data1 <= 2'b0;
    TRACK1_MM_lane1_data2 <= 2'b0;
    TRACK1_MM_lane1_data3 <= 2'b0;
    
    TRACK1_MM_lane2_data0 <= 2'b0;
    TRACK1_MM_lane2_data1 <= 2'b0;
    TRACK1_MM_lane2_data2 <= 2'b0;
    TRACK1_MM_lane2_data3 <= 2'b0;
    
    TRACK1_MM_lane3_data0 <= 2'b0;
    TRACK1_MM_lane3_data1 <= 2'b0;
    TRACK1_MM_lane3_data2 <= 2'b0;
    TRACK1_MM_lane3_data3 <= 2'b0;
    
    TRACK1_MM_lane4_data0 <= 2'b0;
    TRACK1_MM_lane4_data1 <= 2'b0;
    TRACK1_MM_lane4_data2 <= 2'b0;
    TRACK1_MM_lane4_data3 <= 2'b0;
    
    TRACK1_MM_lane5_data0 <= 2'b0;
    TRACK1_MM_lane5_data1 <= 2'b0;
    TRACK1_MM_lane5_data2 <= 2'b0;
    TRACK1_MM_lane5_data3 <= 2'b0;
    
    TRACK1_MM_lane6_data0 <= 2'b0;
    TRACK1_MM_lane6_data1 <= 2'b0;
    TRACK1_MM_lane6_data2 <= 2'b0;
    TRACK1_MM_lane6_data3 <= 2'b0;
    
    TRACK1_DTHETA_lane0_data0 <= 5'b0;
    TRACK1_DTHETA_lane0_data1 <= 5'b0;
    TRACK1_DTHETA_lane0_data2 <= 5'b0;
    TRACK1_DTHETA_lane0_data3 <= 5'b0;
    
    TRACK1_DTHETA_lane1_data0 <= 5'b0;
    TRACK1_DTHETA_lane1_data1 <= 5'b0;
    TRACK1_DTHETA_lane1_data2 <= 5'b0;
    TRACK1_DTHETA_lane1_data3 <= 5'b0;
    
    TRACK1_DTHETA_lane2_data0 <= 5'b0;
    TRACK1_DTHETA_lane2_data1 <= 5'b0;
    TRACK1_DTHETA_lane2_data2 <= 5'b0;
    TRACK1_DTHETA_lane2_data3 <= 5'b0;
    
    TRACK1_DTHETA_lane3_data0 <= 5'b0;
    TRACK1_DTHETA_lane3_data1 <= 5'b0;
    TRACK1_DTHETA_lane3_data2 <= 5'b0;
    TRACK1_DTHETA_lane3_data3 <= 5'b0;
    
    TRACK1_DTHETA_lane4_data0 <= 5'b0;
    TRACK1_DTHETA_lane4_data1 <= 5'b0;
    TRACK1_DTHETA_lane4_data2 <= 5'b0;
    TRACK1_DTHETA_lane4_data3 <= 5'b0;
    
    TRACK1_DTHETA_lane5_data0 <= 5'b0;
    TRACK1_DTHETA_lane5_data1 <= 5'b0;
    TRACK1_DTHETA_lane5_data2 <= 5'b0;
    TRACK1_DTHETA_lane5_data3 <= 5'b0;
    
    TRACK1_DTHETA_lane6_data0 <= 5'b0;
    TRACK1_DTHETA_lane6_data1 <= 5'b0;
    TRACK1_DTHETA_lane6_data2 <= 5'b0;
    TRACK1_DTHETA_lane6_data3 <= 5'b0;
    
    TRACK1_PHI_lane0_data0 <= 6'b0;
    TRACK1_PHI_lane0_data1 <= 6'b0;
    TRACK1_PHI_lane0_data2 <= 6'b0;
    TRACK1_PHI_lane0_data3 <= 6'b0;
    
    TRACK1_PHI_lane1_data0 <= 6'b0;
    TRACK1_PHI_lane1_data1 <= 6'b0;
    TRACK1_PHI_lane1_data2 <= 6'b0;
    TRACK1_PHI_lane1_data3 <= 6'b0;
    
    TRACK1_PHI_lane2_data0 <= 6'b0;
    TRACK1_PHI_lane2_data1 <= 6'b0;
    TRACK1_PHI_lane2_data2 <= 6'b0;
    TRACK1_PHI_lane2_data3 <= 6'b0;
    
    TRACK1_PHI_lane3_data0 <= 6'b0;
    TRACK1_PHI_lane3_data1 <= 6'b0;
    TRACK1_PHI_lane3_data2 <= 6'b0;
    TRACK1_PHI_lane3_data3 <= 6'b0;
    
    TRACK1_PHI_lane4_data0 <= 6'b0;
    TRACK1_PHI_lane4_data1 <= 6'b0;
    TRACK1_PHI_lane4_data2 <= 6'b0;
    TRACK1_PHI_lane4_data3 <= 6'b0;
    
    TRACK1_PHI_lane5_data0 <= 6'b0;
    TRACK1_PHI_lane5_data1 <= 6'b0;
    TRACK1_PHI_lane5_data2 <= 6'b0;
    TRACK1_PHI_lane5_data3 <= 6'b0;
    
    TRACK1_PHI_lane6_data0 <= 6'b0;
    TRACK1_PHI_lane6_data1 <= 6'b0;
    TRACK1_PHI_lane6_data2 <= 6'b0;
    TRACK1_PHI_lane6_data3 <= 6'b0;
    
    TRACK1_ETA_lane0_data0 <= 8'b0;
    TRACK1_ETA_lane0_data1 <= 8'b0;
    TRACK1_ETA_lane0_data2 <= 8'b0;
    TRACK1_ETA_lane0_data3 <= 8'b0;
    
    TRACK1_ETA_lane1_data0 <= 8'b0;
    TRACK1_ETA_lane1_data1 <= 8'b0;
    TRACK1_ETA_lane1_data2 <= 8'b0;
    TRACK1_ETA_lane1_data3 <= 8'b0;
    
    TRACK1_ETA_lane2_data0 <= 8'b0;
    TRACK1_ETA_lane2_data1 <= 8'b0;
    TRACK1_ETA_lane2_data2 <= 8'b0;
    TRACK1_ETA_lane2_data3 <= 8'b0;
    
    TRACK1_ETA_lane3_data0 <= 8'b0;
    TRACK1_ETA_lane3_data1 <= 8'b0;
    TRACK1_ETA_lane3_data2 <= 8'b0;
    TRACK1_ETA_lane3_data3 <= 8'b0;
    
    TRACK1_ETA_lane4_data0 <= 8'b0;
    TRACK1_ETA_lane4_data1 <= 8'b0;
    TRACK1_ETA_lane4_data2 <= 8'b0;
    TRACK1_ETA_lane4_data3 <= 8'b0;
    
    TRACK1_ETA_lane5_data0 <= 8'b0;
    TRACK1_ETA_lane5_data1 <= 8'b0;
    TRACK1_ETA_lane5_data2 <= 8'b0;
    TRACK1_ETA_lane5_data3 <= 8'b0;
    
    TRACK1_ETA_lane6_data0 <= 8'b0;
    TRACK1_ETA_lane6_data1 <= 8'b0;
    TRACK1_ETA_lane6_data2 <= 8'b0;
    TRACK1_ETA_lane6_data3 <= 8'b0;
    
    TRACK2_SPARE_lane0_data0 <= 1'b0;
    TRACK2_SPARE_lane0_data1 <= 1'b0;
    TRACK2_SPARE_lane0_data2 <= 1'b0;
    TRACK2_SPARE_lane0_data3 <= 1'b0;
    
    TRACK2_SPARE_lane1_data0 <= 1'b0;
    TRACK2_SPARE_lane1_data1 <= 1'b0;
    TRACK2_SPARE_lane1_data2 <= 1'b0;
    TRACK2_SPARE_lane1_data3 <= 1'b0;
    
    TRACK2_SPARE_lane2_data0 <= 1'b0;
    TRACK2_SPARE_lane2_data1 <= 1'b0;
    TRACK2_SPARE_lane2_data2 <= 1'b0;
    TRACK2_SPARE_lane2_data3 <= 1'b0;
    
    TRACK2_SPARE_lane3_data0 <= 1'b0;
    TRACK2_SPARE_lane3_data1 <= 1'b0;
    TRACK2_SPARE_lane3_data2 <= 1'b0;
    TRACK2_SPARE_lane3_data3 <= 1'b0;
    
    TRACK2_SPARE_lane4_data0 <= 1'b0;
    TRACK2_SPARE_lane4_data1 <= 1'b0;
    TRACK2_SPARE_lane4_data2 <= 1'b0;
    TRACK2_SPARE_lane4_data3 <= 1'b0;
    
    TRACK2_SPARE_lane5_data0 <= 1'b0;
    TRACK2_SPARE_lane5_data1 <= 1'b0;
    TRACK2_SPARE_lane5_data2 <= 1'b0;
    TRACK2_SPARE_lane5_data3 <= 1'b0;
    
    TRACK2_SPARE_lane6_data0 <= 1'b0;
    TRACK2_SPARE_lane6_data1 <= 1'b0;
    TRACK2_SPARE_lane6_data2 <= 1'b0;
    TRACK2_SPARE_lane6_data3 <= 1'b0;
    
    TRACK2_sTGC_lane0_data0 <= 2'b0;
    TRACK2_sTGC_lane0_data1 <= 2'b0;
    TRACK2_sTGC_lane0_data2 <= 2'b0;
    TRACK2_sTGC_lane0_data3 <= 2'b0;
    
    TRACK2_sTGC_lane1_data0 <= 2'b0;
    TRACK2_sTGC_lane1_data1 <= 2'b0;
    TRACK2_sTGC_lane1_data2 <= 2'b0;
    TRACK2_sTGC_lane1_data3 <= 2'b0;
    
    TRACK2_sTGC_lane2_data0 <= 2'b0;
    TRACK2_sTGC_lane2_data1 <= 2'b0;
    TRACK2_sTGC_lane2_data2 <= 2'b0;
    TRACK2_sTGC_lane2_data3 <= 2'b0;
    
    TRACK2_sTGC_lane3_data0 <= 2'b0;
    TRACK2_sTGC_lane3_data1 <= 2'b0;
    TRACK2_sTGC_lane3_data2 <= 2'b0;
    TRACK2_sTGC_lane3_data3 <= 2'b0;
    
    TRACK2_sTGC_lane4_data0 <= 2'b0;
    TRACK2_sTGC_lane4_data1 <= 2'b0;
    TRACK2_sTGC_lane4_data2 <= 2'b0;
    TRACK2_sTGC_lane4_data3 <= 2'b0;
    
    TRACK2_sTGC_lane5_data0 <= 2'b0;
    TRACK2_sTGC_lane5_data1 <= 2'b0;
    TRACK2_sTGC_lane5_data2 <= 2'b0;
    TRACK2_sTGC_lane5_data3 <= 2'b0;
    
    TRACK2_sTGC_lane6_data0 <= 2'b0;
    TRACK2_sTGC_lane6_data1 <= 2'b0;
    TRACK2_sTGC_lane6_data2 <= 2'b0;
    TRACK2_sTGC_lane6_data3 <= 2'b0;
    
    TRACK2_MM_lane0_data0 <= 2'b0;
    TRACK2_MM_lane0_data1 <= 2'b0;
    TRACK2_MM_lane0_data2 <= 2'b0;
    TRACK2_MM_lane0_data3 <= 2'b0;
    
    TRACK2_MM_lane1_data0 <= 2'b0;
    TRACK2_MM_lane1_data1 <= 2'b0;
    TRACK2_MM_lane1_data2 <= 2'b0;
    TRACK2_MM_lane1_data3 <= 2'b0;
    
    TRACK2_MM_lane2_data0 <= 2'b0;
    TRACK2_MM_lane2_data1 <= 2'b0;
    TRACK2_MM_lane2_data2 <= 2'b0;
    TRACK2_MM_lane2_data3 <= 2'b0;
    
    TRACK2_MM_lane3_data0 <= 2'b0;
    TRACK2_MM_lane3_data1 <= 2'b0;
    TRACK2_MM_lane3_data2 <= 2'b0;
    TRACK2_MM_lane3_data3 <= 2'b0;
    
    TRACK2_MM_lane4_data0 <= 2'b0;
    TRACK2_MM_lane4_data1 <= 2'b0;
    TRACK2_MM_lane4_data2 <= 2'b0;
    TRACK2_MM_lane4_data3 <= 2'b0;
    
    TRACK2_MM_lane5_data0 <= 2'b0;
    TRACK2_MM_lane5_data1 <= 2'b0;
    TRACK2_MM_lane5_data2 <= 2'b0;
    TRACK2_MM_lane5_data3 <= 2'b0;
    
    TRACK2_MM_lane6_data0 <= 2'b0;
    TRACK2_MM_lane6_data1 <= 2'b0;
    TRACK2_MM_lane6_data2 <= 2'b0;
    TRACK2_MM_lane6_data3 <= 2'b0;
    
    TRACK2_DTHETA_lane0_data0 <= 5'b0;
    TRACK2_DTHETA_lane0_data1 <= 5'b0;
    TRACK2_DTHETA_lane0_data2 <= 5'b0;
    TRACK2_DTHETA_lane0_data3 <= 5'b0;
    
    TRACK2_DTHETA_lane1_data0 <= 5'b0;
    TRACK2_DTHETA_lane1_data1 <= 5'b0;
    TRACK2_DTHETA_lane1_data2 <= 5'b0;
    TRACK2_DTHETA_lane1_data3 <= 5'b0;
    
    TRACK2_DTHETA_lane2_data0 <= 5'b0;
    TRACK2_DTHETA_lane2_data1 <= 5'b0;
    TRACK2_DTHETA_lane2_data2 <= 5'b0;
    TRACK2_DTHETA_lane2_data3 <= 5'b0;
    
    TRACK2_DTHETA_lane3_data0 <= 5'b0;
    TRACK2_DTHETA_lane3_data1 <= 5'b0;
    TRACK2_DTHETA_lane3_data2 <= 5'b0;
    TRACK2_DTHETA_lane3_data3 <= 5'b0;
    
    TRACK2_DTHETA_lane4_data0 <= 5'b0;
    TRACK2_DTHETA_lane4_data1 <= 5'b0;
    TRACK2_DTHETA_lane4_data2 <= 5'b0;
    TRACK2_DTHETA_lane4_data3 <= 5'b0;
    
    TRACK2_DTHETA_lane5_data0 <= 5'b0;
    TRACK2_DTHETA_lane5_data1 <= 5'b0;
    TRACK2_DTHETA_lane5_data2 <= 5'b0;
    TRACK2_DTHETA_lane5_data3 <= 5'b0;
    
    TRACK2_DTHETA_lane6_data0 <= 5'b0;
    TRACK2_DTHETA_lane6_data1 <= 5'b0;
    TRACK2_DTHETA_lane6_data2 <= 5'b0;
    TRACK2_DTHETA_lane6_data3 <= 5'b0;
    
    TRACK2_PHI_lane0_data0 <= 6'b0;
    TRACK2_PHI_lane0_data1 <= 6'b0;
    TRACK2_PHI_lane0_data2 <= 6'b0;
    TRACK2_PHI_lane0_data3 <= 6'b0;
    
    TRACK2_PHI_lane1_data0 <= 6'b0;
    TRACK2_PHI_lane1_data1 <= 6'b0;
    TRACK2_PHI_lane1_data2 <= 6'b0;
    TRACK2_PHI_lane1_data3 <= 6'b0;
    
    TRACK2_PHI_lane2_data0 <= 6'b0;
    TRACK2_PHI_lane2_data1 <= 6'b0;
    TRACK2_PHI_lane2_data2 <= 6'b0;
    TRACK2_PHI_lane2_data3 <= 6'b0;
    
    TRACK2_PHI_lane3_data0 <= 6'b0;
    TRACK2_PHI_lane3_data1 <= 6'b0;
    TRACK2_PHI_lane3_data2 <= 6'b0;
    TRACK2_PHI_lane3_data3 <= 6'b0;
    
    TRACK2_PHI_lane4_data0 <= 6'b0;
    TRACK2_PHI_lane4_data1 <= 6'b0;
    TRACK2_PHI_lane4_data2 <= 6'b0;
    TRACK2_PHI_lane4_data3 <= 6'b0;
    
    TRACK2_PHI_lane5_data0 <= 6'b0;
    TRACK2_PHI_lane5_data1 <= 6'b0;
    TRACK2_PHI_lane5_data2 <= 6'b0;
    TRACK2_PHI_lane5_data3 <= 6'b0;
    
    TRACK2_PHI_lane6_data0 <= 6'b0;
    TRACK2_PHI_lane6_data1 <= 6'b0;
    TRACK2_PHI_lane6_data2 <= 6'b0;
    TRACK2_PHI_lane6_data3 <= 6'b0;
    
    TRACK2_ETA_lane0_data0 <= 8'b0;
    TRACK2_ETA_lane0_data1 <= 8'b0;
    TRACK2_ETA_lane0_data2 <= 8'b0;
    TRACK2_ETA_lane0_data3 <= 8'b0;
    
    TRACK2_ETA_lane1_data0 <= 8'b0;
    TRACK2_ETA_lane1_data1 <= 8'b0;
    TRACK2_ETA_lane1_data2 <= 8'b0;
    TRACK2_ETA_lane1_data3 <= 8'b0;
    
    TRACK2_ETA_lane2_data0 <= 8'b0;
    TRACK2_ETA_lane2_data1 <= 8'b0;
    TRACK2_ETA_lane2_data2 <= 8'b0;
    TRACK2_ETA_lane2_data3 <= 8'b0;
    
    TRACK2_ETA_lane3_data0 <= 8'b0;
    TRACK2_ETA_lane3_data1 <= 8'b0;
    TRACK2_ETA_lane3_data2 <= 8'b0;
    TRACK2_ETA_lane3_data3 <= 8'b0;
    
    TRACK2_ETA_lane4_data0 <= 8'b0;
    TRACK2_ETA_lane4_data1 <= 8'b0;
    TRACK2_ETA_lane4_data2 <= 8'b0;
    TRACK2_ETA_lane4_data3 <= 8'b0;
    
    TRACK2_ETA_lane5_data0 <= 8'b0;
    TRACK2_ETA_lane5_data1 <= 8'b0;
    TRACK2_ETA_lane5_data2 <= 8'b0;
    TRACK2_ETA_lane5_data3 <= 8'b0;
    
    TRACK2_ETA_lane6_data0 <= 8'b0;
    TRACK2_ETA_lane6_data1 <= 8'b0;
    TRACK2_ETA_lane6_data2 <= 8'b0;
    TRACK2_ETA_lane6_data3 <= 8'b0;
    
    TRACK3_SPARE_lane0_data0 <= 1'b0;
    TRACK3_SPARE_lane0_data1 <= 1'b0;
    TRACK3_SPARE_lane0_data2 <= 1'b0;
    TRACK3_SPARE_lane0_data3 <= 1'b0;
    
    TRACK3_SPARE_lane1_data0 <= 1'b0;
    TRACK3_SPARE_lane1_data1 <= 1'b0;
    TRACK3_SPARE_lane1_data2 <= 1'b0;
    TRACK3_SPARE_lane1_data3 <= 1'b0;
    
    TRACK3_SPARE_lane2_data0 <= 1'b0;
    TRACK3_SPARE_lane2_data1 <= 1'b0;
    TRACK3_SPARE_lane2_data2 <= 1'b0;
    TRACK3_SPARE_lane2_data3 <= 1'b0;
    
    TRACK3_SPARE_lane3_data0 <= 1'b0;
    TRACK3_SPARE_lane3_data1 <= 1'b0;
    TRACK3_SPARE_lane3_data2 <= 1'b0;
    TRACK3_SPARE_lane3_data3 <= 1'b0;
    
    TRACK3_SPARE_lane4_data0 <= 1'b0;
    TRACK3_SPARE_lane4_data1 <= 1'b0;
    TRACK3_SPARE_lane4_data2 <= 1'b0;
    TRACK3_SPARE_lane4_data3 <= 1'b0;
    
    TRACK3_SPARE_lane5_data0 <= 1'b0;
    TRACK3_SPARE_lane5_data1 <= 1'b0;
    TRACK3_SPARE_lane5_data2 <= 1'b0;
    TRACK3_SPARE_lane5_data3 <= 1'b0;
    
    TRACK3_SPARE_lane6_data0 <= 1'b0;
    TRACK3_SPARE_lane6_data1 <= 1'b0;
    TRACK3_SPARE_lane6_data2 <= 1'b0;
    TRACK3_SPARE_lane6_data3 <= 1'b0;
    
    TRACK3_sTGC_lane0_data0 <= 2'b0;
    TRACK3_sTGC_lane0_data1 <= 2'b0;
    TRACK3_sTGC_lane0_data2 <= 2'b0;
    TRACK3_sTGC_lane0_data3 <= 2'b0;
    
    TRACK3_sTGC_lane1_data0 <= 2'b0;
    TRACK3_sTGC_lane1_data1 <= 2'b0;
    TRACK3_sTGC_lane1_data2 <= 2'b0;
    TRACK3_sTGC_lane1_data3 <= 2'b0;
    
    TRACK3_sTGC_lane2_data0 <= 2'b0;
    TRACK3_sTGC_lane2_data1 <= 2'b0;
    TRACK3_sTGC_lane2_data2 <= 2'b0;
    TRACK3_sTGC_lane2_data3 <= 2'b0;
    
    TRACK3_sTGC_lane3_data0 <= 2'b0;
    TRACK3_sTGC_lane3_data1 <= 2'b0;
    TRACK3_sTGC_lane3_data2 <= 2'b0;
    TRACK3_sTGC_lane3_data3 <= 2'b0;
    
    TRACK3_sTGC_lane4_data0 <= 2'b0;
    TRACK3_sTGC_lane4_data1 <= 2'b0;
    TRACK3_sTGC_lane4_data2 <= 2'b0;
    TRACK3_sTGC_lane4_data3 <= 2'b0;
    
    TRACK3_sTGC_lane5_data0 <= 2'b0;
    TRACK3_sTGC_lane5_data1 <= 2'b0;
    TRACK3_sTGC_lane5_data2 <= 2'b0;
    TRACK3_sTGC_lane5_data3 <= 2'b0;
    
    TRACK3_sTGC_lane6_data0 <= 2'b0;
    TRACK3_sTGC_lane6_data1 <= 2'b0;
    TRACK3_sTGC_lane6_data2 <= 2'b0;
    TRACK3_sTGC_lane6_data3 <= 2'b0;
    
    TRACK3_MM_lane0_data0 <= 2'b0;
    TRACK3_MM_lane0_data1 <= 2'b0;
    TRACK3_MM_lane0_data2 <= 2'b0;
    TRACK3_MM_lane0_data3 <= 2'b0;
    
    TRACK3_MM_lane1_data0 <= 2'b0;
    TRACK3_MM_lane1_data1 <= 2'b0;
    TRACK3_MM_lane1_data2 <= 2'b0;
    TRACK3_MM_lane1_data3 <= 2'b0;
    
    TRACK3_MM_lane2_data0 <= 2'b0;
    TRACK3_MM_lane2_data1 <= 2'b0;
    TRACK3_MM_lane2_data2 <= 2'b0;
    TRACK3_MM_lane2_data3 <= 2'b0;
    
    TRACK3_MM_lane3_data0 <= 2'b0;
    TRACK3_MM_lane3_data1 <= 2'b0;
    TRACK3_MM_lane3_data2 <= 2'b0;
    TRACK3_MM_lane3_data3 <= 2'b0;
    
    TRACK3_MM_lane4_data0 <= 2'b0;
    TRACK3_MM_lane4_data1 <= 2'b0;
    TRACK3_MM_lane4_data2 <= 2'b0;
    TRACK3_MM_lane4_data3 <= 2'b0;
    
    TRACK3_MM_lane5_data0 <= 2'b0;
    TRACK3_MM_lane5_data1 <= 2'b0;
    TRACK3_MM_lane5_data2 <= 2'b0;
    TRACK3_MM_lane5_data3 <= 2'b0;
    
    TRACK3_MM_lane6_data0 <= 2'b0;
    TRACK3_MM_lane6_data1 <= 2'b0;
    TRACK3_MM_lane6_data2 <= 2'b0;
    TRACK3_MM_lane6_data3 <= 2'b0;
    
    TRACK3_DTHETA_lane0_data0 <= 5'b0;
    TRACK3_DTHETA_lane0_data1 <= 5'b0;
    TRACK3_DTHETA_lane0_data2 <= 5'b0;
    TRACK3_DTHETA_lane0_data3 <= 5'b0;
    
    TRACK3_DTHETA_lane1_data0 <= 5'b0;
    TRACK3_DTHETA_lane1_data1 <= 5'b0;
    TRACK3_DTHETA_lane1_data2 <= 5'b0;
    TRACK3_DTHETA_lane1_data3 <= 5'b0;
    
    TRACK3_DTHETA_lane2_data0 <= 5'b0;
    TRACK3_DTHETA_lane2_data1 <= 5'b0;
    TRACK3_DTHETA_lane2_data2 <= 5'b0;
    TRACK3_DTHETA_lane2_data3 <= 5'b0;
    
    TRACK3_DTHETA_lane3_data0 <= 5'b0;
    TRACK3_DTHETA_lane3_data1 <= 5'b0;
    TRACK3_DTHETA_lane3_data2 <= 5'b0;
    TRACK3_DTHETA_lane3_data3 <= 5'b0;
    
    TRACK3_DTHETA_lane4_data0 <= 5'b0;
    TRACK3_DTHETA_lane4_data1 <= 5'b0;
    TRACK3_DTHETA_lane4_data2 <= 5'b0;
    TRACK3_DTHETA_lane4_data3 <= 5'b0;
    
    TRACK3_DTHETA_lane5_data0 <= 5'b0;
    TRACK3_DTHETA_lane5_data1 <= 5'b0;
    TRACK3_DTHETA_lane5_data2 <= 5'b0;
    TRACK3_DTHETA_lane5_data3 <= 5'b0;
    
    TRACK3_DTHETA_lane6_data0 <= 5'b0;
    TRACK3_DTHETA_lane6_data1 <= 5'b0;
    TRACK3_DTHETA_lane6_data2 <= 5'b0;
    TRACK3_DTHETA_lane6_data3 <= 5'b0;
    
    TRACK3_PHI_lane0_data0 <= 6'b0;
    TRACK3_PHI_lane0_data1 <= 6'b0;
    TRACK3_PHI_lane0_data2 <= 6'b0;
    TRACK3_PHI_lane0_data3 <= 6'b0;
    
    TRACK3_PHI_lane1_data0 <= 6'b0;
    TRACK3_PHI_lane1_data1 <= 6'b0;
    TRACK3_PHI_lane1_data2 <= 6'b0;
    TRACK3_PHI_lane1_data3 <= 6'b0;
    
    TRACK3_PHI_lane2_data0 <= 6'b0;
    TRACK3_PHI_lane2_data1 <= 6'b0;
    TRACK3_PHI_lane2_data2 <= 6'b0;
    TRACK3_PHI_lane2_data3 <= 6'b0;
    
    TRACK3_PHI_lane3_data0 <= 6'b0;
    TRACK3_PHI_lane3_data1 <= 6'b0;
    TRACK3_PHI_lane3_data2 <= 6'b0;
    TRACK3_PHI_lane3_data3 <= 6'b0;
    
    TRACK3_PHI_lane4_data0 <= 6'b0;
    TRACK3_PHI_lane4_data1 <= 6'b0;
    TRACK3_PHI_lane4_data2 <= 6'b0;
    TRACK3_PHI_lane4_data3 <= 6'b0;
    
    TRACK3_PHI_lane5_data0 <= 6'b0;
    TRACK3_PHI_lane5_data1 <= 6'b0;
    TRACK3_PHI_lane5_data2 <= 6'b0;
    TRACK3_PHI_lane5_data3 <= 6'b0;
    
    TRACK3_PHI_lane6_data0 <= 6'b0;
    TRACK3_PHI_lane6_data1 <= 6'b0;
    TRACK3_PHI_lane6_data2 <= 6'b0;
    TRACK3_PHI_lane6_data3 <= 6'b0;
    
    TRACK3_ETA_lane0_data0 <= 8'b0;
    TRACK3_ETA_lane0_data1 <= 8'b0;
    TRACK3_ETA_lane0_data2 <= 8'b0;
    TRACK3_ETA_lane0_data3 <= 8'b0;
    
    TRACK3_ETA_lane1_data0 <= 8'b0;
    TRACK3_ETA_lane1_data1 <= 8'b0;
    TRACK3_ETA_lane1_data2 <= 8'b0;
    TRACK3_ETA_lane1_data3 <= 8'b0;
    
    TRACK3_ETA_lane2_data0 <= 8'b0;
    TRACK3_ETA_lane2_data1 <= 8'b0;
    TRACK3_ETA_lane2_data2 <= 8'b0;
    TRACK3_ETA_lane2_data3 <= 8'b0;
    
    TRACK3_ETA_lane3_data0 <= 8'b0;
    TRACK3_ETA_lane3_data1 <= 8'b0;
    TRACK3_ETA_lane3_data2 <= 8'b0;
    TRACK3_ETA_lane3_data3 <= 8'b0;
    
    TRACK3_ETA_lane4_data0 <= 8'b0;
    TRACK3_ETA_lane4_data1 <= 8'b0;
    TRACK3_ETA_lane4_data2 <= 8'b0;
    TRACK3_ETA_lane4_data3 <= 8'b0;
    
    TRACK3_ETA_lane5_data0 <= 8'b0;
    TRACK3_ETA_lane5_data1 <= 8'b0;
    TRACK3_ETA_lane5_data2 <= 8'b0;
    TRACK3_ETA_lane5_data3 <= 8'b0;
    
    TRACK3_ETA_lane6_data0 <= 8'b0;
    TRACK3_ETA_lane6_data1 <= 8'b0;
    TRACK3_ETA_lane6_data2 <= 8'b0;
    TRACK3_ETA_lane6_data3 <= 8'b0;
end

reg [2:0] shift_reg_40;     
reg [2:0] shift_reg_160;    
reg [2:0] shift_reg_TX;    
reg write_40; 
reg write_160; 
reg write_TX; 
        



// WRITE asynchronus
always@(posedge ws) begin
    case (VME_A)        
// Reset
        default : begin
        end    
    endcase
end


// Write 40 MHz
always@(posedge TTC_CLK) begin
    shift_reg_40[2:0] <= {shift_reg_40[1:0], ws};
    if(write_40) begin
        write_40 <= 1'b0;
    end
    else if (shift_reg_40[2]==1'b0 && shift_reg_40[0] == 1'b1) begin
        write_40 <= 1'b1;
        case (VME_A)
        `Addr_TEST : begin
            Test_reg <= VME_DIN[15:0];
        end
        `Addr_RESET : begin
            Reset_TTC_out <= VME_DIN[0];
        end
        `Addr_GTX_TX_RESET : begin
            GTX_TX_reset_out <= VME_DIN[0];
        end
        `Addr_CLK_RESET : begin
            CLK_reset_out <= VME_DIN[0];
        end
        `Addr_SCALER_RESET : begin
            Scaler_reset_out <= VME_DIN[0];
        end
        `Addr_GTX_RX_RESET : begin
            GTX_RX_reset_out <= VME_DIN[0];
        end
        `Addr_DELAY_RESET : begin
            Delay_reset_out <= VME_DIN[0];
        end
        `Addr_L1BUFFER_RESET : begin
            L1Buffer_reset_out <= VME_DIN[0];
        end
        `Addr_DERANDOMIZER_RESET : begin
            Derandomizer_reset_out <= VME_DIN[0];
        end
// gtx phase
	    `Addr_GTX0_PHASE_SELECT : begin
	        gtx0_phase_select <= VME_DIN[3:0];
	    end
	    `Addr_GTX1_PHASE_SELECT : begin
	        gtx1_phase_select <= VME_DIN[3:0];
	    end
	    `Addr_GTX2_PHASE_SELECT : begin
	        gtx2_phase_select <= VME_DIN[3:0];
	    end
	    `Addr_GTX3_PHASE_SELECT : begin
	        gtx3_phase_select <= VME_DIN[3:0];
	    end
	    `Addr_GTX4_PHASE_SELECT : begin
	        gtx4_phase_select <= VME_DIN[3:0];
	    end
	    `Addr_GTX5_PHASE_SELECT : begin
	        gtx5_phase_select <= VME_DIN[3:0];
	    end
	    `Addr_GTX6_PHASE_SELECT : begin
	        gtx6_phase_select <= VME_DIN[3:0];
	    end
	    `Addr_GTX7_PHASE_SELECT : begin
	        gtx7_phase_select <= VME_DIN[3:0];
	    end
	    `Addr_GTX8_PHASE_SELECT : begin
	        gtx8_phase_select <= VME_DIN[3:0];
	    end
	    `Addr_GTX9_PHASE_SELECT : begin
	        gtx9_phase_select <= VME_DIN[3:0];
	    end
	    `Addr_GTX10_PHASE_SELECT : begin
	        gtx10_phase_select <= VME_DIN[3:0];
	    end
	    `Addr_GTX11_PHASE_SELECT : begin
	        gtx11_phase_select <= VME_DIN[3:0];
	    end
// Emulator
       `Addr_EMULATOR_RESET : begin
            EMULATOR_reset_out <= VME_DIN[0];
        end        
        `Addr_EMULATOR_DATA_LENGTH : begin
            EMULATOR_data_length <= VME_DIN[15:0];
        end
        `Addr_EMULATOR_ENABLLE : begin
            EMULATOR_enable <= VME_DIN[0];
        end      
        `Addr_EMULATOR_GTX_HEADER : begin
            EMULATOR_GTX_header <= VME_DIN[15:0];
        end

        `Addr_TRACK0_ETA : begin
            TRACK0_eta[7:0] <= VME_DIN[7:0];
        end
        `Addr_TRACK0_PHI : begin
            TRACK0_phi[5:0] <= VME_DIN[5:0];
        end
        `Addr_TRACK0_DELTATHETA : begin
            TRACK0_deltatheta[4:0] <= VME_DIN[4:0];
        end
        `Addr_TRACK0_MM : begin
            TRACK0_mm[1:0] <= VME_DIN[1:0];
        end
        `Addr_TRACK0_sTGC : begin
            TRACK0_stgc[1:0] <= VME_DIN[1:0];
        end
        `Addr_TRACK0_SPARE : begin
            TRACK0_spare <= VME_DIN[0];
        end 
                
        `Addr_TRACK1_ETA : begin
            TRACK1_eta[7:0] <= VME_DIN[7:0];
        end
        `Addr_TRACK1_PHI : begin
            TRACK1_phi[5:0] <= VME_DIN[5:0];
        end
        `Addr_TRACK1_DELTATHETA : begin
            TRACK1_deltatheta[4:0] <= VME_DIN[4:0];
        end
        `Addr_TRACK1_MM : begin
            TRACK1_mm[1:0] <= VME_DIN[1:0];
        end
        `Addr_TRACK1_sTGC : begin
            TRACK1_stgc[1:0] <= VME_DIN[1:0];
        end
        `Addr_TRACK1_SPARE : begin
            TRACK1_spare <= VME_DIN[0];
        end     
        
        `Addr_TRACK2_ETA : begin
            TRACK2_eta[7:0] <= VME_DIN[7:0];
        end
        `Addr_TRACK2_PHI : begin
            TRACK2_phi[5:0] <= VME_DIN[5:0];
        end
        `Addr_TRACK2_DELTATHETA : begin
            TRACK2_deltatheta[4:0] <= VME_DIN[4:0];
        end
        `Addr_TRACK2_MM : begin
            TRACK2_mm[1:0] <= VME_DIN[1:0];
        end
        `Addr_TRACK2_sTGC : begin
            TRACK2_stgc[1:0] <= VME_DIN[1:0];
        end
        `Addr_TRACK2_SPARE : begin
            TRACK2_spare <= VME_DIN[0];
        end             
                  
        `Addr_TRACK3_ETA : begin
            TRACK3_eta[7:0] <= VME_DIN[7:0];
        end
        `Addr_TRACK3_PHI : begin
            TRACK3_phi[5:0] <= VME_DIN[5:0];
        end
        `Addr_TRACK3_DELTATHETA : begin
            TRACK3_deltatheta[4:0] <= VME_DIN[4:0];
        end
        `Addr_TRACK3_MM : begin
            TRACK3_mm[1:0] <= VME_DIN[1:0];
        end
        `Addr_TRACK3_sTGC : begin
            TRACK3_stgc[1:0] <= VME_DIN[1:0];
        end
        `Addr_TRACK3_SPARE : begin
            TRACK3_spare <= VME_DIN[0];
        end                   

	`Addr_TRACK0_SPARE_lane0_0 : begin
	    TRACK0_SPARE_lane0_data0<= VME_DIN[0];
	end
	`Addr_TRACK0_SPARE_lane0_1 : begin
	    TRACK0_SPARE_lane0_data1<= VME_DIN[0];
	end
	`Addr_TRACK0_SPARE_lane0_2 : begin
	    TRACK0_SPARE_lane0_data2<= VME_DIN[0];
	end
	`Addr_TRACK0_SPARE_lane0_3 : begin
	    TRACK0_SPARE_lane0_data3<= VME_DIN[0];
	end
	
	`Addr_TRACK0_SPARE_lane1_0 : begin
	    TRACK0_SPARE_lane1_data0<= VME_DIN[0];
	end
	`Addr_TRACK0_SPARE_lane1_1 : begin
	    TRACK0_SPARE_lane1_data1<= VME_DIN[0];
	end
	`Addr_TRACK0_SPARE_lane1_2 : begin
	    TRACK0_SPARE_lane1_data2<= VME_DIN[0];
	end
	`Addr_TRACK0_SPARE_lane1_3 : begin
	    TRACK0_SPARE_lane1_data3<= VME_DIN[0];
	end
	
	`Addr_TRACK0_SPARE_lane2_0 : begin
	    TRACK0_SPARE_lane2_data0<= VME_DIN[0];
	end
	`Addr_TRACK0_SPARE_lane2_1 : begin
	    TRACK0_SPARE_lane2_data1<= VME_DIN[0];
	end
	`Addr_TRACK0_SPARE_lane2_2 : begin
	    TRACK0_SPARE_lane2_data2<= VME_DIN[0];
	end
	`Addr_TRACK0_SPARE_lane2_3 : begin
	    TRACK0_SPARE_lane2_data3<= VME_DIN[0];
	end
	
	`Addr_TRACK0_SPARE_lane3_0 : begin
	    TRACK0_SPARE_lane3_data0<= VME_DIN[0];
	end
	`Addr_TRACK0_SPARE_lane3_1 : begin
	    TRACK0_SPARE_lane3_data1<= VME_DIN[0];
	end
	`Addr_TRACK0_SPARE_lane3_2 : begin
	    TRACK0_SPARE_lane3_data2<= VME_DIN[0];
	end
	`Addr_TRACK0_SPARE_lane3_3 : begin
	    TRACK0_SPARE_lane3_data3<= VME_DIN[0];
	end
	
	`Addr_TRACK0_SPARE_lane4_0 : begin
	    TRACK0_SPARE_lane4_data0<= VME_DIN[0];
	end
	`Addr_TRACK0_SPARE_lane4_1 : begin
	    TRACK0_SPARE_lane4_data1<= VME_DIN[0];
	end
	`Addr_TRACK0_SPARE_lane4_2 : begin
	    TRACK0_SPARE_lane4_data2<= VME_DIN[0];
	end
	`Addr_TRACK0_SPARE_lane4_3 : begin
	    TRACK0_SPARE_lane4_data3<= VME_DIN[0];
	end
	
	`Addr_TRACK0_SPARE_lane5_0 : begin
	    TRACK0_SPARE_lane5_data0<= VME_DIN[0];
	end
	`Addr_TRACK0_SPARE_lane5_1 : begin
	    TRACK0_SPARE_lane5_data1<= VME_DIN[0];
	end
	`Addr_TRACK0_SPARE_lane5_2 : begin
	    TRACK0_SPARE_lane5_data2<= VME_DIN[0];
	end
	`Addr_TRACK0_SPARE_lane5_3 : begin
	    TRACK0_SPARE_lane5_data3<= VME_DIN[0];
	end
	
	`Addr_TRACK0_SPARE_lane6_0 : begin
	    TRACK0_SPARE_lane6_data0<= VME_DIN[0];
	end
	`Addr_TRACK0_SPARE_lane6_1 : begin
	    TRACK0_SPARE_lane6_data1<= VME_DIN[0];
	end
	`Addr_TRACK0_SPARE_lane6_2 : begin
	    TRACK0_SPARE_lane6_data2<= VME_DIN[0];
	end
	`Addr_TRACK0_SPARE_lane6_3 : begin
	    TRACK0_SPARE_lane6_data3<= VME_DIN[0];
	end
	
	`Addr_TRACK0_sTGC_lane0_0 : begin
	    TRACK0_sTGC_lane0_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK0_sTGC_lane0_1 : begin
	    TRACK0_sTGC_lane0_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK0_sTGC_lane0_2 : begin
	    TRACK0_sTGC_lane0_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK0_sTGC_lane0_3 : begin
	    TRACK0_sTGC_lane0_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK0_sTGC_lane1_0 : begin
	    TRACK0_sTGC_lane1_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK0_sTGC_lane1_1 : begin
	    TRACK0_sTGC_lane1_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK0_sTGC_lane1_2 : begin
	    TRACK0_sTGC_lane1_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK0_sTGC_lane1_3 : begin
	    TRACK0_sTGC_lane1_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK0_sTGC_lane2_0 : begin
	    TRACK0_sTGC_lane2_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK0_sTGC_lane2_1 : begin
	    TRACK0_sTGC_lane2_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK0_sTGC_lane2_2 : begin
	    TRACK0_sTGC_lane2_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK0_sTGC_lane2_3 : begin
	    TRACK0_sTGC_lane2_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK0_sTGC_lane3_0 : begin
	    TRACK0_sTGC_lane3_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK0_sTGC_lane3_1 : begin
	    TRACK0_sTGC_lane3_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK0_sTGC_lane3_2 : begin
	    TRACK0_sTGC_lane3_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK0_sTGC_lane3_3 : begin
	    TRACK0_sTGC_lane3_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK0_sTGC_lane4_0 : begin
	    TRACK0_sTGC_lane4_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK0_sTGC_lane4_1 : begin
	    TRACK0_sTGC_lane4_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK0_sTGC_lane4_2 : begin
	    TRACK0_sTGC_lane4_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK0_sTGC_lane4_3 : begin
	    TRACK0_sTGC_lane4_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK0_sTGC_lane5_0 : begin
	    TRACK0_sTGC_lane5_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK0_sTGC_lane5_1 : begin
	    TRACK0_sTGC_lane5_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK0_sTGC_lane5_2 : begin
	    TRACK0_sTGC_lane5_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK0_sTGC_lane5_3 : begin
	    TRACK0_sTGC_lane5_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK0_sTGC_lane6_0 : begin
	    TRACK0_sTGC_lane6_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK0_sTGC_lane6_1 : begin
	    TRACK0_sTGC_lane6_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK0_sTGC_lane6_2 : begin
	    TRACK0_sTGC_lane6_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK0_sTGC_lane6_3 : begin
	    TRACK0_sTGC_lane6_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK0_MM_lane0_0 : begin
	    TRACK0_MM_lane0_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK0_MM_lane0_1 : begin
	    TRACK0_MM_lane0_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK0_MM_lane0_2 : begin
	    TRACK0_MM_lane0_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK0_MM_lane0_3 : begin
	    TRACK0_MM_lane0_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK0_MM_lane1_0 : begin
	    TRACK0_MM_lane1_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK0_MM_lane1_1 : begin
	    TRACK0_MM_lane1_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK0_MM_lane1_2 : begin
	    TRACK0_MM_lane1_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK0_MM_lane1_3 : begin
	    TRACK0_MM_lane1_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK0_MM_lane2_0 : begin
	    TRACK0_MM_lane2_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK0_MM_lane2_1 : begin
	    TRACK0_MM_lane2_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK0_MM_lane2_2 : begin
	    TRACK0_MM_lane2_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK0_MM_lane2_3 : begin
	    TRACK0_MM_lane2_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK0_MM_lane3_0 : begin
	    TRACK0_MM_lane3_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK0_MM_lane3_1 : begin
	    TRACK0_MM_lane3_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK0_MM_lane3_2 : begin
	    TRACK0_MM_lane3_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK0_MM_lane3_3 : begin
	    TRACK0_MM_lane3_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK0_MM_lane4_0 : begin
	    TRACK0_MM_lane4_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK0_MM_lane4_1 : begin
	    TRACK0_MM_lane4_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK0_MM_lane4_2 : begin
	    TRACK0_MM_lane4_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK0_MM_lane4_3 : begin
	    TRACK0_MM_lane4_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK0_MM_lane5_0 : begin
	    TRACK0_MM_lane5_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK0_MM_lane5_1 : begin
	    TRACK0_MM_lane5_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK0_MM_lane5_2 : begin
	    TRACK0_MM_lane5_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK0_MM_lane5_3 : begin
	    TRACK0_MM_lane5_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK0_MM_lane6_0 : begin
	    TRACK0_MM_lane6_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK0_MM_lane6_1 : begin
	    TRACK0_MM_lane6_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK0_MM_lane6_2 : begin
	    TRACK0_MM_lane6_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK0_MM_lane6_3 : begin
	    TRACK0_MM_lane6_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK0_DTHETA_lane0_0 : begin
	    TRACK0_DTHETA_lane0_data0[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK0_DTHETA_lane0_1 : begin
	    TRACK0_DTHETA_lane0_data1[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK0_DTHETA_lane0_2 : begin
	    TRACK0_DTHETA_lane0_data2[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK0_DTHETA_lane0_3 : begin
	    TRACK0_DTHETA_lane0_data3[4:0] <= VME_DIN[4:0];
	end
	
	`Addr_TRACK0_DTHETA_lane1_0 : begin
	    TRACK0_DTHETA_lane1_data0[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK0_DTHETA_lane1_1 : begin
	    TRACK0_DTHETA_lane1_data1[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK0_DTHETA_lane1_2 : begin
	    TRACK0_DTHETA_lane1_data2[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK0_DTHETA_lane1_3 : begin
	    TRACK0_DTHETA_lane1_data3[4:0] <= VME_DIN[4:0];
	end
	
	`Addr_TRACK0_DTHETA_lane2_0 : begin
	    TRACK0_DTHETA_lane2_data0[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK0_DTHETA_lane2_1 : begin
	    TRACK0_DTHETA_lane2_data1[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK0_DTHETA_lane2_2 : begin
	    TRACK0_DTHETA_lane2_data2[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK0_DTHETA_lane2_3 : begin
	    TRACK0_DTHETA_lane2_data3[4:0] <= VME_DIN[4:0];
	end
	
	`Addr_TRACK0_DTHETA_lane3_0 : begin
	    TRACK0_DTHETA_lane3_data0[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK0_DTHETA_lane3_1 : begin
	    TRACK0_DTHETA_lane3_data1[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK0_DTHETA_lane3_2 : begin
	    TRACK0_DTHETA_lane3_data2[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK0_DTHETA_lane3_3 : begin
	    TRACK0_DTHETA_lane3_data3[4:0] <= VME_DIN[4:0];
	end
	
	`Addr_TRACK0_DTHETA_lane4_0 : begin
	    TRACK0_DTHETA_lane4_data0[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK0_DTHETA_lane4_1 : begin
	    TRACK0_DTHETA_lane4_data1[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK0_DTHETA_lane4_2 : begin
	    TRACK0_DTHETA_lane4_data2[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK0_DTHETA_lane4_3 : begin
	    TRACK0_DTHETA_lane4_data3[4:0] <= VME_DIN[4:0];
	end
	
	`Addr_TRACK0_DTHETA_lane5_0 : begin
	    TRACK0_DTHETA_lane5_data0[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK0_DTHETA_lane5_1 : begin
	    TRACK0_DTHETA_lane5_data1[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK0_DTHETA_lane5_2 : begin
	    TRACK0_DTHETA_lane5_data2[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK0_DTHETA_lane5_3 : begin
	    TRACK0_DTHETA_lane5_data3[4:0] <= VME_DIN[4:0];
	end
	
	`Addr_TRACK0_DTHETA_lane6_0 : begin
	    TRACK0_DTHETA_lane6_data0[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK0_DTHETA_lane6_1 : begin
	    TRACK0_DTHETA_lane6_data1[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK0_DTHETA_lane6_2 : begin
	    TRACK0_DTHETA_lane6_data2[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK0_DTHETA_lane6_3 : begin
	    TRACK0_DTHETA_lane6_data3[4:0] <= VME_DIN[4:0];
	end
	
	`Addr_TRACK0_PHI_lane0_0 : begin
	    TRACK0_PHI_lane0_data0[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK0_PHI_lane0_1 : begin
	    TRACK0_PHI_lane0_data1[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK0_PHI_lane0_2 : begin
	    TRACK0_PHI_lane0_data2[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK0_PHI_lane0_3 : begin
	    TRACK0_PHI_lane0_data3[5:0] <= VME_DIN[5:0];
	end
	
	`Addr_TRACK0_PHI_lane1_0 : begin
	    TRACK0_PHI_lane1_data0[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK0_PHI_lane1_1 : begin
	    TRACK0_PHI_lane1_data1[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK0_PHI_lane1_2 : begin
	    TRACK0_PHI_lane1_data2[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK0_PHI_lane1_3 : begin
	    TRACK0_PHI_lane1_data3[5:0] <= VME_DIN[5:0];
	end
	
	`Addr_TRACK0_PHI_lane2_0 : begin
	    TRACK0_PHI_lane2_data0[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK0_PHI_lane2_1 : begin
	    TRACK0_PHI_lane2_data1[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK0_PHI_lane2_2 : begin
	    TRACK0_PHI_lane2_data2[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK0_PHI_lane2_3 : begin
	    TRACK0_PHI_lane2_data3[5:0] <= VME_DIN[5:0];
	end
	
	`Addr_TRACK0_PHI_lane3_0 : begin
	    TRACK0_PHI_lane3_data0[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK0_PHI_lane3_1 : begin
	    TRACK0_PHI_lane3_data1[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK0_PHI_lane3_2 : begin
	    TRACK0_PHI_lane3_data2[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK0_PHI_lane3_3 : begin
	    TRACK0_PHI_lane3_data3[5:0] <= VME_DIN[5:0];
	end
	
	`Addr_TRACK0_PHI_lane4_0 : begin
	    TRACK0_PHI_lane4_data0[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK0_PHI_lane4_1 : begin
	    TRACK0_PHI_lane4_data1[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK0_PHI_lane4_2 : begin
	    TRACK0_PHI_lane4_data2[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK0_PHI_lane4_3 : begin
	    TRACK0_PHI_lane4_data3[5:0] <= VME_DIN[5:0];
	end
	
	`Addr_TRACK0_PHI_lane5_0 : begin
	    TRACK0_PHI_lane5_data0[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK0_PHI_lane5_1 : begin
	    TRACK0_PHI_lane5_data1[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK0_PHI_lane5_2 : begin
	    TRACK0_PHI_lane5_data2[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK0_PHI_lane5_3 : begin
	    TRACK0_PHI_lane5_data3[5:0] <= VME_DIN[5:0];
	end
	
	`Addr_TRACK0_PHI_lane6_0 : begin
	    TRACK0_PHI_lane6_data0[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK0_PHI_lane6_1 : begin
	    TRACK0_PHI_lane6_data1[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK0_PHI_lane6_2 : begin
	    TRACK0_PHI_lane6_data2[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK0_PHI_lane6_3 : begin
	    TRACK0_PHI_lane6_data3[5:0] <= VME_DIN[5:0];
	end
	
	`Addr_TRACK0_ETA_lane0_0 : begin
	    TRACK0_ETA_lane0_data0[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK0_ETA_lane0_1 : begin
	    TRACK0_ETA_lane0_data1[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK0_ETA_lane0_2 : begin
	    TRACK0_ETA_lane0_data2[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK0_ETA_lane0_3 : begin
	    TRACK0_ETA_lane0_data3[7:0] <= VME_DIN[7:0];
	end
	
	`Addr_TRACK0_ETA_lane1_0 : begin
	    TRACK0_ETA_lane1_data0[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK0_ETA_lane1_1 : begin
	    TRACK0_ETA_lane1_data1[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK0_ETA_lane1_2 : begin
	    TRACK0_ETA_lane1_data2[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK0_ETA_lane1_3 : begin
	    TRACK0_ETA_lane1_data3[7:0] <= VME_DIN[7:0];
	end
	
	`Addr_TRACK0_ETA_lane2_0 : begin
	    TRACK0_ETA_lane2_data0[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK0_ETA_lane2_1 : begin
	    TRACK0_ETA_lane2_data1[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK0_ETA_lane2_2 : begin
	    TRACK0_ETA_lane2_data2[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK0_ETA_lane2_3 : begin
	    TRACK0_ETA_lane2_data3[7:0] <= VME_DIN[7:0];
	end
	
	`Addr_TRACK0_ETA_lane3_0 : begin
	    TRACK0_ETA_lane3_data0[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK0_ETA_lane3_1 : begin
	    TRACK0_ETA_lane3_data1[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK0_ETA_lane3_2 : begin
	    TRACK0_ETA_lane3_data2[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK0_ETA_lane3_3 : begin
	    TRACK0_ETA_lane3_data3[7:0] <= VME_DIN[7:0];
	end
	
	`Addr_TRACK0_ETA_lane4_0 : begin
	    TRACK0_ETA_lane4_data0[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK0_ETA_lane4_1 : begin
	    TRACK0_ETA_lane4_data1[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK0_ETA_lane4_2 : begin
	    TRACK0_ETA_lane4_data2[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK0_ETA_lane4_3 : begin
	    TRACK0_ETA_lane4_data3[7:0] <= VME_DIN[7:0];
	end
	
	`Addr_TRACK0_ETA_lane5_0 : begin
	    TRACK0_ETA_lane5_data0[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK0_ETA_lane5_1 : begin
	    TRACK0_ETA_lane5_data1[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK0_ETA_lane5_2 : begin
	    TRACK0_ETA_lane5_data2[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK0_ETA_lane5_3 : begin
	    TRACK0_ETA_lane5_data3[7:0] <= VME_DIN[7:0];
	end
	
	`Addr_TRACK0_ETA_lane6_0 : begin
	    TRACK0_ETA_lane6_data0[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK0_ETA_lane6_1 : begin
	    TRACK0_ETA_lane6_data1[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK0_ETA_lane6_2 : begin
	    TRACK0_ETA_lane6_data2[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK0_ETA_lane6_3 : begin
	    TRACK0_ETA_lane6_data3[7:0] <= VME_DIN[7:0];
	end
	
	`Addr_TRACK1_SPARE_lane0_0 : begin
	    TRACK1_SPARE_lane0_data0<= VME_DIN[0];
	end
	`Addr_TRACK1_SPARE_lane0_1 : begin
	    TRACK1_SPARE_lane0_data1<= VME_DIN[0];
	end
	`Addr_TRACK1_SPARE_lane0_2 : begin
	    TRACK1_SPARE_lane0_data2<= VME_DIN[0];
	end
	`Addr_TRACK1_SPARE_lane0_3 : begin
	    TRACK1_SPARE_lane0_data3<= VME_DIN[0];
	end
	
	`Addr_TRACK1_SPARE_lane1_0 : begin
	    TRACK1_SPARE_lane1_data0<= VME_DIN[0];
	end
	`Addr_TRACK1_SPARE_lane1_1 : begin
	    TRACK1_SPARE_lane1_data1<= VME_DIN[0];
	end
	`Addr_TRACK1_SPARE_lane1_2 : begin
	    TRACK1_SPARE_lane1_data2<= VME_DIN[0];
	end
	`Addr_TRACK1_SPARE_lane1_3 : begin
	    TRACK1_SPARE_lane1_data3<= VME_DIN[0];
	end
	
	`Addr_TRACK1_SPARE_lane2_0 : begin
	    TRACK1_SPARE_lane2_data0<= VME_DIN[0];
	end
	`Addr_TRACK1_SPARE_lane2_1 : begin
	    TRACK1_SPARE_lane2_data1<= VME_DIN[0];
	end
	`Addr_TRACK1_SPARE_lane2_2 : begin
	    TRACK1_SPARE_lane2_data2<= VME_DIN[0];
	end
	`Addr_TRACK1_SPARE_lane2_3 : begin
	    TRACK1_SPARE_lane2_data3<= VME_DIN[0];
	end
	
	`Addr_TRACK1_SPARE_lane3_0 : begin
	    TRACK1_SPARE_lane3_data0<= VME_DIN[0];
	end
	`Addr_TRACK1_SPARE_lane3_1 : begin
	    TRACK1_SPARE_lane3_data1<= VME_DIN[0];
	end
	`Addr_TRACK1_SPARE_lane3_2 : begin
	    TRACK1_SPARE_lane3_data2<= VME_DIN[0];
	end
	`Addr_TRACK1_SPARE_lane3_3 : begin
	    TRACK1_SPARE_lane3_data3<= VME_DIN[0];
	end
	
	`Addr_TRACK1_SPARE_lane4_0 : begin
	    TRACK1_SPARE_lane4_data0<= VME_DIN[0];
	end
	`Addr_TRACK1_SPARE_lane4_1 : begin
	    TRACK1_SPARE_lane4_data1<= VME_DIN[0];
	end
	`Addr_TRACK1_SPARE_lane4_2 : begin
	    TRACK1_SPARE_lane4_data2<= VME_DIN[0];
	end
	`Addr_TRACK1_SPARE_lane4_3 : begin
	    TRACK1_SPARE_lane4_data3<= VME_DIN[0];
	end
	
	`Addr_TRACK1_SPARE_lane5_0 : begin
	    TRACK1_SPARE_lane5_data0<= VME_DIN[0];
	end
	`Addr_TRACK1_SPARE_lane5_1 : begin
	    TRACK1_SPARE_lane5_data1<= VME_DIN[0];
	end
	`Addr_TRACK1_SPARE_lane5_2 : begin
	    TRACK1_SPARE_lane5_data2<= VME_DIN[0];
	end
	`Addr_TRACK1_SPARE_lane5_3 : begin
	    TRACK1_SPARE_lane5_data3<= VME_DIN[0];
	end
	
	`Addr_TRACK1_SPARE_lane6_0 : begin
	    TRACK1_SPARE_lane6_data0<= VME_DIN[0];
	end
	`Addr_TRACK1_SPARE_lane6_1 : begin
	    TRACK1_SPARE_lane6_data1<= VME_DIN[0];
	end
	`Addr_TRACK1_SPARE_lane6_2 : begin
	    TRACK1_SPARE_lane6_data2<= VME_DIN[0];
	end
	`Addr_TRACK1_SPARE_lane6_3 : begin
	    TRACK1_SPARE_lane6_data3<= VME_DIN[0];
	end
	
	`Addr_TRACK1_sTGC_lane0_0 : begin
	    TRACK1_sTGC_lane0_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK1_sTGC_lane0_1 : begin
	    TRACK1_sTGC_lane0_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK1_sTGC_lane0_2 : begin
	    TRACK1_sTGC_lane0_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK1_sTGC_lane0_3 : begin
	    TRACK1_sTGC_lane0_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK1_sTGC_lane1_0 : begin
	    TRACK1_sTGC_lane1_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK1_sTGC_lane1_1 : begin
	    TRACK1_sTGC_lane1_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK1_sTGC_lane1_2 : begin
	    TRACK1_sTGC_lane1_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK1_sTGC_lane1_3 : begin
	    TRACK1_sTGC_lane1_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK1_sTGC_lane2_0 : begin
	    TRACK1_sTGC_lane2_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK1_sTGC_lane2_1 : begin
	    TRACK1_sTGC_lane2_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK1_sTGC_lane2_2 : begin
	    TRACK1_sTGC_lane2_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK1_sTGC_lane2_3 : begin
	    TRACK1_sTGC_lane2_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK1_sTGC_lane3_0 : begin
	    TRACK1_sTGC_lane3_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK1_sTGC_lane3_1 : begin
	    TRACK1_sTGC_lane3_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK1_sTGC_lane3_2 : begin
	    TRACK1_sTGC_lane3_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK1_sTGC_lane3_3 : begin
	    TRACK1_sTGC_lane3_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK1_sTGC_lane4_0 : begin
	    TRACK1_sTGC_lane4_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK1_sTGC_lane4_1 : begin
	    TRACK1_sTGC_lane4_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK1_sTGC_lane4_2 : begin
	    TRACK1_sTGC_lane4_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK1_sTGC_lane4_3 : begin
	    TRACK1_sTGC_lane4_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK1_sTGC_lane5_0 : begin
	    TRACK1_sTGC_lane5_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK1_sTGC_lane5_1 : begin
	    TRACK1_sTGC_lane5_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK1_sTGC_lane5_2 : begin
	    TRACK1_sTGC_lane5_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK1_sTGC_lane5_3 : begin
	    TRACK1_sTGC_lane5_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK1_sTGC_lane6_0 : begin
	    TRACK1_sTGC_lane6_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK1_sTGC_lane6_1 : begin
	    TRACK1_sTGC_lane6_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK1_sTGC_lane6_2 : begin
	    TRACK1_sTGC_lane6_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK1_sTGC_lane6_3 : begin
	    TRACK1_sTGC_lane6_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK1_MM_lane0_0 : begin
	    TRACK1_MM_lane0_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK1_MM_lane0_1 : begin
	    TRACK1_MM_lane0_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK1_MM_lane0_2 : begin
	    TRACK1_MM_lane0_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK1_MM_lane0_3 : begin
	    TRACK1_MM_lane0_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK1_MM_lane1_0 : begin
	    TRACK1_MM_lane1_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK1_MM_lane1_1 : begin
	    TRACK1_MM_lane1_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK1_MM_lane1_2 : begin
	    TRACK1_MM_lane1_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK1_MM_lane1_3 : begin
	    TRACK1_MM_lane1_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK1_MM_lane2_0 : begin
	    TRACK1_MM_lane2_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK1_MM_lane2_1 : begin
	    TRACK1_MM_lane2_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK1_MM_lane2_2 : begin
	    TRACK1_MM_lane2_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK1_MM_lane2_3 : begin
	    TRACK1_MM_lane2_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK1_MM_lane3_0 : begin
	    TRACK1_MM_lane3_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK1_MM_lane3_1 : begin
	    TRACK1_MM_lane3_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK1_MM_lane3_2 : begin
	    TRACK1_MM_lane3_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK1_MM_lane3_3 : begin
	    TRACK1_MM_lane3_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK1_MM_lane4_0 : begin
	    TRACK1_MM_lane4_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK1_MM_lane4_1 : begin
	    TRACK1_MM_lane4_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK1_MM_lane4_2 : begin
	    TRACK1_MM_lane4_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK1_MM_lane4_3 : begin
	    TRACK1_MM_lane4_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK1_MM_lane5_0 : begin
	    TRACK1_MM_lane5_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK1_MM_lane5_1 : begin
	    TRACK1_MM_lane5_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK1_MM_lane5_2 : begin
	    TRACK1_MM_lane5_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK1_MM_lane5_3 : begin
	    TRACK1_MM_lane5_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK1_MM_lane6_0 : begin
	    TRACK1_MM_lane6_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK1_MM_lane6_1 : begin
	    TRACK1_MM_lane6_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK1_MM_lane6_2 : begin
	    TRACK1_MM_lane6_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK1_MM_lane6_3 : begin
	    TRACK1_MM_lane6_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK1_DTHETA_lane0_0 : begin
	    TRACK1_DTHETA_lane0_data0[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK1_DTHETA_lane0_1 : begin
	    TRACK1_DTHETA_lane0_data1[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK1_DTHETA_lane0_2 : begin
	    TRACK1_DTHETA_lane0_data2[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK1_DTHETA_lane0_3 : begin
	    TRACK1_DTHETA_lane0_data3[4:0] <= VME_DIN[4:0];
	end
	
	`Addr_TRACK1_DTHETA_lane1_0 : begin
	    TRACK1_DTHETA_lane1_data0[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK1_DTHETA_lane1_1 : begin
	    TRACK1_DTHETA_lane1_data1[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK1_DTHETA_lane1_2 : begin
	    TRACK1_DTHETA_lane1_data2[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK1_DTHETA_lane1_3 : begin
	    TRACK1_DTHETA_lane1_data3[4:0] <= VME_DIN[4:0];
	end
	
	`Addr_TRACK1_DTHETA_lane2_0 : begin
	    TRACK1_DTHETA_lane2_data0[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK1_DTHETA_lane2_1 : begin
	    TRACK1_DTHETA_lane2_data1[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK1_DTHETA_lane2_2 : begin
	    TRACK1_DTHETA_lane2_data2[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK1_DTHETA_lane2_3 : begin
	    TRACK1_DTHETA_lane2_data3[4:0] <= VME_DIN[4:0];
	end
	
	`Addr_TRACK1_DTHETA_lane3_0 : begin
	    TRACK1_DTHETA_lane3_data0[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK1_DTHETA_lane3_1 : begin
	    TRACK1_DTHETA_lane3_data1[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK1_DTHETA_lane3_2 : begin
	    TRACK1_DTHETA_lane3_data2[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK1_DTHETA_lane3_3 : begin
	    TRACK1_DTHETA_lane3_data3[4:0] <= VME_DIN[4:0];
	end
	
	`Addr_TRACK1_DTHETA_lane4_0 : begin
	    TRACK1_DTHETA_lane4_data0[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK1_DTHETA_lane4_1 : begin
	    TRACK1_DTHETA_lane4_data1[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK1_DTHETA_lane4_2 : begin
	    TRACK1_DTHETA_lane4_data2[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK1_DTHETA_lane4_3 : begin
	    TRACK1_DTHETA_lane4_data3[4:0] <= VME_DIN[4:0];
	end
	
	`Addr_TRACK1_DTHETA_lane5_0 : begin
	    TRACK1_DTHETA_lane5_data0[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK1_DTHETA_lane5_1 : begin
	    TRACK1_DTHETA_lane5_data1[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK1_DTHETA_lane5_2 : begin
	    TRACK1_DTHETA_lane5_data2[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK1_DTHETA_lane5_3 : begin
	    TRACK1_DTHETA_lane5_data3[4:0] <= VME_DIN[4:0];
	end
	
	`Addr_TRACK1_DTHETA_lane6_0 : begin
	    TRACK1_DTHETA_lane6_data0[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK1_DTHETA_lane6_1 : begin
	    TRACK1_DTHETA_lane6_data1[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK1_DTHETA_lane6_2 : begin
	    TRACK1_DTHETA_lane6_data2[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK1_DTHETA_lane6_3 : begin
	    TRACK1_DTHETA_lane6_data3[4:0] <= VME_DIN[4:0];
	end
	
	`Addr_TRACK1_PHI_lane0_0 : begin
	    TRACK1_PHI_lane0_data0[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK1_PHI_lane0_1 : begin
	    TRACK1_PHI_lane0_data1[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK1_PHI_lane0_2 : begin
	    TRACK1_PHI_lane0_data2[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK1_PHI_lane0_3 : begin
	    TRACK1_PHI_lane0_data3[5:0] <= VME_DIN[5:0];
	end
	
	`Addr_TRACK1_PHI_lane1_0 : begin
	    TRACK1_PHI_lane1_data0[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK1_PHI_lane1_1 : begin
	    TRACK1_PHI_lane1_data1[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK1_PHI_lane1_2 : begin
	    TRACK1_PHI_lane1_data2[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK1_PHI_lane1_3 : begin
	    TRACK1_PHI_lane1_data3[5:0] <= VME_DIN[5:0];
	end
	
	`Addr_TRACK1_PHI_lane2_0 : begin
	    TRACK1_PHI_lane2_data0[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK1_PHI_lane2_1 : begin
	    TRACK1_PHI_lane2_data1[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK1_PHI_lane2_2 : begin
	    TRACK1_PHI_lane2_data2[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK1_PHI_lane2_3 : begin
	    TRACK1_PHI_lane2_data3[5:0] <= VME_DIN[5:0];
	end
	
	`Addr_TRACK1_PHI_lane3_0 : begin
	    TRACK1_PHI_lane3_data0[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK1_PHI_lane3_1 : begin
	    TRACK1_PHI_lane3_data1[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK1_PHI_lane3_2 : begin
	    TRACK1_PHI_lane3_data2[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK1_PHI_lane3_3 : begin
	    TRACK1_PHI_lane3_data3[5:0] <= VME_DIN[5:0];
	end
	
	`Addr_TRACK1_PHI_lane4_0 : begin
	    TRACK1_PHI_lane4_data0[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK1_PHI_lane4_1 : begin
	    TRACK1_PHI_lane4_data1[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK1_PHI_lane4_2 : begin
	    TRACK1_PHI_lane4_data2[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK1_PHI_lane4_3 : begin
	    TRACK1_PHI_lane4_data3[5:0] <= VME_DIN[5:0];
	end
	
	`Addr_TRACK1_PHI_lane5_0 : begin
	    TRACK1_PHI_lane5_data0[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK1_PHI_lane5_1 : begin
	    TRACK1_PHI_lane5_data1[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK1_PHI_lane5_2 : begin
	    TRACK1_PHI_lane5_data2[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK1_PHI_lane5_3 : begin
	    TRACK1_PHI_lane5_data3[5:0] <= VME_DIN[5:0];
	end
	
	`Addr_TRACK1_PHI_lane6_0 : begin
	    TRACK1_PHI_lane6_data0[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK1_PHI_lane6_1 : begin
	    TRACK1_PHI_lane6_data1[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK1_PHI_lane6_2 : begin
	    TRACK1_PHI_lane6_data2[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK1_PHI_lane6_3 : begin
	    TRACK1_PHI_lane6_data3[5:0] <= VME_DIN[5:0];
	end
	
	`Addr_TRACK1_ETA_lane0_0 : begin
	    TRACK1_ETA_lane0_data0[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK1_ETA_lane0_1 : begin
	    TRACK1_ETA_lane0_data1[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK1_ETA_lane0_2 : begin
	    TRACK1_ETA_lane0_data2[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK1_ETA_lane0_3 : begin
	    TRACK1_ETA_lane0_data3[7:0] <= VME_DIN[7:0];
	end
	
	`Addr_TRACK1_ETA_lane1_0 : begin
	    TRACK1_ETA_lane1_data0[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK1_ETA_lane1_1 : begin
	    TRACK1_ETA_lane1_data1[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK1_ETA_lane1_2 : begin
	    TRACK1_ETA_lane1_data2[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK1_ETA_lane1_3 : begin
	    TRACK1_ETA_lane1_data3[7:0] <= VME_DIN[7:0];
	end
	
	`Addr_TRACK1_ETA_lane2_0 : begin
	    TRACK1_ETA_lane2_data0[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK1_ETA_lane2_1 : begin
	    TRACK1_ETA_lane2_data1[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK1_ETA_lane2_2 : begin
	    TRACK1_ETA_lane2_data2[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK1_ETA_lane2_3 : begin
	    TRACK1_ETA_lane2_data3[7:0] <= VME_DIN[7:0];
	end
	
	`Addr_TRACK1_ETA_lane3_0 : begin
	    TRACK1_ETA_lane3_data0[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK1_ETA_lane3_1 : begin
	    TRACK1_ETA_lane3_data1[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK1_ETA_lane3_2 : begin
	    TRACK1_ETA_lane3_data2[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK1_ETA_lane3_3 : begin
	    TRACK1_ETA_lane3_data3[7:0] <= VME_DIN[7:0];
	end
	
	`Addr_TRACK1_ETA_lane4_0 : begin
	    TRACK1_ETA_lane4_data0[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK1_ETA_lane4_1 : begin
	    TRACK1_ETA_lane4_data1[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK1_ETA_lane4_2 : begin
	    TRACK1_ETA_lane4_data2[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK1_ETA_lane4_3 : begin
	    TRACK1_ETA_lane4_data3[7:0] <= VME_DIN[7:0];
	end
	
	`Addr_TRACK1_ETA_lane5_0 : begin
	    TRACK1_ETA_lane5_data0[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK1_ETA_lane5_1 : begin
	    TRACK1_ETA_lane5_data1[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK1_ETA_lane5_2 : begin
	    TRACK1_ETA_lane5_data2[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK1_ETA_lane5_3 : begin
	    TRACK1_ETA_lane5_data3[7:0] <= VME_DIN[7:0];
	end
	
	`Addr_TRACK1_ETA_lane6_0 : begin
	    TRACK1_ETA_lane6_data0[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK1_ETA_lane6_1 : begin
	    TRACK1_ETA_lane6_data1[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK1_ETA_lane6_2 : begin
	    TRACK1_ETA_lane6_data2[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK1_ETA_lane6_3 : begin
	    TRACK1_ETA_lane6_data3[7:0] <= VME_DIN[7:0];
	end
	
	`Addr_TRACK2_SPARE_lane0_0 : begin
	    TRACK2_SPARE_lane0_data0<= VME_DIN[0];
	end
	`Addr_TRACK2_SPARE_lane0_1 : begin
	    TRACK2_SPARE_lane0_data1<= VME_DIN[0];
	end
	`Addr_TRACK2_SPARE_lane0_2 : begin
	    TRACK2_SPARE_lane0_data2<= VME_DIN[0];
	end
	`Addr_TRACK2_SPARE_lane0_3 : begin
	    TRACK2_SPARE_lane0_data3<= VME_DIN[0];
	end
	
	`Addr_TRACK2_SPARE_lane1_0 : begin
	    TRACK2_SPARE_lane1_data0<= VME_DIN[0];
	end
	`Addr_TRACK2_SPARE_lane1_1 : begin
	    TRACK2_SPARE_lane1_data1<= VME_DIN[0];
	end
	`Addr_TRACK2_SPARE_lane1_2 : begin
	    TRACK2_SPARE_lane1_data2<= VME_DIN[0];
	end
	`Addr_TRACK2_SPARE_lane1_3 : begin
	    TRACK2_SPARE_lane1_data3<= VME_DIN[0];
	end
	
	`Addr_TRACK2_SPARE_lane2_0 : begin
	    TRACK2_SPARE_lane2_data0<= VME_DIN[0];
	end
	`Addr_TRACK2_SPARE_lane2_1 : begin
	    TRACK2_SPARE_lane2_data1<= VME_DIN[0];
	end
	`Addr_TRACK2_SPARE_lane2_2 : begin
	    TRACK2_SPARE_lane2_data2<= VME_DIN[0];
	end
	`Addr_TRACK2_SPARE_lane2_3 : begin
	    TRACK2_SPARE_lane2_data3<= VME_DIN[0];
	end
	
	`Addr_TRACK2_SPARE_lane3_0 : begin
	    TRACK2_SPARE_lane3_data0<= VME_DIN[0];
	end
	`Addr_TRACK2_SPARE_lane3_1 : begin
	    TRACK2_SPARE_lane3_data1<= VME_DIN[0];
	end
	`Addr_TRACK2_SPARE_lane3_2 : begin
	    TRACK2_SPARE_lane3_data2<= VME_DIN[0];
	end
	`Addr_TRACK2_SPARE_lane3_3 : begin
	    TRACK2_SPARE_lane3_data3<= VME_DIN[0];
	end
	
	`Addr_TRACK2_SPARE_lane4_0 : begin
	    TRACK2_SPARE_lane4_data0<= VME_DIN[0];
	end
	`Addr_TRACK2_SPARE_lane4_1 : begin
	    TRACK2_SPARE_lane4_data1<= VME_DIN[0];
	end
	`Addr_TRACK2_SPARE_lane4_2 : begin
	    TRACK2_SPARE_lane4_data2<= VME_DIN[0];
	end
	`Addr_TRACK2_SPARE_lane4_3 : begin
	    TRACK2_SPARE_lane4_data3<= VME_DIN[0];
	end
	
	`Addr_TRACK2_SPARE_lane5_0 : begin
	    TRACK2_SPARE_lane5_data0<= VME_DIN[0];
	end
	`Addr_TRACK2_SPARE_lane5_1 : begin
	    TRACK2_SPARE_lane5_data1<= VME_DIN[0];
	end
	`Addr_TRACK2_SPARE_lane5_2 : begin
	    TRACK2_SPARE_lane5_data2<= VME_DIN[0];
	end
	`Addr_TRACK2_SPARE_lane5_3 : begin
	    TRACK2_SPARE_lane5_data3<= VME_DIN[0];
	end
	
	`Addr_TRACK2_SPARE_lane6_0 : begin
	    TRACK2_SPARE_lane6_data0<= VME_DIN[0];
	end
	`Addr_TRACK2_SPARE_lane6_1 : begin
	    TRACK2_SPARE_lane6_data1<= VME_DIN[0];
	end
	`Addr_TRACK2_SPARE_lane6_2 : begin
	    TRACK2_SPARE_lane6_data2<= VME_DIN[0];
	end
	`Addr_TRACK2_SPARE_lane6_3 : begin
	    TRACK2_SPARE_lane6_data3<= VME_DIN[0];
	end
	
	`Addr_TRACK2_sTGC_lane0_0 : begin
	    TRACK2_sTGC_lane0_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK2_sTGC_lane0_1 : begin
	    TRACK2_sTGC_lane0_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK2_sTGC_lane0_2 : begin
	    TRACK2_sTGC_lane0_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK2_sTGC_lane0_3 : begin
	    TRACK2_sTGC_lane0_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK2_sTGC_lane1_0 : begin
	    TRACK2_sTGC_lane1_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK2_sTGC_lane1_1 : begin
	    TRACK2_sTGC_lane1_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK2_sTGC_lane1_2 : begin
	    TRACK2_sTGC_lane1_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK2_sTGC_lane1_3 : begin
	    TRACK2_sTGC_lane1_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK2_sTGC_lane2_0 : begin
	    TRACK2_sTGC_lane2_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK2_sTGC_lane2_1 : begin
	    TRACK2_sTGC_lane2_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK2_sTGC_lane2_2 : begin
	    TRACK2_sTGC_lane2_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK2_sTGC_lane2_3 : begin
	    TRACK2_sTGC_lane2_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK2_sTGC_lane3_0 : begin
	    TRACK2_sTGC_lane3_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK2_sTGC_lane3_1 : begin
	    TRACK2_sTGC_lane3_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK2_sTGC_lane3_2 : begin
	    TRACK2_sTGC_lane3_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK2_sTGC_lane3_3 : begin
	    TRACK2_sTGC_lane3_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK2_sTGC_lane4_0 : begin
	    TRACK2_sTGC_lane4_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK2_sTGC_lane4_1 : begin
	    TRACK2_sTGC_lane4_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK2_sTGC_lane4_2 : begin
	    TRACK2_sTGC_lane4_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK2_sTGC_lane4_3 : begin
	    TRACK2_sTGC_lane4_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK2_sTGC_lane5_0 : begin
	    TRACK2_sTGC_lane5_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK2_sTGC_lane5_1 : begin
	    TRACK2_sTGC_lane5_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK2_sTGC_lane5_2 : begin
	    TRACK2_sTGC_lane5_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK2_sTGC_lane5_3 : begin
	    TRACK2_sTGC_lane5_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK2_sTGC_lane6_0 : begin
	    TRACK2_sTGC_lane6_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK2_sTGC_lane6_1 : begin
	    TRACK2_sTGC_lane6_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK2_sTGC_lane6_2 : begin
	    TRACK2_sTGC_lane6_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK2_sTGC_lane6_3 : begin
	    TRACK2_sTGC_lane6_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK2_MM_lane0_0 : begin
	    TRACK2_MM_lane0_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK2_MM_lane0_1 : begin
	    TRACK2_MM_lane0_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK2_MM_lane0_2 : begin
	    TRACK2_MM_lane0_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK2_MM_lane0_3 : begin
	    TRACK2_MM_lane0_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK2_MM_lane1_0 : begin
	    TRACK2_MM_lane1_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK2_MM_lane1_1 : begin
	    TRACK2_MM_lane1_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK2_MM_lane1_2 : begin
	    TRACK2_MM_lane1_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK2_MM_lane1_3 : begin
	    TRACK2_MM_lane1_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK2_MM_lane2_0 : begin
	    TRACK2_MM_lane2_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK2_MM_lane2_1 : begin
	    TRACK2_MM_lane2_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK2_MM_lane2_2 : begin
	    TRACK2_MM_lane2_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK2_MM_lane2_3 : begin
	    TRACK2_MM_lane2_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK2_MM_lane3_0 : begin
	    TRACK2_MM_lane3_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK2_MM_lane3_1 : begin
	    TRACK2_MM_lane3_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK2_MM_lane3_2 : begin
	    TRACK2_MM_lane3_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK2_MM_lane3_3 : begin
	    TRACK2_MM_lane3_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK2_MM_lane4_0 : begin
	    TRACK2_MM_lane4_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK2_MM_lane4_1 : begin
	    TRACK2_MM_lane4_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK2_MM_lane4_2 : begin
	    TRACK2_MM_lane4_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK2_MM_lane4_3 : begin
	    TRACK2_MM_lane4_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK2_MM_lane5_0 : begin
	    TRACK2_MM_lane5_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK2_MM_lane5_1 : begin
	    TRACK2_MM_lane5_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK2_MM_lane5_2 : begin
	    TRACK2_MM_lane5_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK2_MM_lane5_3 : begin
	    TRACK2_MM_lane5_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK2_MM_lane6_0 : begin
	    TRACK2_MM_lane6_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK2_MM_lane6_1 : begin
	    TRACK2_MM_lane6_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK2_MM_lane6_2 : begin
	    TRACK2_MM_lane6_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK2_MM_lane6_3 : begin
	    TRACK2_MM_lane6_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK2_DTHETA_lane0_0 : begin
	    TRACK2_DTHETA_lane0_data0[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK2_DTHETA_lane0_1 : begin
	    TRACK2_DTHETA_lane0_data1[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK2_DTHETA_lane0_2 : begin
	    TRACK2_DTHETA_lane0_data2[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK2_DTHETA_lane0_3 : begin
	    TRACK2_DTHETA_lane0_data3[4:0] <= VME_DIN[4:0];
	end
	
	`Addr_TRACK2_DTHETA_lane1_0 : begin
	    TRACK2_DTHETA_lane1_data0[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK2_DTHETA_lane1_1 : begin
	    TRACK2_DTHETA_lane1_data1[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK2_DTHETA_lane1_2 : begin
	    TRACK2_DTHETA_lane1_data2[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK2_DTHETA_lane1_3 : begin
	    TRACK2_DTHETA_lane1_data3[4:0] <= VME_DIN[4:0];
	end
	
	`Addr_TRACK2_DTHETA_lane2_0 : begin
	    TRACK2_DTHETA_lane2_data0[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK2_DTHETA_lane2_1 : begin
	    TRACK2_DTHETA_lane2_data1[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK2_DTHETA_lane2_2 : begin
	    TRACK2_DTHETA_lane2_data2[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK2_DTHETA_lane2_3 : begin
	    TRACK2_DTHETA_lane2_data3[4:0] <= VME_DIN[4:0];
	end
	
	`Addr_TRACK2_DTHETA_lane3_0 : begin
	    TRACK2_DTHETA_lane3_data0[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK2_DTHETA_lane3_1 : begin
	    TRACK2_DTHETA_lane3_data1[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK2_DTHETA_lane3_2 : begin
	    TRACK2_DTHETA_lane3_data2[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK2_DTHETA_lane3_3 : begin
	    TRACK2_DTHETA_lane3_data3[4:0] <= VME_DIN[4:0];
	end
	
	`Addr_TRACK2_DTHETA_lane4_0 : begin
	    TRACK2_DTHETA_lane4_data0[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK2_DTHETA_lane4_1 : begin
	    TRACK2_DTHETA_lane4_data1[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK2_DTHETA_lane4_2 : begin
	    TRACK2_DTHETA_lane4_data2[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK2_DTHETA_lane4_3 : begin
	    TRACK2_DTHETA_lane4_data3[4:0] <= VME_DIN[4:0];
	end
	
	`Addr_TRACK2_DTHETA_lane5_0 : begin
	    TRACK2_DTHETA_lane5_data0[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK2_DTHETA_lane5_1 : begin
	    TRACK2_DTHETA_lane5_data1[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK2_DTHETA_lane5_2 : begin
	    TRACK2_DTHETA_lane5_data2[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK2_DTHETA_lane5_3 : begin
	    TRACK2_DTHETA_lane5_data3[4:0] <= VME_DIN[4:0];
	end
	
	`Addr_TRACK2_DTHETA_lane6_0 : begin
	    TRACK2_DTHETA_lane6_data0[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK2_DTHETA_lane6_1 : begin
	    TRACK2_DTHETA_lane6_data1[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK2_DTHETA_lane6_2 : begin
	    TRACK2_DTHETA_lane6_data2[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK2_DTHETA_lane6_3 : begin
	    TRACK2_DTHETA_lane6_data3[4:0] <= VME_DIN[4:0];
	end
	
	`Addr_TRACK2_PHI_lane0_0 : begin
	    TRACK2_PHI_lane0_data0[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK2_PHI_lane0_1 : begin
	    TRACK2_PHI_lane0_data1[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK2_PHI_lane0_2 : begin
	    TRACK2_PHI_lane0_data2[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK2_PHI_lane0_3 : begin
	    TRACK2_PHI_lane0_data3[5:0] <= VME_DIN[5:0];
	end
	
	`Addr_TRACK2_PHI_lane1_0 : begin
	    TRACK2_PHI_lane1_data0[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK2_PHI_lane1_1 : begin
	    TRACK2_PHI_lane1_data1[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK2_PHI_lane1_2 : begin
	    TRACK2_PHI_lane1_data2[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK2_PHI_lane1_3 : begin
	    TRACK2_PHI_lane1_data3[5:0] <= VME_DIN[5:0];
	end
	
	`Addr_TRACK2_PHI_lane2_0 : begin
	    TRACK2_PHI_lane2_data0[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK2_PHI_lane2_1 : begin
	    TRACK2_PHI_lane2_data1[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK2_PHI_lane2_2 : begin
	    TRACK2_PHI_lane2_data2[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK2_PHI_lane2_3 : begin
	    TRACK2_PHI_lane2_data3[5:0] <= VME_DIN[5:0];
	end
	
	`Addr_TRACK2_PHI_lane3_0 : begin
	    TRACK2_PHI_lane3_data0[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK2_PHI_lane3_1 : begin
	    TRACK2_PHI_lane3_data1[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK2_PHI_lane3_2 : begin
	    TRACK2_PHI_lane3_data2[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK2_PHI_lane3_3 : begin
	    TRACK2_PHI_lane3_data3[5:0] <= VME_DIN[5:0];
	end
	
	`Addr_TRACK2_PHI_lane4_0 : begin
	    TRACK2_PHI_lane4_data0[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK2_PHI_lane4_1 : begin
	    TRACK2_PHI_lane4_data1[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK2_PHI_lane4_2 : begin
	    TRACK2_PHI_lane4_data2[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK2_PHI_lane4_3 : begin
	    TRACK2_PHI_lane4_data3[5:0] <= VME_DIN[5:0];
	end
	
	`Addr_TRACK2_PHI_lane5_0 : begin
	    TRACK2_PHI_lane5_data0[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK2_PHI_lane5_1 : begin
	    TRACK2_PHI_lane5_data1[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK2_PHI_lane5_2 : begin
	    TRACK2_PHI_lane5_data2[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK2_PHI_lane5_3 : begin
	    TRACK2_PHI_lane5_data3[5:0] <= VME_DIN[5:0];
	end
	
	`Addr_TRACK2_PHI_lane6_0 : begin
	    TRACK2_PHI_lane6_data0[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK2_PHI_lane6_1 : begin
	    TRACK2_PHI_lane6_data1[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK2_PHI_lane6_2 : begin
	    TRACK2_PHI_lane6_data2[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK2_PHI_lane6_3 : begin
	    TRACK2_PHI_lane6_data3[5:0] <= VME_DIN[5:0];
	end
	
	`Addr_TRACK2_ETA_lane0_0 : begin
	    TRACK2_ETA_lane0_data0[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK2_ETA_lane0_1 : begin
	    TRACK2_ETA_lane0_data1[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK2_ETA_lane0_2 : begin
	    TRACK2_ETA_lane0_data2[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK2_ETA_lane0_3 : begin
	    TRACK2_ETA_lane0_data3[7:0] <= VME_DIN[7:0];
	end
	
	`Addr_TRACK2_ETA_lane1_0 : begin
	    TRACK2_ETA_lane1_data0[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK2_ETA_lane1_1 : begin
	    TRACK2_ETA_lane1_data1[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK2_ETA_lane1_2 : begin
	    TRACK2_ETA_lane1_data2[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK2_ETA_lane1_3 : begin
	    TRACK2_ETA_lane1_data3[7:0] <= VME_DIN[7:0];
	end
	
	`Addr_TRACK2_ETA_lane2_0 : begin
	    TRACK2_ETA_lane2_data0[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK2_ETA_lane2_1 : begin
	    TRACK2_ETA_lane2_data1[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK2_ETA_lane2_2 : begin
	    TRACK2_ETA_lane2_data2[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK2_ETA_lane2_3 : begin
	    TRACK2_ETA_lane2_data3[7:0] <= VME_DIN[7:0];
	end
	
	`Addr_TRACK2_ETA_lane3_0 : begin
	    TRACK2_ETA_lane3_data0[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK2_ETA_lane3_1 : begin
	    TRACK2_ETA_lane3_data1[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK2_ETA_lane3_2 : begin
	    TRACK2_ETA_lane3_data2[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK2_ETA_lane3_3 : begin
	    TRACK2_ETA_lane3_data3[7:0] <= VME_DIN[7:0];
	end
	
	`Addr_TRACK2_ETA_lane4_0 : begin
	    TRACK2_ETA_lane4_data0[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK2_ETA_lane4_1 : begin
	    TRACK2_ETA_lane4_data1[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK2_ETA_lane4_2 : begin
	    TRACK2_ETA_lane4_data2[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK2_ETA_lane4_3 : begin
	    TRACK2_ETA_lane4_data3[7:0] <= VME_DIN[7:0];
	end
	
	`Addr_TRACK2_ETA_lane5_0 : begin
	    TRACK2_ETA_lane5_data0[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK2_ETA_lane5_1 : begin
	    TRACK2_ETA_lane5_data1[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK2_ETA_lane5_2 : begin
	    TRACK2_ETA_lane5_data2[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK2_ETA_lane5_3 : begin
	    TRACK2_ETA_lane5_data3[7:0] <= VME_DIN[7:0];
	end
	
	`Addr_TRACK2_ETA_lane6_0 : begin
	    TRACK2_ETA_lane6_data0[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK2_ETA_lane6_1 : begin
	    TRACK2_ETA_lane6_data1[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK2_ETA_lane6_2 : begin
	    TRACK2_ETA_lane6_data2[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK2_ETA_lane6_3 : begin
	    TRACK2_ETA_lane6_data3[7:0] <= VME_DIN[7:0];
	end
	
	`Addr_TRACK3_SPARE_lane0_0 : begin
	    TRACK3_SPARE_lane0_data0<= VME_DIN[0];
	end
	`Addr_TRACK3_SPARE_lane0_1 : begin
	    TRACK3_SPARE_lane0_data1<= VME_DIN[0];
	end
	`Addr_TRACK3_SPARE_lane0_2 : begin
	    TRACK3_SPARE_lane0_data2<= VME_DIN[0];
	end
	`Addr_TRACK3_SPARE_lane0_3 : begin
	    TRACK3_SPARE_lane0_data3<= VME_DIN[0];
	end
	
	`Addr_TRACK3_SPARE_lane1_0 : begin
	    TRACK3_SPARE_lane1_data0<= VME_DIN[0];
	end
	`Addr_TRACK3_SPARE_lane1_1 : begin
	    TRACK3_SPARE_lane1_data1<= VME_DIN[0];
	end
	`Addr_TRACK3_SPARE_lane1_2 : begin
	    TRACK3_SPARE_lane1_data2<= VME_DIN[0];
	end
	`Addr_TRACK3_SPARE_lane1_3 : begin
	    TRACK3_SPARE_lane1_data3<= VME_DIN[0];
	end
	
	`Addr_TRACK3_SPARE_lane2_0 : begin
	    TRACK3_SPARE_lane2_data0<= VME_DIN[0];
	end
	`Addr_TRACK3_SPARE_lane2_1 : begin
	    TRACK3_SPARE_lane2_data1<= VME_DIN[0];
	end
	`Addr_TRACK3_SPARE_lane2_2 : begin
	    TRACK3_SPARE_lane2_data2<= VME_DIN[0];
	end
	`Addr_TRACK3_SPARE_lane2_3 : begin
	    TRACK3_SPARE_lane2_data3<= VME_DIN[0];
	end
	
	`Addr_TRACK3_SPARE_lane3_0 : begin
	    TRACK3_SPARE_lane3_data0<= VME_DIN[0];
	end
	`Addr_TRACK3_SPARE_lane3_1 : begin
	    TRACK3_SPARE_lane3_data1<= VME_DIN[0];
	end
	`Addr_TRACK3_SPARE_lane3_2 : begin
	    TRACK3_SPARE_lane3_data2<= VME_DIN[0];
	end
	`Addr_TRACK3_SPARE_lane3_3 : begin
	    TRACK3_SPARE_lane3_data3<= VME_DIN[0];
	end
	
	`Addr_TRACK3_SPARE_lane4_0 : begin
	    TRACK3_SPARE_lane4_data0<= VME_DIN[0];
	end
	`Addr_TRACK3_SPARE_lane4_1 : begin
	    TRACK3_SPARE_lane4_data1<= VME_DIN[0];
	end
	`Addr_TRACK3_SPARE_lane4_2 : begin
	    TRACK3_SPARE_lane4_data2<= VME_DIN[0];
	end
	`Addr_TRACK3_SPARE_lane4_3 : begin
	    TRACK3_SPARE_lane4_data3<= VME_DIN[0];
	end
	
	`Addr_TRACK3_SPARE_lane5_0 : begin
	    TRACK3_SPARE_lane5_data0<= VME_DIN[0];
	end
	`Addr_TRACK3_SPARE_lane5_1 : begin
	    TRACK3_SPARE_lane5_data1<= VME_DIN[0];
	end
	`Addr_TRACK3_SPARE_lane5_2 : begin
	    TRACK3_SPARE_lane5_data2<= VME_DIN[0];
	end
	`Addr_TRACK3_SPARE_lane5_3 : begin
	    TRACK3_SPARE_lane5_data3<= VME_DIN[0];
	end
	
	`Addr_TRACK3_SPARE_lane6_0 : begin
	    TRACK3_SPARE_lane6_data0<= VME_DIN[0];
	end
	`Addr_TRACK3_SPARE_lane6_1 : begin
	    TRACK3_SPARE_lane6_data1<= VME_DIN[0];
	end
	`Addr_TRACK3_SPARE_lane6_2 : begin
	    TRACK3_SPARE_lane6_data2<= VME_DIN[0];
	end
	`Addr_TRACK3_SPARE_lane6_3 : begin
	    TRACK3_SPARE_lane6_data3<= VME_DIN[0];
	end
	
	`Addr_TRACK3_sTGC_lane0_0 : begin
	    TRACK3_sTGC_lane0_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK3_sTGC_lane0_1 : begin
	    TRACK3_sTGC_lane0_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK3_sTGC_lane0_2 : begin
	    TRACK3_sTGC_lane0_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK3_sTGC_lane0_3 : begin
	    TRACK3_sTGC_lane0_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK3_sTGC_lane1_0 : begin
	    TRACK3_sTGC_lane1_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK3_sTGC_lane1_1 : begin
	    TRACK3_sTGC_lane1_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK3_sTGC_lane1_2 : begin
	    TRACK3_sTGC_lane1_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK3_sTGC_lane1_3 : begin
	    TRACK3_sTGC_lane1_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK3_sTGC_lane2_0 : begin
	    TRACK3_sTGC_lane2_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK3_sTGC_lane2_1 : begin
	    TRACK3_sTGC_lane2_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK3_sTGC_lane2_2 : begin
	    TRACK3_sTGC_lane2_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK3_sTGC_lane2_3 : begin
	    TRACK3_sTGC_lane2_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK3_sTGC_lane3_0 : begin
	    TRACK3_sTGC_lane3_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK3_sTGC_lane3_1 : begin
	    TRACK3_sTGC_lane3_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK3_sTGC_lane3_2 : begin
	    TRACK3_sTGC_lane3_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK3_sTGC_lane3_3 : begin
	    TRACK3_sTGC_lane3_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK3_sTGC_lane4_0 : begin
	    TRACK3_sTGC_lane4_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK3_sTGC_lane4_1 : begin
	    TRACK3_sTGC_lane4_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK3_sTGC_lane4_2 : begin
	    TRACK3_sTGC_lane4_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK3_sTGC_lane4_3 : begin
	    TRACK3_sTGC_lane4_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK3_sTGC_lane5_0 : begin
	    TRACK3_sTGC_lane5_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK3_sTGC_lane5_1 : begin
	    TRACK3_sTGC_lane5_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK3_sTGC_lane5_2 : begin
	    TRACK3_sTGC_lane5_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK3_sTGC_lane5_3 : begin
	    TRACK3_sTGC_lane5_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK3_sTGC_lane6_0 : begin
	    TRACK3_sTGC_lane6_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK3_sTGC_lane6_1 : begin
	    TRACK3_sTGC_lane6_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK3_sTGC_lane6_2 : begin
	    TRACK3_sTGC_lane6_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK3_sTGC_lane6_3 : begin
	    TRACK3_sTGC_lane6_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK3_MM_lane0_0 : begin
	    TRACK3_MM_lane0_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK3_MM_lane0_1 : begin
	    TRACK3_MM_lane0_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK3_MM_lane0_2 : begin
	    TRACK3_MM_lane0_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK3_MM_lane0_3 : begin
	    TRACK3_MM_lane0_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK3_MM_lane1_0 : begin
	    TRACK3_MM_lane1_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK3_MM_lane1_1 : begin
	    TRACK3_MM_lane1_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK3_MM_lane1_2 : begin
	    TRACK3_MM_lane1_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK3_MM_lane1_3 : begin
	    TRACK3_MM_lane1_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK3_MM_lane2_0 : begin
	    TRACK3_MM_lane2_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK3_MM_lane2_1 : begin
	    TRACK3_MM_lane2_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK3_MM_lane2_2 : begin
	    TRACK3_MM_lane2_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK3_MM_lane2_3 : begin
	    TRACK3_MM_lane2_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK3_MM_lane3_0 : begin
	    TRACK3_MM_lane3_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK3_MM_lane3_1 : begin
	    TRACK3_MM_lane3_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK3_MM_lane3_2 : begin
	    TRACK3_MM_lane3_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK3_MM_lane3_3 : begin
	    TRACK3_MM_lane3_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK3_MM_lane4_0 : begin
	    TRACK3_MM_lane4_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK3_MM_lane4_1 : begin
	    TRACK3_MM_lane4_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK3_MM_lane4_2 : begin
	    TRACK3_MM_lane4_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK3_MM_lane4_3 : begin
	    TRACK3_MM_lane4_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK3_MM_lane5_0 : begin
	    TRACK3_MM_lane5_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK3_MM_lane5_1 : begin
	    TRACK3_MM_lane5_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK3_MM_lane5_2 : begin
	    TRACK3_MM_lane5_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK3_MM_lane5_3 : begin
	    TRACK3_MM_lane5_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK3_MM_lane6_0 : begin
	    TRACK3_MM_lane6_data0[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK3_MM_lane6_1 : begin
	    TRACK3_MM_lane6_data1[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK3_MM_lane6_2 : begin
	    TRACK3_MM_lane6_data2[1:0] <= VME_DIN[1:0];
	end
	`Addr_TRACK3_MM_lane6_3 : begin
	    TRACK3_MM_lane6_data3[1:0] <= VME_DIN[1:0];
	end
	
	`Addr_TRACK3_DTHETA_lane0_0 : begin
	    TRACK3_DTHETA_lane0_data0[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK3_DTHETA_lane0_1 : begin
	    TRACK3_DTHETA_lane0_data1[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK3_DTHETA_lane0_2 : begin
	    TRACK3_DTHETA_lane0_data2[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK3_DTHETA_lane0_3 : begin
	    TRACK3_DTHETA_lane0_data3[4:0] <= VME_DIN[4:0];
	end
	
	`Addr_TRACK3_DTHETA_lane1_0 : begin
	    TRACK3_DTHETA_lane1_data0[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK3_DTHETA_lane1_1 : begin
	    TRACK3_DTHETA_lane1_data1[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK3_DTHETA_lane1_2 : begin
	    TRACK3_DTHETA_lane1_data2[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK3_DTHETA_lane1_3 : begin
	    TRACK3_DTHETA_lane1_data3[4:0] <= VME_DIN[4:0];
	end
	
	`Addr_TRACK3_DTHETA_lane2_0 : begin
	    TRACK3_DTHETA_lane2_data0[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK3_DTHETA_lane2_1 : begin
	    TRACK3_DTHETA_lane2_data1[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK3_DTHETA_lane2_2 : begin
	    TRACK3_DTHETA_lane2_data2[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK3_DTHETA_lane2_3 : begin
	    TRACK3_DTHETA_lane2_data3[4:0] <= VME_DIN[4:0];
	end
	
	`Addr_TRACK3_DTHETA_lane3_0 : begin
	    TRACK3_DTHETA_lane3_data0[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK3_DTHETA_lane3_1 : begin
	    TRACK3_DTHETA_lane3_data1[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK3_DTHETA_lane3_2 : begin
	    TRACK3_DTHETA_lane3_data2[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK3_DTHETA_lane3_3 : begin
	    TRACK3_DTHETA_lane3_data3[4:0] <= VME_DIN[4:0];
	end
	
	`Addr_TRACK3_DTHETA_lane4_0 : begin
	    TRACK3_DTHETA_lane4_data0[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK3_DTHETA_lane4_1 : begin
	    TRACK3_DTHETA_lane4_data1[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK3_DTHETA_lane4_2 : begin
	    TRACK3_DTHETA_lane4_data2[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK3_DTHETA_lane4_3 : begin
	    TRACK3_DTHETA_lane4_data3[4:0] <= VME_DIN[4:0];
	end
	
	`Addr_TRACK3_DTHETA_lane5_0 : begin
	    TRACK3_DTHETA_lane5_data0[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK3_DTHETA_lane5_1 : begin
	    TRACK3_DTHETA_lane5_data1[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK3_DTHETA_lane5_2 : begin
	    TRACK3_DTHETA_lane5_data2[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK3_DTHETA_lane5_3 : begin
	    TRACK3_DTHETA_lane5_data3[4:0] <= VME_DIN[4:0];
	end
	
	`Addr_TRACK3_DTHETA_lane6_0 : begin
	    TRACK3_DTHETA_lane6_data0[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK3_DTHETA_lane6_1 : begin
	    TRACK3_DTHETA_lane6_data1[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK3_DTHETA_lane6_2 : begin
	    TRACK3_DTHETA_lane6_data2[4:0] <= VME_DIN[4:0];
	end
	`Addr_TRACK3_DTHETA_lane6_3 : begin
	    TRACK3_DTHETA_lane6_data3[4:0] <= VME_DIN[4:0];
	end
	
	`Addr_TRACK3_PHI_lane0_0 : begin
	    TRACK3_PHI_lane0_data0[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK3_PHI_lane0_1 : begin
	    TRACK3_PHI_lane0_data1[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK3_PHI_lane0_2 : begin
	    TRACK3_PHI_lane0_data2[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK3_PHI_lane0_3 : begin
	    TRACK3_PHI_lane0_data3[5:0] <= VME_DIN[5:0];
	end
	
	`Addr_TRACK3_PHI_lane1_0 : begin
	    TRACK3_PHI_lane1_data0[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK3_PHI_lane1_1 : begin
	    TRACK3_PHI_lane1_data1[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK3_PHI_lane1_2 : begin
	    TRACK3_PHI_lane1_data2[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK3_PHI_lane1_3 : begin
	    TRACK3_PHI_lane1_data3[5:0] <= VME_DIN[5:0];
	end
	
	`Addr_TRACK3_PHI_lane2_0 : begin
	    TRACK3_PHI_lane2_data0[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK3_PHI_lane2_1 : begin
	    TRACK3_PHI_lane2_data1[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK3_PHI_lane2_2 : begin
	    TRACK3_PHI_lane2_data2[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK3_PHI_lane2_3 : begin
	    TRACK3_PHI_lane2_data3[5:0] <= VME_DIN[5:0];
	end
	
	`Addr_TRACK3_PHI_lane3_0 : begin
	    TRACK3_PHI_lane3_data0[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK3_PHI_lane3_1 : begin
	    TRACK3_PHI_lane3_data1[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK3_PHI_lane3_2 : begin
	    TRACK3_PHI_lane3_data2[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK3_PHI_lane3_3 : begin
	    TRACK3_PHI_lane3_data3[5:0] <= VME_DIN[5:0];
	end
	
	`Addr_TRACK3_PHI_lane4_0 : begin
	    TRACK3_PHI_lane4_data0[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK3_PHI_lane4_1 : begin
	    TRACK3_PHI_lane4_data1[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK3_PHI_lane4_2 : begin
	    TRACK3_PHI_lane4_data2[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK3_PHI_lane4_3 : begin
	    TRACK3_PHI_lane4_data3[5:0] <= VME_DIN[5:0];
	end
	
	`Addr_TRACK3_PHI_lane5_0 : begin
	    TRACK3_PHI_lane5_data0[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK3_PHI_lane5_1 : begin
	    TRACK3_PHI_lane5_data1[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK3_PHI_lane5_2 : begin
	    TRACK3_PHI_lane5_data2[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK3_PHI_lane5_3 : begin
	    TRACK3_PHI_lane5_data3[5:0] <= VME_DIN[5:0];
	end
	
	`Addr_TRACK3_PHI_lane6_0 : begin
	    TRACK3_PHI_lane6_data0[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK3_PHI_lane6_1 : begin
	    TRACK3_PHI_lane6_data1[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK3_PHI_lane6_2 : begin
	    TRACK3_PHI_lane6_data2[5:0] <= VME_DIN[5:0];
	end
	`Addr_TRACK3_PHI_lane6_3 : begin
	    TRACK3_PHI_lane6_data3[5:0] <= VME_DIN[5:0];
	end
	
	`Addr_TRACK3_ETA_lane0_0 : begin
	    TRACK3_ETA_lane0_data0[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK3_ETA_lane0_1 : begin
	    TRACK3_ETA_lane0_data1[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK3_ETA_lane0_2 : begin
	    TRACK3_ETA_lane0_data2[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK3_ETA_lane0_3 : begin
	    TRACK3_ETA_lane0_data3[7:0] <= VME_DIN[7:0];
	end
	
	`Addr_TRACK3_ETA_lane1_0 : begin
	    TRACK3_ETA_lane1_data0[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK3_ETA_lane1_1 : begin
	    TRACK3_ETA_lane1_data1[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK3_ETA_lane1_2 : begin
	    TRACK3_ETA_lane1_data2[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK3_ETA_lane1_3 : begin
	    TRACK3_ETA_lane1_data3[7:0] <= VME_DIN[7:0];
	end
	
	`Addr_TRACK3_ETA_lane2_0 : begin
	    TRACK3_ETA_lane2_data0[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK3_ETA_lane2_1 : begin
	    TRACK3_ETA_lane2_data1[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK3_ETA_lane2_2 : begin
	    TRACK3_ETA_lane2_data2[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK3_ETA_lane2_3 : begin
	    TRACK3_ETA_lane2_data3[7:0] <= VME_DIN[7:0];
	end
	
	`Addr_TRACK3_ETA_lane3_0 : begin
	    TRACK3_ETA_lane3_data0[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK3_ETA_lane3_1 : begin
	    TRACK3_ETA_lane3_data1[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK3_ETA_lane3_2 : begin
	    TRACK3_ETA_lane3_data2[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK3_ETA_lane3_3 : begin
	    TRACK3_ETA_lane3_data3[7:0] <= VME_DIN[7:0];
	end
	
	`Addr_TRACK3_ETA_lane4_0 : begin
	    TRACK3_ETA_lane4_data0[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK3_ETA_lane4_1 : begin
	    TRACK3_ETA_lane4_data1[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK3_ETA_lane4_2 : begin
	    TRACK3_ETA_lane4_data2[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK3_ETA_lane4_3 : begin
	    TRACK3_ETA_lane4_data3[7:0] <= VME_DIN[7:0];
	end
	
	`Addr_TRACK3_ETA_lane5_0 : begin
	    TRACK3_ETA_lane5_data0[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK3_ETA_lane5_1 : begin
	    TRACK3_ETA_lane5_data1[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK3_ETA_lane5_2 : begin
	    TRACK3_ETA_lane5_data2[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK3_ETA_lane5_3 : begin
	    TRACK3_ETA_lane5_data3[7:0] <= VME_DIN[7:0];
	end
	
	`Addr_TRACK3_ETA_lane6_0 : begin
	    TRACK3_ETA_lane6_data0[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK3_ETA_lane6_1 : begin
	    TRACK3_ETA_lane6_data1[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK3_ETA_lane6_2 : begin
	    TRACK3_ETA_lane6_data2[7:0] <= VME_DIN[7:0];
	end
	`Addr_TRACK3_ETA_lane6_3 : begin
	    TRACK3_ETA_lane6_data3[7:0] <= VME_DIN[7:0];
	end





        `Addr_MONITORING_RESET : begin
            Monitoring_reset_out <= VME_DIN[0];
        end
        `Addr_LUT_INIT_RESET : begin
            LUT_init_reset_out <= VME_DIN[0];
        end
        `Addr_GT0_RX_RESET : begin
            gt0_rx_reset_out <= VME_DIN[0];
        end
        `Addr_GT1_RX_RESET : begin
            gt1_rx_reset_out <= VME_DIN[0];
        end
        `Addr_GT2_RX_RESET : begin
            gt2_rx_reset_out <= VME_DIN[0];
        end
        `Addr_GT3_RX_RESET : begin
            gt3_rx_reset_out <= VME_DIN[0];
        end
        `Addr_GT4_RX_RESET : begin
            gt4_rx_reset_out <= VME_DIN[0];
        end
        `Addr_GT5_RX_RESET : begin
            gt5_rx_reset_out <= VME_DIN[0];
        end
        `Addr_GT6_RX_RESET : begin
            gt6_rx_reset_out <= VME_DIN[0];
        end
        `Addr_GT7_RX_RESET : begin
            gt7_rx_reset_out <= VME_DIN[0];
        end
        `Addr_GT8_RX_RESET : begin
            gt8_rx_reset_out <= VME_DIN[0];
        end
        `Addr_GT9_RX_RESET : begin
            gt9_rx_reset_out <= VME_DIN[0];
        end
        `Addr_GT10_RX_RESET : begin
            gt10_rx_reset_out <= VME_DIN[0];
        end
        `Addr_GT11_RX_RESET : begin
            gt11_rx_reset_out <= VME_DIN[0];
        end

// Delay
        `Addr_DELAY_GTX0 : begin
            Delay_gtx0[7:0] <= VME_DIN[7:0];
        end
        `Addr_DELAY_GTX1 : begin
            Delay_gtx1[7:0] <= VME_DIN[7:0];
        end
        `Addr_DELAY_GTX2 : begin
            Delay_gtx2[7:0] <= VME_DIN[7:0];
        end
        `Addr_DELAY_GTX3 : begin
            Delay_gtx3[7:0] <= VME_DIN[7:0];
        end
        `Addr_DELAY_GTX4 : begin
            Delay_gtx4[7:0] <= VME_DIN[7:0];
        end
        `Addr_DELAY_GTX5 : begin
            Delay_gtx5[7:0] <= VME_DIN[7:0];
        end
        `Addr_DELAY_GTX6 : begin
            Delay_gtx6[7:0] <= VME_DIN[7:0];
        end
        `Addr_DELAY_GTX7 : begin
            Delay_gtx7[7:0] <= VME_DIN[7:0];
        end
        `Addr_DELAY_GTX8 : begin
            Delay_gtx8[7:0] <= VME_DIN[7:0];
        end
        `Addr_DELAY_GTX9 : begin
            Delay_gtx9[7:0] <= VME_DIN[7:0];
        end
        `Addr_DELAY_GTX10 : begin
            Delay_gtx10[7:0] <= VME_DIN[7:0];
        end
        `Addr_DELAY_GTX11 : begin
            Delay_gtx11[7:0] <= VME_DIN[7:0];
        end

        `Addr_DELAY_GLINK0 : begin
            Delay_glink0[7:0] <= VME_DIN[7:0];
        end
        `Addr_DELAY_GLINK1 : begin
            Delay_glink1[7:0] <= VME_DIN[7:0];
        end
        `Addr_DELAY_GLINK2 : begin
            Delay_glink2[7:0] <= VME_DIN[7:0];
        end
        `Addr_DELAY_GLINK3 : begin
            Delay_glink3[7:0] <= VME_DIN[7:0];
        end
        `Addr_DELAY_GLINK4 : begin
            Delay_glink4[7:0] <= VME_DIN[7:0];
        end
        `Addr_DELAY_GLINK5 : begin
            Delay_glink5[7:0] <= VME_DIN[7:0];
        end
        `Addr_DELAY_GLINK6 : begin
            Delay_glink6[7:0] <= VME_DIN[7:0];
        end
        `Addr_DELAY_GLINK7 : begin
            Delay_glink7[7:0] <= VME_DIN[7:0];
        end
        `Addr_DELAY_GLINK8 : begin
            Delay_glink8[7:0] <= VME_DIN[7:0];
        end
        `Addr_DELAY_GLINK9 : begin
            Delay_glink9[7:0] <= VME_DIN[7:0];
        end
        `Addr_DELAY_GLINK10 : begin
            Delay_glink10[7:0] <= VME_DIN[7:0];
        end
        `Addr_DELAY_GLINK11 : begin
            Delay_glink11[7:0] <= VME_DIN[7:0];
        end
        `Addr_DELAY_GLINK12 : begin
            Delay_glink12[7:0] <= VME_DIN[7:0];
        end
        `Addr_DELAY_GLINK13 : begin
            Delay_glink13[7:0] <= VME_DIN[7:0];
        end
        
        `Addr_DELAY_L1A : begin
            Delay_L1A[7:0] <= VME_DIN[7:0];
        end
        `Addr_DELAY_BCR : begin
            Delay_BCR[7:0] <= VME_DIN[7:0];
        end
        `Addr_DELAY_TRIG_BCR : begin
            Delay_trig_BCR[7:0] <= VME_DIN[7:0];
        end
        `Addr_DELAY_ECR : begin
            Delay_ECR[7:0] <= VME_DIN[7:0];
        end
        `Addr_DELAY_TTC_RESET : begin
            Delay_TTC_RESET[7:0] <= VME_DIN[7:0];
        end
        `Addr_DELAY_TEST_PULSE : begin
            Delay_TEST_PULSE[7:0] <= VME_DIN[7:0];
        end


// Mask    
        `Addr_MASK_RX0 : begin
            Mask_gtx0[1:0] <= VME_DIN[1:0];
        end
        `Addr_MASK_RX1 : begin
            Mask_gtx1[1:0] <= VME_DIN[1:0];
        end
        `Addr_MASK_RX2 : begin
            Mask_gtx2[1:0] <= VME_DIN[1:0];
        end
        `Addr_MASK_RX3 : begin
            Mask_gtx3[1:0] <= VME_DIN[1:0];
        end
        `Addr_MASK_RX4 : begin
            Mask_gtx4[1:0] <= VME_DIN[1:0];
        end
        `Addr_MASK_RX5 : begin
            Mask_gtx5[1:0] <= VME_DIN[1:0];
        end
        `Addr_MASK_RX6 : begin
            Mask_gtx6[1:0] <= VME_DIN[1:0];
        end
        `Addr_MASK_RX7 : begin
            Mask_gtx7[1:0] <= VME_DIN[1:0];
        end
        `Addr_MASK_RX8 : begin
            Mask_gtx8[1:0] <= VME_DIN[1:0];
        end
        `Addr_MASK_RX9 : begin
            Mask_gtx9[1:0] <= VME_DIN[1:0];
        end
        `Addr_MASK_RX10 : begin
            Mask_gtx10[1:0] <= VME_DIN[1:0];
        end
        `Addr_MASK_RX11 : begin
            Mask_gtx11[1:0] <= VME_DIN[1:0];
        end

        `Addr_MASK_L1A : begin
            Mask_L1A <= VME_DIN[0];
        end
        `Addr_MASK_BCR : begin
            Mask_BCR <= VME_DIN[0];
        end
        `Addr_MASK_TRIG_BCR : begin
            Mask_trig_BCR <= VME_DIN[0];
        end
        `Addr_MASK_ECR : begin
            Mask_ECR <= VME_DIN[0];
        end
        `Addr_MASK_TTC_RESET : begin
            Mask_TTC_RESET <= VME_DIN[0];
        end
        `Addr_MASK_TEST_PULSE : begin
            Mask_TEST_PULSE <= VME_DIN[0];
        end


        `Addr_MASK_GLINK0 : begin
            Mask_glink0[1:0] <= VME_DIN[1:0];
        end
        `Addr_MASK_GLINK1 : begin
            Mask_glink1[1:0] <= VME_DIN[1:0];
        end
        `Addr_MASK_GLINK2 : begin
            Mask_glink2[1:0] <= VME_DIN[1:0];
        end
        `Addr_MASK_GLINK3 : begin
            Mask_glink3[1:0] <= VME_DIN[1:0];
        end
        `Addr_MASK_GLINK4 : begin
            Mask_glink4[1:0] <= VME_DIN[1:0];
        end
        `Addr_MASK_GLINK5 : begin
            Mask_glink5[1:0] <= VME_DIN[1:0];
        end
        `Addr_MASK_GLINK6 : begin
            Mask_glink6[1:0] <= VME_DIN[1:0];
        end
        `Addr_MASK_GLINK7 : begin
            Mask_glink7[1:0] <= VME_DIN[1:0];
        end
        `Addr_MASK_GLINK8 : begin
            Mask_glink8[1:0] <= VME_DIN[1:0];
        end
        `Addr_MASK_GLINK9 : begin
            Mask_glink9[1:0] <= VME_DIN[1:0];
        end
        `Addr_MASK_GLINK10 : begin
            Mask_glink10[1:0] <= VME_DIN[1:0];
        end
        `Addr_MASK_GLINK11 : begin
            Mask_glink11[1:0] <= VME_DIN[1:0];
        end
        `Addr_MASK_GLINK12 : begin
            Mask_glink12[1:0] <= VME_DIN[1:0];
        end
        `Addr_MASK_GLINK13 : begin
            Mask_glink13[1:0] <= VME_DIN[1:0];
        end


        `Addr_TEST_PULSE_LENGTH: begin
               Test_Pulse_Length[15:0] <= VME_DIN[15:0];
        end
        `Addr_TEST_PULSE_WAIT_LENGTH: begin
               Test_Pulse_Wait_Length[15:0] <= VME_DIN[15:0];
        end
        `Addr_TEST_PULSE_ENABLE: begin
               Test_Pulse_Enable <= VME_DIN[0];
        end
        
        `Addr_SLID : begin
            SLID[11:0] <= VME_DIN[11:0]; 
        end
        `Addr_READOUT_BC : begin
            Readout_BC  <= VME_DIN[15:0];
        end
        `Addr_L1BUFFER_DEPTH : begin
            L1Buffer_depth    <= VME_DIN[6:0];
        end
        `Addr_TRIGL1BUFFER_DEPTH : begin
            trigL1Buffer_depth    <= VME_DIN[6:0];
        end
        `Addr_L1BUFFER_BW_DEPTH : begin
            L1Buffer_BW_depth    <= VME_DIN[6:0];
        end

        `Addr_LANE_SELECTOR : begin
            lane_selector_out[6:0] <= VME_DIN[6:0];
        end
        `Addr_XADC_MON_ENABLE : begin
            xadc_mon_enable_out <= VME_DIN[0];
        end     
        `Addr_XADC_MON_ADDR : begin
            xadc_mon_addr_out <= VME_DIN[6:0];
        end                  
        `Addr_L1A_MANUAL_PARAMETER : begin
            L1A_manual_parameter[15:0] <= VME_DIN[15:0];
        end
        `Addr_BUSY_PROPAGATION : begin
            Busy_propagation <= VME_DIN[0];
        end
        `Addr_GTX0_TEST_DATA1 : begin
            gtx0_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GTX0_TEST_DATA2 : begin
            gtx0_test_data2 <= VME_DIN[15:0];
        end
        `Addr_GTX0_TEST_DATA3 : begin
            gtx0_test_data3 <= VME_DIN[15:0];
        end
        `Addr_GTX0_TEST_DATA4 : begin
            gtx0_test_data4 <= VME_DIN[15:0];
        end
        `Addr_GTX0_TEST_DATA5 : begin
            gtx0_test_data5 <= VME_DIN[15:0];
        end
        `Addr_GTX0_TEST_DATA6 : begin
            gtx0_test_data6 <= VME_DIN[15:0];
        end
        `Addr_GTX0_TEST_DATA7 : begin
            gtx0_test_data7 <= VME_DIN[15:0];
        end
        `Addr_GTX1_TEST_DATA1 : begin
            gtx1_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GTX1_TEST_DATA2 : begin
            gtx1_test_data2 <= VME_DIN[15:0];
        end
        `Addr_GTX1_TEST_DATA3 : begin
            gtx1_test_data3 <= VME_DIN[15:0];
        end
        `Addr_GTX1_TEST_DATA4 : begin
            gtx1_test_data4 <= VME_DIN[15:0];
        end
        `Addr_GTX1_TEST_DATA5 : begin
            gtx1_test_data5 <= VME_DIN[15:0];
        end
        `Addr_GTX1_TEST_DATA6 : begin
            gtx1_test_data6 <= VME_DIN[15:0];
        end
        `Addr_GTX1_TEST_DATA7 : begin
            gtx1_test_data7 <= VME_DIN[15:0];
        end
        `Addr_GTX2_TEST_DATA1 : begin
            gtx2_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GTX2_TEST_DATA2 : begin
            gtx2_test_data2 <= VME_DIN[15:0];
        end
        `Addr_GTX2_TEST_DATA3 : begin
            gtx2_test_data3 <= VME_DIN[15:0];
        end
        `Addr_GTX2_TEST_DATA4 : begin
            gtx2_test_data4 <= VME_DIN[15:0];
        end
        `Addr_GTX2_TEST_DATA5 : begin
            gtx2_test_data5 <= VME_DIN[15:0];
        end
        `Addr_GTX2_TEST_DATA6 : begin
            gtx2_test_data6 <= VME_DIN[15:0];
        end
        `Addr_GTX2_TEST_DATA7 : begin
            gtx2_test_data7 <= VME_DIN[15:0];
        end
        `Addr_GTX3_TEST_DATA1 : begin
            gtx3_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GTX3_TEST_DATA2 : begin
            gtx3_test_data2 <= VME_DIN[15:0];
        end
        `Addr_GTX3_TEST_DATA3 : begin
            gtx3_test_data3 <= VME_DIN[15:0];
        end
        `Addr_GTX3_TEST_DATA4 : begin
            gtx3_test_data4 <= VME_DIN[15:0];
        end
        `Addr_GTX3_TEST_DATA5 : begin
            gtx3_test_data5 <= VME_DIN[15:0];
        end
        `Addr_GTX3_TEST_DATA6 : begin
            gtx3_test_data6 <= VME_DIN[15:0];
        end
        `Addr_GTX3_TEST_DATA7 : begin
            gtx3_test_data7 <= VME_DIN[15:0];
        end
        `Addr_GTX4_TEST_DATA1 : begin
            gtx4_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GTX4_TEST_DATA2 : begin
            gtx4_test_data2 <= VME_DIN[15:0];
        end
        `Addr_GTX4_TEST_DATA3 : begin
            gtx4_test_data3 <= VME_DIN[15:0];
        end
        `Addr_GTX4_TEST_DATA4 : begin
            gtx4_test_data4 <= VME_DIN[15:0];
        end
        `Addr_GTX4_TEST_DATA5 : begin
            gtx4_test_data5 <= VME_DIN[15:0];
        end
        `Addr_GTX4_TEST_DATA6 : begin
            gtx4_test_data6 <= VME_DIN[15:0];
        end
        `Addr_GTX4_TEST_DATA7 : begin
            gtx4_test_data7 <= VME_DIN[15:0];
        end
        `Addr_GTX5_TEST_DATA1 : begin
            gtx5_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GTX5_TEST_DATA2 : begin
            gtx5_test_data2 <= VME_DIN[15:0];
        end
        `Addr_GTX5_TEST_DATA3 : begin
            gtx5_test_data3 <= VME_DIN[15:0];
        end
        `Addr_GTX5_TEST_DATA4 : begin
            gtx5_test_data4 <= VME_DIN[15:0];
        end
        `Addr_GTX5_TEST_DATA5 : begin
            gtx5_test_data5 <= VME_DIN[15:0];
        end
        `Addr_GTX5_TEST_DATA6 : begin
            gtx5_test_data6 <= VME_DIN[15:0];
        end
        `Addr_GTX5_TEST_DATA7 : begin
            gtx5_test_data7 <= VME_DIN[15:0];
        end
        `Addr_GTX6_TEST_DATA1 : begin
            gtx6_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GTX6_TEST_DATA2 : begin
            gtx6_test_data2 <= VME_DIN[15:0];
        end
        `Addr_GTX6_TEST_DATA3 : begin
            gtx6_test_data3 <= VME_DIN[15:0];
        end
        `Addr_GTX6_TEST_DATA4 : begin
            gtx6_test_data4 <= VME_DIN[15:0];
        end
        `Addr_GTX6_TEST_DATA5 : begin
            gtx6_test_data5 <= VME_DIN[15:0];
        end
        `Addr_GTX6_TEST_DATA6 : begin
            gtx6_test_data6 <= VME_DIN[15:0];
        end
        `Addr_GTX6_TEST_DATA7 : begin
            gtx6_test_data7 <= VME_DIN[15:0];
        end
        `Addr_GTX7_TEST_DATA1 : begin
            gtx7_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GTX7_TEST_DATA2 : begin
            gtx7_test_data2 <= VME_DIN[15:0];
        end
        `Addr_GTX7_TEST_DATA3 : begin
            gtx7_test_data3 <= VME_DIN[15:0];
        end
        `Addr_GTX7_TEST_DATA4 : begin
            gtx7_test_data4 <= VME_DIN[15:0];
        end
        `Addr_GTX7_TEST_DATA5 : begin
            gtx7_test_data5 <= VME_DIN[15:0];
        end
        `Addr_GTX7_TEST_DATA6 : begin
            gtx7_test_data6 <= VME_DIN[15:0];
        end
        `Addr_GTX7_TEST_DATA7 : begin
            gtx7_test_data7 <= VME_DIN[15:0];
        end
        `Addr_GTX8_TEST_DATA1 : begin
            gtx8_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GTX8_TEST_DATA2 : begin
            gtx8_test_data2 <= VME_DIN[15:0];
        end
        `Addr_GTX8_TEST_DATA3 : begin
            gtx8_test_data3 <= VME_DIN[15:0];
        end
        `Addr_GTX8_TEST_DATA4 : begin
            gtx8_test_data4 <= VME_DIN[15:0];
        end
        `Addr_GTX8_TEST_DATA5 : begin
            gtx8_test_data5 <= VME_DIN[15:0];
        end
        `Addr_GTX8_TEST_DATA6 : begin
            gtx8_test_data6 <= VME_DIN[15:0];
        end
        `Addr_GTX8_TEST_DATA7 : begin
            gtx8_test_data7 <= VME_DIN[15:0];
        end
        `Addr_GTX9_TEST_DATA1 : begin
            gtx9_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GTX9_TEST_DATA2 : begin
            gtx9_test_data2 <= VME_DIN[15:0];
        end
        `Addr_GTX9_TEST_DATA3 : begin
            gtx9_test_data3 <= VME_DIN[15:0];
        end
        `Addr_GTX9_TEST_DATA4 : begin
            gtx9_test_data4 <= VME_DIN[15:0];
        end
        `Addr_GTX9_TEST_DATA5 : begin
            gtx9_test_data5 <= VME_DIN[15:0];
        end
        `Addr_GTX9_TEST_DATA6 : begin
            gtx9_test_data6 <= VME_DIN[15:0];
        end
        `Addr_GTX9_TEST_DATA7 : begin
            gtx9_test_data7 <= VME_DIN[15:0];
        end
        `Addr_GTX10_TEST_DATA1 : begin
            gtx10_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GTX10_TEST_DATA2 : begin
            gtx10_test_data2 <= VME_DIN[15:0];
        end
        `Addr_GTX10_TEST_DATA3 : begin
            gtx10_test_data3 <= VME_DIN[15:0];
        end
        `Addr_GTX10_TEST_DATA4 : begin
            gtx10_test_data4 <= VME_DIN[15:0];
        end
        `Addr_GTX10_TEST_DATA5 : begin
            gtx10_test_data5 <= VME_DIN[15:0];
        end
        `Addr_GTX10_TEST_DATA6 : begin
            gtx10_test_data6 <= VME_DIN[15:0];
        end
        `Addr_GTX10_TEST_DATA7 : begin
            gtx10_test_data7 <= VME_DIN[15:0];
        end
        `Addr_GTX11_TEST_DATA1 : begin
            gtx11_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GTX11_TEST_DATA2 : begin
            gtx11_test_data2 <= VME_DIN[15:0];
        end
        `Addr_GTX11_TEST_DATA3 : begin
            gtx11_test_data3 <= VME_DIN[15:0];
        end
        `Addr_GTX11_TEST_DATA4 : begin
            gtx11_test_data4 <= VME_DIN[15:0];
        end
        `Addr_GTX11_TEST_DATA5 : begin
            gtx11_test_data5 <= VME_DIN[15:0];
        end
        `Addr_GTX11_TEST_DATA6 : begin
            gtx11_test_data6 <= VME_DIN[15:0];
        end
        `Addr_GTX11_TEST_DATA7 : begin
            gtx11_test_data7 <= VME_DIN[15:0];
        end
        `Addr_GLINK0_TEST_DATA1 : begin
            glink0_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GLINK0_TEST_DATA2 : begin
            glink0_test_data2 <= VME_DIN[3:0];
        end
        `Addr_GLINK1_TEST_DATA1 : begin
            glink1_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GLINK1_TEST_DATA2 : begin
            glink1_test_data2 <= VME_DIN[3:0];
        end
        `Addr_GLINK2_TEST_DATA1 : begin
            glink2_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GLINK2_TEST_DATA2 : begin
            glink2_test_data2 <= VME_DIN[3:0];
        end
        `Addr_GLINK3_TEST_DATA1 : begin
            glink3_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GLINK3_TEST_DATA2 : begin
            glink3_test_data2 <= VME_DIN[3:0];
        end
        `Addr_GLINK4_TEST_DATA1 : begin
            glink4_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GLINK4_TEST_DATA2 : begin
            glink4_test_data2 <= VME_DIN[3:0];
        end
        `Addr_GLINK5_TEST_DATA1 : begin
            glink5_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GLINK5_TEST_DATA2 : begin
            glink5_test_data2 <= VME_DIN[3:0];
        end
        `Addr_GLINK6_TEST_DATA1 : begin
            glink6_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GLINK6_TEST_DATA2 : begin
            glink6_test_data2 <= VME_DIN[3:0];
        end
        `Addr_GLINK7_TEST_DATA1 : begin
            glink7_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GLINK7_TEST_DATA2 : begin
            glink7_test_data2 <= VME_DIN[3:0];
        end
        `Addr_GLINK8_TEST_DATA1 : begin
            glink8_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GLINK8_TEST_DATA2 : begin
            glink8_test_data2 <= VME_DIN[3:0];
        end
        `Addr_GLINK9_TEST_DATA1 : begin
            glink9_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GLINK9_TEST_DATA2 : begin
            glink9_test_data2 <= VME_DIN[3:0];
        end
        `Addr_GLINK10_TEST_DATA1 : begin
            glink10_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GLINK10_TEST_DATA2 : begin
            glink10_test_data2 <= VME_DIN[3:0];
        end
        `Addr_GLINK11_TEST_DATA1 : begin
            glink11_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GLINK11_TEST_DATA2 : begin
            glink11_test_data2 <= VME_DIN[3:0];
        end
        `Addr_GLINK12_TEST_DATA1 : begin
            glink12_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GLINK12_TEST_DATA2 : begin
            glink12_test_data2 <= VME_DIN[3:0];
        end
        `Addr_GLINK13_TEST_DATA1 : begin
            glink13_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GLINK13_TEST_DATA2 : begin
            glink13_test_data2 <= VME_DIN[3:0];
        end

// LUT initialization
        `Addr_LUT_INIT_MODE : begin
            LUT_init_mode <= VME_DIN[1:0];
        end
        `Addr_LUT_INIT_DATA : begin
            LUT_init_data <= VME_DIN[15:0];
            LUT_init_wr_en_reg <= LUT_init_wr_en_reg + 1;
        end
        `Addr_LUT_RD_ADDRESS : begin
            LUT_rd_address <= VME_DIN[14:0];
        end
        `Addr_ALIGN_ETA_NSW_0 : begin
            Align_eta_NSW_0 <= VME_DIN[7:0];
        end
        `Addr_ALIGN_ETA_NSW_1 : begin
            Align_eta_NSW_1 <= VME_DIN[7:0];
        end
        `Addr_ALIGN_ETA_NSW_2 : begin
            Align_eta_NSW_2 <= VME_DIN[7:0];
        end
        `Addr_ALIGN_ETA_NSW_3 : begin
            Align_eta_NSW_3 <= VME_DIN[7:0];
        end
        `Addr_ALIGN_ETA_NSW_4 : begin
            Align_eta_NSW_4 <= VME_DIN[7:0];
        end
        `Addr_ALIGN_ETA_NSW_5 : begin
            Align_eta_NSW_5 <= VME_DIN[7:0];
        end
        `Addr_ALIGN_ETA_RPC : begin
            Align_eta_RPC <= VME_DIN[7:0];
        end
        `Addr_ALIGN_PHI_NSW_0 : begin
            Align_phi_NSW_0 <= VME_DIN[7:0];
        end
        `Addr_ALIGN_PHI_NSW_1 : begin
            Align_phi_NSW_1 <= VME_DIN[7:0];
        end
        `Addr_ALIGN_PHI_NSW_2 : begin
            Align_phi_NSW_2 <= VME_DIN[7:0];
        end
        `Addr_ALIGN_PHI_NSW_3 : begin
            Align_phi_NSW_3 <= VME_DIN[7:0];
        end
        `Addr_ALIGN_PHI_NSW_4 : begin
            Align_phi_NSW_4 <= VME_DIN[7:0];
        end
        `Addr_ALIGN_PHI_NSW_5 : begin
            Align_phi_NSW_5 <= VME_DIN[7:0];
        end
        `Addr_ALIGN_PHI_RPC : begin
            Align_phi_RPC <= VME_DIN[7:0];
        end
        default : begin
        end
    endcase
    end
end


// especially for LUT init protocol
reg LUT_init_wr_en_reg = 0;
reg LUT_init_wr_en_buf = 0;
always@(posedge TTC_CLK) begin
    if (LUT_init_wr_en_buf != LUT_init_wr_en_reg) LUT_init_wr_en <= 1'b1;
    else LUT_init_wr_en <= 1'b0;
    LUT_init_wr_en_buf <= LUT_init_wr_en_reg;
end


// WRITE 160 MHz
always@(posedge CLK_160) begin
    shift_reg_160[2:0] <= {shift_reg_160[1:0], ws};
    if(write_160) begin
        write_160 <= 1'b0;
    end
    else if (shift_reg_160[2]==1'b0 && shift_reg_160[0]==1'b1) begin
        write_160 <= 1'b1;
        case (VME_A)
        `Addr_RESET : begin
            Reset_160_out <= VME_DIN[0];
        end
        `Addr_SITCP_RESET : begin
            SiTCP_reset_out <= VME_DIN[0];
        end        
        `Addr_SITCP_FIFO_RESET : begin
            SiTCP_FIFO_reset_out <= VME_DIN[0];
        end        
        `Addr_ZERO_SUPP_RESET : begin
            ZeroSupp_reset_out <= VME_DIN[0];
        end
        `Addr_FIFO_RESET : begin
            FIFO_reset_out <= VME_DIN[0];
        end
        `Addr_MONITORING_FIFO_wr_en : begin
            monitoring_FIFO_wr_en <= VME_DIN[0];
        end
        `Addr_MONITORING_FIFO_rd_en : begin
            monitoring_FIFO_rd_en    <= VME_DIN[0];
        end
        endcase
        end
end



// WRITE TXUSRCLK
always@(posedge TXUSRCLK_in) begin
    shift_reg_TX[2:0] <= {shift_reg_TX[1:0], ws};
    if(write_TX) begin
        write_TX <= 1'b0;
    end
    else if (shift_reg_TX[2]==1'b0 && shift_reg_TX[0]==1'b1) begin
        write_TX <= 1'b1;
        case (VME_A)
        `Addr_RESET : begin
            Reset_TX_out <= VME_DIN[0];
        end
        `Addr_TX_LOGIC_RESET : begin
            TX_Logic_reset_out <= VME_DIN[0];
        end
        `Addr_FULLBOARD_ID : begin
            FullBoard_ID <= VME_DIN[15:0];
        end
        endcase
        end
end

   
    
    //Read
always@(negedge rs) begin     
    case (VME_A [11:0])

// Test and bifile version
     `Addr_TEST : begin
          VME_DOUT[15:0] <= Test_reg[15:0];
        end
        `Addr_BITFILE_VERSION : begin
          VME_DOUT[15:0] <= bitfile_version[15:0];
        end


// Reset
        `Addr_RESET : begin
          VME_DOUT[15:0] <= {15'b0, Reset_TTC_out};
        end
        `Addr_SCALER_RESET : begin
          VME_DOUT[15:0] <= {15'b0, Scaler_reset_out};
        end
        `Addr_GTX_TX_RESET : begin
          VME_DOUT[15:0] <= {15'b0, GTX_TX_reset_out};
        end
        `Addr_GTX_RX_RESET : begin
          VME_DOUT[15:0] <= {15'b0, GTX_RX_reset_out};
        end
        `Addr_DELAY_RESET : begin
          VME_DOUT[15:0] <= {15'b0, Delay_reset_out};
        end
        `Addr_L1BUFFER_RESET : begin
          VME_DOUT[15:0] <= {15'b0, L1Buffer_reset_out};
        end
        `Addr_DERANDOMIZER_RESET : begin
          VME_DOUT[15:0] <= {15'b0, Derandomizer_reset_out};
        end
        `Addr_ZERO_SUPP_RESET : begin
          VME_DOUT[15:0] <= {15'b0, ZeroSupp_reset_out};
        end
        `Addr_CLK_RESET : begin
          VME_DOUT[15:0] <= {15'b0, CLK_reset_out};
        end
        `Addr_FIFO_RESET : begin
          VME_DOUT[15:0] <= {15'b0, FIFO_reset_out};
        end
        `Addr_SITCP_RESET : begin
          VME_DOUT[15:0] <= {15'b0, SiTCP_reset_out};
        end
        `Addr_SITCP_FIFO_RESET : begin
          VME_DOUT[15:0] <= {15'b0, SiTCP_FIFO_reset_out};
        end
        `Addr_TX_LOGIC_RESET : begin
          VME_DOUT[15:0] <= {15'b0, TX_Logic_reset_out};
        end
// gtx phase
	    `Addr_GTX0_PHASE_SELECT : begin
	        VME_DOUT [15:0] <= {11'h0, gtx0_phase_select};
	    end
	    `Addr_GTX1_PHASE_SELECT : begin
	        VME_DOUT [15:0] <= {11'h0, gtx1_phase_select};
	    end
	    `Addr_GTX2_PHASE_SELECT : begin
	        VME_DOUT [15:0] <= {11'h0, gtx2_phase_select};
	    end
	    `Addr_GTX3_PHASE_SELECT : begin
	        VME_DOUT [15:0] <= {11'h0, gtx3_phase_select};
	    end
	    `Addr_GTX4_PHASE_SELECT : begin
	        VME_DOUT [15:0] <= {11'h0, gtx4_phase_select};
	    end
	    `Addr_GTX5_PHASE_SELECT : begin
	        VME_DOUT [15:0] <= {11'h0, gtx5_phase_select};
	    end
	    `Addr_GTX6_PHASE_SELECT : begin
	        VME_DOUT [15:0] <= {11'h0, gtx6_phase_select};
	    end
	    `Addr_GTX7_PHASE_SELECT : begin
	        VME_DOUT [15:0] <= {11'h0, gtx7_phase_select};
	    end
	    `Addr_GTX8_PHASE_SELECT : begin
	        VME_DOUT [15:0] <= {11'h0, gtx8_phase_select};
	    end
	    `Addr_GTX9_PHASE_SELECT : begin
	        VME_DOUT [15:0] <= {11'h0, gtx9_phase_select};
	    end
	    `Addr_GTX10_PHASE_SELECT : begin
	        VME_DOUT [15:0] <= {11'h0, gtx10_phase_select};
	    end
	    `Addr_GTX11_PHASE_SELECT : begin
	        VME_DOUT [15:0] <= {11'h0, gtx11_phase_select};
	    end
	    `Addr_GTX0_PHASE_MONITOR0 : begin
	        VME_DOUT [15:0] <= {10'h0, gtx0_phase_monitor_0};
	    end
	    `Addr_GTX1_PHASE_MONITOR0 : begin
	        VME_DOUT [15:0] <= {10'h0, gtx1_phase_monitor_0};
	    end
	    `Addr_GTX2_PHASE_MONITOR0 : begin
	        VME_DOUT [15:0] <= {10'h0, gtx2_phase_monitor_0};
	    end
	    `Addr_GTX3_PHASE_MONITOR0 : begin
	        VME_DOUT [15:0] <= {10'h0, gtx3_phase_monitor_0};
	    end
	    `Addr_GTX4_PHASE_MONITOR0 : begin
	        VME_DOUT [15:0] <= {10'h0, gtx4_phase_monitor_0};
	    end
	    `Addr_GTX5_PHASE_MONITOR0 : begin
	        VME_DOUT [15:0] <= {10'h0, gtx5_phase_monitor_0};
	    end
	    `Addr_GTX6_PHASE_MONITOR0 : begin
	        VME_DOUT [15:0] <= {10'h0, gtx6_phase_monitor_0};
	    end
	    `Addr_GTX7_PHASE_MONITOR0 : begin
	        VME_DOUT [15:0] <= {10'h0, gtx7_phase_monitor_0};
	    end
	    `Addr_GTX8_PHASE_MONITOR0 : begin
	        VME_DOUT [15:0] <= {10'h0, gtx8_phase_monitor_0};
	    end
	    `Addr_GTX9_PHASE_MONITOR0 : begin
	        VME_DOUT [15:0] <= {10'h0, gtx9_phase_monitor_0};
	    end
	    `Addr_GTX10_PHASE_MONITOR0 : begin
	        VME_DOUT [15:0] <= {10'h0, gtx10_phase_monitor_0};
	    end
	    `Addr_GTX11_PHASE_MONITOR0 : begin
	        VME_DOUT [15:0] <= {10'h0, gtx11_phase_monitor_0};
	    end
	    `Addr_GTX0_PHASE_MONITOR1 : begin
	        VME_DOUT [15:0] <= {10'h0, gtx0_phase_monitor_1};
	    end
	    `Addr_GTX1_PHASE_MONITOR1 : begin
	        VME_DOUT [15:0] <= {10'h0, gtx1_phase_monitor_1};
	    end
	    `Addr_GTX2_PHASE_MONITOR1 : begin
	        VME_DOUT [15:0] <= {10'h0, gtx2_phase_monitor_1};
	    end
	    `Addr_GTX3_PHASE_MONITOR1 : begin
	        VME_DOUT [15:0] <= {10'h0, gtx3_phase_monitor_1};
	    end
	    `Addr_GTX4_PHASE_MONITOR1 : begin
	        VME_DOUT [15:0] <= {10'h0, gtx4_phase_monitor_1};
	    end
	    `Addr_GTX5_PHASE_MONITOR1 : begin
	        VME_DOUT [15:0] <= {10'h0, gtx5_phase_monitor_1};
	    end
	    `Addr_GTX6_PHASE_MONITOR1 : begin
	        VME_DOUT [15:0] <= {10'h0, gtx6_phase_monitor_1};
	    end
	    `Addr_GTX7_PHASE_MONITOR1 : begin
	        VME_DOUT [15:0] <= {10'h0, gtx7_phase_monitor_1};
	    end
	    `Addr_GTX8_PHASE_MONITOR1 : begin
	        VME_DOUT [15:0] <= {10'h0, gtx8_phase_monitor_1};
	    end
	    `Addr_GTX9_PHASE_MONITOR1 : begin
	        VME_DOUT [15:0] <= {10'h0, gtx9_phase_monitor_1};
	    end
	    `Addr_GTX10_PHASE_MONITOR1 : begin
	        VME_DOUT [15:0] <= {10'h0, gtx10_phase_monitor_1};
	    end
	    `Addr_GTX11_PHASE_MONITOR1 : begin
	        VME_DOUT [15:0] <= {10'h0, gtx11_phase_monitor_1};
	    end
	    `Addr_GTX0_PHASE_MONITOR2 : begin
	        VME_DOUT [15:0] <= {10'h0, gtx0_phase_monitor_2};
	    end
	    `Addr_GTX1_PHASE_MONITOR2 : begin
	        VME_DOUT [15:0] <= {10'h0, gtx1_phase_monitor_2};
	    end
	    `Addr_GTX2_PHASE_MONITOR2 : begin
	        VME_DOUT [15:0] <= {10'h0, gtx2_phase_monitor_2};
	    end
	    `Addr_GTX3_PHASE_MONITOR2 : begin
	        VME_DOUT [15:0] <= {10'h0, gtx3_phase_monitor_2};
	    end
	    `Addr_GTX4_PHASE_MONITOR2 : begin
	        VME_DOUT [15:0] <= {10'h0, gtx4_phase_monitor_2};
	    end
	    `Addr_GTX5_PHASE_MONITOR2 : begin
	        VME_DOUT [15:0] <= {10'h0, gtx5_phase_monitor_2};
	    end
	    `Addr_GTX6_PHASE_MONITOR2 : begin
	        VME_DOUT [15:0] <= {10'h0, gtx6_phase_monitor_2};
	    end
	    `Addr_GTX7_PHASE_MONITOR2 : begin
	        VME_DOUT [15:0] <= {10'h0, gtx7_phase_monitor_2};
	    end
	    `Addr_GTX8_PHASE_MONITOR2 : begin
	        VME_DOUT [15:0] <= {10'h0, gtx8_phase_monitor_2};
	    end
	    `Addr_GTX9_PHASE_MONITOR2 : begin
	        VME_DOUT [15:0] <= {10'h0, gtx9_phase_monitor_2};
	    end
	    `Addr_GTX10_PHASE_MONITOR2 : begin
	        VME_DOUT [15:0] <= {10'h0, gtx10_phase_monitor_2};
	    end
	    `Addr_GTX11_PHASE_MONITOR2 : begin
	        VME_DOUT [15:0] <= {10'h0, gtx11_phase_monitor_2};
	    end
	    `Addr_GTX0_PHASE_MONITOR3 : begin
	        VME_DOUT [15:0] <= {10'h0, gtx0_phase_monitor_3};
	    end
	    `Addr_GTX1_PHASE_MONITOR3 : begin
	        VME_DOUT [15:0] <= {10'h0, gtx1_phase_monitor_3};
	    end
	    `Addr_GTX2_PHASE_MONITOR3 : begin
	        VME_DOUT [15:0] <= {10'h0, gtx2_phase_monitor_3};
	    end
	    `Addr_GTX3_PHASE_MONITOR3 : begin
	        VME_DOUT [15:0] <= {10'h0, gtx3_phase_monitor_3};
	    end
	    `Addr_GTX4_PHASE_MONITOR3 : begin
	        VME_DOUT [15:0] <= {10'h0, gtx4_phase_monitor_3};
	    end
	    `Addr_GTX5_PHASE_MONITOR3 : begin
	        VME_DOUT [15:0] <= {10'h0, gtx5_phase_monitor_3};
	    end
	    `Addr_GTX6_PHASE_MONITOR3 : begin
	        VME_DOUT [15:0] <= {10'h0, gtx6_phase_monitor_3};
	    end
	    `Addr_GTX7_PHASE_MONITOR3 : begin
	        VME_DOUT [15:0] <= {10'h0, gtx7_phase_monitor_3};
	    end
	    `Addr_GTX8_PHASE_MONITOR3 : begin
	        VME_DOUT [15:0] <= {10'h0, gtx8_phase_monitor_3};
	    end
	    `Addr_GTX9_PHASE_MONITOR3 : begin
	        VME_DOUT [15:0] <= {10'h0, gtx9_phase_monitor_3};
	    end
	    `Addr_GTX10_PHASE_MONITOR3 : begin
	        VME_DOUT [15:0] <= {10'h0, gtx10_phase_monitor_3};
	    end
	    `Addr_GTX11_PHASE_MONITOR3 : begin
	        VME_DOUT [15:0] <= {10'h0, gtx11_phase_monitor_3};
	    end
// Emulator
        `Addr_EMULATOR_RESET : begin
            VME_DOUT [15:0] <= {15'b0, EMULATOR_reset_out};
        end        
        `Addr_EMULATOR_ENABLLE : begin
            VME_DOUT [15:0] <= {15'b0, EMULATOR_enable};
        end
        `Addr_EMULATOR_DATA_LENGTH : begin
            VME_DOUT [15:0] <= EMULATOR_data_length[15:0];
        end       
        `Addr_EMULATOR_GTX_HEADER : begin
           VME_DOUT [15:0] <= EMULATOR_GTX_header[15:0];
        end

        `Addr_TRACK0_ETA : begin
            VME_DOUT [15:0] <= {8'b0, TRACK0_eta[7:0]};
        end
        `Addr_TRACK0_PHI : begin
            VME_DOUT [15:0] <= {10'b0, TRACK0_phi[5:0]};
        end
        `Addr_TRACK0_DELTATHETA : begin
            VME_DOUT [15:0] <= {11'b0, TRACK0_deltatheta[4:0]};
        end
        `Addr_TRACK0_MM : begin
            VME_DOUT [15:0] <= {14'b0, TRACK0_mm[1:0]};
        end 
        `Addr_TRACK0_sTGC : begin
            VME_DOUT [15:0] <= {14'b0, TRACK0_stgc[1:0]};
        end
        `Addr_TRACK0_SPARE : begin
            VME_DOUT [15:0] <= {15'b0, TRACK0_spare};
        end

        `Addr_TRACK1_ETA : begin
            VME_DOUT [15:0] <= {8'b0, TRACK1_eta[7:0]};
        end
        `Addr_TRACK1_PHI : begin
            VME_DOUT [15:0] <= {10'b0, TRACK1_phi[5:0]};
        end
        `Addr_TRACK1_DELTATHETA : begin
            VME_DOUT [15:0] <= {11'b0, TRACK1_deltatheta[4:0]};
        end
        `Addr_TRACK1_MM : begin
            VME_DOUT [15:0] <= {14'b0, TRACK1_mm[1:0]};
        end 
        `Addr_TRACK1_sTGC : begin
            VME_DOUT [15:0] <= {14'b0, TRACK1_stgc[1:0]};
        end
        `Addr_TRACK1_SPARE : begin
            VME_DOUT [15:0] <= {15'b0, TRACK1_spare};
        end  

        `Addr_TRACK2_ETA : begin
            VME_DOUT [15:0] <= {8'b0, TRACK2_eta[7:0]};
        end
        `Addr_TRACK2_PHI : begin
            VME_DOUT [15:0] <= {10'b0, TRACK2_phi[5:0]};
        end
        `Addr_TRACK2_DELTATHETA : begin
            VME_DOUT [15:0] <= {11'b0, TRACK2_deltatheta[4:0]};
        end
        `Addr_TRACK2_MM : begin
            VME_DOUT [15:0] <= {14'b0, TRACK2_mm[1:0]};
        end 
        `Addr_TRACK2_sTGC : begin
            VME_DOUT [15:0] <= {14'b0, TRACK2_stgc[1:0]};
        end
        `Addr_TRACK2_SPARE : begin
            VME_DOUT [15:0] <= {15'b0, TRACK2_spare};
        end
        
        `Addr_TRACK3_ETA : begin
            VME_DOUT [15:0] <= {8'b0, TRACK3_eta[7:0]};
        end
        `Addr_TRACK3_PHI : begin
            VME_DOUT [15:0] <= {10'b0, TRACK3_phi[5:0]};
        end
        `Addr_TRACK3_DELTATHETA : begin
            VME_DOUT [15:0] <= {11'b0, TRACK3_deltatheta[4:0]};
        end
        `Addr_TRACK3_MM : begin
            VME_DOUT [15:0] <= {14'b0, TRACK3_mm[1:0]};
        end 
        `Addr_TRACK3_sTGC : begin
            VME_DOUT [15:0] <= {14'b0, TRACK3_stgc[1:0]};
        end
        `Addr_TRACK3_SPARE : begin
            VME_DOUT [15:0] <= {15'b0, TRACK3_spare};
        end

	`Addr_TRACK0_SPARE_lane0_0 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK0_SPARE_lane0_data0};
	end
	`Addr_TRACK0_SPARE_lane0_1 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK0_SPARE_lane0_data1};
	end
	`Addr_TRACK0_SPARE_lane0_2 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK0_SPARE_lane0_data2};
	end
	`Addr_TRACK0_SPARE_lane0_3 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK0_SPARE_lane0_data3};
	end
	
	`Addr_TRACK0_SPARE_lane1_0 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK0_SPARE_lane1_data0};
	end
	`Addr_TRACK0_SPARE_lane1_1 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK0_SPARE_lane1_data1};
	end
	`Addr_TRACK0_SPARE_lane1_2 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK0_SPARE_lane1_data2};
	end
	`Addr_TRACK0_SPARE_lane1_3 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK0_SPARE_lane1_data3};
	end
	
	`Addr_TRACK0_SPARE_lane2_0 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK0_SPARE_lane2_data0};
	end
	`Addr_TRACK0_SPARE_lane2_1 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK0_SPARE_lane2_data1};
	end
	`Addr_TRACK0_SPARE_lane2_2 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK0_SPARE_lane2_data2};
	end
	`Addr_TRACK0_SPARE_lane2_3 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK0_SPARE_lane2_data3};
	end
	
	`Addr_TRACK0_SPARE_lane3_0 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK0_SPARE_lane3_data0};
	end
	`Addr_TRACK0_SPARE_lane3_1 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK0_SPARE_lane3_data1};
	end
	`Addr_TRACK0_SPARE_lane3_2 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK0_SPARE_lane3_data2};
	end
	`Addr_TRACK0_SPARE_lane3_3 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK0_SPARE_lane3_data3};
	end
	
	`Addr_TRACK0_SPARE_lane4_0 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK0_SPARE_lane4_data0};
	end
	`Addr_TRACK0_SPARE_lane4_1 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK0_SPARE_lane4_data1};
	end
	`Addr_TRACK0_SPARE_lane4_2 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK0_SPARE_lane4_data2};
	end
	`Addr_TRACK0_SPARE_lane4_3 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK0_SPARE_lane4_data3};
	end
	
	`Addr_TRACK0_SPARE_lane5_0 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK0_SPARE_lane5_data0};
	end
	`Addr_TRACK0_SPARE_lane5_1 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK0_SPARE_lane5_data1};
	end
	`Addr_TRACK0_SPARE_lane5_2 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK0_SPARE_lane5_data2};
	end
	`Addr_TRACK0_SPARE_lane5_3 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK0_SPARE_lane5_data3};
	end
	
	`Addr_TRACK0_SPARE_lane6_0 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK0_SPARE_lane6_data0};
	end
	`Addr_TRACK0_SPARE_lane6_1 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK0_SPARE_lane6_data1};
	end
	`Addr_TRACK0_SPARE_lane6_2 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK0_SPARE_lane6_data2};
	end
	`Addr_TRACK0_SPARE_lane6_3 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK0_SPARE_lane6_data3};
	end
	
	`Addr_TRACK0_sTGC_lane0_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_sTGC_lane0_data0[1:0]};
	end
	`Addr_TRACK0_sTGC_lane0_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_sTGC_lane0_data1[1:0]};
	end
	`Addr_TRACK0_sTGC_lane0_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_sTGC_lane0_data2[1:0]};
	end
	`Addr_TRACK0_sTGC_lane0_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_sTGC_lane0_data3[1:0]};
	end
	
	`Addr_TRACK0_sTGC_lane1_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_sTGC_lane1_data0[1:0]};
	end
	`Addr_TRACK0_sTGC_lane1_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_sTGC_lane1_data1[1:0]};
	end
	`Addr_TRACK0_sTGC_lane1_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_sTGC_lane1_data2[1:0]};
	end
	`Addr_TRACK0_sTGC_lane1_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_sTGC_lane1_data3[1:0]};
	end
	
	`Addr_TRACK0_sTGC_lane2_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_sTGC_lane2_data0[1:0]};
	end
	`Addr_TRACK0_sTGC_lane2_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_sTGC_lane2_data1[1:0]};
	end
	`Addr_TRACK0_sTGC_lane2_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_sTGC_lane2_data2[1:0]};
	end
	`Addr_TRACK0_sTGC_lane2_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_sTGC_lane2_data3[1:0]};
	end
	
	`Addr_TRACK0_sTGC_lane3_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_sTGC_lane3_data0[1:0]};
	end
	`Addr_TRACK0_sTGC_lane3_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_sTGC_lane3_data1[1:0]};
	end
	`Addr_TRACK0_sTGC_lane3_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_sTGC_lane3_data2[1:0]};
	end
	`Addr_TRACK0_sTGC_lane3_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_sTGC_lane3_data3[1:0]};
	end
	
	`Addr_TRACK0_sTGC_lane4_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_sTGC_lane4_data0[1:0]};
	end
	`Addr_TRACK0_sTGC_lane4_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_sTGC_lane4_data1[1:0]};
	end
	`Addr_TRACK0_sTGC_lane4_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_sTGC_lane4_data2[1:0]};
	end
	`Addr_TRACK0_sTGC_lane4_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_sTGC_lane4_data3[1:0]};
	end
	
	`Addr_TRACK0_sTGC_lane5_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_sTGC_lane5_data0[1:0]};
	end
	`Addr_TRACK0_sTGC_lane5_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_sTGC_lane5_data1[1:0]};
	end
	`Addr_TRACK0_sTGC_lane5_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_sTGC_lane5_data2[1:0]};
	end
	`Addr_TRACK0_sTGC_lane5_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_sTGC_lane5_data3[1:0]};
	end
	
	`Addr_TRACK0_sTGC_lane6_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_sTGC_lane6_data0[1:0]};
	end
	`Addr_TRACK0_sTGC_lane6_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_sTGC_lane6_data1[1:0]};
	end
	`Addr_TRACK0_sTGC_lane6_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_sTGC_lane6_data2[1:0]};
	end
	`Addr_TRACK0_sTGC_lane6_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_sTGC_lane6_data3[1:0]};
	end
	
	`Addr_TRACK0_MM_lane0_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_MM_lane0_data0[1:0]};
	end
	`Addr_TRACK0_MM_lane0_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_MM_lane0_data1[1:0]};
	end
	`Addr_TRACK0_MM_lane0_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_MM_lane0_data2[1:0]};
	end
	`Addr_TRACK0_MM_lane0_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_MM_lane0_data3[1:0]};
	end
	
	`Addr_TRACK0_MM_lane1_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_MM_lane1_data0[1:0]};
	end
	`Addr_TRACK0_MM_lane1_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_MM_lane1_data1[1:0]};
	end
	`Addr_TRACK0_MM_lane1_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_MM_lane1_data2[1:0]};
	end
	`Addr_TRACK0_MM_lane1_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_MM_lane1_data3[1:0]};
	end
	
	`Addr_TRACK0_MM_lane2_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_MM_lane2_data0[1:0]};
	end
	`Addr_TRACK0_MM_lane2_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_MM_lane2_data1[1:0]};
	end
	`Addr_TRACK0_MM_lane2_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_MM_lane2_data2[1:0]};
	end
	`Addr_TRACK0_MM_lane2_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_MM_lane2_data3[1:0]};
	end
	
	`Addr_TRACK0_MM_lane3_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_MM_lane3_data0[1:0]};
	end
	`Addr_TRACK0_MM_lane3_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_MM_lane3_data1[1:0]};
	end
	`Addr_TRACK0_MM_lane3_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_MM_lane3_data2[1:0]};
	end
	`Addr_TRACK0_MM_lane3_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_MM_lane3_data3[1:0]};
	end
	
	`Addr_TRACK0_MM_lane4_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_MM_lane4_data0[1:0]};
	end
	`Addr_TRACK0_MM_lane4_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_MM_lane4_data1[1:0]};
	end
	`Addr_TRACK0_MM_lane4_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_MM_lane4_data2[1:0]};
	end
	`Addr_TRACK0_MM_lane4_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_MM_lane4_data3[1:0]};
	end
	
	`Addr_TRACK0_MM_lane5_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_MM_lane5_data0[1:0]};
	end
	`Addr_TRACK0_MM_lane5_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_MM_lane5_data1[1:0]};
	end
	`Addr_TRACK0_MM_lane5_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_MM_lane5_data2[1:0]};
	end
	`Addr_TRACK0_MM_lane5_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_MM_lane5_data3[1:0]};
	end
	
	`Addr_TRACK0_MM_lane6_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_MM_lane6_data0[1:0]};
	end
	`Addr_TRACK0_MM_lane6_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_MM_lane6_data1[1:0]};
	end
	`Addr_TRACK0_MM_lane6_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_MM_lane6_data2[1:0]};
	end
	`Addr_TRACK0_MM_lane6_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK0_MM_lane6_data3[1:0]};
	end
	
	`Addr_TRACK0_DTHETA_lane0_0 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK0_DTHETA_lane0_data0[4:0]};
	end
	`Addr_TRACK0_DTHETA_lane0_1 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK0_DTHETA_lane0_data1[4:0]};
	end
	`Addr_TRACK0_DTHETA_lane0_2 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK0_DTHETA_lane0_data2[4:0]};
	end
	`Addr_TRACK0_DTHETA_lane0_3 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK0_DTHETA_lane0_data3[4:0]};
	end
	
	`Addr_TRACK0_DTHETA_lane1_0 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK0_DTHETA_lane1_data0[4:0]};
	end
	`Addr_TRACK0_DTHETA_lane1_1 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK0_DTHETA_lane1_data1[4:0]};
	end
	`Addr_TRACK0_DTHETA_lane1_2 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK0_DTHETA_lane1_data2[4:0]};
	end
	`Addr_TRACK0_DTHETA_lane1_3 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK0_DTHETA_lane1_data3[4:0]};
	end
	
	`Addr_TRACK0_DTHETA_lane2_0 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK0_DTHETA_lane2_data0[4:0]};
	end
	`Addr_TRACK0_DTHETA_lane2_1 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK0_DTHETA_lane2_data1[4:0]};
	end
	`Addr_TRACK0_DTHETA_lane2_2 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK0_DTHETA_lane2_data2[4:0]};
	end
	`Addr_TRACK0_DTHETA_lane2_3 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK0_DTHETA_lane2_data3[4:0]};
	end
	
	`Addr_TRACK0_DTHETA_lane3_0 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK0_DTHETA_lane3_data0[4:0]};
	end
	`Addr_TRACK0_DTHETA_lane3_1 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK0_DTHETA_lane3_data1[4:0]};
	end
	`Addr_TRACK0_DTHETA_lane3_2 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK0_DTHETA_lane3_data2[4:0]};
	end
	`Addr_TRACK0_DTHETA_lane3_3 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK0_DTHETA_lane3_data3[4:0]};
	end
	
	`Addr_TRACK0_DTHETA_lane4_0 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK0_DTHETA_lane4_data0[4:0]};
	end
	`Addr_TRACK0_DTHETA_lane4_1 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK0_DTHETA_lane4_data1[4:0]};
	end
	`Addr_TRACK0_DTHETA_lane4_2 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK0_DTHETA_lane4_data2[4:0]};
	end
	`Addr_TRACK0_DTHETA_lane4_3 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK0_DTHETA_lane4_data3[4:0]};
	end
	
	`Addr_TRACK0_DTHETA_lane5_0 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK0_DTHETA_lane5_data0[4:0]};
	end
	`Addr_TRACK0_DTHETA_lane5_1 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK0_DTHETA_lane5_data1[4:0]};
	end
	`Addr_TRACK0_DTHETA_lane5_2 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK0_DTHETA_lane5_data2[4:0]};
	end
	`Addr_TRACK0_DTHETA_lane5_3 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK0_DTHETA_lane5_data3[4:0]};
	end
	
	`Addr_TRACK0_DTHETA_lane6_0 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK0_DTHETA_lane6_data0[4:0]};
	end
	`Addr_TRACK0_DTHETA_lane6_1 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK0_DTHETA_lane6_data1[4:0]};
	end
	`Addr_TRACK0_DTHETA_lane6_2 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK0_DTHETA_lane6_data2[4:0]};
	end
	`Addr_TRACK0_DTHETA_lane6_3 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK0_DTHETA_lane6_data3[4:0]};
	end
	
	`Addr_TRACK0_PHI_lane0_0 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK0_PHI_lane0_data0[5:0]};
	end
	`Addr_TRACK0_PHI_lane0_1 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK0_PHI_lane0_data1[5:0]};
	end
	`Addr_TRACK0_PHI_lane0_2 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK0_PHI_lane0_data2[5:0]};
	end
	`Addr_TRACK0_PHI_lane0_3 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK0_PHI_lane0_data3[5:0]};
	end
	
	`Addr_TRACK0_PHI_lane1_0 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK0_PHI_lane1_data0[5:0]};
	end
	`Addr_TRACK0_PHI_lane1_1 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK0_PHI_lane1_data1[5:0]};
	end
	`Addr_TRACK0_PHI_lane1_2 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK0_PHI_lane1_data2[5:0]};
	end
	`Addr_TRACK0_PHI_lane1_3 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK0_PHI_lane1_data3[5:0]};
	end
	
	`Addr_TRACK0_PHI_lane2_0 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK0_PHI_lane2_data0[5:0]};
	end
	`Addr_TRACK0_PHI_lane2_1 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK0_PHI_lane2_data1[5:0]};
	end
	`Addr_TRACK0_PHI_lane2_2 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK0_PHI_lane2_data2[5:0]};
	end
	`Addr_TRACK0_PHI_lane2_3 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK0_PHI_lane2_data3[5:0]};
	end
	
	`Addr_TRACK0_PHI_lane3_0 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK0_PHI_lane3_data0[5:0]};
	end
	`Addr_TRACK0_PHI_lane3_1 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK0_PHI_lane3_data1[5:0]};
	end
	`Addr_TRACK0_PHI_lane3_2 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK0_PHI_lane3_data2[5:0]};
	end
	`Addr_TRACK0_PHI_lane3_3 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK0_PHI_lane3_data3[5:0]};
	end
	
	`Addr_TRACK0_PHI_lane4_0 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK0_PHI_lane4_data0[5:0]};
	end
	`Addr_TRACK0_PHI_lane4_1 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK0_PHI_lane4_data1[5:0]};
	end
	`Addr_TRACK0_PHI_lane4_2 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK0_PHI_lane4_data2[5:0]};
	end
	`Addr_TRACK0_PHI_lane4_3 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK0_PHI_lane4_data3[5:0]};
	end
	
	`Addr_TRACK0_PHI_lane5_0 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK0_PHI_lane5_data0[5:0]};
	end
	`Addr_TRACK0_PHI_lane5_1 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK0_PHI_lane5_data1[5:0]};
	end
	`Addr_TRACK0_PHI_lane5_2 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK0_PHI_lane5_data2[5:0]};
	end
	`Addr_TRACK0_PHI_lane5_3 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK0_PHI_lane5_data3[5:0]};
	end
	
	`Addr_TRACK0_PHI_lane6_0 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK0_PHI_lane6_data0[5:0]};
	end
	`Addr_TRACK0_PHI_lane6_1 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK0_PHI_lane6_data1[5:0]};
	end
	`Addr_TRACK0_PHI_lane6_2 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK0_PHI_lane6_data2[5:0]};
	end
	`Addr_TRACK0_PHI_lane6_3 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK0_PHI_lane6_data3[5:0]};
	end
	
	`Addr_TRACK0_ETA_lane0_0 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK0_ETA_lane0_data0[7:0]};
	end
	`Addr_TRACK0_ETA_lane0_1 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK0_ETA_lane0_data1[7:0]};
	end
	`Addr_TRACK0_ETA_lane0_2 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK0_ETA_lane0_data2[7:0]};
	end
	`Addr_TRACK0_ETA_lane0_3 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK0_ETA_lane0_data3[7:0]};
	end
	
	`Addr_TRACK0_ETA_lane1_0 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK0_ETA_lane1_data0[7:0]};
	end
	`Addr_TRACK0_ETA_lane1_1 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK0_ETA_lane1_data1[7:0]};
	end
	`Addr_TRACK0_ETA_lane1_2 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK0_ETA_lane1_data2[7:0]};
	end
	`Addr_TRACK0_ETA_lane1_3 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK0_ETA_lane1_data3[7:0]};
	end
	
	`Addr_TRACK0_ETA_lane2_0 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK0_ETA_lane2_data0[7:0]};
	end
	`Addr_TRACK0_ETA_lane2_1 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK0_ETA_lane2_data1[7:0]};
	end
	`Addr_TRACK0_ETA_lane2_2 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK0_ETA_lane2_data2[7:0]};
	end
	`Addr_TRACK0_ETA_lane2_3 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK0_ETA_lane2_data3[7:0]};
	end
	
	`Addr_TRACK0_ETA_lane3_0 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK0_ETA_lane3_data0[7:0]};
	end
	`Addr_TRACK0_ETA_lane3_1 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK0_ETA_lane3_data1[7:0]};
	end
	`Addr_TRACK0_ETA_lane3_2 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK0_ETA_lane3_data2[7:0]};
	end
	`Addr_TRACK0_ETA_lane3_3 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK0_ETA_lane3_data3[7:0]};
	end
	
	`Addr_TRACK0_ETA_lane4_0 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK0_ETA_lane4_data0[7:0]};
	end
	`Addr_TRACK0_ETA_lane4_1 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK0_ETA_lane4_data1[7:0]};
	end
	`Addr_TRACK0_ETA_lane4_2 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK0_ETA_lane4_data2[7:0]};
	end
	`Addr_TRACK0_ETA_lane4_3 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK0_ETA_lane4_data3[7:0]};
	end
	
	`Addr_TRACK0_ETA_lane5_0 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK0_ETA_lane5_data0[7:0]};
	end
	`Addr_TRACK0_ETA_lane5_1 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK0_ETA_lane5_data1[7:0]};
	end
	`Addr_TRACK0_ETA_lane5_2 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK0_ETA_lane5_data2[7:0]};
	end
	`Addr_TRACK0_ETA_lane5_3 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK0_ETA_lane5_data3[7:0]};
	end
	
	`Addr_TRACK0_ETA_lane6_0 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK0_ETA_lane6_data0[7:0]};
	end
	`Addr_TRACK0_ETA_lane6_1 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK0_ETA_lane6_data1[7:0]};
	end
	`Addr_TRACK0_ETA_lane6_2 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK0_ETA_lane6_data2[7:0]};
	end
	`Addr_TRACK0_ETA_lane6_3 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK0_ETA_lane6_data3[7:0]};
	end
	
	`Addr_TRACK1_SPARE_lane0_0 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK1_SPARE_lane0_data0};
	end
	`Addr_TRACK1_SPARE_lane0_1 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK1_SPARE_lane0_data1};
	end
	`Addr_TRACK1_SPARE_lane0_2 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK1_SPARE_lane0_data2};
	end
	`Addr_TRACK1_SPARE_lane0_3 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK1_SPARE_lane0_data3};
	end
	
	`Addr_TRACK1_SPARE_lane1_0 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK1_SPARE_lane1_data0};
	end
	`Addr_TRACK1_SPARE_lane1_1 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK1_SPARE_lane1_data1};
	end
	`Addr_TRACK1_SPARE_lane1_2 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK1_SPARE_lane1_data2};
	end
	`Addr_TRACK1_SPARE_lane1_3 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK1_SPARE_lane1_data3};
	end
	
	`Addr_TRACK1_SPARE_lane2_0 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK1_SPARE_lane2_data0};
	end
	`Addr_TRACK1_SPARE_lane2_1 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK1_SPARE_lane2_data1};
	end
	`Addr_TRACK1_SPARE_lane2_2 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK1_SPARE_lane2_data2};
	end
	`Addr_TRACK1_SPARE_lane2_3 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK1_SPARE_lane2_data3};
	end
	
	`Addr_TRACK1_SPARE_lane3_0 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK1_SPARE_lane3_data0};
	end
	`Addr_TRACK1_SPARE_lane3_1 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK1_SPARE_lane3_data1};
	end
	`Addr_TRACK1_SPARE_lane3_2 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK1_SPARE_lane3_data2};
	end
	`Addr_TRACK1_SPARE_lane3_3 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK1_SPARE_lane3_data3};
	end
	
	`Addr_TRACK1_SPARE_lane4_0 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK1_SPARE_lane4_data0};
	end
	`Addr_TRACK1_SPARE_lane4_1 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK1_SPARE_lane4_data1};
	end
	`Addr_TRACK1_SPARE_lane4_2 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK1_SPARE_lane4_data2};
	end
	`Addr_TRACK1_SPARE_lane4_3 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK1_SPARE_lane4_data3};
	end
	
	`Addr_TRACK1_SPARE_lane5_0 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK1_SPARE_lane5_data0};
	end
	`Addr_TRACK1_SPARE_lane5_1 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK1_SPARE_lane5_data1};
	end
	`Addr_TRACK1_SPARE_lane5_2 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK1_SPARE_lane5_data2};
	end
	`Addr_TRACK1_SPARE_lane5_3 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK1_SPARE_lane5_data3};
	end
	
	`Addr_TRACK1_SPARE_lane6_0 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK1_SPARE_lane6_data0};
	end
	`Addr_TRACK1_SPARE_lane6_1 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK1_SPARE_lane6_data1};
	end
	`Addr_TRACK1_SPARE_lane6_2 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK1_SPARE_lane6_data2};
	end
	`Addr_TRACK1_SPARE_lane6_3 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK1_SPARE_lane6_data3};
	end
	
	`Addr_TRACK1_sTGC_lane0_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_sTGC_lane0_data0[1:0]};
	end
	`Addr_TRACK1_sTGC_lane0_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_sTGC_lane0_data1[1:0]};
	end
	`Addr_TRACK1_sTGC_lane0_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_sTGC_lane0_data2[1:0]};
	end
	`Addr_TRACK1_sTGC_lane0_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_sTGC_lane0_data3[1:0]};
	end
	
	`Addr_TRACK1_sTGC_lane1_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_sTGC_lane1_data0[1:0]};
	end
	`Addr_TRACK1_sTGC_lane1_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_sTGC_lane1_data1[1:0]};
	end
	`Addr_TRACK1_sTGC_lane1_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_sTGC_lane1_data2[1:0]};
	end
	`Addr_TRACK1_sTGC_lane1_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_sTGC_lane1_data3[1:0]};
	end
	
	`Addr_TRACK1_sTGC_lane2_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_sTGC_lane2_data0[1:0]};
	end
	`Addr_TRACK1_sTGC_lane2_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_sTGC_lane2_data1[1:0]};
	end
	`Addr_TRACK1_sTGC_lane2_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_sTGC_lane2_data2[1:0]};
	end
	`Addr_TRACK1_sTGC_lane2_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_sTGC_lane2_data3[1:0]};
	end
	
	`Addr_TRACK1_sTGC_lane3_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_sTGC_lane3_data0[1:0]};
	end
	`Addr_TRACK1_sTGC_lane3_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_sTGC_lane3_data1[1:0]};
	end
	`Addr_TRACK1_sTGC_lane3_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_sTGC_lane3_data2[1:0]};
	end
	`Addr_TRACK1_sTGC_lane3_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_sTGC_lane3_data3[1:0]};
	end
	
	`Addr_TRACK1_sTGC_lane4_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_sTGC_lane4_data0[1:0]};
	end
	`Addr_TRACK1_sTGC_lane4_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_sTGC_lane4_data1[1:0]};
	end
	`Addr_TRACK1_sTGC_lane4_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_sTGC_lane4_data2[1:0]};
	end
	`Addr_TRACK1_sTGC_lane4_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_sTGC_lane4_data3[1:0]};
	end
	
	`Addr_TRACK1_sTGC_lane5_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_sTGC_lane5_data0[1:0]};
	end
	`Addr_TRACK1_sTGC_lane5_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_sTGC_lane5_data1[1:0]};
	end
	`Addr_TRACK1_sTGC_lane5_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_sTGC_lane5_data2[1:0]};
	end
	`Addr_TRACK1_sTGC_lane5_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_sTGC_lane5_data3[1:0]};
	end
	
	`Addr_TRACK1_sTGC_lane6_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_sTGC_lane6_data0[1:0]};
	end
	`Addr_TRACK1_sTGC_lane6_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_sTGC_lane6_data1[1:0]};
	end
	`Addr_TRACK1_sTGC_lane6_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_sTGC_lane6_data2[1:0]};
	end
	`Addr_TRACK1_sTGC_lane6_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_sTGC_lane6_data3[1:0]};
	end
	
	`Addr_TRACK1_MM_lane0_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_MM_lane0_data0[1:0]};
	end
	`Addr_TRACK1_MM_lane0_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_MM_lane0_data1[1:0]};
	end
	`Addr_TRACK1_MM_lane0_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_MM_lane0_data2[1:0]};
	end
	`Addr_TRACK1_MM_lane0_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_MM_lane0_data3[1:0]};
	end
	
	`Addr_TRACK1_MM_lane1_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_MM_lane1_data0[1:0]};
	end
	`Addr_TRACK1_MM_lane1_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_MM_lane1_data1[1:0]};
	end
	`Addr_TRACK1_MM_lane1_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_MM_lane1_data2[1:0]};
	end
	`Addr_TRACK1_MM_lane1_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_MM_lane1_data3[1:0]};
	end
	
	`Addr_TRACK1_MM_lane2_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_MM_lane2_data0[1:0]};
	end
	`Addr_TRACK1_MM_lane2_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_MM_lane2_data1[1:0]};
	end
	`Addr_TRACK1_MM_lane2_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_MM_lane2_data2[1:0]};
	end
	`Addr_TRACK1_MM_lane2_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_MM_lane2_data3[1:0]};
	end
	
	`Addr_TRACK1_MM_lane3_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_MM_lane3_data0[1:0]};
	end
	`Addr_TRACK1_MM_lane3_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_MM_lane3_data1[1:0]};
	end
	`Addr_TRACK1_MM_lane3_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_MM_lane3_data2[1:0]};
	end
	`Addr_TRACK1_MM_lane3_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_MM_lane3_data3[1:0]};
	end
	
	`Addr_TRACK1_MM_lane4_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_MM_lane4_data0[1:0]};
	end
	`Addr_TRACK1_MM_lane4_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_MM_lane4_data1[1:0]};
	end
	`Addr_TRACK1_MM_lane4_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_MM_lane4_data2[1:0]};
	end
	`Addr_TRACK1_MM_lane4_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_MM_lane4_data3[1:0]};
	end
	
	`Addr_TRACK1_MM_lane5_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_MM_lane5_data0[1:0]};
	end
	`Addr_TRACK1_MM_lane5_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_MM_lane5_data1[1:0]};
	end
	`Addr_TRACK1_MM_lane5_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_MM_lane5_data2[1:0]};
	end
	`Addr_TRACK1_MM_lane5_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_MM_lane5_data3[1:0]};
	end
	
	`Addr_TRACK1_MM_lane6_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_MM_lane6_data0[1:0]};
	end
	`Addr_TRACK1_MM_lane6_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_MM_lane6_data1[1:0]};
	end
	`Addr_TRACK1_MM_lane6_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_MM_lane6_data2[1:0]};
	end
	`Addr_TRACK1_MM_lane6_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK1_MM_lane6_data3[1:0]};
	end
	
	`Addr_TRACK1_DTHETA_lane0_0 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK1_DTHETA_lane0_data0[4:0]};
	end
	`Addr_TRACK1_DTHETA_lane0_1 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK1_DTHETA_lane0_data1[4:0]};
	end
	`Addr_TRACK1_DTHETA_lane0_2 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK1_DTHETA_lane0_data2[4:0]};
	end
	`Addr_TRACK1_DTHETA_lane0_3 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK1_DTHETA_lane0_data3[4:0]};
	end
	
	`Addr_TRACK1_DTHETA_lane1_0 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK1_DTHETA_lane1_data0[4:0]};
	end
	`Addr_TRACK1_DTHETA_lane1_1 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK1_DTHETA_lane1_data1[4:0]};
	end
	`Addr_TRACK1_DTHETA_lane1_2 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK1_DTHETA_lane1_data2[4:0]};
	end
	`Addr_TRACK1_DTHETA_lane1_3 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK1_DTHETA_lane1_data3[4:0]};
	end
	
	`Addr_TRACK1_DTHETA_lane2_0 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK1_DTHETA_lane2_data0[4:0]};
	end
	`Addr_TRACK1_DTHETA_lane2_1 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK1_DTHETA_lane2_data1[4:0]};
	end
	`Addr_TRACK1_DTHETA_lane2_2 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK1_DTHETA_lane2_data2[4:0]};
	end
	`Addr_TRACK1_DTHETA_lane2_3 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK1_DTHETA_lane2_data3[4:0]};
	end
	
	`Addr_TRACK1_DTHETA_lane3_0 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK1_DTHETA_lane3_data0[4:0]};
	end
	`Addr_TRACK1_DTHETA_lane3_1 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK1_DTHETA_lane3_data1[4:0]};
	end
	`Addr_TRACK1_DTHETA_lane3_2 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK1_DTHETA_lane3_data2[4:0]};
	end
	`Addr_TRACK1_DTHETA_lane3_3 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK1_DTHETA_lane3_data3[4:0]};
	end
	
	`Addr_TRACK1_DTHETA_lane4_0 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK1_DTHETA_lane4_data0[4:0]};
	end
	`Addr_TRACK1_DTHETA_lane4_1 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK1_DTHETA_lane4_data1[4:0]};
	end
	`Addr_TRACK1_DTHETA_lane4_2 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK1_DTHETA_lane4_data2[4:0]};
	end
	`Addr_TRACK1_DTHETA_lane4_3 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK1_DTHETA_lane4_data3[4:0]};
	end
	
	`Addr_TRACK1_DTHETA_lane5_0 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK1_DTHETA_lane5_data0[4:0]};
	end
	`Addr_TRACK1_DTHETA_lane5_1 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK1_DTHETA_lane5_data1[4:0]};
	end
	`Addr_TRACK1_DTHETA_lane5_2 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK1_DTHETA_lane5_data2[4:0]};
	end
	`Addr_TRACK1_DTHETA_lane5_3 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK1_DTHETA_lane5_data3[4:0]};
	end
	
	`Addr_TRACK1_DTHETA_lane6_0 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK1_DTHETA_lane6_data0[4:0]};
	end
	`Addr_TRACK1_DTHETA_lane6_1 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK1_DTHETA_lane6_data1[4:0]};
	end
	`Addr_TRACK1_DTHETA_lane6_2 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK1_DTHETA_lane6_data2[4:0]};
	end
	`Addr_TRACK1_DTHETA_lane6_3 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK1_DTHETA_lane6_data3[4:0]};
	end
	
	`Addr_TRACK1_PHI_lane0_0 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK1_PHI_lane0_data0[5:0]};
	end
	`Addr_TRACK1_PHI_lane0_1 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK1_PHI_lane0_data1[5:0]};
	end
	`Addr_TRACK1_PHI_lane0_2 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK1_PHI_lane0_data2[5:0]};
	end
	`Addr_TRACK1_PHI_lane0_3 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK1_PHI_lane0_data3[5:0]};
	end
	
	`Addr_TRACK1_PHI_lane1_0 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK1_PHI_lane1_data0[5:0]};
	end
	`Addr_TRACK1_PHI_lane1_1 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK1_PHI_lane1_data1[5:0]};
	end
	`Addr_TRACK1_PHI_lane1_2 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK1_PHI_lane1_data2[5:0]};
	end
	`Addr_TRACK1_PHI_lane1_3 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK1_PHI_lane1_data3[5:0]};
	end
	
	`Addr_TRACK1_PHI_lane2_0 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK1_PHI_lane2_data0[5:0]};
	end
	`Addr_TRACK1_PHI_lane2_1 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK1_PHI_lane2_data1[5:0]};
	end
	`Addr_TRACK1_PHI_lane2_2 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK1_PHI_lane2_data2[5:0]};
	end
	`Addr_TRACK1_PHI_lane2_3 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK1_PHI_lane2_data3[5:0]};
	end
	
	`Addr_TRACK1_PHI_lane3_0 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK1_PHI_lane3_data0[5:0]};
	end
	`Addr_TRACK1_PHI_lane3_1 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK1_PHI_lane3_data1[5:0]};
	end
	`Addr_TRACK1_PHI_lane3_2 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK1_PHI_lane3_data2[5:0]};
	end
	`Addr_TRACK1_PHI_lane3_3 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK1_PHI_lane3_data3[5:0]};
	end
	
	`Addr_TRACK1_PHI_lane4_0 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK1_PHI_lane4_data0[5:0]};
	end
	`Addr_TRACK1_PHI_lane4_1 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK1_PHI_lane4_data1[5:0]};
	end
	`Addr_TRACK1_PHI_lane4_2 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK1_PHI_lane4_data2[5:0]};
	end
	`Addr_TRACK1_PHI_lane4_3 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK1_PHI_lane4_data3[5:0]};
	end
	
	`Addr_TRACK1_PHI_lane5_0 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK1_PHI_lane5_data0[5:0]};
	end
	`Addr_TRACK1_PHI_lane5_1 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK1_PHI_lane5_data1[5:0]};
	end
	`Addr_TRACK1_PHI_lane5_2 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK1_PHI_lane5_data2[5:0]};
	end
	`Addr_TRACK1_PHI_lane5_3 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK1_PHI_lane5_data3[5:0]};
	end
	
	`Addr_TRACK1_PHI_lane6_0 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK1_PHI_lane6_data0[5:0]};
	end
	`Addr_TRACK1_PHI_lane6_1 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK1_PHI_lane6_data1[5:0]};
	end
	`Addr_TRACK1_PHI_lane6_2 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK1_PHI_lane6_data2[5:0]};
	end
	`Addr_TRACK1_PHI_lane6_3 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK1_PHI_lane6_data3[5:0]};
	end
	
	`Addr_TRACK1_ETA_lane0_0 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK1_ETA_lane0_data0[7:0]};
	end
	`Addr_TRACK1_ETA_lane0_1 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK1_ETA_lane0_data1[7:0]};
	end
	`Addr_TRACK1_ETA_lane0_2 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK1_ETA_lane0_data2[7:0]};
	end
	`Addr_TRACK1_ETA_lane0_3 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK1_ETA_lane0_data3[7:0]};
	end
	
	`Addr_TRACK1_ETA_lane1_0 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK1_ETA_lane1_data0[7:0]};
	end
	`Addr_TRACK1_ETA_lane1_1 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK1_ETA_lane1_data1[7:0]};
	end
	`Addr_TRACK1_ETA_lane1_2 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK1_ETA_lane1_data2[7:0]};
	end
	`Addr_TRACK1_ETA_lane1_3 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK1_ETA_lane1_data3[7:0]};
	end
	
	`Addr_TRACK1_ETA_lane2_0 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK1_ETA_lane2_data0[7:0]};
	end
	`Addr_TRACK1_ETA_lane2_1 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK1_ETA_lane2_data1[7:0]};
	end
	`Addr_TRACK1_ETA_lane2_2 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK1_ETA_lane2_data2[7:0]};
	end
	`Addr_TRACK1_ETA_lane2_3 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK1_ETA_lane2_data3[7:0]};
	end
	
	`Addr_TRACK1_ETA_lane3_0 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK1_ETA_lane3_data0[7:0]};
	end
	`Addr_TRACK1_ETA_lane3_1 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK1_ETA_lane3_data1[7:0]};
	end
	`Addr_TRACK1_ETA_lane3_2 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK1_ETA_lane3_data2[7:0]};
	end
	`Addr_TRACK1_ETA_lane3_3 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK1_ETA_lane3_data3[7:0]};
	end
	
	`Addr_TRACK1_ETA_lane4_0 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK1_ETA_lane4_data0[7:0]};
	end
	`Addr_TRACK1_ETA_lane4_1 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK1_ETA_lane4_data1[7:0]};
	end
	`Addr_TRACK1_ETA_lane4_2 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK1_ETA_lane4_data2[7:0]};
	end
	`Addr_TRACK1_ETA_lane4_3 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK1_ETA_lane4_data3[7:0]};
	end
	
	`Addr_TRACK1_ETA_lane5_0 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK1_ETA_lane5_data0[7:0]};
	end
	`Addr_TRACK1_ETA_lane5_1 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK1_ETA_lane5_data1[7:0]};
	end
	`Addr_TRACK1_ETA_lane5_2 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK1_ETA_lane5_data2[7:0]};
	end
	`Addr_TRACK1_ETA_lane5_3 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK1_ETA_lane5_data3[7:0]};
	end
	
	`Addr_TRACK1_ETA_lane6_0 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK1_ETA_lane6_data0[7:0]};
	end
	`Addr_TRACK1_ETA_lane6_1 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK1_ETA_lane6_data1[7:0]};
	end
	`Addr_TRACK1_ETA_lane6_2 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK1_ETA_lane6_data2[7:0]};
	end
	`Addr_TRACK1_ETA_lane6_3 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK1_ETA_lane6_data3[7:0]};
	end
	
	`Addr_TRACK2_SPARE_lane0_0 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK2_SPARE_lane0_data0};
	end
	`Addr_TRACK2_SPARE_lane0_1 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK2_SPARE_lane0_data1};
	end
	`Addr_TRACK2_SPARE_lane0_2 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK2_SPARE_lane0_data2};
	end
	`Addr_TRACK2_SPARE_lane0_3 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK2_SPARE_lane0_data3};
	end
	
	`Addr_TRACK2_SPARE_lane1_0 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK2_SPARE_lane1_data0};
	end
	`Addr_TRACK2_SPARE_lane1_1 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK2_SPARE_lane1_data1};
	end
	`Addr_TRACK2_SPARE_lane1_2 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK2_SPARE_lane1_data2};
	end
	`Addr_TRACK2_SPARE_lane1_3 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK2_SPARE_lane1_data3};
	end
	
	`Addr_TRACK2_SPARE_lane2_0 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK2_SPARE_lane2_data0};
	end
	`Addr_TRACK2_SPARE_lane2_1 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK2_SPARE_lane2_data1};
	end
	`Addr_TRACK2_SPARE_lane2_2 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK2_SPARE_lane2_data2};
	end
	`Addr_TRACK2_SPARE_lane2_3 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK2_SPARE_lane2_data3};
	end
	
	`Addr_TRACK2_SPARE_lane3_0 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK2_SPARE_lane3_data0};
	end
	`Addr_TRACK2_SPARE_lane3_1 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK2_SPARE_lane3_data1};
	end
	`Addr_TRACK2_SPARE_lane3_2 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK2_SPARE_lane3_data2};
	end
	`Addr_TRACK2_SPARE_lane3_3 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK2_SPARE_lane3_data3};
	end
	
	`Addr_TRACK2_SPARE_lane4_0 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK2_SPARE_lane4_data0};
	end
	`Addr_TRACK2_SPARE_lane4_1 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK2_SPARE_lane4_data1};
	end
	`Addr_TRACK2_SPARE_lane4_2 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK2_SPARE_lane4_data2};
	end
	`Addr_TRACK2_SPARE_lane4_3 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK2_SPARE_lane4_data3};
	end
	
	`Addr_TRACK2_SPARE_lane5_0 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK2_SPARE_lane5_data0};
	end
	`Addr_TRACK2_SPARE_lane5_1 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK2_SPARE_lane5_data1};
	end
	`Addr_TRACK2_SPARE_lane5_2 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK2_SPARE_lane5_data2};
	end
	`Addr_TRACK2_SPARE_lane5_3 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK2_SPARE_lane5_data3};
	end
	
	`Addr_TRACK2_SPARE_lane6_0 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK2_SPARE_lane6_data0};
	end
	`Addr_TRACK2_SPARE_lane6_1 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK2_SPARE_lane6_data1};
	end
	`Addr_TRACK2_SPARE_lane6_2 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK2_SPARE_lane6_data2};
	end
	`Addr_TRACK2_SPARE_lane6_3 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK2_SPARE_lane6_data3};
	end
	
	`Addr_TRACK2_sTGC_lane0_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_sTGC_lane0_data0[1:0]};
	end
	`Addr_TRACK2_sTGC_lane0_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_sTGC_lane0_data1[1:0]};
	end
	`Addr_TRACK2_sTGC_lane0_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_sTGC_lane0_data2[1:0]};
	end
	`Addr_TRACK2_sTGC_lane0_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_sTGC_lane0_data3[1:0]};
	end
	
	`Addr_TRACK2_sTGC_lane1_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_sTGC_lane1_data0[1:0]};
	end
	`Addr_TRACK2_sTGC_lane1_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_sTGC_lane1_data1[1:0]};
	end
	`Addr_TRACK2_sTGC_lane1_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_sTGC_lane1_data2[1:0]};
	end
	`Addr_TRACK2_sTGC_lane1_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_sTGC_lane1_data3[1:0]};
	end
	
	`Addr_TRACK2_sTGC_lane2_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_sTGC_lane2_data0[1:0]};
	end
	`Addr_TRACK2_sTGC_lane2_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_sTGC_lane2_data1[1:0]};
	end
	`Addr_TRACK2_sTGC_lane2_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_sTGC_lane2_data2[1:0]};
	end
	`Addr_TRACK2_sTGC_lane2_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_sTGC_lane2_data3[1:0]};
	end
	
	`Addr_TRACK2_sTGC_lane3_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_sTGC_lane3_data0[1:0]};
	end
	`Addr_TRACK2_sTGC_lane3_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_sTGC_lane3_data1[1:0]};
	end
	`Addr_TRACK2_sTGC_lane3_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_sTGC_lane3_data2[1:0]};
	end
	`Addr_TRACK2_sTGC_lane3_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_sTGC_lane3_data3[1:0]};
	end
	
	`Addr_TRACK2_sTGC_lane4_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_sTGC_lane4_data0[1:0]};
	end
	`Addr_TRACK2_sTGC_lane4_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_sTGC_lane4_data1[1:0]};
	end
	`Addr_TRACK2_sTGC_lane4_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_sTGC_lane4_data2[1:0]};
	end
	`Addr_TRACK2_sTGC_lane4_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_sTGC_lane4_data3[1:0]};
	end
	
	`Addr_TRACK2_sTGC_lane5_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_sTGC_lane5_data0[1:0]};
	end
	`Addr_TRACK2_sTGC_lane5_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_sTGC_lane5_data1[1:0]};
	end
	`Addr_TRACK2_sTGC_lane5_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_sTGC_lane5_data2[1:0]};
	end
	`Addr_TRACK2_sTGC_lane5_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_sTGC_lane5_data3[1:0]};
	end
	
	`Addr_TRACK2_sTGC_lane6_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_sTGC_lane6_data0[1:0]};
	end
	`Addr_TRACK2_sTGC_lane6_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_sTGC_lane6_data1[1:0]};
	end
	`Addr_TRACK2_sTGC_lane6_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_sTGC_lane6_data2[1:0]};
	end
	`Addr_TRACK2_sTGC_lane6_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_sTGC_lane6_data3[1:0]};
	end
	
	`Addr_TRACK2_MM_lane0_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_MM_lane0_data0[1:0]};
	end
	`Addr_TRACK2_MM_lane0_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_MM_lane0_data1[1:0]};
	end
	`Addr_TRACK2_MM_lane0_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_MM_lane0_data2[1:0]};
	end
	`Addr_TRACK2_MM_lane0_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_MM_lane0_data3[1:0]};
	end
	
	`Addr_TRACK2_MM_lane1_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_MM_lane1_data0[1:0]};
	end
	`Addr_TRACK2_MM_lane1_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_MM_lane1_data1[1:0]};
	end
	`Addr_TRACK2_MM_lane1_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_MM_lane1_data2[1:0]};
	end
	`Addr_TRACK2_MM_lane1_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_MM_lane1_data3[1:0]};
	end
	
	`Addr_TRACK2_MM_lane2_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_MM_lane2_data0[1:0]};
	end
	`Addr_TRACK2_MM_lane2_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_MM_lane2_data1[1:0]};
	end
	`Addr_TRACK2_MM_lane2_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_MM_lane2_data2[1:0]};
	end
	`Addr_TRACK2_MM_lane2_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_MM_lane2_data3[1:0]};
	end
	
	`Addr_TRACK2_MM_lane3_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_MM_lane3_data0[1:0]};
	end
	`Addr_TRACK2_MM_lane3_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_MM_lane3_data1[1:0]};
	end
	`Addr_TRACK2_MM_lane3_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_MM_lane3_data2[1:0]};
	end
	`Addr_TRACK2_MM_lane3_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_MM_lane3_data3[1:0]};
	end
	
	`Addr_TRACK2_MM_lane4_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_MM_lane4_data0[1:0]};
	end
	`Addr_TRACK2_MM_lane4_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_MM_lane4_data1[1:0]};
	end
	`Addr_TRACK2_MM_lane4_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_MM_lane4_data2[1:0]};
	end
	`Addr_TRACK2_MM_lane4_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_MM_lane4_data3[1:0]};
	end
	
	`Addr_TRACK2_MM_lane5_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_MM_lane5_data0[1:0]};
	end
	`Addr_TRACK2_MM_lane5_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_MM_lane5_data1[1:0]};
	end
	`Addr_TRACK2_MM_lane5_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_MM_lane5_data2[1:0]};
	end
	`Addr_TRACK2_MM_lane5_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_MM_lane5_data3[1:0]};
	end
	
	`Addr_TRACK2_MM_lane6_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_MM_lane6_data0[1:0]};
	end
	`Addr_TRACK2_MM_lane6_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_MM_lane6_data1[1:0]};
	end
	`Addr_TRACK2_MM_lane6_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_MM_lane6_data2[1:0]};
	end
	`Addr_TRACK2_MM_lane6_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK2_MM_lane6_data3[1:0]};
	end
	
	`Addr_TRACK2_DTHETA_lane0_0 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK2_DTHETA_lane0_data0[4:0]};
	end
	`Addr_TRACK2_DTHETA_lane0_1 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK2_DTHETA_lane0_data1[4:0]};
	end
	`Addr_TRACK2_DTHETA_lane0_2 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK2_DTHETA_lane0_data2[4:0]};
	end
	`Addr_TRACK2_DTHETA_lane0_3 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK2_DTHETA_lane0_data3[4:0]};
	end
	
	`Addr_TRACK2_DTHETA_lane1_0 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK2_DTHETA_lane1_data0[4:0]};
	end
	`Addr_TRACK2_DTHETA_lane1_1 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK2_DTHETA_lane1_data1[4:0]};
	end
	`Addr_TRACK2_DTHETA_lane1_2 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK2_DTHETA_lane1_data2[4:0]};
	end
	`Addr_TRACK2_DTHETA_lane1_3 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK2_DTHETA_lane1_data3[4:0]};
	end
	
	`Addr_TRACK2_DTHETA_lane2_0 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK2_DTHETA_lane2_data0[4:0]};
	end
	`Addr_TRACK2_DTHETA_lane2_1 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK2_DTHETA_lane2_data1[4:0]};
	end
	`Addr_TRACK2_DTHETA_lane2_2 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK2_DTHETA_lane2_data2[4:0]};
	end
	`Addr_TRACK2_DTHETA_lane2_3 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK2_DTHETA_lane2_data3[4:0]};
	end
	
	`Addr_TRACK2_DTHETA_lane3_0 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK2_DTHETA_lane3_data0[4:0]};
	end
	`Addr_TRACK2_DTHETA_lane3_1 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK2_DTHETA_lane3_data1[4:0]};
	end
	`Addr_TRACK2_DTHETA_lane3_2 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK2_DTHETA_lane3_data2[4:0]};
	end
	`Addr_TRACK2_DTHETA_lane3_3 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK2_DTHETA_lane3_data3[4:0]};
	end
	
	`Addr_TRACK2_DTHETA_lane4_0 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK2_DTHETA_lane4_data0[4:0]};
	end
	`Addr_TRACK2_DTHETA_lane4_1 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK2_DTHETA_lane4_data1[4:0]};
	end
	`Addr_TRACK2_DTHETA_lane4_2 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK2_DTHETA_lane4_data2[4:0]};
	end
	`Addr_TRACK2_DTHETA_lane4_3 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK2_DTHETA_lane4_data3[4:0]};
	end
	
	`Addr_TRACK2_DTHETA_lane5_0 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK2_DTHETA_lane5_data0[4:0]};
	end
	`Addr_TRACK2_DTHETA_lane5_1 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK2_DTHETA_lane5_data1[4:0]};
	end
	`Addr_TRACK2_DTHETA_lane5_2 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK2_DTHETA_lane5_data2[4:0]};
	end
	`Addr_TRACK2_DTHETA_lane5_3 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK2_DTHETA_lane5_data3[4:0]};
	end
	
	`Addr_TRACK2_DTHETA_lane6_0 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK2_DTHETA_lane6_data0[4:0]};
	end
	`Addr_TRACK2_DTHETA_lane6_1 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK2_DTHETA_lane6_data1[4:0]};
	end
	`Addr_TRACK2_DTHETA_lane6_2 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK2_DTHETA_lane6_data2[4:0]};
	end
	`Addr_TRACK2_DTHETA_lane6_3 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK2_DTHETA_lane6_data3[4:0]};
	end
	
	`Addr_TRACK2_PHI_lane0_0 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK2_PHI_lane0_data0[5:0]};
	end
	`Addr_TRACK2_PHI_lane0_1 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK2_PHI_lane0_data1[5:0]};
	end
	`Addr_TRACK2_PHI_lane0_2 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK2_PHI_lane0_data2[5:0]};
	end
	`Addr_TRACK2_PHI_lane0_3 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK2_PHI_lane0_data3[5:0]};
	end
	
	`Addr_TRACK2_PHI_lane1_0 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK2_PHI_lane1_data0[5:0]};
	end
	`Addr_TRACK2_PHI_lane1_1 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK2_PHI_lane1_data1[5:0]};
	end
	`Addr_TRACK2_PHI_lane1_2 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK2_PHI_lane1_data2[5:0]};
	end
	`Addr_TRACK2_PHI_lane1_3 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK2_PHI_lane1_data3[5:0]};
	end
	
	`Addr_TRACK2_PHI_lane2_0 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK2_PHI_lane2_data0[5:0]};
	end
	`Addr_TRACK2_PHI_lane2_1 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK2_PHI_lane2_data1[5:0]};
	end
	`Addr_TRACK2_PHI_lane2_2 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK2_PHI_lane2_data2[5:0]};
	end
	`Addr_TRACK2_PHI_lane2_3 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK2_PHI_lane2_data3[5:0]};
	end
	
	`Addr_TRACK2_PHI_lane3_0 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK2_PHI_lane3_data0[5:0]};
	end
	`Addr_TRACK2_PHI_lane3_1 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK2_PHI_lane3_data1[5:0]};
	end
	`Addr_TRACK2_PHI_lane3_2 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK2_PHI_lane3_data2[5:0]};
	end
	`Addr_TRACK2_PHI_lane3_3 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK2_PHI_lane3_data3[5:0]};
	end
	
	`Addr_TRACK2_PHI_lane4_0 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK2_PHI_lane4_data0[5:0]};
	end
	`Addr_TRACK2_PHI_lane4_1 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK2_PHI_lane4_data1[5:0]};
	end
	`Addr_TRACK2_PHI_lane4_2 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK2_PHI_lane4_data2[5:0]};
	end
	`Addr_TRACK2_PHI_lane4_3 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK2_PHI_lane4_data3[5:0]};
	end
	
	`Addr_TRACK2_PHI_lane5_0 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK2_PHI_lane5_data0[5:0]};
	end
	`Addr_TRACK2_PHI_lane5_1 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK2_PHI_lane5_data1[5:0]};
	end
	`Addr_TRACK2_PHI_lane5_2 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK2_PHI_lane5_data2[5:0]};
	end
	`Addr_TRACK2_PHI_lane5_3 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK2_PHI_lane5_data3[5:0]};
	end
	
	`Addr_TRACK2_PHI_lane6_0 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK2_PHI_lane6_data0[5:0]};
	end
	`Addr_TRACK2_PHI_lane6_1 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK2_PHI_lane6_data1[5:0]};
	end
	`Addr_TRACK2_PHI_lane6_2 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK2_PHI_lane6_data2[5:0]};
	end
	`Addr_TRACK2_PHI_lane6_3 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK2_PHI_lane6_data3[5:0]};
	end
	
	`Addr_TRACK2_ETA_lane0_0 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK2_ETA_lane0_data0[7:0]};
	end
	`Addr_TRACK2_ETA_lane0_1 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK2_ETA_lane0_data1[7:0]};
	end
	`Addr_TRACK2_ETA_lane0_2 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK2_ETA_lane0_data2[7:0]};
	end
	`Addr_TRACK2_ETA_lane0_3 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK2_ETA_lane0_data3[7:0]};
	end
	
	`Addr_TRACK2_ETA_lane1_0 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK2_ETA_lane1_data0[7:0]};
	end
	`Addr_TRACK2_ETA_lane1_1 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK2_ETA_lane1_data1[7:0]};
	end
	`Addr_TRACK2_ETA_lane1_2 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK2_ETA_lane1_data2[7:0]};
	end
	`Addr_TRACK2_ETA_lane1_3 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK2_ETA_lane1_data3[7:0]};
	end
	
	`Addr_TRACK2_ETA_lane2_0 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK2_ETA_lane2_data0[7:0]};
	end
	`Addr_TRACK2_ETA_lane2_1 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK2_ETA_lane2_data1[7:0]};
	end
	`Addr_TRACK2_ETA_lane2_2 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK2_ETA_lane2_data2[7:0]};
	end
	`Addr_TRACK2_ETA_lane2_3 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK2_ETA_lane2_data3[7:0]};
	end
	
	`Addr_TRACK2_ETA_lane3_0 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK2_ETA_lane3_data0[7:0]};
	end
	`Addr_TRACK2_ETA_lane3_1 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK2_ETA_lane3_data1[7:0]};
	end
	`Addr_TRACK2_ETA_lane3_2 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK2_ETA_lane3_data2[7:0]};
	end
	`Addr_TRACK2_ETA_lane3_3 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK2_ETA_lane3_data3[7:0]};
	end
	
	`Addr_TRACK2_ETA_lane4_0 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK2_ETA_lane4_data0[7:0]};
	end
	`Addr_TRACK2_ETA_lane4_1 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK2_ETA_lane4_data1[7:0]};
	end
	`Addr_TRACK2_ETA_lane4_2 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK2_ETA_lane4_data2[7:0]};
	end
	`Addr_TRACK2_ETA_lane4_3 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK2_ETA_lane4_data3[7:0]};
	end
	
	`Addr_TRACK2_ETA_lane5_0 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK2_ETA_lane5_data0[7:0]};
	end
	`Addr_TRACK2_ETA_lane5_1 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK2_ETA_lane5_data1[7:0]};
	end
	`Addr_TRACK2_ETA_lane5_2 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK2_ETA_lane5_data2[7:0]};
	end
	`Addr_TRACK2_ETA_lane5_3 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK2_ETA_lane5_data3[7:0]};
	end
	
	`Addr_TRACK2_ETA_lane6_0 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK2_ETA_lane6_data0[7:0]};
	end
	`Addr_TRACK2_ETA_lane6_1 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK2_ETA_lane6_data1[7:0]};
	end
	`Addr_TRACK2_ETA_lane6_2 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK2_ETA_lane6_data2[7:0]};
	end
	`Addr_TRACK2_ETA_lane6_3 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK2_ETA_lane6_data3[7:0]};
	end
	
	`Addr_TRACK3_SPARE_lane0_0 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK3_SPARE_lane0_data0};
	end
	`Addr_TRACK3_SPARE_lane0_1 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK3_SPARE_lane0_data1};
	end
	`Addr_TRACK3_SPARE_lane0_2 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK3_SPARE_lane0_data2};
	end
	`Addr_TRACK3_SPARE_lane0_3 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK3_SPARE_lane0_data3};
	end
	
	`Addr_TRACK3_SPARE_lane1_0 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK3_SPARE_lane1_data0};
	end
	`Addr_TRACK3_SPARE_lane1_1 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK3_SPARE_lane1_data1};
	end
	`Addr_TRACK3_SPARE_lane1_2 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK3_SPARE_lane1_data2};
	end
	`Addr_TRACK3_SPARE_lane1_3 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK3_SPARE_lane1_data3};
	end
	
	`Addr_TRACK3_SPARE_lane2_0 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK3_SPARE_lane2_data0};
	end
	`Addr_TRACK3_SPARE_lane2_1 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK3_SPARE_lane2_data1};
	end
	`Addr_TRACK3_SPARE_lane2_2 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK3_SPARE_lane2_data2};
	end
	`Addr_TRACK3_SPARE_lane2_3 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK3_SPARE_lane2_data3};
	end
	
	`Addr_TRACK3_SPARE_lane3_0 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK3_SPARE_lane3_data0};
	end
	`Addr_TRACK3_SPARE_lane3_1 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK3_SPARE_lane3_data1};
	end
	`Addr_TRACK3_SPARE_lane3_2 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK3_SPARE_lane3_data2};
	end
	`Addr_TRACK3_SPARE_lane3_3 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK3_SPARE_lane3_data3};
	end
	
	`Addr_TRACK3_SPARE_lane4_0 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK3_SPARE_lane4_data0};
	end
	`Addr_TRACK3_SPARE_lane4_1 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK3_SPARE_lane4_data1};
	end
	`Addr_TRACK3_SPARE_lane4_2 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK3_SPARE_lane4_data2};
	end
	`Addr_TRACK3_SPARE_lane4_3 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK3_SPARE_lane4_data3};
	end
	
	`Addr_TRACK3_SPARE_lane5_0 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK3_SPARE_lane5_data0};
	end
	`Addr_TRACK3_SPARE_lane5_1 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK3_SPARE_lane5_data1};
	end
	`Addr_TRACK3_SPARE_lane5_2 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK3_SPARE_lane5_data2};
	end
	`Addr_TRACK3_SPARE_lane5_3 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK3_SPARE_lane5_data3};
	end
	
	`Addr_TRACK3_SPARE_lane6_0 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK3_SPARE_lane6_data0};
	end
	`Addr_TRACK3_SPARE_lane6_1 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK3_SPARE_lane6_data1};
	end
	`Addr_TRACK3_SPARE_lane6_2 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK3_SPARE_lane6_data2};
	end
	`Addr_TRACK3_SPARE_lane6_3 : begin
	    VME_DOUT[15:0] <= {15'b0, TRACK3_SPARE_lane6_data3};
	end
	
	`Addr_TRACK3_sTGC_lane0_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_sTGC_lane0_data0[1:0]};
	end
	`Addr_TRACK3_sTGC_lane0_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_sTGC_lane0_data1[1:0]};
	end
	`Addr_TRACK3_sTGC_lane0_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_sTGC_lane0_data2[1:0]};
	end
	`Addr_TRACK3_sTGC_lane0_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_sTGC_lane0_data3[1:0]};
	end
	
	`Addr_TRACK3_sTGC_lane1_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_sTGC_lane1_data0[1:0]};
	end
	`Addr_TRACK3_sTGC_lane1_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_sTGC_lane1_data1[1:0]};
	end
	`Addr_TRACK3_sTGC_lane1_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_sTGC_lane1_data2[1:0]};
	end
	`Addr_TRACK3_sTGC_lane1_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_sTGC_lane1_data3[1:0]};
	end
	
	`Addr_TRACK3_sTGC_lane2_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_sTGC_lane2_data0[1:0]};
	end
	`Addr_TRACK3_sTGC_lane2_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_sTGC_lane2_data1[1:0]};
	end
	`Addr_TRACK3_sTGC_lane2_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_sTGC_lane2_data2[1:0]};
	end
	`Addr_TRACK3_sTGC_lane2_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_sTGC_lane2_data3[1:0]};
	end
	
	`Addr_TRACK3_sTGC_lane3_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_sTGC_lane3_data0[1:0]};
	end
	`Addr_TRACK3_sTGC_lane3_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_sTGC_lane3_data1[1:0]};
	end
	`Addr_TRACK3_sTGC_lane3_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_sTGC_lane3_data2[1:0]};
	end
	`Addr_TRACK3_sTGC_lane3_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_sTGC_lane3_data3[1:0]};
	end
	
	`Addr_TRACK3_sTGC_lane4_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_sTGC_lane4_data0[1:0]};
	end
	`Addr_TRACK3_sTGC_lane4_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_sTGC_lane4_data1[1:0]};
	end
	`Addr_TRACK3_sTGC_lane4_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_sTGC_lane4_data2[1:0]};
	end
	`Addr_TRACK3_sTGC_lane4_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_sTGC_lane4_data3[1:0]};
	end
	
	`Addr_TRACK3_sTGC_lane5_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_sTGC_lane5_data0[1:0]};
	end
	`Addr_TRACK3_sTGC_lane5_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_sTGC_lane5_data1[1:0]};
	end
	`Addr_TRACK3_sTGC_lane5_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_sTGC_lane5_data2[1:0]};
	end
	`Addr_TRACK3_sTGC_lane5_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_sTGC_lane5_data3[1:0]};
	end
	
	`Addr_TRACK3_sTGC_lane6_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_sTGC_lane6_data0[1:0]};
	end
	`Addr_TRACK3_sTGC_lane6_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_sTGC_lane6_data1[1:0]};
	end
	`Addr_TRACK3_sTGC_lane6_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_sTGC_lane6_data2[1:0]};
	end
	`Addr_TRACK3_sTGC_lane6_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_sTGC_lane6_data3[1:0]};
	end
	
	`Addr_TRACK3_MM_lane0_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_MM_lane0_data0[1:0]};
	end
	`Addr_TRACK3_MM_lane0_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_MM_lane0_data1[1:0]};
	end
	`Addr_TRACK3_MM_lane0_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_MM_lane0_data2[1:0]};
	end
	`Addr_TRACK3_MM_lane0_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_MM_lane0_data3[1:0]};
	end
	
	`Addr_TRACK3_MM_lane1_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_MM_lane1_data0[1:0]};
	end
	`Addr_TRACK3_MM_lane1_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_MM_lane1_data1[1:0]};
	end
	`Addr_TRACK3_MM_lane1_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_MM_lane1_data2[1:0]};
	end
	`Addr_TRACK3_MM_lane1_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_MM_lane1_data3[1:0]};
	end
	
	`Addr_TRACK3_MM_lane2_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_MM_lane2_data0[1:0]};
	end
	`Addr_TRACK3_MM_lane2_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_MM_lane2_data1[1:0]};
	end
	`Addr_TRACK3_MM_lane2_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_MM_lane2_data2[1:0]};
	end
	`Addr_TRACK3_MM_lane2_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_MM_lane2_data3[1:0]};
	end
	
	`Addr_TRACK3_MM_lane3_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_MM_lane3_data0[1:0]};
	end
	`Addr_TRACK3_MM_lane3_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_MM_lane3_data1[1:0]};
	end
	`Addr_TRACK3_MM_lane3_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_MM_lane3_data2[1:0]};
	end
	`Addr_TRACK3_MM_lane3_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_MM_lane3_data3[1:0]};
	end
	
	`Addr_TRACK3_MM_lane4_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_MM_lane4_data0[1:0]};
	end
	`Addr_TRACK3_MM_lane4_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_MM_lane4_data1[1:0]};
	end
	`Addr_TRACK3_MM_lane4_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_MM_lane4_data2[1:0]};
	end
	`Addr_TRACK3_MM_lane4_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_MM_lane4_data3[1:0]};
	end
	
	`Addr_TRACK3_MM_lane5_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_MM_lane5_data0[1:0]};
	end
	`Addr_TRACK3_MM_lane5_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_MM_lane5_data1[1:0]};
	end
	`Addr_TRACK3_MM_lane5_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_MM_lane5_data2[1:0]};
	end
	`Addr_TRACK3_MM_lane5_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_MM_lane5_data3[1:0]};
	end
	
	`Addr_TRACK3_MM_lane6_0 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_MM_lane6_data0[1:0]};
	end
	`Addr_TRACK3_MM_lane6_1 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_MM_lane6_data1[1:0]};
	end
	`Addr_TRACK3_MM_lane6_2 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_MM_lane6_data2[1:0]};
	end
	`Addr_TRACK3_MM_lane6_3 : begin
	    VME_DOUT[15:0] <= {14'b0, TRACK3_MM_lane6_data3[1:0]};
	end
	
	`Addr_TRACK3_DTHETA_lane0_0 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK3_DTHETA_lane0_data0[4:0]};
	end
	`Addr_TRACK3_DTHETA_lane0_1 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK3_DTHETA_lane0_data1[4:0]};
	end
	`Addr_TRACK3_DTHETA_lane0_2 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK3_DTHETA_lane0_data2[4:0]};
	end
	`Addr_TRACK3_DTHETA_lane0_3 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK3_DTHETA_lane0_data3[4:0]};
	end
	
	`Addr_TRACK3_DTHETA_lane1_0 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK3_DTHETA_lane1_data0[4:0]};
	end
	`Addr_TRACK3_DTHETA_lane1_1 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK3_DTHETA_lane1_data1[4:0]};
	end
	`Addr_TRACK3_DTHETA_lane1_2 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK3_DTHETA_lane1_data2[4:0]};
	end
	`Addr_TRACK3_DTHETA_lane1_3 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK3_DTHETA_lane1_data3[4:0]};
	end
	
	`Addr_TRACK3_DTHETA_lane2_0 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK3_DTHETA_lane2_data0[4:0]};
	end
	`Addr_TRACK3_DTHETA_lane2_1 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK3_DTHETA_lane2_data1[4:0]};
	end
	`Addr_TRACK3_DTHETA_lane2_2 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK3_DTHETA_lane2_data2[4:0]};
	end
	`Addr_TRACK3_DTHETA_lane2_3 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK3_DTHETA_lane2_data3[4:0]};
	end
	
	`Addr_TRACK3_DTHETA_lane3_0 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK3_DTHETA_lane3_data0[4:0]};
	end
	`Addr_TRACK3_DTHETA_lane3_1 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK3_DTHETA_lane3_data1[4:0]};
	end
	`Addr_TRACK3_DTHETA_lane3_2 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK3_DTHETA_lane3_data2[4:0]};
	end
	`Addr_TRACK3_DTHETA_lane3_3 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK3_DTHETA_lane3_data3[4:0]};
	end
	
	`Addr_TRACK3_DTHETA_lane4_0 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK3_DTHETA_lane4_data0[4:0]};
	end
	`Addr_TRACK3_DTHETA_lane4_1 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK3_DTHETA_lane4_data1[4:0]};
	end
	`Addr_TRACK3_DTHETA_lane4_2 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK3_DTHETA_lane4_data2[4:0]};
	end
	`Addr_TRACK3_DTHETA_lane4_3 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK3_DTHETA_lane4_data3[4:0]};
	end
	
	`Addr_TRACK3_DTHETA_lane5_0 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK3_DTHETA_lane5_data0[4:0]};
	end
	`Addr_TRACK3_DTHETA_lane5_1 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK3_DTHETA_lane5_data1[4:0]};
	end
	`Addr_TRACK3_DTHETA_lane5_2 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK3_DTHETA_lane5_data2[4:0]};
	end
	`Addr_TRACK3_DTHETA_lane5_3 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK3_DTHETA_lane5_data3[4:0]};
	end
	
	`Addr_TRACK3_DTHETA_lane6_0 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK3_DTHETA_lane6_data0[4:0]};
	end
	`Addr_TRACK3_DTHETA_lane6_1 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK3_DTHETA_lane6_data1[4:0]};
	end
	`Addr_TRACK3_DTHETA_lane6_2 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK3_DTHETA_lane6_data2[4:0]};
	end
	`Addr_TRACK3_DTHETA_lane6_3 : begin
	    VME_DOUT[15:0] <= {11'b0, TRACK3_DTHETA_lane6_data3[4:0]};
	end
	
	`Addr_TRACK3_PHI_lane0_0 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK3_PHI_lane0_data0[5:0]};
	end
	`Addr_TRACK3_PHI_lane0_1 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK3_PHI_lane0_data1[5:0]};
	end
	`Addr_TRACK3_PHI_lane0_2 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK3_PHI_lane0_data2[5:0]};
	end
	`Addr_TRACK3_PHI_lane0_3 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK3_PHI_lane0_data3[5:0]};
	end
	
	`Addr_TRACK3_PHI_lane1_0 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK3_PHI_lane1_data0[5:0]};
	end
	`Addr_TRACK3_PHI_lane1_1 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK3_PHI_lane1_data1[5:0]};
	end
	`Addr_TRACK3_PHI_lane1_2 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK3_PHI_lane1_data2[5:0]};
	end
	`Addr_TRACK3_PHI_lane1_3 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK3_PHI_lane1_data3[5:0]};
	end
	
	`Addr_TRACK3_PHI_lane2_0 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK3_PHI_lane2_data0[5:0]};
	end
	`Addr_TRACK3_PHI_lane2_1 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK3_PHI_lane2_data1[5:0]};
	end
	`Addr_TRACK3_PHI_lane2_2 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK3_PHI_lane2_data2[5:0]};
	end
	`Addr_TRACK3_PHI_lane2_3 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK3_PHI_lane2_data3[5:0]};
	end
	
	`Addr_TRACK3_PHI_lane3_0 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK3_PHI_lane3_data0[5:0]};
	end
	`Addr_TRACK3_PHI_lane3_1 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK3_PHI_lane3_data1[5:0]};
	end
	`Addr_TRACK3_PHI_lane3_2 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK3_PHI_lane3_data2[5:0]};
	end
	`Addr_TRACK3_PHI_lane3_3 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK3_PHI_lane3_data3[5:0]};
	end
	
	`Addr_TRACK3_PHI_lane4_0 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK3_PHI_lane4_data0[5:0]};
	end
	`Addr_TRACK3_PHI_lane4_1 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK3_PHI_lane4_data1[5:0]};
	end
	`Addr_TRACK3_PHI_lane4_2 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK3_PHI_lane4_data2[5:0]};
	end
	`Addr_TRACK3_PHI_lane4_3 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK3_PHI_lane4_data3[5:0]};
	end
	
	`Addr_TRACK3_PHI_lane5_0 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK3_PHI_lane5_data0[5:0]};
	end
	`Addr_TRACK3_PHI_lane5_1 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK3_PHI_lane5_data1[5:0]};
	end
	`Addr_TRACK3_PHI_lane5_2 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK3_PHI_lane5_data2[5:0]};
	end
	`Addr_TRACK3_PHI_lane5_3 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK3_PHI_lane5_data3[5:0]};
	end
	
	`Addr_TRACK3_PHI_lane6_0 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK3_PHI_lane6_data0[5:0]};
	end
	`Addr_TRACK3_PHI_lane6_1 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK3_PHI_lane6_data1[5:0]};
	end
	`Addr_TRACK3_PHI_lane6_2 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK3_PHI_lane6_data2[5:0]};
	end
	`Addr_TRACK3_PHI_lane6_3 : begin
	    VME_DOUT[15:0] <= {10'b0, TRACK3_PHI_lane6_data3[5:0]};
	end
	
	`Addr_TRACK3_ETA_lane0_0 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK3_ETA_lane0_data0[7:0]};
	end
	`Addr_TRACK3_ETA_lane0_1 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK3_ETA_lane0_data1[7:0]};
	end
	`Addr_TRACK3_ETA_lane0_2 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK3_ETA_lane0_data2[7:0]};
	end
	`Addr_TRACK3_ETA_lane0_3 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK3_ETA_lane0_data3[7:0]};
	end
	
	`Addr_TRACK3_ETA_lane1_0 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK3_ETA_lane1_data0[7:0]};
	end
	`Addr_TRACK3_ETA_lane1_1 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK3_ETA_lane1_data1[7:0]};
	end
	`Addr_TRACK3_ETA_lane1_2 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK3_ETA_lane1_data2[7:0]};
	end
	`Addr_TRACK3_ETA_lane1_3 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK3_ETA_lane1_data3[7:0]};
	end
	
	`Addr_TRACK3_ETA_lane2_0 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK3_ETA_lane2_data0[7:0]};
	end
	`Addr_TRACK3_ETA_lane2_1 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK3_ETA_lane2_data1[7:0]};
	end
	`Addr_TRACK3_ETA_lane2_2 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK3_ETA_lane2_data2[7:0]};
	end
	`Addr_TRACK3_ETA_lane2_3 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK3_ETA_lane2_data3[7:0]};
	end
	
	`Addr_TRACK3_ETA_lane3_0 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK3_ETA_lane3_data0[7:0]};
	end
	`Addr_TRACK3_ETA_lane3_1 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK3_ETA_lane3_data1[7:0]};
	end
	`Addr_TRACK3_ETA_lane3_2 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK3_ETA_lane3_data2[7:0]};
	end
	`Addr_TRACK3_ETA_lane3_3 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK3_ETA_lane3_data3[7:0]};
	end
	
	`Addr_TRACK3_ETA_lane4_0 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK3_ETA_lane4_data0[7:0]};
	end
	`Addr_TRACK3_ETA_lane4_1 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK3_ETA_lane4_data1[7:0]};
	end
	`Addr_TRACK3_ETA_lane4_2 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK3_ETA_lane4_data2[7:0]};
	end
	`Addr_TRACK3_ETA_lane4_3 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK3_ETA_lane4_data3[7:0]};
	end
	
	`Addr_TRACK3_ETA_lane5_0 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK3_ETA_lane5_data0[7:0]};
	end
	`Addr_TRACK3_ETA_lane5_1 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK3_ETA_lane5_data1[7:0]};
	end
	`Addr_TRACK3_ETA_lane5_2 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK3_ETA_lane5_data2[7:0]};
	end
	`Addr_TRACK3_ETA_lane5_3 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK3_ETA_lane5_data3[7:0]};
	end
	
	`Addr_TRACK3_ETA_lane6_0 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK3_ETA_lane6_data0[7:0]};
	end
	`Addr_TRACK3_ETA_lane6_1 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK3_ETA_lane6_data1[7:0]};
	end
	`Addr_TRACK3_ETA_lane6_2 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK3_ETA_lane6_data2[7:0]};
	end
	`Addr_TRACK3_ETA_lane6_3 : begin
	    VME_DOUT[15:0] <= {8'b0, TRACK3_ETA_lane6_data3[7:0]};
	end







        `Addr_MONITORING_RESET : begin
          VME_DOUT[15:0] <= {15'b0, Monitoring_reset_out};
        end
        `Addr_LUT_INIT_RESET : begin
          VME_DOUT[15:0] <= {15'b0, LUT_init_reset_out};
        end
        `Addr_GT0_RX_RESET : begin
          VME_DOUT[15:0] <= {15'b0, gt0_rx_reset_out};
        end
        `Addr_GT1_RX_RESET : begin
          VME_DOUT[15:0] <= {15'b0, gt1_rx_reset_out};
        end
        `Addr_GT2_RX_RESET : begin
          VME_DOUT[15:0] <= {15'b0, gt2_rx_reset_out};
        end
        `Addr_GT3_RX_RESET : begin
          VME_DOUT[15:0] <= {15'b0, gt3_rx_reset_out};
        end
        `Addr_GT4_RX_RESET : begin
          VME_DOUT[15:0] <= {15'b0, gt4_rx_reset_out};
        end
        `Addr_GT5_RX_RESET : begin
          VME_DOUT[15:0] <= {15'b0, gt5_rx_reset_out};
        end
        `Addr_GT6_RX_RESET : begin
          VME_DOUT[15:0] <= {15'b0, gt6_rx_reset_out};
        end
        `Addr_GT7_RX_RESET : begin
          VME_DOUT[15:0] <= {15'b0, gt7_rx_reset_out};
        end
        `Addr_GT8_RX_RESET : begin
          VME_DOUT[15:0] <= {15'b0, gt8_rx_reset_out};
        end
        `Addr_GT9_RX_RESET : begin
          VME_DOUT[15:0] <= {15'b0, gt9_rx_reset_out};
        end
        `Addr_GT10_RX_RESET : begin
          VME_DOUT[15:0] <= {15'b0, gt10_rx_reset_out};
        end
        `Addr_GT11_RX_RESET : begin
          VME_DOUT[15:0] <= {15'b0, gt11_rx_reset_out};
        end


// Error Scaler
        `Addr_ERROR_SCALER_GT0 : begin
            VME_DOUT [15:0] <= Error_Scaler_gt_0[15:0];
        end
        `Addr_ERROR_SCALER_GT1 : begin
            VME_DOUT [15:0] <= Error_Scaler_gt_1[15:0];
        end
        `Addr_ERROR_SCALER_GT2 : begin
            VME_DOUT [15:0] <= Error_Scaler_gt_2[15:0];
        end
        `Addr_ERROR_SCALER_GT3 : begin
            VME_DOUT [15:0] <= Error_Scaler_gt_3[15:0];
        end
        `Addr_ERROR_SCALER_GT4 : begin
            VME_DOUT [15:0] <= Error_Scaler_gt_4[15:0];
        end
        `Addr_ERROR_SCALER_GT5 : begin
            VME_DOUT [15:0] <= Error_Scaler_gt_5[15:0];
        end
        `Addr_ERROR_SCALER_GT6 : begin
            VME_DOUT [15:0] <= Error_Scaler_gt_6[15:0];
        end
        `Addr_ERROR_SCALER_GT7 : begin
            VME_DOUT [15:0] <= Error_Scaler_gt_7[15:0];
        end
        `Addr_ERROR_SCALER_GT8 : begin
            VME_DOUT [15:0] <= Error_Scaler_gt_8[15:0];
        end
        `Addr_ERROR_SCALER_GT9 : begin
            VME_DOUT [15:0] <= Error_Scaler_gt_9[15:0];
        end
        `Addr_ERROR_SCALER_GT10 : begin
            VME_DOUT [15:0] <= Error_Scaler_gt_10[15:0];
        end
        `Addr_ERROR_SCALER_GT11 : begin
            VME_DOUT [15:0] <= Error_Scaler_gt_11[15:0];
        end


// Status
        `Addr_STATUS_GT0 : begin
            VME_DOUT [15:0] <= Status_gt_0[15:0];
        end
        `Addr_STATUS_GT1 : begin
            VME_DOUT [15:0] <= Status_gt_1[15:0];
        end
        `Addr_STATUS_GT2 : begin
            VME_DOUT [15:0] <= Status_gt_2[15:0];
        end
        `Addr_STATUS_GT3 : begin
            VME_DOUT [15:0] <= Status_gt_3[15:0];
        end
        `Addr_STATUS_GT4 : begin
            VME_DOUT [15:0] <= Status_gt_4[15:0];
        end
        `Addr_STATUS_GT5 : begin
            VME_DOUT [15:0] <= Status_gt_5[15:0];
        end
        `Addr_STATUS_GT6 : begin
            VME_DOUT [15:0] <= Status_gt_6[15:0];
        end
        `Addr_STATUS_GT7 : begin
            VME_DOUT [15:0] <= Status_gt_7[15:0];
        end
        `Addr_STATUS_GT8 : begin
            VME_DOUT [15:0] <= Status_gt_8[15:0];
        end
        `Addr_STATUS_GT9 : begin
            VME_DOUT [15:0] <= Status_gt_9[15:0];
        end
        `Addr_STATUS_GT10 : begin
            VME_DOUT [15:0] <= Status_gt_10[15:0];
        end
        `Addr_STATUS_GT11 : begin
            VME_DOUT [15:0] <= Status_gt_11[15:0];
        end
      
// Delay      
        `Addr_DELAY_GTX0 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_gtx0[7:0]};
        end
        `Addr_DELAY_GTX1 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_gtx1[7:0]};
        end
        `Addr_DELAY_GTX2 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_gtx2[7:0]};
        end
        `Addr_DELAY_GTX3 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_gtx3[7:0]};
        end
        `Addr_DELAY_GTX4 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_gtx4[7:0]};
        end
        `Addr_DELAY_GTX5 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_gtx5[7:0]};
        end
        `Addr_DELAY_GTX6 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_gtx6[7:0]};
        end
        `Addr_DELAY_GTX7 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_gtx7[7:0]};
        end
        `Addr_DELAY_GTX8 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_gtx8[7:0]};
        end
        `Addr_DELAY_GTX9 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_gtx9[7:0]};
        end
        `Addr_DELAY_GTX10 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_gtx10[7:0]};
        end
        `Addr_DELAY_GTX11 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_gtx11[7:0]};
        end

        `Addr_DELAY_GLINK0 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_glink0[7:0]};
        end
        `Addr_DELAY_GLINK1 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_glink1[7:0]};
        end
        `Addr_DELAY_GLINK2 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_glink2[7:0]};
        end
        `Addr_DELAY_GLINK3 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_glink3[7:0]};
        end
        `Addr_DELAY_GLINK4 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_glink4[7:0]};
        end
        `Addr_DELAY_GLINK5 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_glink5[7:0]};
        end
        `Addr_DELAY_GLINK6 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_glink6[7:0]};
        end
        `Addr_DELAY_GLINK7 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_glink7[7:0]};
        end
        `Addr_DELAY_GLINK8 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_glink8[7:0]};
        end
        `Addr_DELAY_GLINK9 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_glink9[7:0]};
        end
        `Addr_DELAY_GLINK10 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_glink10[7:0]};
        end
        `Addr_DELAY_GLINK11 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_glink11[7:0]};
        end
        `Addr_DELAY_GLINK12 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_glink12[7:0]};
        end
        `Addr_DELAY_GLINK13 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_glink13[7:0]};
        end
      
        `Addr_DELAY_L1A : begin
             VME_DOUT [15:0] <= {8'b0, Delay_L1A[7:0]};
        end
        `Addr_DELAY_BCR : begin
             VME_DOUT [15:0] <= {8'b0, Delay_BCR[7:0]};
        end
        `Addr_DELAY_TRIG_BCR : begin
             VME_DOUT [15:0] <= {8'b0, Delay_trig_BCR[7:0]};
        end
        `Addr_DELAY_ECR : begin
             VME_DOUT [15:0] <= {8'b0, Delay_ECR[7:0]};
        end
        `Addr_DELAY_TTC_RESET : begin
             VME_DOUT [15:0] <= {8'b0, Delay_TTC_RESET[7:0]};
        end
        `Addr_DELAY_TEST_PULSE : begin
             VME_DOUT [15:0] <= {8'b0, Delay_TEST_PULSE[7:0]};
        end



// Mask

        `Addr_MASK_RX0 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_gtx0[1:0]};
        end
        `Addr_MASK_RX1 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_gtx1[1:0]};
        end
        `Addr_MASK_RX2 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_gtx2[1:0]};
        end
        `Addr_MASK_RX3 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_gtx3[1:0]};
        end
        `Addr_MASK_RX4 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_gtx4[1:0]};
        end
        `Addr_MASK_RX5 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_gtx5[1:0]};
        end
        `Addr_MASK_RX6 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_gtx6[1:0]};
        end
        `Addr_MASK_RX7 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_gtx7[1:0]};
        end
        `Addr_MASK_RX8 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_gtx8[1:0]};
        end
        `Addr_MASK_RX9 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_gtx9[1:0]};
        end
        `Addr_MASK_RX10 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_gtx10[1:0]};
        end
        `Addr_MASK_RX11 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_gtx11[1:0]};
        end

        `Addr_MASK_L1A : begin
             VME_DOUT [15:0] <= {15'b0, Mask_L1A};
        end
        `Addr_MASK_BCR : begin
             VME_DOUT [15:0] <= {15'b0, Mask_BCR};
        end
        `Addr_MASK_TRIG_BCR : begin
             VME_DOUT [15:0] <= {15'b0, Mask_trig_BCR};
        end
        `Addr_MASK_ECR : begin
             VME_DOUT [15:0] <= {15'b0, Mask_ECR};
        end
        `Addr_MASK_TTC_RESET : begin
             VME_DOUT [15:0] <= {15'b0, Mask_TTC_RESET};
        end
        `Addr_MASK_TEST_PULSE : begin
             VME_DOUT [15:0] <= {15'b0, Mask_TEST_PULSE};
        end


        `Addr_MASK_GLINK0 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_glink0[1:0]};
        end
        `Addr_MASK_GLINK1 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_glink1[1:0]};
        end
        `Addr_MASK_GLINK2 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_glink2[1:0]};
        end
        `Addr_MASK_GLINK3 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_glink3[1:0]};
        end
        `Addr_MASK_GLINK4 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_glink4[1:0]};
        end
        `Addr_MASK_GLINK5 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_glink5[1:0]};
        end
        `Addr_MASK_GLINK6 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_glink6[1:0]};
        end
        `Addr_MASK_GLINK7 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_glink7[1:0]};
        end
        `Addr_MASK_GLINK8 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_glink8[1:0]};
        end
        `Addr_MASK_GLINK9 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_glink9[1:0]};
        end
        `Addr_MASK_GLINK10 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_glink10[1:0]};
        end
        `Addr_MASK_GLINK11 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_glink11[1:0]};
        end
        `Addr_MASK_GLINK12 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_glink12[1:0]};
        end
        `Addr_MASK_GLINK13 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_glink13[1:0]};
        end

        `Addr_TEST_PULSE_LENGTH: begin
             VME_DOUT [15:0] <=  Test_Pulse_Length[15:0];
        end
        `Addr_TEST_PULSE_WAIT_LENGTH: begin
             VME_DOUT [15:0] <=  Test_Pulse_Wait_Length[15:0];
        end
        `Addr_TEST_PULSE_ENABLE: begin
             VME_DOUT [15:0] <=  {15'b0, Test_Pulse_Enable};
        end


// IDs
        `Addr_L1A_COUNTER0 : begin
            VME_DOUT [15:0] <= L1A_Counter0[15:0]; 
        end
        `Addr_L1A_COUNTER1 : begin
            VME_DOUT [15:0] <= L1A_Counter1[15:0]; 
        end
        `Addr_L1ID : begin
            VME_DOUT [15:0] <= {4'b0, L1ID[11:0]}; 
        end
        `Addr_BCID : begin
            VME_DOUT [15:0] <= {4'b0, BCID[11:0]}; 
        end
        `Addr_SLID : begin
            VME_DOUT [15:0] <= {4'b0, SLID[11:0]}; 
        end
        `Addr_READOUT_BC : begin
            VME_DOUT [15:0] <= Readout_BC;      
        end
        `Addr_L1BUFFER_DEPTH : begin
            VME_DOUT [15:0] <= {9'h0,  L1Buffer_depth[6:0]};      
        end
        `Addr_TRIGL1BUFFER_DEPTH : begin
            VME_DOUT [15:0] <= {9'h0,  trigL1Buffer_depth[6:0]};      
        end
        `Addr_L1BUFFER_BW_DEPTH : begin
            VME_DOUT [15:0] <= {9'h0,  L1Buffer_BW_depth[6:0]};      
        end


// Readout Status
        `Addr_L1BUFFER_STATUS : begin
            VME_DOUT [15:0] <= L1Buffer_status;      
        end
        `Addr_DERANDOMIZER_STATUS : begin
            VME_DOUT [15:0] <= Derandomizer_status[15:0];      
        end
        `Addr_DERANDOMIZER_COUNT1_1 : begin
            VME_DOUT [15:0] <= Derandomizer_Count1_1[15:0];      
        end
        `Addr_DERANDOMIZER_COUNT1_2 : begin
            VME_DOUT [15:0] <= Derandomizer_Count1_2[15:0];      
        end
        `Addr_DERANDOMIZER_COUNT2 : begin
            VME_DOUT [15:0] <= Derandomizer_Count2[15:0];      
        end
        `Addr_DERANDOMIZER_COUNT3 : begin
            VME_DOUT [15:0] <= Derandomizer_Count3[15:0];      
        end
    

        `Addr_ZERO_SUPPRESS_STATUS : begin
            VME_DOUT [15:0] <= ZeroSuppress_status;      
        end
        `Addr_ZERO_SUPPRESS_COUNT : begin
            VME_DOUT [15:0] <= ZeroSuppress_Count;      
        end
        `Addr_DATA_SIZE : begin
            VME_DOUT [15:0] <= {4'b0, data_size};      
        end

        `Addr_SITCP_COUNT : begin
            VME_DOUT [15:0] <= SiTCP_Count;      
        end

        `Addr_BUSY_COUNT : begin
            VME_DOUT [15:0] <= BUSY_Count;      
        end


// TX data, address, send back data 
        `Addr_FULLBOARD_ID : begin
            VME_DOUT [15:0] <= {FullBoard_ID};
        end

    
        `Addr_MONITORING_FIFO_OUT_0 : begin
            VME_DOUT [15:0] <= monitoring_FIFO_out_0[15:0];       
        end
        `Addr_MONITORING_FIFO_OUT_1 : begin
            VME_DOUT [15:0] <= monitoring_FIFO_out_1[15:0];       
        end
        `Addr_MONITORING_FIFO_OUT_2 : begin
            VME_DOUT [15:0] <= monitoring_FIFO_out_2[15:0];       
        end
        `Addr_MONITORING_FIFO_OUT_3 : begin
            VME_DOUT [15:0] <= monitoring_FIFO_out_3[15:0];       
        end
        `Addr_MONITORING_FIFO_OUT_4 : begin
            VME_DOUT [15:0] <= monitoring_FIFO_out_4[15:0];       
        end
        `Addr_MONITORING_FIFO_OUT_5 : begin
            VME_DOUT [15:0] <= monitoring_FIFO_out_5[15:0];       
        end
        `Addr_MONITORING_FIFO_OUT_6 : begin
            VME_DOUT [15:0] <= monitoring_FIFO_out_6[15:0];       
        end
        `Addr_MONITORING_FIFO_OUT_7 : begin
            VME_DOUT [15:0] <= monitoring_FIFO_out_7[15:0];       
        end
        `Addr_MONITORING_FIFO_OUT_8 : begin
            VME_DOUT [15:0] <= monitoring_FIFO_out_8[15:0];       
        end
        `Addr_MONITORING_FIFO_OUT_9 : begin
            VME_DOUT [15:0] <= monitoring_FIFO_out_9[15:0];       
        end
        `Addr_MONITORING_FIFO_OUT_10 : begin
            VME_DOUT [15:0] <= monitoring_FIFO_out_10[15:0];       
        end
        `Addr_MONITORING_FIFO_OUT_11 : begin
            VME_DOUT [15:0] <= monitoring_FIFO_out_11[15:0];       
        end
        `Addr_MONITORING_FIFO_OUT_12 : begin
            VME_DOUT [15:0] <= monitoring_FIFO_out_12[15:0];       
        end
        `Addr_MONITORING_FIFO_OUT_13 : begin
            VME_DOUT [15:0] <= monitoring_FIFO_out_13[15:0];       
        end
        `Addr_MONITORING_FIFO_OUT_14 : begin
            VME_DOUT [15:0] <= monitoring_FIFO_out_14[15:0];       
        end
        `Addr_MONITORING_FIFO_OUT_15 : begin
            VME_DOUT [15:0] <= monitoring_FIFO_out_15[15:0];       
        end
        `Addr_MONITORING_FIFO_wr_en : begin
            VME_DOUT [15:0] <= {15'b0, monitoring_FIFO_wr_en};      
        end
        `Addr_MONITORING_FIFO_rd_en : begin
          VME_DOUT [15:0]<= {15'b0, monitoring_FIFO_rd_en};
        end
        `Addr_LANE_SELECTOR : begin
            VME_DOUT [15:0] <= {9'h0, lane_selector_out[6:0]}; 
        end
        `Addr_XADC_MON_ENABLE : begin
            VME_DOUT [15:0] <= {15'h0, xadc_mon_enable_out}; 
        end
        `Addr_XADC_MON_ADDR  : begin
            VME_DOUT [15:0] <= {9'h0, xadc_mon_addr_out[6:0]}; 
        end              
        `Addr_XADC_MONITOR_OUT : begin
            VME_DOUT [15:0] <= xadc_monitor_in[15:0]; 
        end        
        `Addr_L1A_MANUAL_PARAMETER : begin
            VME_DOUT [15:0] <= L1A_manual_parameter[15:0]; 
        end
        `Addr_BUSY_PROPAGATION : begin
            VME_DOUT [15:0] <= {15'h0, Busy_propagation}; 
        end

        `Addr_GTX0_TEST_DATA1 : begin
            VME_DOUT [15:0] <= gtx0_test_data1[15:0]; 
        end
        `Addr_GTX0_TEST_DATA2 : begin
            VME_DOUT [15:0] <= gtx0_test_data2[15:0]; 
        end
        `Addr_GTX0_TEST_DATA3 : begin
            VME_DOUT [15:0] <= gtx0_test_data3[15:0]; 
        end
        `Addr_GTX0_TEST_DATA4 : begin
            VME_DOUT [15:0] <= gtx0_test_data4[15:0]; 
        end
        `Addr_GTX0_TEST_DATA5 : begin
            VME_DOUT [15:0] <= gtx0_test_data5[15:0]; 
        end
        `Addr_GTX0_TEST_DATA6 : begin
            VME_DOUT [15:0] <= gtx0_test_data6[15:0]; 
        end
        `Addr_GTX0_TEST_DATA7 : begin
            VME_DOUT [15:0] <= gtx0_test_data7[15:0]; 
        end
        `Addr_GTX1_TEST_DATA1 : begin
            VME_DOUT [15:0] <= gtx1_test_data1[15:0]; 
        end
        `Addr_GTX1_TEST_DATA2 : begin
            VME_DOUT [15:0] <= gtx1_test_data2[15:0]; 
        end
        `Addr_GTX1_TEST_DATA3 : begin
            VME_DOUT [15:0] <= gtx1_test_data3[15:0]; 
        end
        `Addr_GTX1_TEST_DATA4 : begin
            VME_DOUT [15:0] <= gtx1_test_data4[15:0]; 
        end
        `Addr_GTX1_TEST_DATA5 : begin
            VME_DOUT [15:0] <= gtx1_test_data5[15:0]; 
        end
        `Addr_GTX1_TEST_DATA6 : begin
            VME_DOUT [15:0] <= gtx1_test_data6[15:0]; 
        end
        `Addr_GTX1_TEST_DATA7 : begin
            VME_DOUT [15:0] <= gtx1_test_data7[15:0]; 
        end
        `Addr_GTX2_TEST_DATA1 : begin
            VME_DOUT [15:0] <= gtx2_test_data1[15:0]; 
        end
        `Addr_GTX2_TEST_DATA2 : begin
            VME_DOUT [15:0] <= gtx2_test_data2[15:0]; 
        end
        `Addr_GTX2_TEST_DATA3 : begin
            VME_DOUT [15:0] <= gtx2_test_data3[15:0]; 
        end
        `Addr_GTX2_TEST_DATA4 : begin
            VME_DOUT [15:0] <= gtx2_test_data4[15:0]; 
        end
        `Addr_GTX2_TEST_DATA5 : begin
            VME_DOUT [15:0] <= gtx2_test_data5[15:0]; 
        end
        `Addr_GTX2_TEST_DATA6 : begin
            VME_DOUT [15:0] <= gtx2_test_data6[15:0]; 
        end
        `Addr_GTX2_TEST_DATA7 : begin
            VME_DOUT [15:0] <= gtx2_test_data7[15:0]; 
        end
        `Addr_GTX3_TEST_DATA1 : begin
            VME_DOUT [15:0] <= gtx3_test_data1[15:0]; 
        end
        `Addr_GTX3_TEST_DATA2 : begin
            VME_DOUT [15:0] <= gtx3_test_data2[15:0]; 
        end
        `Addr_GTX3_TEST_DATA3 : begin
            VME_DOUT [15:0] <= gtx3_test_data3[15:0]; 
        end
        `Addr_GTX3_TEST_DATA4 : begin
            VME_DOUT [15:0] <= gtx3_test_data4[15:0]; 
        end
        `Addr_GTX3_TEST_DATA5 : begin
            VME_DOUT [15:0] <= gtx3_test_data5[15:0]; 
        end
        `Addr_GTX3_TEST_DATA6 : begin
            VME_DOUT [15:0] <= gtx3_test_data6[15:0]; 
        end
        `Addr_GTX3_TEST_DATA7 : begin
            VME_DOUT [15:0] <= gtx3_test_data7[15:0]; 
        end
        `Addr_GTX4_TEST_DATA1 : begin
            VME_DOUT [15:0] <= gtx4_test_data1[15:0]; 
        end
        `Addr_GTX4_TEST_DATA2 : begin
            VME_DOUT [15:0] <= gtx4_test_data2[15:0]; 
        end
        `Addr_GTX4_TEST_DATA3 : begin
            VME_DOUT [15:0] <= gtx4_test_data3[15:0]; 
        end
        `Addr_GTX4_TEST_DATA4 : begin
            VME_DOUT [15:0] <= gtx4_test_data4[15:0]; 
        end
        `Addr_GTX4_TEST_DATA5 : begin
            VME_DOUT [15:0] <= gtx4_test_data5[15:0]; 
        end
        `Addr_GTX4_TEST_DATA6 : begin
            VME_DOUT [15:0] <= gtx4_test_data6[15:0]; 
        end
        `Addr_GTX4_TEST_DATA7 : begin
            VME_DOUT [15:0] <= gtx4_test_data7[15:0]; 
        end
        `Addr_GTX5_TEST_DATA1 : begin
            VME_DOUT [15:0] <= gtx5_test_data1[15:0]; 
        end
        `Addr_GTX5_TEST_DATA2 : begin
            VME_DOUT [15:0] <= gtx5_test_data2[15:0]; 
        end
        `Addr_GTX5_TEST_DATA3 : begin
            VME_DOUT [15:0] <= gtx5_test_data3[15:0]; 
        end
        `Addr_GTX5_TEST_DATA4 : begin
            VME_DOUT [15:0] <= gtx5_test_data4[15:0]; 
        end
        `Addr_GTX5_TEST_DATA5 : begin
            VME_DOUT [15:0] <= gtx5_test_data5[15:0]; 
        end
        `Addr_GTX5_TEST_DATA6 : begin
            VME_DOUT [15:0] <= gtx5_test_data6[15:0]; 
        end
        `Addr_GTX5_TEST_DATA7 : begin
            VME_DOUT [15:0] <= gtx5_test_data7[15:0]; 
        end
        `Addr_GTX6_TEST_DATA1 : begin
            VME_DOUT [15:0] <= gtx6_test_data1[15:0]; 
        end
        `Addr_GTX6_TEST_DATA2 : begin
            VME_DOUT [15:0] <= gtx6_test_data2[15:0]; 
        end
        `Addr_GTX6_TEST_DATA3 : begin
            VME_DOUT [15:0] <= gtx6_test_data3[15:0]; 
        end
        `Addr_GTX6_TEST_DATA4 : begin
            VME_DOUT [15:0] <= gtx6_test_data4[15:0]; 
        end
        `Addr_GTX6_TEST_DATA5 : begin
            VME_DOUT [15:0] <= gtx6_test_data5[15:0]; 
        end
        `Addr_GTX6_TEST_DATA6 : begin
            VME_DOUT [15:0] <= gtx6_test_data6[15:0]; 
        end
        `Addr_GTX6_TEST_DATA7 : begin
            VME_DOUT [15:0] <= gtx6_test_data7[15:0]; 
        end
        `Addr_GTX7_TEST_DATA1 : begin
            VME_DOUT [15:0] <= gtx7_test_data1[15:0]; 
        end
        `Addr_GTX7_TEST_DATA2 : begin
            VME_DOUT [15:0] <= gtx7_test_data2[15:0]; 
        end
        `Addr_GTX7_TEST_DATA3 : begin
            VME_DOUT [15:0] <= gtx7_test_data3[15:0]; 
        end
        `Addr_GTX7_TEST_DATA4 : begin
            VME_DOUT [15:0] <= gtx7_test_data4[15:0]; 
        end
        `Addr_GTX7_TEST_DATA5 : begin
            VME_DOUT [15:0] <= gtx7_test_data5[15:0]; 
        end
        `Addr_GTX7_TEST_DATA6 : begin
            VME_DOUT [15:0] <= gtx7_test_data6[15:0]; 
        end
        `Addr_GTX7_TEST_DATA7 : begin
            VME_DOUT [15:0] <= gtx7_test_data7[15:0]; 
        end
        `Addr_GTX8_TEST_DATA1 : begin
             VME_DOUT [15:0] <= gtx8_test_data1[15:0]; 
       end
        `Addr_GTX8_TEST_DATA2 : begin
            VME_DOUT [15:0] <= gtx8_test_data2[15:0]; 
        end
        `Addr_GTX8_TEST_DATA3 : begin
             VME_DOUT [15:0] <= gtx8_test_data3[15:0]; 
       end
        `Addr_GTX8_TEST_DATA4 : begin
           VME_DOUT [15:0] <= gtx8_test_data4[15:0]; 
       end
       `Addr_GTX8_TEST_DATA5 : begin
           VME_DOUT [15:0] <= gtx8_test_data5[15:0]; 
       end
       `Addr_GTX8_TEST_DATA6 : begin
           VME_DOUT [15:0] <= gtx8_test_data6[15:0]; 
       end
       `Addr_GTX8_TEST_DATA7 : begin
           VME_DOUT [15:0] <= gtx8_test_data7[15:0]; 
       end
        `Addr_GTX9_TEST_DATA1 : begin
            VME_DOUT [15:0] <= gtx9_test_data1[15:0]; 
        end
        `Addr_GTX9_TEST_DATA2 : begin
            VME_DOUT [15:0] <= gtx9_test_data2[15:0]; 
        end
        `Addr_GTX9_TEST_DATA3 : begin
            VME_DOUT [15:0] <= gtx9_test_data3[15:0]; 
        end
        `Addr_GTX9_TEST_DATA4 : begin
            VME_DOUT [15:0] <= gtx9_test_data4[15:0]; 
        end
        `Addr_GTX9_TEST_DATA5 : begin
            VME_DOUT [15:0] <= gtx9_test_data5[15:0]; 
        end
        `Addr_GTX9_TEST_DATA6 : begin
            VME_DOUT [15:0] <= gtx9_test_data6[15:0]; 
        end
        `Addr_GTX9_TEST_DATA7 : begin
            VME_DOUT [15:0] <= gtx9_test_data7[15:0]; 
        end
        `Addr_GTX10_TEST_DATA1 : begin
            VME_DOUT [15:0] <= gtx10_test_data1[15:0]; 
        end
        `Addr_GTX10_TEST_DATA2 : begin
            VME_DOUT [15:0] <= gtx10_test_data2[15:0]; 
        end
        `Addr_GTX10_TEST_DATA3 : begin
            VME_DOUT [15:0] <= gtx10_test_data3[15:0]; 
        end
        `Addr_GTX10_TEST_DATA4 : begin
            VME_DOUT [15:0] <= gtx10_test_data4[15:0]; 
        end
        `Addr_GTX10_TEST_DATA5 : begin
            VME_DOUT [15:0] <= gtx10_test_data5[15:0]; 
        end
        `Addr_GTX10_TEST_DATA6 : begin
            VME_DOUT [15:0] <= gtx10_test_data6[15:0]; 
        end
        `Addr_GTX10_TEST_DATA7 : begin
            VME_DOUT [15:0] <= gtx10_test_data7[15:0]; 
        end
        `Addr_GTX11_TEST_DATA1 : begin
            VME_DOUT [15:0] <= gtx11_test_data1[15:0]; 
        end
        `Addr_GTX11_TEST_DATA2 : begin
            VME_DOUT [15:0] <= gtx11_test_data2[15:0]; 
        end
        `Addr_GTX11_TEST_DATA3 : begin
            VME_DOUT [15:0] <= gtx11_test_data3[15:0]; 
        end
        `Addr_GTX11_TEST_DATA4 : begin
            VME_DOUT [15:0] <= gtx11_test_data4[15:0]; 
        end
        `Addr_GTX11_TEST_DATA5 : begin
            VME_DOUT [15:0] <= gtx11_test_data5[15:0]; 
        end
        `Addr_GTX11_TEST_DATA6 : begin
            VME_DOUT [15:0] <= gtx11_test_data6[15:0]; 
        end
        `Addr_GTX11_TEST_DATA7 : begin
            VME_DOUT [15:0] <= gtx11_test_data7[15:0]; 
        end
        `Addr_GLINK0_TEST_DATA1 : begin
            VME_DOUT [15:0] <= glink0_test_data1[15:0]; 
        end
        `Addr_GLINK0_TEST_DATA2 : begin
            VME_DOUT [15:0] <= {12'b0, glink0_test_data2[3:0]}; 
        end
        `Addr_GLINK1_TEST_DATA1 : begin
            VME_DOUT [15:0] <= glink1_test_data1[15:0]; 
        end
        `Addr_GLINK1_TEST_DATA2 : begin
            VME_DOUT [15:0] <= {12'b0, glink1_test_data2[3:0]}; 
        end
        `Addr_GLINK2_TEST_DATA1 : begin
            VME_DOUT [15:0] <= glink2_test_data1[15:0]; 
        end
        `Addr_GLINK2_TEST_DATA2 : begin
            VME_DOUT [15:0] <= {12'b0, glink2_test_data2[3:0]}; 
        end
        `Addr_GLINK3_TEST_DATA1 : begin
            VME_DOUT [15:0] <= glink3_test_data1[15:0]; 
        end
        `Addr_GLINK3_TEST_DATA2 : begin
            VME_DOUT [15:0] <= {12'b0, glink3_test_data2[3:0]}; 
        end
        `Addr_GLINK4_TEST_DATA1 : begin
            VME_DOUT [15:0] <= glink4_test_data1[15:0]; 
        end
        `Addr_GLINK4_TEST_DATA2 : begin
            VME_DOUT [15:0] <= {12'b0, glink4_test_data2[3:0]}; 
        end
        `Addr_GLINK5_TEST_DATA1 : begin
            VME_DOUT [15:0] <= glink5_test_data1[15:0]; 
        end
        `Addr_GLINK5_TEST_DATA2 : begin
            VME_DOUT [15:0] <= {12'b0, glink5_test_data2[3:0]}; 
        end
        `Addr_GLINK6_TEST_DATA1 : begin
            VME_DOUT [15:0] <= glink6_test_data1[15:0]; 
        end
        `Addr_GLINK6_TEST_DATA2 : begin
            VME_DOUT [15:0] <= {12'b0, glink6_test_data2[3:0]}; 
        end
        `Addr_GLINK7_TEST_DATA1 : begin
            VME_DOUT [15:0] <= glink7_test_data1[15:0]; 
        end
        `Addr_GLINK7_TEST_DATA2 : begin
            VME_DOUT [15:0] <= {12'b0, glink7_test_data2[3:0]}; 
        end
        `Addr_GLINK8_TEST_DATA1 : begin
            VME_DOUT [15:0] <= glink8_test_data1[15:0]; 
        end
        `Addr_GLINK8_TEST_DATA2 : begin
            VME_DOUT [15:0] <= {12'b0, glink8_test_data2[3:0]}; 
        end
        `Addr_GLINK9_TEST_DATA1 : begin
            VME_DOUT [15:0] <= glink9_test_data1[15:0]; 
        end
        `Addr_GLINK9_TEST_DATA2 : begin
            VME_DOUT [15:0] <= {12'b0, glink9_test_data2[3:0]}; 
        end
        `Addr_GLINK10_TEST_DATA1 : begin
            VME_DOUT [15:0] <= glink10_test_data1[15:0]; 
        end
        `Addr_GLINK10_TEST_DATA2 : begin
            VME_DOUT [15:0] <= {12'b0, glink10_test_data2[3:0]}; 
        end
        `Addr_GLINK11_TEST_DATA1 : begin
            VME_DOUT [15:0] <= glink11_test_data1[15:0]; 
        end
        `Addr_GLINK11_TEST_DATA2 : begin
            VME_DOUT [15:0] <= {12'b0, glink11_test_data2[3:0]}; 
        end
        `Addr_GLINK12_TEST_DATA1 : begin
            VME_DOUT [15:0] <= glink12_test_data1[15:0]; 
        end
        `Addr_GLINK12_TEST_DATA2 : begin
            VME_DOUT [15:0] <= {12'b0, glink12_test_data2[3:0]}; 
        end
        `Addr_GLINK13_TEST_DATA1 : begin
            VME_DOUT [15:0] <= glink13_test_data1[15:0]; 
        end
        `Addr_GLINK13_TEST_DATA2 : begin
            VME_DOUT [15:0] <= {12'b0, glink13_test_data2[3:0]}; 
        end
        `Addr_LUT_INIT_MODE : begin
            VME_DOUT [15:0] <= {14'h0, LUT_init_mode[1:0]}; 
        end
        `Addr_LUT_INIT_DATA : begin
            VME_DOUT [15:0] <= LUT_init_data[15:0]; 
        end
        `Addr_LUT_INIT_ROI : begin
            VME_DOUT [15:0] <= {LUT_init_RoI[7:0], 1'b0, LUT_init_type[2:0], 3'b0, SectorID}; 
        end
        `Addr_LUT_INIT_ADDRESS : begin
            VME_DOUT [15:0] <= {1'b0, LUT_init_Address[14:0]}; 
        end
        `Addr_LUT_INIT_OUTPUT : begin
            VME_DOUT [15:0] <= {11'b0, LUT_init_data_out[4:0]}; 
        end
        `Addr_LUT_INIT_STATE : begin
            VME_DOUT [15:0] <= {7'b0, LUT_init_valid, LUT_init_state[3:0], 2'b0, LUT_init_full, LUT_init_data_flg}; 
        end
        `Addr_LUT_RD_DATA : begin
            VME_DOUT [15:0] <= {11'b0, LUT_rd_data[4:0]}; 
        end
        `Addr_LUT_RD_ADDRESS : begin
            VME_DOUT [15:0] <= {1'b0, LUT_rd_address[14:0]}; 
        end
        `Addr_ALIGN_ETA_NSW_0 : begin
            VME_DOUT[15:0] <= {8'b0, Align_eta_NSW_0[7:0]};
        end
        `Addr_ALIGN_ETA_NSW_1 : begin
            VME_DOUT[15:0] <= {8'b0, Align_eta_NSW_1[7:0]};
        end
        `Addr_ALIGN_ETA_NSW_2 : begin
            VME_DOUT[15:0] <= {8'b0, Align_eta_NSW_2[7:0]};
        end
        `Addr_ALIGN_ETA_NSW_3 : begin
            VME_DOUT[15:0] <= {8'b0, Align_eta_NSW_3[7:0]};
        end
        `Addr_ALIGN_ETA_NSW_4 : begin
            VME_DOUT[15:0] <= {8'b0, Align_eta_NSW_4[7:0]};
        end
        `Addr_ALIGN_ETA_NSW_5 : begin
            VME_DOUT[15:0] <= {8'b0, Align_eta_NSW_5[7:0]};
        end
        `Addr_ALIGN_ETA_RPC : begin
            VME_DOUT[15:0] <= {8'b0, Align_eta_RPC[7:0]};
        end
        `Addr_ALIGN_PHI_NSW_0 : begin
            VME_DOUT[15:0] <= {10'b0, Align_phi_NSW_0[5:0]};
        end
        `Addr_ALIGN_PHI_NSW_1 : begin
            VME_DOUT[15:0] <= {10'b0, Align_phi_NSW_1[5:0]};
        end
        `Addr_ALIGN_PHI_NSW_2 : begin
            VME_DOUT[15:0] <= {10'b0, Align_phi_NSW_2[5:0]};
        end
        `Addr_ALIGN_PHI_NSW_3 : begin
            VME_DOUT[15:0] <= {10'b0, Align_phi_NSW_3[5:0]};
        end
        `Addr_ALIGN_PHI_NSW_4 : begin
            VME_DOUT[15:0] <= {10'b0, Align_phi_NSW_4[5:0]};
        end
        `Addr_ALIGN_PHI_NSW_5 : begin
            VME_DOUT[15:0] <= {10'b0, Align_phi_NSW_5[5:0]};
        end
        `Addr_ALIGN_PHI_RPC : begin
            VME_DOUT[15:0] <= {8'b0, Align_phi_RPC[7:0]};
        end
	`Addr_tmp0 : begin
	    VME_DOUT[15:0] <= tmp0;
	end
	`Addr_tmp1 : begin
	    VME_DOUT[15:0] <= tmp1;
	end
	`Addr_tmp2 : begin
	    VME_DOUT[15:0] <= tmp2;
	end
	`Addr_tmp3 : begin
	    VME_DOUT[15:0] <= tmp3;
	end
	`Addr_tmp4 : begin
	    VME_DOUT[15:0] <= tmp4;
	end
	`Addr_tmp5 : begin
	    VME_DOUT[15:0] <= tmp5;
	end
	`Addr_tmp6 : begin
	    VME_DOUT[15:0] <= tmp6;
	end
        default : begin
            VME_DOUT [15:0] <= 16'h1992;
        end
    endcase
    end
    
endmodule
