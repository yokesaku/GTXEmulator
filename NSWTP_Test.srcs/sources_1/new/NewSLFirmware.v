`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Kyoto University
// Engineer: 
// 
// Create Date: 2016/02/10 12:00:00
// Design Name: 
// Module Name: NewSLFirmware 
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module NewSLFirmware
(
// Common Ports
    input wire  FPGA_CLK,   // Clock chosen by jumper pin: EXT, INT or TTC 
    input wire FPGA_CLK40,  // Clock from INT; Quartz on the board

// TTC Signals
    input wire L1A,
    input wire BCR,
    input wire ECR,
    input wire TTC_RESET,
    input wire TEST_PULSE_TRIGGER,
    output wire BUSY,

// GTX 
    input wire  Q2_CLK0_GTREFCLK_PAD_N_IN,
    input wire  Q2_CLK0_GTREFCLK_PAD_P_IN,
    input  wire [11:0]  RXN_IN,
    input  wire [11:0]  RXP_IN,
    output wire [11:0]  TXN_OUT,
    output wire [11:0]  TXP_OUT,
    output wire TX_DISABLE0,
    output wire TX_DISABLE1,
    output wire TX_DISABLE2,

// VME    
    input wire OE,
    input wire CE,
    input wire WE,
    input wire [11:0] VME_A,
    inout wire [15:0] VME_D,

// Test Pins
    output wire  [15:0] TESTPIN,
    
// NIM outs    
    output wire NIMOUT_0,
    output wire NIMOUT_1,
    output wire NIMOUT_2,
    output wire NIMOUT_3
);

// Wire Declarations

// Clock wires
wire TTC_CLK;
wire CLK_25;
wire CLK_125;
wire CLK_160;    
wire Coin_CLK_160;    
wire CLK_320;    
wire locked;

// GTX latch clocks
wire CLK_40_0;
wire CLK_40_1;
wire CLK_40_2;
wire CLK_40_3;



// Reset
wire RESET_TTC;
wire RESET_160;
wire RESET_TX;
wire Scaler_reset;
wire TX_reset;
wire RX_reset;
wire Delay_reset;
wire L1Buffer_reset;
wire Derandomizer_reset;
wire ZeroSupp_reset;
wire CLK_reset;
wire FIFO_reset;
wire SiTCP_reset;
wire SiTCP_FIFO_reset;
wire TX_Logic_reset;
wire L1A_manual_reset;

wire gt0_rx_reset;
wire gt1_rx_reset;
wire gt2_rx_reset;
wire gt3_rx_reset;
wire gt4_rx_reset;
wire gt5_rx_reset;
wire gt6_rx_reset;
wire gt7_rx_reset;
wire gt8_rx_reset;
wire gt9_rx_reset;
wire gt10_rx_reset;
wire gt11_rx_reset;



// GTX 

// GTX TX interface to IP
wire [31:0] gt0_txdata;
wire [3:0]  gt0_txcharisk;
wire [31:0] gt1_txdata;
wire [3:0]  gt1_txcharisk;
wire [31:0] gt2_txdata;
wire [3:0]  gt2_txcharisk;
wire [31:0] gt3_txdata;
wire [3:0]  gt3_txcharisk;
wire [31:0] gt4_txdata;
wire [3:0]  gt4_txcharisk;
wire [31:0] gt5_txdata;
wire [3:0]  gt5_txcharisk;
wire [31:0] gt6_txdata;
wire [3:0]  gt6_txcharisk;
wire [31:0] gt7_txdata;
wire [3:0]  gt7_txcharisk;
wire [31:0] gt8_txdata;
wire [3:0]  gt8_txcharisk;
wire [31:0] gt9_txdata;
wire [3:0]  gt9_txcharisk;
wire [15:0] gt10_txdata;
wire [1:0]  gt10_txcharisk;
wire [15:0] gt11_txdata;
wire [1:0]  gt11_txcharisk;
wire gt_txusrclk2;
wire gt_txusrclk2_80;

// GTX RX interface to IP
wire [31:0] gt0_rxdata;
wire [3:0]  gt0_rxcharisk;
wire [31:0] gt1_rxdata;
wire [3:0]  gt1_rxcharisk;
wire [31:0] gt2_rxdata;
wire [3:0]  gt2_rxcharisk;
wire [31:0] gt3_rxdata;
wire [3:0]  gt3_rxcharisk;
wire [31:0] gt4_rxdata;
wire [3:0]  gt4_rxcharisk;
wire [31:0] gt5_rxdata;
wire [3:0]  gt5_rxcharisk;
wire [31:0] gt6_rxdata;
wire [3:0]  gt6_rxcharisk;
wire [31:0] gt7_rxdata;
wire [3:0]  gt7_rxcharisk;
wire [31:0] gt8_rxdata;
wire [3:0]  gt8_rxcharisk;
wire [31:0] gt9_rxdata;
wire [3:0]  gt9_rxcharisk;
wire [15:0] gt10_rxdata;
wire [1:0]  gt10_rxcharisk;
wire [15:0] gt11_rxdata;
wire [1:0]  gt11_rxcharisk;

wire gt0_rxusrclk;
wire gt1_rxusrclk;
wire gt2_rxusrclk;
wire gt3_rxusrclk;
wire gt4_rxusrclk;
wire gt5_rxusrclk;
wire gt6_rxusrclk;
wire gt7_rxusrclk;
wire gt8_rxusrclk;
wire gt9_rxusrclk;
wire gt10_rxusrclk;
wire gt11_rxusrclk;

// GTX Status and debugs
wire [5:0] gt0_status;
wire [5:0] gt1_status;
wire [5:0] gt2_status;
wire [5:0] gt3_status;
wire [5:0] gt4_status;
wire [5:0] gt5_status;
wire [5:0] gt6_status;
wire [5:0] gt7_status;
wire [5:0] gt8_status;
wire [5:0] gt9_status;
wire [5:0] gt10_status;
wire [5:0] gt11_status;

wire [15:0] gt0_error_scaler;
wire [15:0] gt1_error_scaler;
wire [15:0] gt2_error_scaler;
wire [15:0] gt3_error_scaler;
wire [15:0] gt4_error_scaler;
wire [15:0] gt5_error_scaler;
wire [15:0] gt6_error_scaler;
wire [15:0] gt7_error_scaler;
wire [15:0] gt8_error_scaler;
wire [15:0] gt9_error_scaler;
wire [15:0] gt10_error_scaler;
wire [15:0] gt11_error_scaler;

wire [3:0]      lane_selector;
wire [2:0]      loopback_mode;


// De-serialized GTX data
wire [111:0]    gtx0_data;
wire [111:0]    gtx1_data;
wire [111:0]    gtx2_data;
wire [111:0]    gtx3_data;
wire [111:0]    gtx4_data;
wire [111:0]    gtx5_data;
wire [111:0]    gtx6_data;
wire [111:0]    gtx7_data;
wire [111:0]    gtx8_data;
wire [111:0]    gtx9_data;
wire [111:0]    gtx10_data;
wire [111:0]    gtx11_data;

// GTX Delay parameters
wire [6:0]      Delay_gtx0;
wire [6:0]      Delay_gtx1;
wire [6:0]      Delay_gtx2;
wire [6:0]      Delay_gtx3;
wire [6:0]      Delay_gtx4;
wire [6:0]      Delay_gtx5;
wire [6:0]      Delay_gtx6;
wire [6:0]      Delay_gtx7;
wire [6:0]      Delay_gtx8;
wire [6:0]      Delay_gtx9;
wire [6:0]      Delay_gtx10;
wire [6:0]      Delay_gtx11;

// GTX deleyed signals
wire [111:0]    gtx0_delayed;
wire [111:0]    gtx1_delayed;
wire [111:0]    gtx2_delayed;
wire [111:0]    gtx3_delayed;
wire [111:0]    gtx4_delayed;
wire [111:0]    gtx5_delayed;
wire [111:0]    gtx6_delayed;
wire [111:0]    gtx7_delayed;
wire [111:0]    gtx8_delayed;
wire [111:0]    gtx9_delayed;
wire [111:0]    gtx10_delayed;
wire [111:0]    gtx11_delayed;
 
// GTX phase selector
wire [3:0]	gtx0_phase_select;
wire [3:0]	gtx1_phase_select;
wire [3:0]	gtx2_phase_select;
wire [3:0]	gtx3_phase_select;
wire [3:0]	gtx4_phase_select;
wire [3:0]	gtx5_phase_select;
wire [3:0]	gtx6_phase_select;
wire [3:0]	gtx7_phase_select;
wire [3:0]	gtx8_phase_select;
wire [3:0]	gtx9_phase_select;
wire [3:0]	gtx10_phase_select;
wire [3:0]	gtx11_phase_select;

// GTX phase monitor 
wire [5:0]	gtx0_phase_monitor_0, gtx0_phase_monitor_1, gtx0_phase_monitor_2, gtx0_phase_monitor_3;
wire [5:0]	gtx1_phase_monitor_0, gtx1_phase_monitor_1, gtx1_phase_monitor_2, gtx1_phase_monitor_3;
wire [5:0]	gtx2_phase_monitor_0, gtx2_phase_monitor_1, gtx2_phase_monitor_2, gtx2_phase_monitor_3;
wire [5:0]	gtx3_phase_monitor_0, gtx3_phase_monitor_1, gtx3_phase_monitor_2, gtx3_phase_monitor_3;
wire [5:0]	gtx4_phase_monitor_0, gtx4_phase_monitor_1, gtx4_phase_monitor_2, gtx4_phase_monitor_3;
wire [5:0]	gtx5_phase_monitor_0, gtx5_phase_monitor_1, gtx5_phase_monitor_2, gtx5_phase_monitor_3;
wire [5:0]	gtx6_phase_monitor_0, gtx6_phase_monitor_1, gtx6_phase_monitor_2, gtx6_phase_monitor_3;
wire [5:0]	gtx7_phase_monitor_0, gtx7_phase_monitor_1, gtx7_phase_monitor_2, gtx7_phase_monitor_3;
wire [5:0]	gtx8_phase_monitor_0, gtx8_phase_monitor_1, gtx8_phase_monitor_2, gtx8_phase_monitor_3;
wire [5:0]	gtx9_phase_monitor_0, gtx9_phase_monitor_1, gtx9_phase_monitor_2, gtx9_phase_monitor_3;
wire [5:0]	gtx10_phase_monitor_0, gtx10_phase_monitor_1, gtx10_phase_monitor_2, gtx10_phase_monitor_3;
wire [5:0]	gtx11_phase_monitor_0, gtx11_phase_monitor_1, gtx11_phase_monitor_2, gtx11_phase_monitor_3;

// wires for readout path 
wire [1581:0]   rx_data_masked;
wire [159:0]    Trig_data_out;

// TTC signals

// TTC Delay Parameters
wire [7:0] Delay_L1A;
wire [7:0] Delay_BCR;
wire [7:0] Delay_trig_BCR;
wire [7:0] Delay_ECR;
wire [7:0] Delay_TTC_RESET;
wire [7:0] Delay_TEST_PULSE;

// TTC delayed signals
wire L1A_delayed;
wire BCR_delayed;
wire trig_BCR_delayed;
wire ECR_delayed;
wire TTC_RESET_delayed;    
wire TEST_PULSE_delayed;

// TTC Mask Parameters
wire Mask_L1A;
wire Mask_BCR;
wire Mask_trig_BCR;
wire Mask_ECR;
wire Mask_TTC_RESET;
wire Mask_TEST_PULSE;

// TTC masked signals
wire L1A_masked;
wire BCR_masked;
wire trig_BCR_masked;
wire ECR_masked;
wire TEST_PULSE_masked;
wire TTC_RESET_masked;    

// ID's
wire [11:0] L1ID;
wire [11:0] BCID;
wire [11:0] trig_BCID;
wire [11:0] SLID;
wire [31:0] L1A_Counter;

// Readout Path
// wires for L1Buffer
wire [1789:0] L1Buffer_data;
wire [6:0] L1Buffer_depth;
wire [6:0] L1Buffer_BW_depth;
wire [6:0] trigL1Buffer_depth;
wire [15:0] Readout_BC;
wire        L1Buffer_flg;
wire [15:0] L1Buffer_status;

// wires for Derandomizer
wire [15:0] Derandomizer_out;
wire        Derandomizer_flg;
wire        Derandomizer_busy;
wire [15:0] FIFO_status;
wire [9:0] Derandomizer_Count1_1;
wire [9:0] Derandomizer_Count1_2;
wire [8:0] Derandomizer_Count2;
wire [9:0] Derandomizer_Count3;


// wires for SiTCP
wire [63:0] SiTCP_in;
wire        SiTCP_full;
wire [12:0] SiTCP_data_count;
wire        SiTCP_busy;
wire [15:0] SiTCP_monitor;

// wires for Zero-Suppress
wire [15:0] zero_data;
wire    zero_flg;
wire    zero_rd;
wire [15:0] zero_status;
wire zero_busy;
wire zero_full;
wire [12:0] zero_count;
wire [11:0] data_size;

wire [15:0] FullBoard_ID;


// monitoring FIFO
wire            write_enb;
wire            read_enb;
wire [255:0]    FIFO_data;

// Signal assignments


// Test pins
assign TESTPIN[0] = TTC_CLK;
assign TESTPIN[1] = gt_txusrclk2;
assign TESTPIN[2] = gt0_rxusrclk;
assign TESTPIN[3] = gt1_rxusrclk;
assign TESTPIN[7:4] = gt0_txdata[11:8];
assign TESTPIN[11:8] = gt1_txdata[11:8];
assign TESTPIN[15:12] = gt0_rxdata[11:8];

// NIM output
//assign NIMOUT_0 = BUSY;
reg NIMOUT_0_reg ;
reg NIMOUT_1_reg ;
always @ (posedge TTC_CLK) begin
    if (gtx0_delayed[15:4] == 12'h0) begin
        NIMOUT_0_reg <= 1'b1;
    end
    else begin 
        NIMOUT_0_reg <= 1'b0;
    end
end

always @ (posedge gt0_rxusrclk) begin
    if (gtx0_data[15:4] == 12'h0) begin
        NIMOUT_1_reg <= 1'b1;
    end
    else begin 
        NIMOUT_1_reg <= 1'b0;
    end
end

//assign NIMOUT_0 = TTC_CLK;
//assign NIMOUT_0 = NIMOUT_0_reg;
//assign NIMOUT_1 = NIMOUT_1_reg;
//assign NIMOUT_2 = gt0_rxusrclk;
//assign NIMOUT_3 = gt1_rxusrclk;
assign NIMOUT_0 = CLK_40_1;
assign NIMOUT_1 = CLK_40_0;
assign NIMOUT_2 = CLK_40_2;
//assign NIMOUT_3 = CLK_40_3;
assign NIMOUT_3 = TTC_CLK;




///////////////////////////////////////////////////////////////////////////////
//================================ Main Logic ===============================//
///////////////////////////////////////////////////////////////////////////////



//************************ tmp L1A generator **************************//
reg [11:0] L1A_Generate_Count;
reg L1A_manual;
wire [11:0] L1A_reset_value;
always @ (posedge TTC_CLK) begin
    if (RESET_TTC||L1A_manual_reset) begin
        L1A_manual <= 1'b0;
        L1A_Generate_Count <= 12'h7ff;
    end
    else if (L1A_Generate_Count == 12'h0)begin
        L1A_manual <= 1'b1;
        L1A_Generate_Count <= L1A_reset_value[11:0];
    end
    else begin 
        L1A_manual <= 1'b0;
        L1A_Generate_Count <= L1A_Generate_Count - 12'b1;    
    end        
end
//*********************************************************************//


// GTX IP Core

GTX_12lane_320MHz_exdes GTX_12lane
(
    .refclk_n_in        (Q2_CLK0_GTREFCLK_PAD_N_IN),
    .refclk_p_in        (Q2_CLK0_GTREFCLK_PAD_P_IN),
    .drpclk_in          (TTC_CLK),
    .reset_in           (RESET_TTC),
    .TX_reset_in        (TX_reset),
    .RX_reset_in        (RX_reset),
    .gt0_rx_reset_in    (gt0_rx_reset),
    .gt1_rx_reset_in    (gt1_rx_reset),
    .gt2_rx_reset_in    (gt2_rx_reset),
    .gt3_rx_reset_in    (gt3_rx_reset),
    .gt4_rx_reset_in    (gt4_rx_reset),
    .gt5_rx_reset_in    (gt5_rx_reset),
    .gt6_rx_reset_in    (gt6_rx_reset),
    .gt7_rx_reset_in    (gt7_rx_reset),
    .gt8_rx_reset_in    (gt8_rx_reset),
    .gt9_rx_reset_in    (gt9_rx_reset),
    .gt10_rx_reset_in   (gt10_rx_reset),
    .gt11_rx_reset_in   (gt11_rx_reset),
    .Scaler_reset_in    (Scaler_reset),

    .RXN_IN             (RXN_IN),
    .RXP_IN             (RXP_IN),
    .TXN_OUT            (TXN_OUT),
    .TXP_OUT            (TXP_OUT),

// tx data 
    .gt0_txdata_in      (gt0_txdata), 
    .gt1_txdata_in      (gt1_txdata), 
    .gt2_txdata_in      (gt2_txdata), 
    .gt3_txdata_in      (gt3_txdata),
    .gt4_txdata_in      (gt4_txdata), 
    .gt5_txdata_in      (gt5_txdata), 
    .gt6_txdata_in      (gt6_txdata), 
    .gt7_txdata_in      (gt7_txdata),
    .gt8_txdata_in      (gt8_txdata), 
    .gt9_txdata_in      (gt9_txdata), 
    .gt10_txdata_in     (gt10_txdata), 
    .gt11_txdata_in     (gt11_txdata),
    
// tx char is k
    .gt0_txcharisk_in   (gt0_txcharisk), 
    .gt1_txcharisk_in   (gt1_txcharisk), 
    .gt2_txcharisk_in   (gt2_txcharisk), 
    .gt3_txcharisk_in   (gt3_txcharisk),
    .gt4_txcharisk_in   (gt4_txcharisk), 
    .gt5_txcharisk_in   (gt5_txcharisk), 
    .gt6_txcharisk_in   (gt6_txcharisk), 
    .gt7_txcharisk_in   (gt7_txcharisk),
    .gt8_txcharisk_in   (gt8_txcharisk), 
    .gt9_txcharisk_in   (gt9_txcharisk), 
    .gt10_txcharisk_in  (gt10_txcharisk), 
    .gt11_txcharisk_in  (gt11_txcharisk),
    
// tx user clock    
    .gt_txusrclk2_out   (gt_txusrclk2),
    .gt_txusrclk2_out_80   (gt_txusrclk2_80),
        
//rx data out
    .gt0_rxdata_out     (gt0_rxdata), 
    .gt1_rxdata_out     (gt1_rxdata), 
    .gt2_rxdata_out     (gt2_rxdata), 
    .gt3_rxdata_out     (gt3_rxdata),
    .gt4_rxdata_out     (gt4_rxdata), 
    .gt5_rxdata_out     (gt5_rxdata), 
    .gt6_rxdata_out     (gt6_rxdata), 
    .gt7_rxdata_out     (gt7_rxdata),
    .gt8_rxdata_out     (gt8_rxdata), 
    .gt9_rxdata_out     (gt9_rxdata), 
    .gt10_rxdata_out    (gt10_rxdata), 
    .gt11_rxdata_out    (gt11_rxdata),
    
// rx char is k
    .gt0_rxcharisk_out  (gt0_rxcharisk), 
    .gt1_rxcharisk_out  (gt1_rxcharisk), 
    .gt2_rxcharisk_out  (gt2_rxcharisk), 
    .gt3_rxcharisk_out  (gt3_rxcharisk),
    .gt4_rxcharisk_out  (gt4_rxcharisk), 
    .gt5_rxcharisk_out  (gt5_rxcharisk), 
    .gt6_rxcharisk_out  (gt6_rxcharisk), 
    .gt7_rxcharisk_out  (gt7_rxcharisk),
    .gt8_rxcharisk_out  (gt8_rxcharisk), 
    .gt9_rxcharisk_out  (gt9_rxcharisk), 
    .gt10_rxcharisk_out (gt10_rxcharisk), 
    .gt11_rxcharisk_out (gt11_rxcharisk),
    
// rx user clock
    .gt0_rxusrclk2_out  (gt0_rxusrclk), 
    .gt1_rxusrclk2_out  (gt1_rxusrclk), 
    .gt2_rxusrclk2_out  (gt2_rxusrclk), 
    .gt3_rxusrclk2_out  (gt3_rxusrclk),
    .gt4_rxusrclk2_out  (gt4_rxusrclk), 
    .gt5_rxusrclk2_out  (gt5_rxusrclk), 
    .gt6_rxusrclk2_out  (gt6_rxusrclk), 
    .gt7_rxusrclk2_out  (gt7_rxusrclk),
    .gt8_rxusrclk2_out  (gt8_rxusrclk), 
    .gt9_rxusrclk2_out  (gt9_rxusrclk), 
    .gt10_rxusrclk2_out (gt10_rxusrclk), 
    .gt11_rxusrclk2_out (gt11_rxusrclk),
    
// gtx status
    .gt0_status_out     (gt0_status),
    .gt1_status_out     (gt1_status),
    .gt2_status_out     (gt2_status),
    .gt3_status_out     (gt3_status),
    .gt4_status_out     (gt4_status),
    .gt5_status_out     (gt5_status),
    .gt6_status_out     (gt6_status),
    .gt7_status_out     (gt7_status),
    .gt8_status_out     (gt8_status),
    .gt9_status_out     (gt9_status),
    .gt10_status_out    (gt10_status),
    .gt11_status_out    (gt11_status),

    .gt0_err_scaler_out (gt0_error_scaler),
    .gt1_err_scaler_out (gt1_error_scaler),
    .gt2_err_scaler_out (gt2_error_scaler),
    .gt3_err_scaler_out (gt3_error_scaler),
    .gt4_err_scaler_out (gt4_error_scaler),
    .gt5_err_scaler_out (gt5_error_scaler),
    .gt6_err_scaler_out (gt6_error_scaler),
    .gt7_err_scaler_out (gt7_error_scaler),
    .gt8_err_scaler_out (gt8_error_scaler),
    .gt9_err_scaler_out (gt9_error_scaler),
    .gt10_err_scaler_out(gt10_error_scaler),
    .gt11_err_scaler_out(gt11_error_scaler),
    
    .TX_DISABLE0_out    (TX_DISABLE0),
    .TX_DISABLE1_out    (TX_DISABLE1),
    .TX_DISABLE2_out    (TX_DISABLE2),
    .loopback_mode_in   (loopback_mode)    
);


// Emulator settings
wire [15:0] GTX_HEADER;
reg [3:0] EventID = 4'h0;
reg [15:0] DATA_length_counter;

wire [23:0] track0_data, track1_data, track2_data, track3_data;
wire [15:0] track_info_data; // BCID, EventID

wire RESET_Emulator;
wire EMULATOR_ENABLE;
wire [15:0] EMULATOR_DATA_LENGTH;

wire [7:0] TRACK0_ETA;
wire [5:0] TRACK0_PHI;
wire [4:0] TRACK0_DELTATHETA;
wire [1:0] TRACK0_MM;
wire [1:0] TRACK0_sTGC;
wire       TRACK0_SPARE;

wire [7:0] TRACK1_ETA;
wire [5:0] TRACK1_PHI;
wire [4:0] TRACK1_DELTATHETA;
wire [1:0] TRACK1_MM;
wire [1:0] TRACK1_sTGC;
wire       TRACK1_SPARE;

wire [7:0] TRACK2_ETA;
wire [5:0] TRACK2_PHI;
wire [4:0] TRACK2_DELTATHETA;
wire [1:0] TRACK2_MM;
wire [1:0] TRACK2_sTGC;
wire       TRACK2_SPARE;

wire [7:0] TRACK3_ETA;
wire [5:0] TRACK3_PHI;
wire [4:0] TRACK3_DELTATHETA;
wire [1:0] TRACK3_MM;
wire [1:0] TRACK3_sTGC;
wire       TRACK3_SPARE;

assign track0_data = {TRACK0_SPARE, TRACK0_sTGC[1:0], TRACK0_MM[1:0], TRACK0_DELTATHETA[4:0], TRACK0_PHI[5:0], TRACK0_ETA[7:0]};
assign track1_data = {TRACK1_SPARE, TRACK1_sTGC[1:0], TRACK1_MM[1:0], TRACK1_DELTATHETA[4:0], TRACK1_PHI[5:0], TRACK1_ETA[7:0]};
assign track2_data = {TRACK2_SPARE, TRACK2_sTGC[1:0], TRACK2_MM[1:0], TRACK2_DELTATHETA[4:0], TRACK2_PHI[5:0], TRACK2_ETA[7:0]};
assign track3_data = {TRACK3_SPARE, TRACK3_sTGC[1:0], TRACK3_MM[1:0], TRACK3_DELTATHETA[4:0], TRACK3_PHI[5:0], TRACK3_ETA[7:0]};
assign track_info_data = {BCID, EventID};

wire   TRACK0_SPARE_lane0[3:0];
wire   TRACK0_SPARE_lane1[3:0];
wire   TRACK0_SPARE_lane2[3:0];
wire   TRACK0_SPARE_lane3[3:0];
wire   TRACK0_SPARE_lane4[3:0];
wire   TRACK0_SPARE_lane5[3:0];
wire   TRACK0_SPARE_lane6[3:0];

wire [1:0]  TRACK0_sTGC_lane0[3:0];
wire [1:0]  TRACK0_sTGC_lane1[3:0];
wire [1:0]  TRACK0_sTGC_lane2[3:0];
wire [1:0]  TRACK0_sTGC_lane3[3:0];
wire [1:0]  TRACK0_sTGC_lane4[3:0];
wire [1:0]  TRACK0_sTGC_lane5[3:0];
wire [1:0]  TRACK0_sTGC_lane6[3:0];

wire [1:0]  TRACK0_MM_lane0[3:0];
wire [1:0]  TRACK0_MM_lane1[3:0];
wire [1:0]  TRACK0_MM_lane2[3:0];
wire [1:0]  TRACK0_MM_lane3[3:0];
wire [1:0]  TRACK0_MM_lane4[3:0];
wire [1:0]  TRACK0_MM_lane5[3:0];
wire [1:0]  TRACK0_MM_lane6[3:0];

wire [4:0]  TRACK0_DTHETA_lane0[3:0];
wire [4:0]  TRACK0_DTHETA_lane1[3:0];
wire [4:0]  TRACK0_DTHETA_lane2[3:0];
wire [4:0]  TRACK0_DTHETA_lane3[3:0];
wire [4:0]  TRACK0_DTHETA_lane4[3:0];
wire [4:0]  TRACK0_DTHETA_lane5[3:0];
wire [4:0]  TRACK0_DTHETA_lane6[3:0];

wire [5:0]  TRACK0_PHI_lane0[3:0];
wire [5:0]  TRACK0_PHI_lane1[3:0];
wire [5:0]  TRACK0_PHI_lane2[3:0];
wire [5:0]  TRACK0_PHI_lane3[3:0];
wire [5:0]  TRACK0_PHI_lane4[3:0];
wire [5:0]  TRACK0_PHI_lane5[3:0];
wire [5:0]  TRACK0_PHI_lane6[3:0];

wire [7:0]  TRACK0_ETA_lane0[3:0];
wire [7:0]  TRACK0_ETA_lane1[3:0];
wire [7:0]  TRACK0_ETA_lane2[3:0];
wire [7:0]  TRACK0_ETA_lane3[3:0];
wire [7:0]  TRACK0_ETA_lane4[3:0];
wire [7:0]  TRACK0_ETA_lane5[3:0];
wire [7:0]  TRACK0_ETA_lane6[3:0];

wire   TRACK1_SPARE_lane0[3:0];
wire   TRACK1_SPARE_lane1[3:0];
wire   TRACK1_SPARE_lane2[3:0];
wire   TRACK1_SPARE_lane3[3:0];
wire   TRACK1_SPARE_lane4[3:0];
wire   TRACK1_SPARE_lane5[3:0];
wire   TRACK1_SPARE_lane6[3:0];

wire [1:0]  TRACK1_sTGC_lane0[3:0];
wire [1:0]  TRACK1_sTGC_lane1[3:0];
wire [1:0]  TRACK1_sTGC_lane2[3:0];
wire [1:0]  TRACK1_sTGC_lane3[3:0];
wire [1:0]  TRACK1_sTGC_lane4[3:0];
wire [1:0]  TRACK1_sTGC_lane5[3:0];
wire [1:0]  TRACK1_sTGC_lane6[3:0];

wire [1:0]  TRACK1_MM_lane0[3:0];
wire [1:0]  TRACK1_MM_lane1[3:0];
wire [1:0]  TRACK1_MM_lane2[3:0];
wire [1:0]  TRACK1_MM_lane3[3:0];
wire [1:0]  TRACK1_MM_lane4[3:0];
wire [1:0]  TRACK1_MM_lane5[3:0];
wire [1:0]  TRACK1_MM_lane6[3:0];

wire [4:0]  TRACK1_DTHETA_lane0[3:0];
wire [4:0]  TRACK1_DTHETA_lane1[3:0];
wire [4:0]  TRACK1_DTHETA_lane2[3:0];
wire [4:0]  TRACK1_DTHETA_lane3[3:0];
wire [4:0]  TRACK1_DTHETA_lane4[3:0];
wire [4:0]  TRACK1_DTHETA_lane5[3:0];
wire [4:0]  TRACK1_DTHETA_lane6[3:0];

wire [5:0]  TRACK1_PHI_lane0[3:0];
wire [5:0]  TRACK1_PHI_lane1[3:0];
wire [5:0]  TRACK1_PHI_lane2[3:0];
wire [5:0]  TRACK1_PHI_lane3[3:0];
wire [5:0]  TRACK1_PHI_lane4[3:0];
wire [5:0]  TRACK1_PHI_lane5[3:0];
wire [5:0]  TRACK1_PHI_lane6[3:0];

wire [7:0]  TRACK1_ETA_lane0[3:0];
wire [7:0]  TRACK1_ETA_lane1[3:0];
wire [7:0]  TRACK1_ETA_lane2[3:0];
wire [7:0]  TRACK1_ETA_lane3[3:0];
wire [7:0]  TRACK1_ETA_lane4[3:0];
wire [7:0]  TRACK1_ETA_lane5[3:0];
wire [7:0]  TRACK1_ETA_lane6[3:0];

wire   TRACK2_SPARE_lane0[3:0];
wire   TRACK2_SPARE_lane1[3:0];
wire   TRACK2_SPARE_lane2[3:0];
wire   TRACK2_SPARE_lane3[3:0];
wire   TRACK2_SPARE_lane4[3:0];
wire   TRACK2_SPARE_lane5[3:0];
wire   TRACK2_SPARE_lane6[3:0];

wire [1:0]  TRACK2_sTGC_lane0[3:0];
wire [1:0]  TRACK2_sTGC_lane1[3:0];
wire [1:0]  TRACK2_sTGC_lane2[3:0];
wire [1:0]  TRACK2_sTGC_lane3[3:0];
wire [1:0]  TRACK2_sTGC_lane4[3:0];
wire [1:0]  TRACK2_sTGC_lane5[3:0];
wire [1:0]  TRACK2_sTGC_lane6[3:0];

wire [1:0]  TRACK2_MM_lane0[3:0];
wire [1:0]  TRACK2_MM_lane1[3:0];
wire [1:0]  TRACK2_MM_lane2[3:0];
wire [1:0]  TRACK2_MM_lane3[3:0];
wire [1:0]  TRACK2_MM_lane4[3:0];
wire [1:0]  TRACK2_MM_lane5[3:0];
wire [1:0]  TRACK2_MM_lane6[3:0];

wire [4:0]  TRACK2_DTHETA_lane0[3:0];
wire [4:0]  TRACK2_DTHETA_lane1[3:0];
wire [4:0]  TRACK2_DTHETA_lane2[3:0];
wire [4:0]  TRACK2_DTHETA_lane3[3:0];
wire [4:0]  TRACK2_DTHETA_lane4[3:0];
wire [4:0]  TRACK2_DTHETA_lane5[3:0];
wire [4:0]  TRACK2_DTHETA_lane6[3:0];

wire [5:0]  TRACK2_PHI_lane0[3:0];
wire [5:0]  TRACK2_PHI_lane1[3:0];
wire [5:0]  TRACK2_PHI_lane2[3:0];
wire [5:0]  TRACK2_PHI_lane3[3:0];
wire [5:0]  TRACK2_PHI_lane4[3:0];
wire [5:0]  TRACK2_PHI_lane5[3:0];
wire [5:0]  TRACK2_PHI_lane6[3:0];

wire [7:0]  TRACK2_ETA_lane0[3:0];
wire [7:0]  TRACK2_ETA_lane1[3:0];
wire [7:0]  TRACK2_ETA_lane2[3:0];
wire [7:0]  TRACK2_ETA_lane3[3:0];
wire [7:0]  TRACK2_ETA_lane4[3:0];
wire [7:0]  TRACK2_ETA_lane5[3:0];
wire [7:0]  TRACK2_ETA_lane6[3:0];

wire   TRACK3_SPARE_lane0[3:0];
wire   TRACK3_SPARE_lane1[3:0];
wire   TRACK3_SPARE_lane2[3:0];
wire   TRACK3_SPARE_lane3[3:0];
wire   TRACK3_SPARE_lane4[3:0];
wire   TRACK3_SPARE_lane5[3:0];
wire   TRACK3_SPARE_lane6[3:0];

wire [1:0]  TRACK3_sTGC_lane0[3:0];
wire [1:0]  TRACK3_sTGC_lane1[3:0];
wire [1:0]  TRACK3_sTGC_lane2[3:0];
wire [1:0]  TRACK3_sTGC_lane3[3:0];
wire [1:0]  TRACK3_sTGC_lane4[3:0];
wire [1:0]  TRACK3_sTGC_lane5[3:0];
wire [1:0]  TRACK3_sTGC_lane6[3:0];

wire [1:0]  TRACK3_MM_lane0[3:0];
wire [1:0]  TRACK3_MM_lane1[3:0];
wire [1:0]  TRACK3_MM_lane2[3:0];
wire [1:0]  TRACK3_MM_lane3[3:0];
wire [1:0]  TRACK3_MM_lane4[3:0];
wire [1:0]  TRACK3_MM_lane5[3:0];
wire [1:0]  TRACK3_MM_lane6[3:0];

wire [4:0]  TRACK3_DTHETA_lane0[3:0];
wire [4:0]  TRACK3_DTHETA_lane1[3:0];
wire [4:0]  TRACK3_DTHETA_lane2[3:0];
wire [4:0]  TRACK3_DTHETA_lane3[3:0];
wire [4:0]  TRACK3_DTHETA_lane4[3:0];
wire [4:0]  TRACK3_DTHETA_lane5[3:0];
wire [4:0]  TRACK3_DTHETA_lane6[3:0];

wire [5:0]  TRACK3_PHI_lane0[3:0];
wire [5:0]  TRACK3_PHI_lane1[3:0];
wire [5:0]  TRACK3_PHI_lane2[3:0];
wire [5:0]  TRACK3_PHI_lane3[3:0];
wire [5:0]  TRACK3_PHI_lane4[3:0];
wire [5:0]  TRACK3_PHI_lane5[3:0];
wire [5:0]  TRACK3_PHI_lane6[3:0];

wire [7:0]  TRACK3_ETA_lane0[3:0];
wire [7:0]  TRACK3_ETA_lane1[3:0];
wire [7:0]  TRACK3_ETA_lane2[3:0];
wire [7:0]  TRACK3_ETA_lane3[3:0];
wire [7:0]  TRACK3_ETA_lane4[3:0];
wire [7:0]  TRACK3_ETA_lane5[3:0];
wire [7:0]  TRACK3_ETA_lane6[3:0];

wire [23:0] gt0_track0, gt0_track1, gt0_track2, gt0_track3;
wire [23:0] gt1_track0, gt1_track1, gt1_track2, gt1_track3;
wire [23:0] gt2_track0, gt2_track1, gt2_track2, gt2_track3;
wire [23:0] gt3_track0, gt3_track1, gt3_track2, gt3_track3;
wire [23:0] gt4_track0, gt4_track1, gt4_track2, gt4_track3;
wire [23:0] gt5_track0, gt5_track1, gt5_track2, gt5_track3;
wire [23:0] gt6_track0, gt6_track1, gt6_track2, gt6_track3;

reg [1:0] data_counter = 2'h0;
always @(posedge TTC_CLK) begin
	data_counter <= data_counter + 2'b1;
end

assign gt0_track0 = {TRACK0_SPARE_lane0[data_counter], TRACK0_sTGC_lane0[data_counter][1:0], TRACK0_MM_lane0[data_counter][1:0], TRACK0_DTHETA_lane0[data_counter][4:0], TRACK0_PHI_lane0[data_counter][5:0], TRACK0_ETA_lane0[data_counter][7:0]};
assign gt0_track1 = {TRACK1_SPARE_lane0[data_counter], TRACK1_sTGC_lane0[data_counter][1:0], TRACK1_MM_lane0[data_counter][1:0], TRACK1_DTHETA_lane0[data_counter][4:0], TRACK1_PHI_lane0[data_counter][5:0], TRACK1_ETA_lane0[data_counter][7:0]};
assign gt0_track2 = {TRACK2_SPARE_lane0[data_counter], TRACK2_sTGC_lane0[data_counter][1:0], TRACK2_MM_lane0[data_counter][1:0], TRACK2_DTHETA_lane0[data_counter][4:0], TRACK2_PHI_lane0[data_counter][5:0], TRACK2_ETA_lane0[data_counter][7:0]};
assign gt0_track3 = {TRACK3_SPARE_lane0[data_counter], TRACK3_sTGC_lane0[data_counter][1:0], TRACK3_MM_lane0[data_counter][1:0], TRACK3_DTHETA_lane0[data_counter][4:0], TRACK3_PHI_lane0[data_counter][5:0], TRACK3_ETA_lane0[data_counter][7:0]};

assign gt1_track0 = {TRACK0_SPARE_lane1[data_counter], TRACK0_sTGC_lane1[data_counter][1:0], TRACK0_MM_lane1[data_counter][1:0], TRACK0_DTHETA_lane1[data_counter][4:0], TRACK0_PHI_lane1[data_counter][5:0], TRACK0_ETA_lane1[data_counter][7:0]};
assign gt1_track1 = {TRACK1_SPARE_lane1[data_counter], TRACK1_sTGC_lane1[data_counter][1:0], TRACK1_MM_lane1[data_counter][1:0], TRACK1_DTHETA_lane1[data_counter][4:0], TRACK1_PHI_lane1[data_counter][5:0], TRACK1_ETA_lane1[data_counter][7:0]};
assign gt1_track2 = {TRACK2_SPARE_lane1[data_counter], TRACK2_sTGC_lane1[data_counter][1:0], TRACK2_MM_lane1[data_counter][1:0], TRACK2_DTHETA_lane1[data_counter][4:0], TRACK2_PHI_lane1[data_counter][5:0], TRACK2_ETA_lane1[data_counter][7:0]};
assign gt1_track3 = {TRACK3_SPARE_lane1[data_counter], TRACK3_sTGC_lane1[data_counter][1:0], TRACK3_MM_lane1[data_counter][1:0], TRACK3_DTHETA_lane1[data_counter][4:0], TRACK3_PHI_lane1[data_counter][5:0], TRACK3_ETA_lane1[data_counter][7:0]};

assign gt0_track0 = {TRACK0_SPARE_lane0[data_counter], TRACK0_sTGC_lane0[data_counter][1:0], TRACK0_MM_lane0[data_counter][1:0], TRACK0_DTHETA_lane0[data_counter][4:0], TRACK0_PHI_lane0[data_counter][5:0], TRACK0_ETA_lane0[data_counter][7:0]};
assign gt0_track1 = {TRACK1_SPARE_lane0[data_counter], TRACK1_sTGC_lane0[data_counter][1:0], TRACK1_MM_lane0[data_counter][1:0], TRACK1_DTHETA_lane0[data_counter][4:0], TRACK1_PHI_lane0[data_counter][5:0], TRACK1_ETA_lane0[data_counter][7:0]};
assign gt0_track2 = {TRACK2_SPARE_lane0[data_counter], TRACK2_sTGC_lane0[data_counter][1:0], TRACK2_MM_lane0[data_counter][1:0], TRACK2_DTHETA_lane0[data_counter][4:0], TRACK2_PHI_lane0[data_counter][5:0], TRACK2_ETA_lane0[data_counter][7:0]};
assign gt0_track3 = {TRACK3_SPARE_lane0[data_counter], TRACK3_sTGC_lane0[data_counter][1:0], TRACK3_MM_lane0[data_counter][1:0], TRACK3_DTHETA_lane0[data_counter][4:0], TRACK3_PHI_lane0[data_counter][5:0], TRACK3_ETA_lane0[data_counter][7:0]};

assign gt2_track0 = {TRACK0_SPARE_lane2[data_counter], TRACK0_sTGC_lane2[data_counter][1:0], TRACK0_MM_lane2[data_counter][1:0], TRACK0_DTHETA_lane2[data_counter][4:0], TRACK0_PHI_lane2[data_counter][5:0], TRACK0_ETA_lane2[data_counter][7:0]};
assign gt2_track1 = {TRACK1_SPARE_lane2[data_counter], TRACK1_sTGC_lane2[data_counter][1:0], TRACK1_MM_lane2[data_counter][1:0], TRACK1_DTHETA_lane2[data_counter][4:0], TRACK1_PHI_lane2[data_counter][5:0], TRACK1_ETA_lane2[data_counter][7:0]};
assign gt2_track2 = {TRACK2_SPARE_lane2[data_counter], TRACK2_sTGC_lane2[data_counter][1:0], TRACK2_MM_lane2[data_counter][1:0], TRACK2_DTHETA_lane2[data_counter][4:0], TRACK2_PHI_lane2[data_counter][5:0], TRACK2_ETA_lane2[data_counter][7:0]};
assign gt2_track3 = {TRACK3_SPARE_lane2[data_counter], TRACK3_sTGC_lane2[data_counter][1:0], TRACK3_MM_lane2[data_counter][1:0], TRACK3_DTHETA_lane2[data_counter][4:0], TRACK3_PHI_lane2[data_counter][5:0], TRACK3_ETA_lane2[data_counter][7:0]};

assign gt3_track0 = {TRACK0_SPARE_lane3[data_counter], TRACK0_sTGC_lane3[data_counter][1:0], TRACK0_MM_lane3[data_counter][1:0], TRACK0_DTHETA_lane3[data_counter][4:0], TRACK0_PHI_lane3[data_counter][5:0], TRACK0_ETA_lane3[data_counter][7:0]};
assign gt3_track1 = {TRACK1_SPARE_lane3[data_counter], TRACK1_sTGC_lane3[data_counter][1:0], TRACK1_MM_lane3[data_counter][1:0], TRACK1_DTHETA_lane3[data_counter][4:0], TRACK1_PHI_lane3[data_counter][5:0], TRACK1_ETA_lane3[data_counter][7:0]};
assign gt3_track2 = {TRACK2_SPARE_lane3[data_counter], TRACK2_sTGC_lane3[data_counter][1:0], TRACK2_MM_lane3[data_counter][1:0], TRACK2_DTHETA_lane3[data_counter][4:0], TRACK2_PHI_lane3[data_counter][5:0], TRACK2_ETA_lane3[data_counter][7:0]};
assign gt3_track3 = {TRACK3_SPARE_lane3[data_counter], TRACK3_sTGC_lane3[data_counter][1:0], TRACK3_MM_lane3[data_counter][1:0], TRACK3_DTHETA_lane3[data_counter][4:0], TRACK3_PHI_lane3[data_counter][5:0], TRACK3_ETA_lane3[data_counter][7:0]};

assign gt4_track0 = {TRACK0_SPARE_lane4[data_counter], TRACK0_sTGC_lane4[data_counter][1:0], TRACK0_MM_lane4[data_counter][1:0], TRACK0_DTHETA_lane4[data_counter][4:0], TRACK0_PHI_lane4[data_counter][5:0], TRACK0_ETA_lane4[data_counter][7:0]};
assign gt4_track1 = {TRACK1_SPARE_lane4[data_counter], TRACK1_sTGC_lane4[data_counter][1:0], TRACK1_MM_lane4[data_counter][1:0], TRACK1_DTHETA_lane4[data_counter][4:0], TRACK1_PHI_lane4[data_counter][5:0], TRACK1_ETA_lane4[data_counter][7:0]};
assign gt4_track2 = {TRACK2_SPARE_lane4[data_counter], TRACK2_sTGC_lane4[data_counter][1:0], TRACK2_MM_lane4[data_counter][1:0], TRACK2_DTHETA_lane4[data_counter][4:0], TRACK2_PHI_lane4[data_counter][5:0], TRACK2_ETA_lane4[data_counter][7:0]};
assign gt4_track3 = {TRACK3_SPARE_lane4[data_counter], TRACK3_sTGC_lane4[data_counter][1:0], TRACK3_MM_lane4[data_counter][1:0], TRACK3_DTHETA_lane4[data_counter][4:0], TRACK3_PHI_lane4[data_counter][5:0], TRACK3_ETA_lane4[data_counter][7:0]};

assign gt5_track0 = {TRACK0_SPARE_lane5[data_counter], TRACK0_sTGC_lane5[data_counter][1:0], TRACK0_MM_lane5[data_counter][1:0], TRACK0_DTHETA_lane5[data_counter][4:0], TRACK0_PHI_lane5[data_counter][5:0], TRACK0_ETA_lane5[data_counter][7:0]};
assign gt5_track1 = {TRACK1_SPARE_lane5[data_counter], TRACK1_sTGC_lane5[data_counter][1:0], TRACK1_MM_lane5[data_counter][1:0], TRACK1_DTHETA_lane5[data_counter][4:0], TRACK1_PHI_lane5[data_counter][5:0], TRACK1_ETA_lane5[data_counter][7:0]};
assign gt5_track2 = {TRACK2_SPARE_lane5[data_counter], TRACK2_sTGC_lane5[data_counter][1:0], TRACK2_MM_lane5[data_counter][1:0], TRACK2_DTHETA_lane5[data_counter][4:0], TRACK2_PHI_lane5[data_counter][5:0], TRACK2_ETA_lane5[data_counter][7:0]};
assign gt5_track3 = {TRACK3_SPARE_lane5[data_counter], TRACK3_sTGC_lane5[data_counter][1:0], TRACK3_MM_lane5[data_counter][1:0], TRACK3_DTHETA_lane5[data_counter][4:0], TRACK3_PHI_lane5[data_counter][5:0], TRACK3_ETA_lane5[data_counter][7:0]};

assign gt6_track0 = {TRACK0_SPARE_lane6[data_counter], TRACK0_sTGC_lane6[data_counter][1:0], TRACK0_MM_lane6[data_counter][1:0], TRACK0_DTHETA_lane6[data_counter][4:0], TRACK0_PHI_lane6[data_counter][5:0], TRACK0_ETA_lane6[data_counter][7:0]};
assign gt6_track1 = {TRACK1_SPARE_lane6[data_counter], TRACK1_sTGC_lane6[data_counter][1:0], TRACK1_MM_lane6[data_counter][1:0], TRACK1_DTHETA_lane6[data_counter][4:0], TRACK1_PHI_lane6[data_counter][5:0], TRACK1_ETA_lane6[data_counter][7:0]};
assign gt6_track2 = {TRACK2_SPARE_lane6[data_counter], TRACK2_sTGC_lane6[data_counter][1:0], TRACK2_MM_lane6[data_counter][1:0], TRACK2_DTHETA_lane6[data_counter][4:0], TRACK2_PHI_lane6[data_counter][5:0], TRACK2_ETA_lane6[data_counter][7:0]};
assign gt6_track3 = {TRACK3_SPARE_lane6[data_counter], TRACK3_sTGC_lane6[data_counter][1:0], TRACK3_MM_lane6[data_counter][1:0], TRACK3_DTHETA_lane6[data_counter][4:0], TRACK3_PHI_lane6[data_counter][5:0], TRACK3_ETA_lane6[data_counter][7:0]};


// GTX Transmit Port (TX Port)
GTX_TX GTX_TX(
// 40MHz Clock in
    .CLK_in             (TTC_CLK),
    .reset_in           (RESET_TX),
    .TX_reset_in        (TX_reset),
    .TX_Logic_reset_in  (TX_Logic_reset),

// tx user clock in
    .gt_txusrclk_in(gt_txusrclk2),
    .gt_txusrclk_in_80(gt_txusrclk2_80),

// TTC signals and ID information
    .BCID_in(trig_BCID),    

// trigger data for tx
    .track0_S0_in   (track0_S0),
    .track1_S0_in   (track1_S0),
    .track2_S0_in   (track2_S0),
    .track3_S0_in   (track3_S0),
    .flag_S0_in     (flag_S0),
    .track0_S1_in   (track0_S1),
    .track1_S1_in   (track1_S1),
    .track2_S1_in   (track2_S1),
    .track3_S1_in   (track3_S1),
    .flag_S1_in     (flag_S1),

// emulator data
    .gtx_header_in(GTX_HEADER),

    //.gt0_word1_in({track0_data[15:0]}),
    //.gt0_word2_in({track1_data[7:0], track0_data[23:16]}),
    //.gt0_word3_in({track1_data[23:8]}),
    //.gt0_word4_in({track2_data[15:0]}),
    //.gt0_word5_in({track3_data[7:0], track2_data[23:16]}),
    //.gt0_word6_in({track3_data[23:8]}),
    //.gt0_word7_in({BCID[11:0], EventID[3:0]}),

    .gt0_word1_in({gt0_track0[15:0]}),
    .gt0_word2_in({gt0_track1[7:0], gt0_track0[23:16]}),
    .gt0_word3_in({gt0_track1[23:8]}),
    .gt0_word4_in({gt0_track2[15:0]}),
    .gt0_word5_in({gt0_track3[7:0], gt0_track2[23:16]}),
    .gt0_word6_in({gt0_track3[23:8]}),
    .gt0_word7_in({BCID[11:0], EventID[3:0]}),

    .gt1_word1_in({gt1_track0[15:0]}),
    .gt1_word2_in({gt1_track1[7:0], gt1_track0[23:16]}),
    .gt1_word3_in({gt1_track1[23:8]}),
    .gt1_word4_in({gt1_track2[15:0]}),
    .gt1_word5_in({gt1_track3[7:0], gt1_track2[23:16]}),
    .gt1_word6_in({gt1_track3[23:8]}),
    .gt1_word7_in({BCID[11:0], EventID[3:0]}),

    .gt2_word1_in({gt2_track0[15:0]}),
    .gt2_word2_in({gt2_track1[7:0], gt2_track0[23:16]}),
    .gt2_word3_in({gt2_track1[23:8]}),
    .gt2_word4_in({gt2_track2[15:0]}),
    .gt2_word5_in({gt2_track3[7:0], gt2_track2[23:16]}),
    .gt2_word6_in({gt2_track3[23:8]}),
    .gt2_word7_in({BCID[11:0], EventID[3:0]}),

    .gt3_word1_in({gt3_track0[15:0]}),
    .gt3_word2_in({gt3_track1[7:0], gt3_track0[23:16]}),
    .gt3_word3_in({gt3_track1[23:8]}),
    .gt3_word4_in({gt3_track2[15:0]}),
    .gt3_word5_in({gt3_track3[7:0], gt3_track2[23:16]}),
    .gt3_word6_in({gt3_track3[23:8]}),
    .gt3_word7_in({BCID[11:0], EventID[3:0]}),

    .gt4_word1_in({gt4_track0[15:0]}),
    .gt4_word2_in({gt4_track1[7:0], gt4_track0[23:16]}),
    .gt4_word3_in({gt4_track1[23:8]}),
    .gt4_word4_in({gt4_track2[15:0]}),
    .gt4_word5_in({gt4_track3[7:0], gt4_track2[23:16]}),
    .gt4_word6_in({gt4_track3[23:8]}),
    .gt4_word7_in({BCID[11:0], EventID[3:0]}),

    .gt5_word1_in({gt5_track0[15:0]}),
    .gt5_word2_in({gt5_track1[7:0], gt5_track0[23:16]}),
    .gt5_word3_in({gt5_track1[23:8]}),
    .gt5_word4_in({gt5_track2[15:0]}),
    .gt5_word5_in({gt5_track3[7:0], gt5_track2[23:16]}),
    .gt5_word6_in({gt5_track3[23:8]}),
    .gt5_word7_in({BCID[11:0], EventID[3:0]}),

    .gt6_word1_in({gt6_track0[15:0]}),
    .gt6_word2_in({gt6_track1[7:0], gt6_track0[23:16]}),
    .gt6_word3_in({gt6_track1[23:8]}),
    .gt6_word4_in({gt6_track2[15:0]}),
    .gt6_word5_in({gt6_track3[7:0], gt6_track2[23:16]}),
    .gt6_word6_in({gt6_track3[23:8]}),
    .gt6_word7_in({BCID[11:0], EventID[3:0]}),

// tx data out
    .gt0_txdata_out(gt0_txdata), .gt1_txdata_out(gt1_txdata), .gt2_txdata_out(gt2_txdata), .gt3_txdata_out(gt3_txdata),
    .gt4_txdata_out(gt4_txdata), .gt5_txdata_out(gt5_txdata), .gt6_txdata_out(gt6_txdata), .gt7_txdata_out(gt7_txdata),
    .gt8_txdata_out(gt8_txdata), .gt9_txdata_out(gt9_txdata), .gt10_txdata_out(gt10_txdata), .gt11_txdata_out(gt11_txdata),

// tx char is k ouot    
    .gt0_txcharisk_out(gt0_txcharisk), .gt1_txcharisk_out(gt1_txcharisk), .gt2_txcharisk_out(gt2_txcharisk), .gt3_txcharisk_out(gt3_txcharisk),
    .gt4_txcharisk_out(gt4_txcharisk), .gt5_txcharisk_out(gt5_txcharisk), .gt6_txcharisk_out(gt6_txcharisk), .gt7_txcharisk_out(gt7_txcharisk),
    .gt8_txcharisk_out(gt8_txcharisk), .gt9_txcharisk_out(gt9_txcharisk), .gt10_txcharisk_out(gt10_txcharisk), .gt11_txcharisk_out(gt11_txcharisk)
);



// GTX Receive Port (RX Port)
GTX_RX GTX_RX(
// rx data in
    .gt0_rxdata_in(gt0_rxdata), .gt1_rxdata_in(gt1_rxdata), .gt2_rxdata_in(gt2_rxdata), .gt3_rxdata_in(gt3_rxdata),
    .gt4_rxdata_in(gt4_rxdata), .gt5_rxdata_in(gt5_rxdata), .gt6_rxdata_in(gt6_rxdata), .gt7_rxdata_in(gt7_rxdata),
    .gt8_rxdata_in(gt8_rxdata), .gt9_rxdata_in(gt9_rxdata), .gt10_rxdata_in(gt10_rxdata), .gt11_rxdata_in(gt11_rxdata),

//rx cha is k in    
    .gt0_rxcharisk_in(gt0_rxcharisk), .gt1_rxcharisk_in(gt1_rxcharisk), .gt2_rxcharisk_in(gt2_rxcharisk), .gt3_rxcharisk_in(gt3_rxcharisk),
    .gt4_rxcharisk_in(gt4_rxcharisk), .gt5_rxcharisk_in(gt5_rxcharisk), .gt6_rxcharisk_in(gt6_rxcharisk), .gt7_rxcharisk_in(gt7_rxcharisk),
    .gt8_rxcharisk_in(gt8_rxcharisk), .gt9_rxcharisk_in(gt9_rxcharisk), .gt10_rxcharisk_in(gt10_rxcharisk), .gt11_rxcharisk_in(gt11_rxcharisk),

//rx user clock in        
    .gt0_rxusrclk_in(gt0_rxusrclk), .gt1_rxusrclk_in(gt1_rxusrclk), .gt2_rxusrclk_in(gt2_rxusrclk), .gt3_rxusrclk_in(gt3_rxusrclk),
    .gt4_rxusrclk_in(gt4_rxusrclk), .gt5_rxusrclk_in(gt5_rxusrclk), .gt6_rxusrclk_in(gt6_rxusrclk), .gt7_rxusrclk_in(gt7_rxusrclk),
    .gt8_rxusrclk_in(gt8_rxusrclk), .gt9_rxusrclk_in(gt9_rxusrclk), .gt10_rxusrclk_in(gt10_rxusrclk), .gt11_rxusrclk_in(gt11_rxusrclk),

    .reset_in        (RESET_TTC),
    .RX_reset_in     (RX_reset),
    .rx0_data_out    (gtx0_data),
    .rx1_data_out    (gtx1_data),
    .rx2_data_out    (gtx2_data),
    .rx3_data_out    (gtx3_data),
    .rx4_data_out    (gtx4_data),
    .rx5_data_out    (gtx5_data),
    .rx6_data_out    (gtx6_data),
    .rx7_data_out    (gtx7_data),
    .rx8_data_out    (gtx8_data),
    .rx9_data_out    (gtx9_data),
    .rx10_data_out   (gtx10_data),
    .rx11_data_out   (gtx11_data)
);



//// Read-Out Line

// GTX Delay Port
GTX_Delay GTX_Delay(
    .CLK_in             (TTC_CLK),
    .CLK_in_0           (CLK_40_0),
    .CLK_in_1           (CLK_40_1),
    .CLK_in_2           (CLK_40_2),
    .CLK_in_3           (CLK_40_3),
    .reset_in           (RESET_TTC),
    .Delay_reset_in     (Delay_reset),
    
    .gtx0_data_in        (gtx0_data[111:0]),
    .gtx1_data_in        (gtx1_data[111:0]),
    .gtx2_data_in        (gtx2_data[111:0]),
    .gtx3_data_in        (gtx3_data[111:0]),
    .gtx4_data_in        (gtx4_data[111:0]),
    .gtx5_data_in        (gtx5_data[111:0]),
    .gtx6_data_in        (gtx6_data[111:0]),
    .gtx7_data_in        (gtx7_data[111:0]),
    .gtx8_data_in        (gtx8_data[111:0]),
    .gtx9_data_in        (gtx9_data[111:0]),
    .gtx10_data_in       (gtx10_data[111:0]),
    .gtx11_data_in       (gtx11_data[111:0]),

    .gtx0_phase_in       (gtx0_phase_select),
    .gtx1_phase_in       (gtx1_phase_select),
    .gtx2_phase_in       (gtx2_phase_select),
    .gtx3_phase_in       (gtx3_phase_select),
    .gtx4_phase_in       (gtx4_phase_select),
    .gtx5_phase_in       (gtx5_phase_select),
    .gtx6_phase_in       (gtx6_phase_select),
    .gtx7_phase_in       (gtx7_phase_select),
    .gtx8_phase_in       (gtx8_phase_select),
    .gtx9_phase_in       (gtx9_phase_select),
    .gtx10_phase_in      (gtx10_phase_select),
    .gtx11_phase_in      (gtx11_phase_select),

    .gtx0_phase_0_out      (gtx0_phase_monitor_0),
    .gtx0_phase_1_out      (gtx0_phase_monitor_1),
    .gtx0_phase_2_out      (gtx0_phase_monitor_2),
    .gtx0_phase_3_out      (gtx0_phase_monitor_3),

    .gtx1_phase_0_out      (gtx1_phase_monitor_0),
    .gtx1_phase_1_out      (gtx1_phase_monitor_1),
    .gtx1_phase_2_out      (gtx1_phase_monitor_2),
    .gtx1_phase_3_out      (gtx1_phase_monitor_3),

    .gtx2_phase_0_out      (gtx2_phase_monitor_0),
    .gtx2_phase_1_out      (gtx2_phase_monitor_1),
    .gtx2_phase_2_out      (gtx2_phase_monitor_2),
    .gtx2_phase_3_out      (gtx2_phase_monitor_3),

    .gtx3_phase_0_out      (gtx3_phase_monitor_0),
    .gtx3_phase_1_out      (gtx3_phase_monitor_1),
    .gtx3_phase_2_out      (gtx3_phase_monitor_2),
    .gtx3_phase_3_out      (gtx3_phase_monitor_3),

    .gtx4_phase_0_out      (gtx4_phase_monitor_0),
    .gtx4_phase_1_out      (gtx4_phase_monitor_1),
    .gtx4_phase_2_out      (gtx4_phase_monitor_2),
    .gtx4_phase_3_out      (gtx4_phase_monitor_3),

    .gtx5_phase_0_out      (gtx5_phase_monitor_0),
    .gtx5_phase_1_out      (gtx5_phase_monitor_1),
    .gtx5_phase_2_out      (gtx5_phase_monitor_2),
    .gtx5_phase_3_out      (gtx5_phase_monitor_3),

    .gtx6_phase_0_out      (gtx6_phase_monitor_0),
    .gtx6_phase_1_out      (gtx6_phase_monitor_1),
    .gtx6_phase_2_out      (gtx6_phase_monitor_2),
    .gtx6_phase_3_out      (gtx6_phase_monitor_3),

    .gtx7_phase_0_out      (gtx7_phase_monitor_0),
    .gtx7_phase_1_out      (gtx7_phase_monitor_1),
    .gtx7_phase_2_out      (gtx7_phase_monitor_2),
    .gtx7_phase_3_out      (gtx7_phase_monitor_3),

    .gtx8_phase_0_out      (gtx8_phase_monitor_0),
    .gtx8_phase_1_out      (gtx8_phase_monitor_1),
    .gtx8_phase_2_out      (gtx8_phase_monitor_2),
    .gtx8_phase_3_out      (gtx8_phase_monitor_3),

    .gtx9_phase_0_out      (gtx9_phase_monitor_0),
    .gtx9_phase_1_out      (gtx9_phase_monitor_1),
    .gtx9_phase_2_out      (gtx9_phase_monitor_2),
    .gtx9_phase_3_out      (gtx9_phase_monitor_3),

    .gtx10_phase_0_out     (gtx10_phase_monitor_0),
    .gtx10_phase_1_out     (gtx10_phase_monitor_1),
    .gtx10_phase_2_out     (gtx10_phase_monitor_2),
    .gtx10_phase_3_out     (gtx10_phase_monitor_3),

    .gtx11_phase_0_out     (gtx11_phase_monitor_0),
    .gtx11_phase_1_out     (gtx11_phase_monitor_1),
    .gtx11_phase_2_out     (gtx11_phase_monitor_2),
    .gtx11_phase_3_out     (gtx11_phase_monitor_3),

    .gtx0_delay_in       (Delay_gtx0),
    .gtx1_delay_in       (Delay_gtx1),
    .gtx2_delay_in       (Delay_gtx2),
    .gtx3_delay_in       (Delay_gtx3),
    .gtx4_delay_in       (Delay_gtx4),
    .gtx5_delay_in       (Delay_gtx5),
    .gtx6_delay_in       (Delay_gtx6),
    .gtx7_delay_in       (Delay_gtx7),
    .gtx8_delay_in       (Delay_gtx8),
    .gtx9_delay_in       (Delay_gtx9),
    .gtx10_delay_in      (Delay_gtx10),
    .gtx11_delay_in      (Delay_gtx11),

    .gtx0_delayed_out    (gtx0_delayed),
    .gtx1_delayed_out    (gtx1_delayed),
    .gtx2_delayed_out    (gtx2_delayed),
    .gtx3_delayed_out    (gtx3_delayed),
    .gtx4_delayed_out    (gtx4_delayed),
    .gtx5_delayed_out    (gtx5_delayed),
    .gtx6_delayed_out    (gtx6_delayed),
    .gtx7_delayed_out    (gtx7_delayed),
    .gtx8_delayed_out    (gtx8_delayed),
    .gtx9_delayed_out    (gtx9_delayed),
    .gtx10_delayed_out   (gtx10_delayed),
    .gtx11_delayed_out   (gtx11_delayed)    

);


// TTC Delays
TTC_Delay TTC_Delay(
// Clock and resets
    .CLK_in             (TTC_CLK),
    .reset_in           (RESET_TTC),
    .Delay_reset_in     (Delay_reset),
// input signals
    .L1A_in             (L1A||L1A_manual),
    .BCR_in             (BCR),
    .ECR_in             (ECR),
    .TTC_RESET_in       (TTC_RESET),
    .TEST_PULSE_in      (TEST_PULSE_TRIGGER),

// TTC delay parameter, common for all TTC signals
    .L1A_delay_in           (Delay_L1A),
    .BCR_delay_in           (Delay_BCR),
    .trig_BCR_delay_in      (Delay_trig_BCR),
    .ECR_delay_in           (Delay_ECR),
    .TTC_RESET_delay_in     (Delay_TTC_RESET),
    .TEST_PULSE_delay_in    (Delay_TEST_PULSE),

// Output ports, Delayed TTC signals
    .L1A_delayed_out        (L1A_delayed),
    .BCR_delayed_out        (BCR_delayed),
    .trig_BCR_delayed_out   (trig_BCR_delayed),
    .ECR_delayed_out        (ECR_delayed),
    .TTC_RESET_delayed_out  (TTC_RESET_delayed),
    .TEST_PULSE_delayed_out (TEST_PULSE_delayed)
);

// Mask
assign L1A_masked = (Mask_L1A) ? L1A_manual : L1A_delayed;
assign BCR_masked = (Mask_BCR) ? 1'b0 : BCR_delayed;
assign trig_BCR_masked = (Mask_trig_BCR) ? 1'b0 : trig_BCR_delayed;
assign ECR_masked = (Mask_ECR) ? 1'b0 : ECR_delayed;
assign TTC_RESET_masked = (Mask_TTC_RESET) ? 1'b0 : TTC_RESET_delayed;
assign TEST_PULSE_masked = (Mask_TEST_PULSE) ? 1'b0 : TEST_PULSE_delayed;

// L1ID & BCID Counter
ID_Counter ID_Counter(
    .CLK_in             (TTC_CLK),
    .reset_in           (RESET_TTC),
    .L1A_in             (L1A_masked),
    .BCR_in             (BCR_masked),    
    .trig_BCR_in        (trig_BCR_masked),    
    .ECR_in             (ECR_masked),
    .BCID_out           (BCID),
    .trig_BCID_out      (trig_BCID),
    .L1ID_out           (L1ID),
    .L1A_Counter_out    (L1A_Counter)
);

Clock_Generator Clock_Generator(
    .FPGA_CLK_in     (FPGA_CLK),
    .FPGA_CLK40_in   (FPGA_CLK40),
    .reset_in        (CLK_reset),
    .TTC_CLK_out     (TTC_CLK),
    .CLK_40_0_out    (CLK_40_0),
    .CLK_40_1_out    (CLK_40_1),
    .CLK_40_2_out    (CLK_40_2),
    .CLK_40_3_out    (CLK_40_3),
    //.CLK_25_out      (CLK_25),
    //.CLK_125_out     (CLK_125),
    .CLK_160_out     (CLK_160),
    //.Coin_CLK_160_out(Coin_CLK_160),
    //.CLK_320_out     (CLK_320),
    .locked_out      (locked)
);

wire [255:0] FIFO_data_input;

assign FIFO_data_input[127:0] = (lane_selector == 4'h0) ? {16'h0, gtx0_delayed} :
                                (lane_selector == 4'h1) ? {16'h0, gtx1_delayed} :
                                (lane_selector == 4'h2) ? {16'h0, gtx2_delayed} :
                                (lane_selector == 4'h3) ? {16'h0, gtx3_delayed} :
                                (lane_selector == 4'h4) ? {16'h0, gtx4_delayed} :
                                (lane_selector == 4'h5) ? {16'h0, gtx5_delayed} :
                                (lane_selector == 4'h6) ? {16'h0, gtx6_delayed} :
                                (lane_selector == 4'h7) ? {16'h0, gtx7_delayed} :
                                (lane_selector == 4'h8) ? {16'h0, gtx8_delayed} :
                                (lane_selector == 4'h9) ? {16'h0, gtx9_delayed} :
                                (lane_selector == 4'ha) ? {16'h0, gtx10_delayed} :
                                (lane_selector == 4'hb) ? {16'h0, gtx11_delayed} :
                                (lane_selector == 4'hc) ? {9'b0, rx_data_masked[237:119]} :
                                (lane_selector == 4'hd) ? {9'b0, rx_data_masked[118:0]} : 128'h0;

assign FIFO_data_input[239:128] = gtx0_data;

assign FIFO_data_input[255:240] = {4'h0, BCID[11:0]};


// ILA
gtx_monitor gtx_monitor(
	.clk       (gt0_rxusrclk), // input wire clk
	.probe0    (gt0_rxcharisk), // input wire [3:0]  probe0  
	.probe1    (gt0_rxdata), // input wire [31:0]  probe1 
	.probe2    (gt1_rxcharisk), // input wire [3:0]  probe2 
	.probe3    (gt1_rxdata), // input wire [31:0]  probe3 
	.probe4    (gt2_rxcharisk), // input wire [3:0]  probe4 
	.probe5    (gt3_rxdata) // input wire [31:0]  probe5
);

// GTX Monitoring FIFO (Only for Debug)
Monitoring Monitoring(
    .CLK_in         (TTC_CLK),
    .data_in        (FIFO_data_input),
    .RST_in         (RESET_160||FIFO_reset),
    .write_enb      (write_enb),
    .read_enb       (read_enb),    
    .FIFO_data_out  (FIFO_data)
);

// VME Module
vme vme(
    .TTC_CLK            (TTC_CLK),
    .CLK_160            (CLK_160),
    .TXUSRCLK_in        (gt_txusrclk2),
    .OE                 (OE),
    .CE                 (CE),
    .WE                 (WE),
    .VME_A              (VME_A[11:0]),
    .VME_D              (VME_D[15:0]),
    
//  Reset       
    .Reset_TTC_out          (RESET_TTC),
    .Reset_160_out          (RESET_160),
    .Reset_TX_out           (RESET_TX),
    .Scaler_reset_out       (Scaler_reset),
    .GTX_TX_reset_out       (TX_reset),
    .GTX_RX_reset_out       (RX_reset),
    .Delay_reset_out        (Delay_reset),
    .L1Buffer_reset_out     (L1Buffer_reset),
    .Derandomizer_reset_out (Derandomizer_reset),
    .ZeroSupp_reset_out     (ZeroSupp_reset),
    .CLK_reset_out          (CLK_reset),
    .FIFO_reset_out         (FIFO_reset),
    .SiTCP_reset_out        (SiTCP_reset),
    .SiTCP_FIFO_reset_out   (SiTCP_FIFO_reset),
    .TX_Logic_reset_out     (TX_Logic_reset),
    .Monitoring_reset_out   (Monitoring_reset),
    .LUT_init_reset_out     (LUT_init_reset),
    .gt0_rx_reset_out       (gt0_rx_reset),
    .gt1_rx_reset_out       (gt1_rx_reset),
    .gt2_rx_reset_out       (gt2_rx_reset),
    .gt3_rx_reset_out       (gt3_rx_reset),
    .gt4_rx_reset_out       (gt4_rx_reset),
    .gt5_rx_reset_out       (gt5_rx_reset),
    .gt6_rx_reset_out       (gt6_rx_reset),
    .gt7_rx_reset_out       (gt7_rx_reset),
    .gt8_rx_reset_out       (gt8_rx_reset),
    .gt9_rx_reset_out       (gt9_rx_reset),
    .gt10_rx_reset_out      (gt10_rx_reset),
    .gt11_rx_reset_out      (gt11_rx_reset),

// emulator
    .EMULATOR_reset_out(RESET_Emulator),
    .EMULATOR_enable(EMULATOR_ENABLE),
    .EMULATOR_data_length(EMULATOR_DATA_LENGTH),
    
    .EMULATOR_GTX_header(GTX_HEADER),

    .TRACK0_eta(TRACK0_ETA),
    .TRACK0_phi(TRACK0_PHI),
    .TRACK0_deltatheta(TRACK0_DELTATHETA),
    .TRACK0_mm(TRACK0_MM),
    .TRACK0_stgc(TRACK0_sTGC),
    .TRACK0_spare(TRACK0_SPARE),
    
    .TRACK1_eta(TRACK1_ETA),
    .TRACK1_phi(TRACK1_PHI),
    .TRACK1_deltatheta(TRACK1_DELTATHETA),
    .TRACK1_mm(TRACK1_MM),
    .TRACK1_stgc(TRACK1_sTGC),
    .TRACK1_spare(TRACK1_SPARE),
    
    .TRACK2_eta(TRACK2_ETA),
    .TRACK2_phi(TRACK2_PHI),
    .TRACK2_deltatheta(TRACK2_DELTATHETA),
    .TRACK2_mm(TRACK2_MM),
    .TRACK2_stgc(TRACK2_sTGC),
    .TRACK2_spare(TRACK2_SPARE),
    
    .TRACK3_eta(TRACK3_ETA),
    .TRACK3_phi(TRACK3_PHI),
    .TRACK3_deltatheta(TRACK3_DELTATHETA),
    .TRACK3_mm(TRACK3_MM),
    .TRACK3_stgc(TRACK3_sTGC),
    .TRACK3_spare(TRACK3_SPARE),            
    
// GTX status
    .Error_Scaler_gt_0     (gt0_error_scaler),
    .Error_Scaler_gt_1     (gt1_error_scaler),
    .Error_Scaler_gt_2     (gt2_error_scaler),
    .Error_Scaler_gt_3     (gt3_error_scaler),
    .Error_Scaler_gt_4     (gt4_error_scaler),
    .Error_Scaler_gt_5     (gt5_error_scaler),
    .Error_Scaler_gt_6     (gt6_error_scaler),
    .Error_Scaler_gt_7     (gt7_error_scaler),
    .Error_Scaler_gt_8     (gt8_error_scaler),
    .Error_Scaler_gt_9     (gt9_error_scaler),
    .Error_Scaler_gt_10    (gt10_error_scaler),
    .Error_Scaler_gt_11    (gt11_error_scaler),

    .Status_gt_0         ({10'h0, gt0_status}),
    .Status_gt_1         ({10'h0, gt1_status}),    
    .Status_gt_2         ({10'h0, gt2_status}),
    .Status_gt_3         ({10'h0, gt3_status}),
    .Status_gt_4         ({10'h0, gt4_status}),
    .Status_gt_5         ({10'h0, gt5_status}),
    .Status_gt_6         ({10'h0, gt6_status}),
    .Status_gt_7         ({10'h0, gt7_status}),
    .Status_gt_8         ({10'h0, gt8_status}),
    .Status_gt_9         ({10'h0, gt9_status}),
    .Status_gt_10         ({10'h0, gt10_status}),
    .Status_gt_11         ({10'h0, gt11_status}),

// Delay
    .Delay_glink0         (Delay_glink0),
    .Delay_glink1         (Delay_glink1),
    .Delay_glink2         (Delay_glink2),
    .Delay_glink3         (Delay_glink3),
    .Delay_glink4         (Delay_glink4),
    .Delay_glink5         (Delay_glink5),
    .Delay_glink6         (Delay_glink6),
    .Delay_glink7         (Delay_glink7),
    .Delay_glink8         (Delay_glink8),
    .Delay_glink9         (Delay_glink9),
    .Delay_glink10        (Delay_glink10),
    .Delay_glink11        (Delay_glink11),
    .Delay_glink12        (Delay_glink12),
    .Delay_glink13        (Delay_glink13),
    
    .Delay_gtx0           (Delay_gtx0),
    .Delay_gtx1           (Delay_gtx1),
    .Delay_gtx2           (Delay_gtx2),
    .Delay_gtx3           (Delay_gtx3),
    .Delay_gtx4           (Delay_gtx4),
    .Delay_gtx5           (Delay_gtx5),
    .Delay_gtx6           (Delay_gtx6),
    .Delay_gtx7           (Delay_gtx7),
    .Delay_gtx8           (Delay_gtx8),
    .Delay_gtx9           (Delay_gtx9),
    .Delay_gtx10          (Delay_gtx10),
    .Delay_gtx11          (Delay_gtx11),

    .Delay_L1A              (Delay_L1A),
    .Delay_BCR              (Delay_BCR),
    .Delay_trig_BCR         (Delay_trig_BCR),
    .Delay_ECR              (Delay_ECR),
    .Delay_TTC_RESET        (Delay_TTC_RESET),
    .Delay_TEST_PULSE       (Delay_TEST_PULSE),

// Mask
    .Mask_gtx0           (Mask_gtx0),
    .Mask_gtx1           (Mask_gtx1),
    .Mask_gtx2           (Mask_gtx2),
    .Mask_gtx3           (Mask_gtx3),
    .Mask_gtx4           (Mask_gtx4),
    .Mask_gtx5           (Mask_gtx5),
    .Mask_gtx6           (Mask_gtx6),
    .Mask_gtx7           (Mask_gtx7),
    .Mask_gtx8           (Mask_gtx8),
    .Mask_gtx9           (Mask_gtx9),
    .Mask_gtx10          (Mask_gtx10),
    .Mask_gtx11          (Mask_gtx11),

    .Mask_L1A           (Mask_L1A),
    .Mask_BCR           (Mask_BCR),
    .Mask_trig_BCR      (Mask_trig_BCR),
    .Mask_ECR           (Mask_ECR),
    .Mask_TTC_RESET     (Mask_TTC_RESET),
    .Mask_TEST_PULSE    (Mask_TEST_PULSE),

    .Mask_glink0   (Mask_glink0),
    .Mask_glink1   (Mask_glink1),
    .Mask_glink2   (Mask_glink2),
    .Mask_glink3   (Mask_glink3),
    .Mask_glink4   (Mask_glink4),
    .Mask_glink5   (Mask_glink5),
    .Mask_glink6   (Mask_glink6),
    .Mask_glink7   (Mask_glink7),
    .Mask_glink8   (Mask_glink8),
    .Mask_glink9   (Mask_glink9),
    .Mask_glink10  (Mask_glink10),
    .Mask_glink11  (Mask_glink11),
    .Mask_glink12  (Mask_glink12),
    .Mask_glink13  (Mask_glink13),
    

//Test pulse
    .Test_Pulse_Length(Test_Pulse_Length),
    .Test_Pulse_Wait_Length(Test_Pulse_Wait_Length),
    .Test_Pulse_Enable(Test_Pulse_Enable),

// ID counters
    .L1A_Counter0       (L1A_Counter[15:0]),
    .L1A_Counter1       (L1A_Counter[31:16]),
    .L1ID               (L1ID),
    .BCID               (BCID),    
    .SLID               (SLID),
    .Readout_BC         (Readout_BC),
    .L1Buffer_depth     (L1Buffer_depth),
    .trigL1Buffer_depth (trigL1Buffer_depth),
    .L1Buffer_BW_depth     (L1Buffer_BW_depth),

    .L1Buffer_status        (L1Buffer_status),
    .Derandomizer_status    (FIFO_status),
    .Derandomizer_Count1_1  ({6'b0, Derandomizer_Count1_1}),
    .Derandomizer_Count1_2  ({6'b0, Derandomizer_Count1_2}),
    .Derandomizer_Count2    ({7'b0, Derandomizer_Count2}),
    .Derandomizer_Count3    ({6'b0, Derandomizer_Count3}),
    
    .ZeroSuppress_status    (zero_status),
    .ZeroSuppress_Count     ({3'b0, zero_count}),
    .data_size              (data_size),
    
    .SiTCP_Count            ({3'b0,SiTCP_data_count}),
    
    .BUSY_Count             (BUSY_counter),    

    .FullBoard_ID       (FullBoard_ID),

    .monitoring_FIFO_out_0        (FIFO_data[15:0]),  
    .monitoring_FIFO_out_1        (FIFO_data[31:16]), 
    .monitoring_FIFO_out_2        (FIFO_data[47:32]), 
    .monitoring_FIFO_out_3        (FIFO_data[63:48]), 
    .monitoring_FIFO_out_4        (FIFO_data[79:64]), 
    .monitoring_FIFO_out_5        (FIFO_data[95:80]), 
    .monitoring_FIFO_out_6        (FIFO_data[111:96]),
    .monitoring_FIFO_out_7        (FIFO_data[127:112]),
    .monitoring_FIFO_out_8        (FIFO_data[143:128]),
    .monitoring_FIFO_out_9        (FIFO_data[159:144]),
    .monitoring_FIFO_out_10       (FIFO_data[175:160]),
    .monitoring_FIFO_out_11       (FIFO_data[191:176]),
    .monitoring_FIFO_out_12       (FIFO_data[207:192]),
    .monitoring_FIFO_out_13       (FIFO_data[223:208]),
    .monitoring_FIFO_out_14       (FIFO_data[239:224]),
    .monitoring_FIFO_out_15       (FIFO_data[255:240]),
    
    .monitoring_FIFO_wr_en     (write_enb),
    .monitoring_FIFO_rd_en     (read_enb),
    .lane_selector_out  ({loopback_mode[2:0], lane_selector[3:0]}),
    .xadc_mon_enable_out        (xadc_mon_enable),
    .xadc_mon_addr_out          (xadc_mon_addr),
    .xadc_monitor_in            (xadc_monitor_out),

    .L1A_manual_parameter       ({L1A_reset_value, 3'b0, L1A_manual_reset}),
    .Busy_propagation           (Busy_propagation),
    
    .gtx0_test_data1     (gtx0_test1),
    .gtx0_test_data2     (gtx0_test2),
    .gtx0_test_data3     (gtx0_test3),
    .gtx0_test_data4     (gtx0_test4),
    .gtx0_test_data5     (gtx0_test5),
    .gtx0_test_data6     (gtx0_test6),
    .gtx0_test_data7     (gtx0_test7),
    .gtx1_test_data1     (gtx1_test1),
    .gtx1_test_data2     (gtx1_test2),
    .gtx1_test_data3     (gtx1_test3),
    .gtx1_test_data4     (gtx1_test4),
    .gtx1_test_data5     (gtx1_test5),
    .gtx1_test_data6     (gtx1_test6),
    .gtx1_test_data7     (gtx1_test7),
    .gtx2_test_data1     (gtx2_test1),
    .gtx2_test_data2     (gtx2_test2),
    .gtx2_test_data3     (gtx2_test3),
    .gtx2_test_data4     (gtx2_test4),
    .gtx2_test_data5     (gtx2_test5),
    .gtx2_test_data6     (gtx2_test6),
    .gtx2_test_data7     (gtx2_test7),
    .gtx3_test_data1     (gtx3_test1),
    .gtx3_test_data2     (gtx3_test2),
    .gtx3_test_data3     (gtx3_test3),
    .gtx3_test_data4     (gtx3_test4),
    .gtx3_test_data5     (gtx3_test5),
    .gtx3_test_data6     (gtx3_test6),
    .gtx3_test_data7     (gtx3_test7),
    .gtx4_test_data1     (gtx4_test1),
    .gtx4_test_data2     (gtx4_test2),
    .gtx4_test_data3     (gtx4_test3),
    .gtx4_test_data4     (gtx4_test4),
    .gtx4_test_data5     (gtx4_test5),
    .gtx4_test_data6     (gtx4_test6),
    .gtx4_test_data7     (gtx4_test7),
    .gtx5_test_data1     (gtx5_test1),
    .gtx5_test_data2     (gtx5_test2),
    .gtx5_test_data3     (gtx5_test3),
    .gtx5_test_data4     (gtx5_test4),
    .gtx5_test_data5     (gtx5_test5),
    .gtx5_test_data6     (gtx5_test6),
    .gtx5_test_data7     (gtx5_test7),
    .gtx6_test_data1     (gtx6_test1),
    .gtx6_test_data2     (gtx6_test2),
    .gtx6_test_data3     (gtx6_test3),
    .gtx6_test_data4     (gtx6_test4),
    .gtx6_test_data5     (gtx6_test5),
    .gtx6_test_data6     (gtx6_test6),
    .gtx6_test_data7     (gtx6_test7),
    .gtx7_test_data1     (gtx7_test1),
    .gtx7_test_data2     (gtx7_test2),
    .gtx7_test_data3     (gtx7_test3),
    .gtx7_test_data4     (gtx7_test4),
    .gtx7_test_data5     (gtx7_test5),
    .gtx7_test_data6     (gtx7_test6),
    .gtx7_test_data7     (gtx7_test7),
    .gtx8_test_data1     (gtx8_test1),
    .gtx8_test_data2     (gtx8_test2),
    .gtx8_test_data3     (gtx8_test3),
    .gtx8_test_data4     (gtx8_test4),
    .gtx8_test_data5     (gtx8_test5),
    .gtx8_test_data6     (gtx8_test6),
    .gtx8_test_data7     (gtx8_test7),
    .gtx9_test_data1     (gtx9_test1),
    .gtx9_test_data2     (gtx9_test2),
    .gtx9_test_data3     (gtx9_test3),
    .gtx9_test_data4     (gtx9_test4),
    .gtx9_test_data5     (gtx9_test5),
    .gtx9_test_data6     (gtx9_test6),
    .gtx9_test_data7     (gtx9_test7),
    .gtx10_test_data1    (gtx10_test1),
    .gtx10_test_data2    (gtx10_test2),
    .gtx10_test_data3    (gtx10_test3),
    .gtx10_test_data4    (gtx10_test4),
    .gtx10_test_data5    (gtx10_test5),
    .gtx10_test_data6    (gtx10_test6),
    .gtx10_test_data7    (gtx10_test7),
    .gtx11_test_data1    (gtx11_test1),
    .gtx11_test_data2    (gtx11_test2),
    .gtx11_test_data3    (gtx11_test3),
    .gtx11_test_data4    (gtx11_test4),
    .gtx11_test_data5    (gtx11_test5),
    .gtx11_test_data6    (gtx11_test6),
    .gtx11_test_data7    (gtx11_test7),

    .gtx0_phase_select  (gtx0_phase_select),
    .gtx1_phase_select  (gtx1_phase_select),
    .gtx2_phase_select  (gtx2_phase_select),
    .gtx3_phase_select  (gtx3_phase_select),
    .gtx4_phase_select  (gtx4_phase_select),
    .gtx5_phase_select  (gtx5_phase_select),
    .gtx6_phase_select  (gtx6_phase_select),
    .gtx7_phase_select  (gtx7_phase_select),
    .gtx8_phase_select  (gtx8_phase_select),
    .gtx9_phase_select  (gtx9_phase_select),
    .gtx10_phase_select  (gtx10_phase_select),
    .gtx11_phase_select  (gtx11_phase_select),

    .gtx0_phase_monitor_0 (gtx0_phase_monitor_0),
    .gtx1_phase_monitor_0 (gtx1_phase_monitor_0),
    .gtx2_phase_monitor_0 (gtx2_phase_monitor_0),
    .gtx3_phase_monitor_0 (gtx3_phase_monitor_0),
    .gtx4_phase_monitor_0 (gtx4_phase_monitor_0),
    .gtx5_phase_monitor_0 (gtx5_phase_monitor_0),
    .gtx6_phase_monitor_0 (gtx6_phase_monitor_0),
    .gtx7_phase_monitor_0 (gtx7_phase_monitor_0),
    .gtx8_phase_monitor_0 (gtx8_phase_monitor_0),
    .gtx9_phase_monitor_0 (gtx9_phase_monitor_0),
    .gtx10_phase_monitor_0 (gtx10_phase_monitor_0),
    .gtx11_phase_monitor_0 (gtx11_phase_monitor_0),

    .gtx0_phase_monitor_1 (gtx0_phase_monitor_1),
    .gtx1_phase_monitor_1 (gtx1_phase_monitor_1),
    .gtx2_phase_monitor_1 (gtx2_phase_monitor_1),
    .gtx3_phase_monitor_1 (gtx3_phase_monitor_1),
    .gtx4_phase_monitor_1 (gtx4_phase_monitor_1),
    .gtx5_phase_monitor_1 (gtx5_phase_monitor_1),
    .gtx6_phase_monitor_1 (gtx6_phase_monitor_1),
    .gtx7_phase_monitor_1 (gtx7_phase_monitor_1),
    .gtx8_phase_monitor_1 (gtx8_phase_monitor_1),
    .gtx9_phase_monitor_1 (gtx9_phase_monitor_1),
    .gtx10_phase_monitor_1 (gtx10_phase_monitor_1),
    .gtx11_phase_monitor_1 (gtx11_phase_monitor_1),

    .gtx0_phase_monitor_2 (gtx0_phase_monitor_2),
    .gtx1_phase_monitor_2 (gtx1_phase_monitor_2),
    .gtx2_phase_monitor_2 (gtx2_phase_monitor_2),
    .gtx3_phase_monitor_2 (gtx3_phase_monitor_2),
    .gtx4_phase_monitor_2 (gtx4_phase_monitor_2),
    .gtx5_phase_monitor_2 (gtx5_phase_monitor_2),
    .gtx6_phase_monitor_2 (gtx6_phase_monitor_2),
    .gtx7_phase_monitor_2 (gtx7_phase_monitor_2),
    .gtx8_phase_monitor_2 (gtx8_phase_monitor_2),
    .gtx9_phase_monitor_2 (gtx9_phase_monitor_2),
    .gtx10_phase_monitor_2 (gtx10_phase_monitor_2),
    .gtx11_phase_monitor_2 (gtx11_phase_monitor_2),

    .gtx0_phase_monitor_3 (gtx0_phase_monitor_3),
    .gtx1_phase_monitor_3 (gtx1_phase_monitor_3),
    .gtx2_phase_monitor_3 (gtx2_phase_monitor_3),
    .gtx3_phase_monitor_3 (gtx3_phase_monitor_3),
    .gtx4_phase_monitor_3 (gtx4_phase_monitor_3),
    .gtx5_phase_monitor_3 (gtx5_phase_monitor_3),
    .gtx6_phase_monitor_3 (gtx6_phase_monitor_3),
    .gtx7_phase_monitor_3 (gtx7_phase_monitor_3),
    .gtx8_phase_monitor_3 (gtx8_phase_monitor_3),
    .gtx9_phase_monitor_3 (gtx9_phase_monitor_3),
    .gtx10_phase_monitor_3 (gtx10_phase_monitor_3),
    .gtx11_phase_monitor_3 (gtx11_phase_monitor_3),
        
    .glink0_test_data1  (glink0_test1),
    .glink0_test_data2  (glink0_test2),
    .glink1_test_data1  (glink1_test1),
    .glink1_test_data2  (glink1_test2),
    .glink2_test_data1  (glink2_test1),
    .glink2_test_data2  (glink2_test2),
    .glink3_test_data1  (glink3_test1),
    .glink3_test_data2  (glink3_test2),
    .glink4_test_data1  (glink4_test1),
    .glink4_test_data2  (glink4_test2),
    .glink5_test_data1  (glink5_test1),
    .glink5_test_data2  (glink5_test2),
    .glink6_test_data1  (glink6_test1),
    .glink6_test_data2  (glink6_test2),
    .glink7_test_data1  (glink7_test1),
    .glink7_test_data2  (glink7_test2),
    .glink8_test_data1  (glink8_test1),
    .glink8_test_data2  (glink8_test2),
    .glink9_test_data1  (glink9_test1),
    .glink9_test_data2  (glink9_test2),
    .glink10_test_data1 (glink10_test1),
    .glink10_test_data2 (glink10_test2),
    .glink11_test_data1 (glink11_test1),
    .glink11_test_data2 (glink11_test2),
    .glink12_test_data1 (glink12_test1),
    .glink12_test_data2 (glink12_test2),
    .glink13_test_data1 (glink13_test1),
    .glink13_test_data2 (glink13_test2),

// LUT initialization
    .LUT_init_mode      (LUT_init_mode),
    .LUT_init_data      (LUT_init_data),
    .LUT_init_wr_en     (LUT_init_wr_en),
    .SectorID           (SectorID),
    .LUT_init_type      (LUT_init_type),
    .LUT_init_RoI       (LUT_init_RoI),
    .LUT_init_Address   (LUT_init_Address),
    .LUT_init_data_out  (LUT_init_data_out),
    .LUT_init_data_flg  (LUT_init_data_flg),
    .LUT_init_full      (LUT_init_full),    
    .LUT_init_state     (LUT_init_state),
    .LUT_init_valid     (LUT_init_valid),
    .LUT_rd_data        (LUT_rd_data),
    .LUT_rd_address     (LUT_rd_address),
    
    .Align_eta_NSW_0    (Align_eta_NSW_0),
    .Align_eta_NSW_1    (Align_eta_NSW_1),
    .Align_eta_NSW_2    (Align_eta_NSW_2),
    .Align_eta_NSW_3    (Align_eta_NSW_3),
    .Align_eta_NSW_4    (Align_eta_NSW_4),
    .Align_eta_NSW_5    (Align_eta_NSW_5),
    .Align_eta_RPC      (Align_eta_RPC),
    .Align_phi_NSW_0    (Align_phi_NSW_0),
    .Align_phi_NSW_1    (Align_phi_NSW_1),
    .Align_phi_NSW_2    (Align_phi_NSW_2),
    .Align_phi_NSW_3    (Align_phi_NSW_3),
    .Align_phi_NSW_4    (Align_phi_NSW_4),
    .Align_phi_NSW_5    (Align_phi_NSW_5),
    .Align_phi_RPC      (Align_phi_RPC),

    .TRACK0_SPARE_lane0_data0(TRACK0_SPARE_lane0[0]),
    .TRACK0_SPARE_lane0_data1(TRACK0_SPARE_lane0[1]),
    .TRACK0_SPARE_lane0_data2(TRACK0_SPARE_lane0[2]),
    .TRACK0_SPARE_lane0_data3(TRACK0_SPARE_lane0[3]),
    
    .TRACK0_SPARE_lane1_data0(TRACK0_SPARE_lane1[0]),
    .TRACK0_SPARE_lane1_data1(TRACK0_SPARE_lane1[1]),
    .TRACK0_SPARE_lane1_data2(TRACK0_SPARE_lane1[2]),
    .TRACK0_SPARE_lane1_data3(TRACK0_SPARE_lane1[3]),
    
    .TRACK0_SPARE_lane2_data0(TRACK0_SPARE_lane2[0]),
    .TRACK0_SPARE_lane2_data1(TRACK0_SPARE_lane2[1]),
    .TRACK0_SPARE_lane2_data2(TRACK0_SPARE_lane2[2]),
    .TRACK0_SPARE_lane2_data3(TRACK0_SPARE_lane2[3]),
    
    .TRACK0_SPARE_lane3_data0(TRACK0_SPARE_lane3[0]),
    .TRACK0_SPARE_lane3_data1(TRACK0_SPARE_lane3[1]),
    .TRACK0_SPARE_lane3_data2(TRACK0_SPARE_lane3[2]),
    .TRACK0_SPARE_lane3_data3(TRACK0_SPARE_lane3[3]),
    
    .TRACK0_SPARE_lane4_data0(TRACK0_SPARE_lane4[0]),
    .TRACK0_SPARE_lane4_data1(TRACK0_SPARE_lane4[1]),
    .TRACK0_SPARE_lane4_data2(TRACK0_SPARE_lane4[2]),
    .TRACK0_SPARE_lane4_data3(TRACK0_SPARE_lane4[3]),
    
    .TRACK0_SPARE_lane5_data0(TRACK0_SPARE_lane5[0]),
    .TRACK0_SPARE_lane5_data1(TRACK0_SPARE_lane5[1]),
    .TRACK0_SPARE_lane5_data2(TRACK0_SPARE_lane5[2]),
    .TRACK0_SPARE_lane5_data3(TRACK0_SPARE_lane5[3]),
    
    .TRACK0_SPARE_lane6_data0(TRACK0_SPARE_lane6[0]),
    .TRACK0_SPARE_lane6_data1(TRACK0_SPARE_lane6[1]),
    .TRACK0_SPARE_lane6_data2(TRACK0_SPARE_lane6[2]),
    .TRACK0_SPARE_lane6_data3(TRACK0_SPARE_lane6[3]),
    
    .TRACK0_sTGC_lane0_data0(TRACK0_sTGC_lane0[0]),
    .TRACK0_sTGC_lane0_data1(TRACK0_sTGC_lane0[1]),
    .TRACK0_sTGC_lane0_data2(TRACK0_sTGC_lane0[2]),
    .TRACK0_sTGC_lane0_data3(TRACK0_sTGC_lane0[3]),
    
    .TRACK0_sTGC_lane1_data0(TRACK0_sTGC_lane1[0]),
    .TRACK0_sTGC_lane1_data1(TRACK0_sTGC_lane1[1]),
    .TRACK0_sTGC_lane1_data2(TRACK0_sTGC_lane1[2]),
    .TRACK0_sTGC_lane1_data3(TRACK0_sTGC_lane1[3]),
    
    .TRACK0_sTGC_lane2_data0(TRACK0_sTGC_lane2[0]),
    .TRACK0_sTGC_lane2_data1(TRACK0_sTGC_lane2[1]),
    .TRACK0_sTGC_lane2_data2(TRACK0_sTGC_lane2[2]),
    .TRACK0_sTGC_lane2_data3(TRACK0_sTGC_lane2[3]),
    
    .TRACK0_sTGC_lane3_data0(TRACK0_sTGC_lane3[0]),
    .TRACK0_sTGC_lane3_data1(TRACK0_sTGC_lane3[1]),
    .TRACK0_sTGC_lane3_data2(TRACK0_sTGC_lane3[2]),
    .TRACK0_sTGC_lane3_data3(TRACK0_sTGC_lane3[3]),
    
    .TRACK0_sTGC_lane4_data0(TRACK0_sTGC_lane4[0]),
    .TRACK0_sTGC_lane4_data1(TRACK0_sTGC_lane4[1]),
    .TRACK0_sTGC_lane4_data2(TRACK0_sTGC_lane4[2]),
    .TRACK0_sTGC_lane4_data3(TRACK0_sTGC_lane4[3]),
    
    .TRACK0_sTGC_lane5_data0(TRACK0_sTGC_lane5[0]),
    .TRACK0_sTGC_lane5_data1(TRACK0_sTGC_lane5[1]),
    .TRACK0_sTGC_lane5_data2(TRACK0_sTGC_lane5[2]),
    .TRACK0_sTGC_lane5_data3(TRACK0_sTGC_lane5[3]),
    
    .TRACK0_sTGC_lane6_data0(TRACK0_sTGC_lane6[0]),
    .TRACK0_sTGC_lane6_data1(TRACK0_sTGC_lane6[1]),
    .TRACK0_sTGC_lane6_data2(TRACK0_sTGC_lane6[2]),
    .TRACK0_sTGC_lane6_data3(TRACK0_sTGC_lane6[3]),
    
    .TRACK0_MM_lane0_data0(TRACK0_MM_lane0[0]),
    .TRACK0_MM_lane0_data1(TRACK0_MM_lane0[1]),
    .TRACK0_MM_lane0_data2(TRACK0_MM_lane0[2]),
    .TRACK0_MM_lane0_data3(TRACK0_MM_lane0[3]),
    
    .TRACK0_MM_lane1_data0(TRACK0_MM_lane1[0]),
    .TRACK0_MM_lane1_data1(TRACK0_MM_lane1[1]),
    .TRACK0_MM_lane1_data2(TRACK0_MM_lane1[2]),
    .TRACK0_MM_lane1_data3(TRACK0_MM_lane1[3]),
    
    .TRACK0_MM_lane2_data0(TRACK0_MM_lane2[0]),
    .TRACK0_MM_lane2_data1(TRACK0_MM_lane2[1]),
    .TRACK0_MM_lane2_data2(TRACK0_MM_lane2[2]),
    .TRACK0_MM_lane2_data3(TRACK0_MM_lane2[3]),
    
    .TRACK0_MM_lane3_data0(TRACK0_MM_lane3[0]),
    .TRACK0_MM_lane3_data1(TRACK0_MM_lane3[1]),
    .TRACK0_MM_lane3_data2(TRACK0_MM_lane3[2]),
    .TRACK0_MM_lane3_data3(TRACK0_MM_lane3[3]),
    
    .TRACK0_MM_lane4_data0(TRACK0_MM_lane4[0]),
    .TRACK0_MM_lane4_data1(TRACK0_MM_lane4[1]),
    .TRACK0_MM_lane4_data2(TRACK0_MM_lane4[2]),
    .TRACK0_MM_lane4_data3(TRACK0_MM_lane4[3]),
    
    .TRACK0_MM_lane5_data0(TRACK0_MM_lane5[0]),
    .TRACK0_MM_lane5_data1(TRACK0_MM_lane5[1]),
    .TRACK0_MM_lane5_data2(TRACK0_MM_lane5[2]),
    .TRACK0_MM_lane5_data3(TRACK0_MM_lane5[3]),
    
    .TRACK0_MM_lane6_data0(TRACK0_MM_lane6[0]),
    .TRACK0_MM_lane6_data1(TRACK0_MM_lane6[1]),
    .TRACK0_MM_lane6_data2(TRACK0_MM_lane6[2]),
    .TRACK0_MM_lane6_data3(TRACK0_MM_lane6[3]),
    
    .TRACK0_DTHETA_lane0_data0(TRACK0_DTHETA_lane0[0]),
    .TRACK0_DTHETA_lane0_data1(TRACK0_DTHETA_lane0[1]),
    .TRACK0_DTHETA_lane0_data2(TRACK0_DTHETA_lane0[2]),
    .TRACK0_DTHETA_lane0_data3(TRACK0_DTHETA_lane0[3]),
    
    .TRACK0_DTHETA_lane1_data0(TRACK0_DTHETA_lane1[0]),
    .TRACK0_DTHETA_lane1_data1(TRACK0_DTHETA_lane1[1]),
    .TRACK0_DTHETA_lane1_data2(TRACK0_DTHETA_lane1[2]),
    .TRACK0_DTHETA_lane1_data3(TRACK0_DTHETA_lane1[3]),
    
    .TRACK0_DTHETA_lane2_data0(TRACK0_DTHETA_lane2[0]),
    .TRACK0_DTHETA_lane2_data1(TRACK0_DTHETA_lane2[1]),
    .TRACK0_DTHETA_lane2_data2(TRACK0_DTHETA_lane2[2]),
    .TRACK0_DTHETA_lane2_data3(TRACK0_DTHETA_lane2[3]),
    
    .TRACK0_DTHETA_lane3_data0(TRACK0_DTHETA_lane3[0]),
    .TRACK0_DTHETA_lane3_data1(TRACK0_DTHETA_lane3[1]),
    .TRACK0_DTHETA_lane3_data2(TRACK0_DTHETA_lane3[2]),
    .TRACK0_DTHETA_lane3_data3(TRACK0_DTHETA_lane3[3]),
    
    .TRACK0_DTHETA_lane4_data0(TRACK0_DTHETA_lane4[0]),
    .TRACK0_DTHETA_lane4_data1(TRACK0_DTHETA_lane4[1]),
    .TRACK0_DTHETA_lane4_data2(TRACK0_DTHETA_lane4[2]),
    .TRACK0_DTHETA_lane4_data3(TRACK0_DTHETA_lane4[3]),
    
    .TRACK0_DTHETA_lane5_data0(TRACK0_DTHETA_lane5[0]),
    .TRACK0_DTHETA_lane5_data1(TRACK0_DTHETA_lane5[1]),
    .TRACK0_DTHETA_lane5_data2(TRACK0_DTHETA_lane5[2]),
    .TRACK0_DTHETA_lane5_data3(TRACK0_DTHETA_lane5[3]),
    
    .TRACK0_DTHETA_lane6_data0(TRACK0_DTHETA_lane6[0]),
    .TRACK0_DTHETA_lane6_data1(TRACK0_DTHETA_lane6[1]),
    .TRACK0_DTHETA_lane6_data2(TRACK0_DTHETA_lane6[2]),
    .TRACK0_DTHETA_lane6_data3(TRACK0_DTHETA_lane6[3]),
    
    .TRACK0_PHI_lane0_data0(TRACK0_PHI_lane0[0]),
    .TRACK0_PHI_lane0_data1(TRACK0_PHI_lane0[1]),
    .TRACK0_PHI_lane0_data2(TRACK0_PHI_lane0[2]),
    .TRACK0_PHI_lane0_data3(TRACK0_PHI_lane0[3]),
    
    .TRACK0_PHI_lane1_data0(TRACK0_PHI_lane1[0]),
    .TRACK0_PHI_lane1_data1(TRACK0_PHI_lane1[1]),
    .TRACK0_PHI_lane1_data2(TRACK0_PHI_lane1[2]),
    .TRACK0_PHI_lane1_data3(TRACK0_PHI_lane1[3]),
    
    .TRACK0_PHI_lane2_data0(TRACK0_PHI_lane2[0]),
    .TRACK0_PHI_lane2_data1(TRACK0_PHI_lane2[1]),
    .TRACK0_PHI_lane2_data2(TRACK0_PHI_lane2[2]),
    .TRACK0_PHI_lane2_data3(TRACK0_PHI_lane2[3]),
    
    .TRACK0_PHI_lane3_data0(TRACK0_PHI_lane3[0]),
    .TRACK0_PHI_lane3_data1(TRACK0_PHI_lane3[1]),
    .TRACK0_PHI_lane3_data2(TRACK0_PHI_lane3[2]),
    .TRACK0_PHI_lane3_data3(TRACK0_PHI_lane3[3]),
    
    .TRACK0_PHI_lane4_data0(TRACK0_PHI_lane4[0]),
    .TRACK0_PHI_lane4_data1(TRACK0_PHI_lane4[1]),
    .TRACK0_PHI_lane4_data2(TRACK0_PHI_lane4[2]),
    .TRACK0_PHI_lane4_data3(TRACK0_PHI_lane4[3]),
    
    .TRACK0_PHI_lane5_data0(TRACK0_PHI_lane5[0]),
    .TRACK0_PHI_lane5_data1(TRACK0_PHI_lane5[1]),
    .TRACK0_PHI_lane5_data2(TRACK0_PHI_lane5[2]),
    .TRACK0_PHI_lane5_data3(TRACK0_PHI_lane5[3]),
    
    .TRACK0_PHI_lane6_data0(TRACK0_PHI_lane6[0]),
    .TRACK0_PHI_lane6_data1(TRACK0_PHI_lane6[1]),
    .TRACK0_PHI_lane6_data2(TRACK0_PHI_lane6[2]),
    .TRACK0_PHI_lane6_data3(TRACK0_PHI_lane6[3]),
    
    .TRACK0_ETA_lane0_data0(TRACK0_ETA_lane0[0]),
    .TRACK0_ETA_lane0_data1(TRACK0_ETA_lane0[1]),
    .TRACK0_ETA_lane0_data2(TRACK0_ETA_lane0[2]),
    .TRACK0_ETA_lane0_data3(TRACK0_ETA_lane0[3]),
    
    .TRACK0_ETA_lane1_data0(TRACK0_ETA_lane1[0]),
    .TRACK0_ETA_lane1_data1(TRACK0_ETA_lane1[1]),
    .TRACK0_ETA_lane1_data2(TRACK0_ETA_lane1[2]),
    .TRACK0_ETA_lane1_data3(TRACK0_ETA_lane1[3]),
    
    .TRACK0_ETA_lane2_data0(TRACK0_ETA_lane2[0]),
    .TRACK0_ETA_lane2_data1(TRACK0_ETA_lane2[1]),
    .TRACK0_ETA_lane2_data2(TRACK0_ETA_lane2[2]),
    .TRACK0_ETA_lane2_data3(TRACK0_ETA_lane2[3]),
    
    .TRACK0_ETA_lane3_data0(TRACK0_ETA_lane3[0]),
    .TRACK0_ETA_lane3_data1(TRACK0_ETA_lane3[1]),
    .TRACK0_ETA_lane3_data2(TRACK0_ETA_lane3[2]),
    .TRACK0_ETA_lane3_data3(TRACK0_ETA_lane3[3]),
    
    .TRACK0_ETA_lane4_data0(TRACK0_ETA_lane4[0]),
    .TRACK0_ETA_lane4_data1(TRACK0_ETA_lane4[1]),
    .TRACK0_ETA_lane4_data2(TRACK0_ETA_lane4[2]),
    .TRACK0_ETA_lane4_data3(TRACK0_ETA_lane4[3]),
    
    .TRACK0_ETA_lane5_data0(TRACK0_ETA_lane5[0]),
    .TRACK0_ETA_lane5_data1(TRACK0_ETA_lane5[1]),
    .TRACK0_ETA_lane5_data2(TRACK0_ETA_lane5[2]),
    .TRACK0_ETA_lane5_data3(TRACK0_ETA_lane5[3]),
    
    .TRACK0_ETA_lane6_data0(TRACK0_ETA_lane6[0]),
    .TRACK0_ETA_lane6_data1(TRACK0_ETA_lane6[1]),
    .TRACK0_ETA_lane6_data2(TRACK0_ETA_lane6[2]),
    .TRACK0_ETA_lane6_data3(TRACK0_ETA_lane6[3]),
    
    .TRACK1_SPARE_lane0_data0(TRACK1_SPARE_lane0[0]),
    .TRACK1_SPARE_lane0_data1(TRACK1_SPARE_lane0[1]),
    .TRACK1_SPARE_lane0_data2(TRACK1_SPARE_lane0[2]),
    .TRACK1_SPARE_lane0_data3(TRACK1_SPARE_lane0[3]),
    
    .TRACK1_SPARE_lane1_data0(TRACK1_SPARE_lane1[0]),
    .TRACK1_SPARE_lane1_data1(TRACK1_SPARE_lane1[1]),
    .TRACK1_SPARE_lane1_data2(TRACK1_SPARE_lane1[2]),
    .TRACK1_SPARE_lane1_data3(TRACK1_SPARE_lane1[3]),
    
    .TRACK1_SPARE_lane2_data0(TRACK1_SPARE_lane2[0]),
    .TRACK1_SPARE_lane2_data1(TRACK1_SPARE_lane2[1]),
    .TRACK1_SPARE_lane2_data2(TRACK1_SPARE_lane2[2]),
    .TRACK1_SPARE_lane2_data3(TRACK1_SPARE_lane2[3]),
    
    .TRACK1_SPARE_lane3_data0(TRACK1_SPARE_lane3[0]),
    .TRACK1_SPARE_lane3_data1(TRACK1_SPARE_lane3[1]),
    .TRACK1_SPARE_lane3_data2(TRACK1_SPARE_lane3[2]),
    .TRACK1_SPARE_lane3_data3(TRACK1_SPARE_lane3[3]),
    
    .TRACK1_SPARE_lane4_data0(TRACK1_SPARE_lane4[0]),
    .TRACK1_SPARE_lane4_data1(TRACK1_SPARE_lane4[1]),
    .TRACK1_SPARE_lane4_data2(TRACK1_SPARE_lane4[2]),
    .TRACK1_SPARE_lane4_data3(TRACK1_SPARE_lane4[3]),
    
    .TRACK1_SPARE_lane5_data0(TRACK1_SPARE_lane5[0]),
    .TRACK1_SPARE_lane5_data1(TRACK1_SPARE_lane5[1]),
    .TRACK1_SPARE_lane5_data2(TRACK1_SPARE_lane5[2]),
    .TRACK1_SPARE_lane5_data3(TRACK1_SPARE_lane5[3]),
    
    .TRACK1_SPARE_lane6_data0(TRACK1_SPARE_lane6[0]),
    .TRACK1_SPARE_lane6_data1(TRACK1_SPARE_lane6[1]),
    .TRACK1_SPARE_lane6_data2(TRACK1_SPARE_lane6[2]),
    .TRACK1_SPARE_lane6_data3(TRACK1_SPARE_lane6[3]),
    
    .TRACK1_sTGC_lane0_data0(TRACK1_sTGC_lane0[0]),
    .TRACK1_sTGC_lane0_data1(TRACK1_sTGC_lane0[1]),
    .TRACK1_sTGC_lane0_data2(TRACK1_sTGC_lane0[2]),
    .TRACK1_sTGC_lane0_data3(TRACK1_sTGC_lane0[3]),
    
    .TRACK1_sTGC_lane1_data0(TRACK1_sTGC_lane1[0]),
    .TRACK1_sTGC_lane1_data1(TRACK1_sTGC_lane1[1]),
    .TRACK1_sTGC_lane1_data2(TRACK1_sTGC_lane1[2]),
    .TRACK1_sTGC_lane1_data3(TRACK1_sTGC_lane1[3]),
    
    .TRACK1_sTGC_lane2_data0(TRACK1_sTGC_lane2[0]),
    .TRACK1_sTGC_lane2_data1(TRACK1_sTGC_lane2[1]),
    .TRACK1_sTGC_lane2_data2(TRACK1_sTGC_lane2[2]),
    .TRACK1_sTGC_lane2_data3(TRACK1_sTGC_lane2[3]),
    
    .TRACK1_sTGC_lane3_data0(TRACK1_sTGC_lane3[0]),
    .TRACK1_sTGC_lane3_data1(TRACK1_sTGC_lane3[1]),
    .TRACK1_sTGC_lane3_data2(TRACK1_sTGC_lane3[2]),
    .TRACK1_sTGC_lane3_data3(TRACK1_sTGC_lane3[3]),
    
    .TRACK1_sTGC_lane4_data0(TRACK1_sTGC_lane4[0]),
    .TRACK1_sTGC_lane4_data1(TRACK1_sTGC_lane4[1]),
    .TRACK1_sTGC_lane4_data2(TRACK1_sTGC_lane4[2]),
    .TRACK1_sTGC_lane4_data3(TRACK1_sTGC_lane4[3]),
    
    .TRACK1_sTGC_lane5_data0(TRACK1_sTGC_lane5[0]),
    .TRACK1_sTGC_lane5_data1(TRACK1_sTGC_lane5[1]),
    .TRACK1_sTGC_lane5_data2(TRACK1_sTGC_lane5[2]),
    .TRACK1_sTGC_lane5_data3(TRACK1_sTGC_lane5[3]),
    
    .TRACK1_sTGC_lane6_data0(TRACK1_sTGC_lane6[0]),
    .TRACK1_sTGC_lane6_data1(TRACK1_sTGC_lane6[1]),
    .TRACK1_sTGC_lane6_data2(TRACK1_sTGC_lane6[2]),
    .TRACK1_sTGC_lane6_data3(TRACK1_sTGC_lane6[3]),
    
    .TRACK1_MM_lane0_data0(TRACK1_MM_lane0[0]),
    .TRACK1_MM_lane0_data1(TRACK1_MM_lane0[1]),
    .TRACK1_MM_lane0_data2(TRACK1_MM_lane0[2]),
    .TRACK1_MM_lane0_data3(TRACK1_MM_lane0[3]),
    
    .TRACK1_MM_lane1_data0(TRACK1_MM_lane1[0]),
    .TRACK1_MM_lane1_data1(TRACK1_MM_lane1[1]),
    .TRACK1_MM_lane1_data2(TRACK1_MM_lane1[2]),
    .TRACK1_MM_lane1_data3(TRACK1_MM_lane1[3]),
    
    .TRACK1_MM_lane2_data0(TRACK1_MM_lane2[0]),
    .TRACK1_MM_lane2_data1(TRACK1_MM_lane2[1]),
    .TRACK1_MM_lane2_data2(TRACK1_MM_lane2[2]),
    .TRACK1_MM_lane2_data3(TRACK1_MM_lane2[3]),
    
    .TRACK1_MM_lane3_data0(TRACK1_MM_lane3[0]),
    .TRACK1_MM_lane3_data1(TRACK1_MM_lane3[1]),
    .TRACK1_MM_lane3_data2(TRACK1_MM_lane3[2]),
    .TRACK1_MM_lane3_data3(TRACK1_MM_lane3[3]),
    
    .TRACK1_MM_lane4_data0(TRACK1_MM_lane4[0]),
    .TRACK1_MM_lane4_data1(TRACK1_MM_lane4[1]),
    .TRACK1_MM_lane4_data2(TRACK1_MM_lane4[2]),
    .TRACK1_MM_lane4_data3(TRACK1_MM_lane4[3]),
    
    .TRACK1_MM_lane5_data0(TRACK1_MM_lane5[0]),
    .TRACK1_MM_lane5_data1(TRACK1_MM_lane5[1]),
    .TRACK1_MM_lane5_data2(TRACK1_MM_lane5[2]),
    .TRACK1_MM_lane5_data3(TRACK1_MM_lane5[3]),
    
    .TRACK1_MM_lane6_data0(TRACK1_MM_lane6[0]),
    .TRACK1_MM_lane6_data1(TRACK1_MM_lane6[1]),
    .TRACK1_MM_lane6_data2(TRACK1_MM_lane6[2]),
    .TRACK1_MM_lane6_data3(TRACK1_MM_lane6[3]),
    
    .TRACK1_DTHETA_lane0_data0(TRACK1_DTHETA_lane0[0]),
    .TRACK1_DTHETA_lane0_data1(TRACK1_DTHETA_lane0[1]),
    .TRACK1_DTHETA_lane0_data2(TRACK1_DTHETA_lane0[2]),
    .TRACK1_DTHETA_lane0_data3(TRACK1_DTHETA_lane0[3]),
    
    .TRACK1_DTHETA_lane1_data0(TRACK1_DTHETA_lane1[0]),
    .TRACK1_DTHETA_lane1_data1(TRACK1_DTHETA_lane1[1]),
    .TRACK1_DTHETA_lane1_data2(TRACK1_DTHETA_lane1[2]),
    .TRACK1_DTHETA_lane1_data3(TRACK1_DTHETA_lane1[3]),
    
    .TRACK1_DTHETA_lane2_data0(TRACK1_DTHETA_lane2[0]),
    .TRACK1_DTHETA_lane2_data1(TRACK1_DTHETA_lane2[1]),
    .TRACK1_DTHETA_lane2_data2(TRACK1_DTHETA_lane2[2]),
    .TRACK1_DTHETA_lane2_data3(TRACK1_DTHETA_lane2[3]),
    
    .TRACK1_DTHETA_lane3_data0(TRACK1_DTHETA_lane3[0]),
    .TRACK1_DTHETA_lane3_data1(TRACK1_DTHETA_lane3[1]),
    .TRACK1_DTHETA_lane3_data2(TRACK1_DTHETA_lane3[2]),
    .TRACK1_DTHETA_lane3_data3(TRACK1_DTHETA_lane3[3]),
    
    .TRACK1_DTHETA_lane4_data0(TRACK1_DTHETA_lane4[0]),
    .TRACK1_DTHETA_lane4_data1(TRACK1_DTHETA_lane4[1]),
    .TRACK1_DTHETA_lane4_data2(TRACK1_DTHETA_lane4[2]),
    .TRACK1_DTHETA_lane4_data3(TRACK1_DTHETA_lane4[3]),
    
    .TRACK1_DTHETA_lane5_data0(TRACK1_DTHETA_lane5[0]),
    .TRACK1_DTHETA_lane5_data1(TRACK1_DTHETA_lane5[1]),
    .TRACK1_DTHETA_lane5_data2(TRACK1_DTHETA_lane5[2]),
    .TRACK1_DTHETA_lane5_data3(TRACK1_DTHETA_lane5[3]),
    
    .TRACK1_DTHETA_lane6_data0(TRACK1_DTHETA_lane6[0]),
    .TRACK1_DTHETA_lane6_data1(TRACK1_DTHETA_lane6[1]),
    .TRACK1_DTHETA_lane6_data2(TRACK1_DTHETA_lane6[2]),
    .TRACK1_DTHETA_lane6_data3(TRACK1_DTHETA_lane6[3]),
    
    .TRACK1_PHI_lane0_data0(TRACK1_PHI_lane0[0]),
    .TRACK1_PHI_lane0_data1(TRACK1_PHI_lane0[1]),
    .TRACK1_PHI_lane0_data2(TRACK1_PHI_lane0[2]),
    .TRACK1_PHI_lane0_data3(TRACK1_PHI_lane0[3]),
    
    .TRACK1_PHI_lane1_data0(TRACK1_PHI_lane1[0]),
    .TRACK1_PHI_lane1_data1(TRACK1_PHI_lane1[1]),
    .TRACK1_PHI_lane1_data2(TRACK1_PHI_lane1[2]),
    .TRACK1_PHI_lane1_data3(TRACK1_PHI_lane1[3]),
    
    .TRACK1_PHI_lane2_data0(TRACK1_PHI_lane2[0]),
    .TRACK1_PHI_lane2_data1(TRACK1_PHI_lane2[1]),
    .TRACK1_PHI_lane2_data2(TRACK1_PHI_lane2[2]),
    .TRACK1_PHI_lane2_data3(TRACK1_PHI_lane2[3]),
    
    .TRACK1_PHI_lane3_data0(TRACK1_PHI_lane3[0]),
    .TRACK1_PHI_lane3_data1(TRACK1_PHI_lane3[1]),
    .TRACK1_PHI_lane3_data2(TRACK1_PHI_lane3[2]),
    .TRACK1_PHI_lane3_data3(TRACK1_PHI_lane3[3]),
    
    .TRACK1_PHI_lane4_data0(TRACK1_PHI_lane4[0]),
    .TRACK1_PHI_lane4_data1(TRACK1_PHI_lane4[1]),
    .TRACK1_PHI_lane4_data2(TRACK1_PHI_lane4[2]),
    .TRACK1_PHI_lane4_data3(TRACK1_PHI_lane4[3]),
    
    .TRACK1_PHI_lane5_data0(TRACK1_PHI_lane5[0]),
    .TRACK1_PHI_lane5_data1(TRACK1_PHI_lane5[1]),
    .TRACK1_PHI_lane5_data2(TRACK1_PHI_lane5[2]),
    .TRACK1_PHI_lane5_data3(TRACK1_PHI_lane5[3]),
    
    .TRACK1_PHI_lane6_data0(TRACK1_PHI_lane6[0]),
    .TRACK1_PHI_lane6_data1(TRACK1_PHI_lane6[1]),
    .TRACK1_PHI_lane6_data2(TRACK1_PHI_lane6[2]),
    .TRACK1_PHI_lane6_data3(TRACK1_PHI_lane6[3]),
    
    .TRACK1_ETA_lane0_data0(TRACK1_ETA_lane0[0]),
    .TRACK1_ETA_lane0_data1(TRACK1_ETA_lane0[1]),
    .TRACK1_ETA_lane0_data2(TRACK1_ETA_lane0[2]),
    .TRACK1_ETA_lane0_data3(TRACK1_ETA_lane0[3]),
    
    .TRACK1_ETA_lane1_data0(TRACK1_ETA_lane1[0]),
    .TRACK1_ETA_lane1_data1(TRACK1_ETA_lane1[1]),
    .TRACK1_ETA_lane1_data2(TRACK1_ETA_lane1[2]),
    .TRACK1_ETA_lane1_data3(TRACK1_ETA_lane1[3]),
    
    .TRACK1_ETA_lane2_data0(TRACK1_ETA_lane2[0]),
    .TRACK1_ETA_lane2_data1(TRACK1_ETA_lane2[1]),
    .TRACK1_ETA_lane2_data2(TRACK1_ETA_lane2[2]),
    .TRACK1_ETA_lane2_data3(TRACK1_ETA_lane2[3]),
    
    .TRACK1_ETA_lane3_data0(TRACK1_ETA_lane3[0]),
    .TRACK1_ETA_lane3_data1(TRACK1_ETA_lane3[1]),
    .TRACK1_ETA_lane3_data2(TRACK1_ETA_lane3[2]),
    .TRACK1_ETA_lane3_data3(TRACK1_ETA_lane3[3]),
    
    .TRACK1_ETA_lane4_data0(TRACK1_ETA_lane4[0]),
    .TRACK1_ETA_lane4_data1(TRACK1_ETA_lane4[1]),
    .TRACK1_ETA_lane4_data2(TRACK1_ETA_lane4[2]),
    .TRACK1_ETA_lane4_data3(TRACK1_ETA_lane4[3]),
    
    .TRACK1_ETA_lane5_data0(TRACK1_ETA_lane5[0]),
    .TRACK1_ETA_lane5_data1(TRACK1_ETA_lane5[1]),
    .TRACK1_ETA_lane5_data2(TRACK1_ETA_lane5[2]),
    .TRACK1_ETA_lane5_data3(TRACK1_ETA_lane5[3]),
    
    .TRACK1_ETA_lane6_data0(TRACK1_ETA_lane6[0]),
    .TRACK1_ETA_lane6_data1(TRACK1_ETA_lane6[1]),
    .TRACK1_ETA_lane6_data2(TRACK1_ETA_lane6[2]),
    .TRACK1_ETA_lane6_data3(TRACK1_ETA_lane6[3]),
    
    .TRACK2_SPARE_lane0_data0(TRACK2_SPARE_lane0[0]),
    .TRACK2_SPARE_lane0_data1(TRACK2_SPARE_lane0[1]),
    .TRACK2_SPARE_lane0_data2(TRACK2_SPARE_lane0[2]),
    .TRACK2_SPARE_lane0_data3(TRACK2_SPARE_lane0[3]),
    
    .TRACK2_SPARE_lane1_data0(TRACK2_SPARE_lane1[0]),
    .TRACK2_SPARE_lane1_data1(TRACK2_SPARE_lane1[1]),
    .TRACK2_SPARE_lane1_data2(TRACK2_SPARE_lane1[2]),
    .TRACK2_SPARE_lane1_data3(TRACK2_SPARE_lane1[3]),
    
    .TRACK2_SPARE_lane2_data0(TRACK2_SPARE_lane2[0]),
    .TRACK2_SPARE_lane2_data1(TRACK2_SPARE_lane2[1]),
    .TRACK2_SPARE_lane2_data2(TRACK2_SPARE_lane2[2]),
    .TRACK2_SPARE_lane2_data3(TRACK2_SPARE_lane2[3]),
    
    .TRACK2_SPARE_lane3_data0(TRACK2_SPARE_lane3[0]),
    .TRACK2_SPARE_lane3_data1(TRACK2_SPARE_lane3[1]),
    .TRACK2_SPARE_lane3_data2(TRACK2_SPARE_lane3[2]),
    .TRACK2_SPARE_lane3_data3(TRACK2_SPARE_lane3[3]),
    
    .TRACK2_SPARE_lane4_data0(TRACK2_SPARE_lane4[0]),
    .TRACK2_SPARE_lane4_data1(TRACK2_SPARE_lane4[1]),
    .TRACK2_SPARE_lane4_data2(TRACK2_SPARE_lane4[2]),
    .TRACK2_SPARE_lane4_data3(TRACK2_SPARE_lane4[3]),
    
    .TRACK2_SPARE_lane5_data0(TRACK2_SPARE_lane5[0]),
    .TRACK2_SPARE_lane5_data1(TRACK2_SPARE_lane5[1]),
    .TRACK2_SPARE_lane5_data2(TRACK2_SPARE_lane5[2]),
    .TRACK2_SPARE_lane5_data3(TRACK2_SPARE_lane5[3]),
    
    .TRACK2_SPARE_lane6_data0(TRACK2_SPARE_lane6[0]),
    .TRACK2_SPARE_lane6_data1(TRACK2_SPARE_lane6[1]),
    .TRACK2_SPARE_lane6_data2(TRACK2_SPARE_lane6[2]),
    .TRACK2_SPARE_lane6_data3(TRACK2_SPARE_lane6[3]),
    
    .TRACK2_sTGC_lane0_data0(TRACK2_sTGC_lane0[0]),
    .TRACK2_sTGC_lane0_data1(TRACK2_sTGC_lane0[1]),
    .TRACK2_sTGC_lane0_data2(TRACK2_sTGC_lane0[2]),
    .TRACK2_sTGC_lane0_data3(TRACK2_sTGC_lane0[3]),
    
    .TRACK2_sTGC_lane1_data0(TRACK2_sTGC_lane1[0]),
    .TRACK2_sTGC_lane1_data1(TRACK2_sTGC_lane1[1]),
    .TRACK2_sTGC_lane1_data2(TRACK2_sTGC_lane1[2]),
    .TRACK2_sTGC_lane1_data3(TRACK2_sTGC_lane1[3]),
    
    .TRACK2_sTGC_lane2_data0(TRACK2_sTGC_lane2[0]),
    .TRACK2_sTGC_lane2_data1(TRACK2_sTGC_lane2[1]),
    .TRACK2_sTGC_lane2_data2(TRACK2_sTGC_lane2[2]),
    .TRACK2_sTGC_lane2_data3(TRACK2_sTGC_lane2[3]),
    
    .TRACK2_sTGC_lane3_data0(TRACK2_sTGC_lane3[0]),
    .TRACK2_sTGC_lane3_data1(TRACK2_sTGC_lane3[1]),
    .TRACK2_sTGC_lane3_data2(TRACK2_sTGC_lane3[2]),
    .TRACK2_sTGC_lane3_data3(TRACK2_sTGC_lane3[3]),
    
    .TRACK2_sTGC_lane4_data0(TRACK2_sTGC_lane4[0]),
    .TRACK2_sTGC_lane4_data1(TRACK2_sTGC_lane4[1]),
    .TRACK2_sTGC_lane4_data2(TRACK2_sTGC_lane4[2]),
    .TRACK2_sTGC_lane4_data3(TRACK2_sTGC_lane4[3]),
    
    .TRACK2_sTGC_lane5_data0(TRACK2_sTGC_lane5[0]),
    .TRACK2_sTGC_lane5_data1(TRACK2_sTGC_lane5[1]),
    .TRACK2_sTGC_lane5_data2(TRACK2_sTGC_lane5[2]),
    .TRACK2_sTGC_lane5_data3(TRACK2_sTGC_lane5[3]),
    
    .TRACK2_sTGC_lane6_data0(TRACK2_sTGC_lane6[0]),
    .TRACK2_sTGC_lane6_data1(TRACK2_sTGC_lane6[1]),
    .TRACK2_sTGC_lane6_data2(TRACK2_sTGC_lane6[2]),
    .TRACK2_sTGC_lane6_data3(TRACK2_sTGC_lane6[3]),
    
    .TRACK2_MM_lane0_data0(TRACK2_MM_lane0[0]),
    .TRACK2_MM_lane0_data1(TRACK2_MM_lane0[1]),
    .TRACK2_MM_lane0_data2(TRACK2_MM_lane0[2]),
    .TRACK2_MM_lane0_data3(TRACK2_MM_lane0[3]),
    
    .TRACK2_MM_lane1_data0(TRACK2_MM_lane1[0]),
    .TRACK2_MM_lane1_data1(TRACK2_MM_lane1[1]),
    .TRACK2_MM_lane1_data2(TRACK2_MM_lane1[2]),
    .TRACK2_MM_lane1_data3(TRACK2_MM_lane1[3]),
    
    .TRACK2_MM_lane2_data0(TRACK2_MM_lane2[0]),
    .TRACK2_MM_lane2_data1(TRACK2_MM_lane2[1]),
    .TRACK2_MM_lane2_data2(TRACK2_MM_lane2[2]),
    .TRACK2_MM_lane2_data3(TRACK2_MM_lane2[3]),
    
    .TRACK2_MM_lane3_data0(TRACK2_MM_lane3[0]),
    .TRACK2_MM_lane3_data1(TRACK2_MM_lane3[1]),
    .TRACK2_MM_lane3_data2(TRACK2_MM_lane3[2]),
    .TRACK2_MM_lane3_data3(TRACK2_MM_lane3[3]),
    
    .TRACK2_MM_lane4_data0(TRACK2_MM_lane4[0]),
    .TRACK2_MM_lane4_data1(TRACK2_MM_lane4[1]),
    .TRACK2_MM_lane4_data2(TRACK2_MM_lane4[2]),
    .TRACK2_MM_lane4_data3(TRACK2_MM_lane4[3]),
    
    .TRACK2_MM_lane5_data0(TRACK2_MM_lane5[0]),
    .TRACK2_MM_lane5_data1(TRACK2_MM_lane5[1]),
    .TRACK2_MM_lane5_data2(TRACK2_MM_lane5[2]),
    .TRACK2_MM_lane5_data3(TRACK2_MM_lane5[3]),
    
    .TRACK2_MM_lane6_data0(TRACK2_MM_lane6[0]),
    .TRACK2_MM_lane6_data1(TRACK2_MM_lane6[1]),
    .TRACK2_MM_lane6_data2(TRACK2_MM_lane6[2]),
    .TRACK2_MM_lane6_data3(TRACK2_MM_lane6[3]),
    
    .TRACK2_DTHETA_lane0_data0(TRACK2_DTHETA_lane0[0]),
    .TRACK2_DTHETA_lane0_data1(TRACK2_DTHETA_lane0[1]),
    .TRACK2_DTHETA_lane0_data2(TRACK2_DTHETA_lane0[2]),
    .TRACK2_DTHETA_lane0_data3(TRACK2_DTHETA_lane0[3]),
    
    .TRACK2_DTHETA_lane1_data0(TRACK2_DTHETA_lane1[0]),
    .TRACK2_DTHETA_lane1_data1(TRACK2_DTHETA_lane1[1]),
    .TRACK2_DTHETA_lane1_data2(TRACK2_DTHETA_lane1[2]),
    .TRACK2_DTHETA_lane1_data3(TRACK2_DTHETA_lane1[3]),
    
    .TRACK2_DTHETA_lane2_data0(TRACK2_DTHETA_lane2[0]),
    .TRACK2_DTHETA_lane2_data1(TRACK2_DTHETA_lane2[1]),
    .TRACK2_DTHETA_lane2_data2(TRACK2_DTHETA_lane2[2]),
    .TRACK2_DTHETA_lane2_data3(TRACK2_DTHETA_lane2[3]),
    
    .TRACK2_DTHETA_lane3_data0(TRACK2_DTHETA_lane3[0]),
    .TRACK2_DTHETA_lane3_data1(TRACK2_DTHETA_lane3[1]),
    .TRACK2_DTHETA_lane3_data2(TRACK2_DTHETA_lane3[2]),
    .TRACK2_DTHETA_lane3_data3(TRACK2_DTHETA_lane3[3]),
    
    .TRACK2_DTHETA_lane4_data0(TRACK2_DTHETA_lane4[0]),
    .TRACK2_DTHETA_lane4_data1(TRACK2_DTHETA_lane4[1]),
    .TRACK2_DTHETA_lane4_data2(TRACK2_DTHETA_lane4[2]),
    .TRACK2_DTHETA_lane4_data3(TRACK2_DTHETA_lane4[3]),
    
    .TRACK2_DTHETA_lane5_data0(TRACK2_DTHETA_lane5[0]),
    .TRACK2_DTHETA_lane5_data1(TRACK2_DTHETA_lane5[1]),
    .TRACK2_DTHETA_lane5_data2(TRACK2_DTHETA_lane5[2]),
    .TRACK2_DTHETA_lane5_data3(TRACK2_DTHETA_lane5[3]),
    
    .TRACK2_DTHETA_lane6_data0(TRACK2_DTHETA_lane6[0]),
    .TRACK2_DTHETA_lane6_data1(TRACK2_DTHETA_lane6[1]),
    .TRACK2_DTHETA_lane6_data2(TRACK2_DTHETA_lane6[2]),
    .TRACK2_DTHETA_lane6_data3(TRACK2_DTHETA_lane6[3]),
    
    .TRACK2_PHI_lane0_data0(TRACK2_PHI_lane0[0]),
    .TRACK2_PHI_lane0_data1(TRACK2_PHI_lane0[1]),
    .TRACK2_PHI_lane0_data2(TRACK2_PHI_lane0[2]),
    .TRACK2_PHI_lane0_data3(TRACK2_PHI_lane0[3]),
    
    .TRACK2_PHI_lane1_data0(TRACK2_PHI_lane1[0]),
    .TRACK2_PHI_lane1_data1(TRACK2_PHI_lane1[1]),
    .TRACK2_PHI_lane1_data2(TRACK2_PHI_lane1[2]),
    .TRACK2_PHI_lane1_data3(TRACK2_PHI_lane1[3]),
    
    .TRACK2_PHI_lane2_data0(TRACK2_PHI_lane2[0]),
    .TRACK2_PHI_lane2_data1(TRACK2_PHI_lane2[1]),
    .TRACK2_PHI_lane2_data2(TRACK2_PHI_lane2[2]),
    .TRACK2_PHI_lane2_data3(TRACK2_PHI_lane2[3]),
    
    .TRACK2_PHI_lane3_data0(TRACK2_PHI_lane3[0]),
    .TRACK2_PHI_lane3_data1(TRACK2_PHI_lane3[1]),
    .TRACK2_PHI_lane3_data2(TRACK2_PHI_lane3[2]),
    .TRACK2_PHI_lane3_data3(TRACK2_PHI_lane3[3]),
    
    .TRACK2_PHI_lane4_data0(TRACK2_PHI_lane4[0]),
    .TRACK2_PHI_lane4_data1(TRACK2_PHI_lane4[1]),
    .TRACK2_PHI_lane4_data2(TRACK2_PHI_lane4[2]),
    .TRACK2_PHI_lane4_data3(TRACK2_PHI_lane4[3]),
    
    .TRACK2_PHI_lane5_data0(TRACK2_PHI_lane5[0]),
    .TRACK2_PHI_lane5_data1(TRACK2_PHI_lane5[1]),
    .TRACK2_PHI_lane5_data2(TRACK2_PHI_lane5[2]),
    .TRACK2_PHI_lane5_data3(TRACK2_PHI_lane5[3]),
    
    .TRACK2_PHI_lane6_data0(TRACK2_PHI_lane6[0]),
    .TRACK2_PHI_lane6_data1(TRACK2_PHI_lane6[1]),
    .TRACK2_PHI_lane6_data2(TRACK2_PHI_lane6[2]),
    .TRACK2_PHI_lane6_data3(TRACK2_PHI_lane6[3]),
    
    .TRACK2_ETA_lane0_data0(TRACK2_ETA_lane0[0]),
    .TRACK2_ETA_lane0_data1(TRACK2_ETA_lane0[1]),
    .TRACK2_ETA_lane0_data2(TRACK2_ETA_lane0[2]),
    .TRACK2_ETA_lane0_data3(TRACK2_ETA_lane0[3]),
    
    .TRACK2_ETA_lane1_data0(TRACK2_ETA_lane1[0]),
    .TRACK2_ETA_lane1_data1(TRACK2_ETA_lane1[1]),
    .TRACK2_ETA_lane1_data2(TRACK2_ETA_lane1[2]),
    .TRACK2_ETA_lane1_data3(TRACK2_ETA_lane1[3]),
    
    .TRACK2_ETA_lane2_data0(TRACK2_ETA_lane2[0]),
    .TRACK2_ETA_lane2_data1(TRACK2_ETA_lane2[1]),
    .TRACK2_ETA_lane2_data2(TRACK2_ETA_lane2[2]),
    .TRACK2_ETA_lane2_data3(TRACK2_ETA_lane2[3]),
    
    .TRACK2_ETA_lane3_data0(TRACK2_ETA_lane3[0]),
    .TRACK2_ETA_lane3_data1(TRACK2_ETA_lane3[1]),
    .TRACK2_ETA_lane3_data2(TRACK2_ETA_lane3[2]),
    .TRACK2_ETA_lane3_data3(TRACK2_ETA_lane3[3]),
    
    .TRACK2_ETA_lane4_data0(TRACK2_ETA_lane4[0]),
    .TRACK2_ETA_lane4_data1(TRACK2_ETA_lane4[1]),
    .TRACK2_ETA_lane4_data2(TRACK2_ETA_lane4[2]),
    .TRACK2_ETA_lane4_data3(TRACK2_ETA_lane4[3]),
    
    .TRACK2_ETA_lane5_data0(TRACK2_ETA_lane5[0]),
    .TRACK2_ETA_lane5_data1(TRACK2_ETA_lane5[1]),
    .TRACK2_ETA_lane5_data2(TRACK2_ETA_lane5[2]),
    .TRACK2_ETA_lane5_data3(TRACK2_ETA_lane5[3]),
    
    .TRACK2_ETA_lane6_data0(TRACK2_ETA_lane6[0]),
    .TRACK2_ETA_lane6_data1(TRACK2_ETA_lane6[1]),
    .TRACK2_ETA_lane6_data2(TRACK2_ETA_lane6[2]),
    .TRACK2_ETA_lane6_data3(TRACK2_ETA_lane6[3]),
    
    .TRACK3_SPARE_lane0_data0(TRACK3_SPARE_lane0[0]),
    .TRACK3_SPARE_lane0_data1(TRACK3_SPARE_lane0[1]),
    .TRACK3_SPARE_lane0_data2(TRACK3_SPARE_lane0[2]),
    .TRACK3_SPARE_lane0_data3(TRACK3_SPARE_lane0[3]),
    
    .TRACK3_SPARE_lane1_data0(TRACK3_SPARE_lane1[0]),
    .TRACK3_SPARE_lane1_data1(TRACK3_SPARE_lane1[1]),
    .TRACK3_SPARE_lane1_data2(TRACK3_SPARE_lane1[2]),
    .TRACK3_SPARE_lane1_data3(TRACK3_SPARE_lane1[3]),
    
    .TRACK3_SPARE_lane2_data0(TRACK3_SPARE_lane2[0]),
    .TRACK3_SPARE_lane2_data1(TRACK3_SPARE_lane2[1]),
    .TRACK3_SPARE_lane2_data2(TRACK3_SPARE_lane2[2]),
    .TRACK3_SPARE_lane2_data3(TRACK3_SPARE_lane2[3]),
    
    .TRACK3_SPARE_lane3_data0(TRACK3_SPARE_lane3[0]),
    .TRACK3_SPARE_lane3_data1(TRACK3_SPARE_lane3[1]),
    .TRACK3_SPARE_lane3_data2(TRACK3_SPARE_lane3[2]),
    .TRACK3_SPARE_lane3_data3(TRACK3_SPARE_lane3[3]),
    
    .TRACK3_SPARE_lane4_data0(TRACK3_SPARE_lane4[0]),
    .TRACK3_SPARE_lane4_data1(TRACK3_SPARE_lane4[1]),
    .TRACK3_SPARE_lane4_data2(TRACK3_SPARE_lane4[2]),
    .TRACK3_SPARE_lane4_data3(TRACK3_SPARE_lane4[3]),
    
    .TRACK3_SPARE_lane5_data0(TRACK3_SPARE_lane5[0]),
    .TRACK3_SPARE_lane5_data1(TRACK3_SPARE_lane5[1]),
    .TRACK3_SPARE_lane5_data2(TRACK3_SPARE_lane5[2]),
    .TRACK3_SPARE_lane5_data3(TRACK3_SPARE_lane5[3]),
    
    .TRACK3_SPARE_lane6_data0(TRACK3_SPARE_lane6[0]),
    .TRACK3_SPARE_lane6_data1(TRACK3_SPARE_lane6[1]),
    .TRACK3_SPARE_lane6_data2(TRACK3_SPARE_lane6[2]),
    .TRACK3_SPARE_lane6_data3(TRACK3_SPARE_lane6[3]),
    
    .TRACK3_sTGC_lane0_data0(TRACK3_sTGC_lane0[0]),
    .TRACK3_sTGC_lane0_data1(TRACK3_sTGC_lane0[1]),
    .TRACK3_sTGC_lane0_data2(TRACK3_sTGC_lane0[2]),
    .TRACK3_sTGC_lane0_data3(TRACK3_sTGC_lane0[3]),
    
    .TRACK3_sTGC_lane1_data0(TRACK3_sTGC_lane1[0]),
    .TRACK3_sTGC_lane1_data1(TRACK3_sTGC_lane1[1]),
    .TRACK3_sTGC_lane1_data2(TRACK3_sTGC_lane1[2]),
    .TRACK3_sTGC_lane1_data3(TRACK3_sTGC_lane1[3]),
    
    .TRACK3_sTGC_lane2_data0(TRACK3_sTGC_lane2[0]),
    .TRACK3_sTGC_lane2_data1(TRACK3_sTGC_lane2[1]),
    .TRACK3_sTGC_lane2_data2(TRACK3_sTGC_lane2[2]),
    .TRACK3_sTGC_lane2_data3(TRACK3_sTGC_lane2[3]),
    
    .TRACK3_sTGC_lane3_data0(TRACK3_sTGC_lane3[0]),
    .TRACK3_sTGC_lane3_data1(TRACK3_sTGC_lane3[1]),
    .TRACK3_sTGC_lane3_data2(TRACK3_sTGC_lane3[2]),
    .TRACK3_sTGC_lane3_data3(TRACK3_sTGC_lane3[3]),
    
    .TRACK3_sTGC_lane4_data0(TRACK3_sTGC_lane4[0]),
    .TRACK3_sTGC_lane4_data1(TRACK3_sTGC_lane4[1]),
    .TRACK3_sTGC_lane4_data2(TRACK3_sTGC_lane4[2]),
    .TRACK3_sTGC_lane4_data3(TRACK3_sTGC_lane4[3]),
    
    .TRACK3_sTGC_lane5_data0(TRACK3_sTGC_lane5[0]),
    .TRACK3_sTGC_lane5_data1(TRACK3_sTGC_lane5[1]),
    .TRACK3_sTGC_lane5_data2(TRACK3_sTGC_lane5[2]),
    .TRACK3_sTGC_lane5_data3(TRACK3_sTGC_lane5[3]),
    
    .TRACK3_sTGC_lane6_data0(TRACK3_sTGC_lane6[0]),
    .TRACK3_sTGC_lane6_data1(TRACK3_sTGC_lane6[1]),
    .TRACK3_sTGC_lane6_data2(TRACK3_sTGC_lane6[2]),
    .TRACK3_sTGC_lane6_data3(TRACK3_sTGC_lane6[3]),
    
    .TRACK3_MM_lane0_data0(TRACK3_MM_lane0[0]),
    .TRACK3_MM_lane0_data1(TRACK3_MM_lane0[1]),
    .TRACK3_MM_lane0_data2(TRACK3_MM_lane0[2]),
    .TRACK3_MM_lane0_data3(TRACK3_MM_lane0[3]),
    
    .TRACK3_MM_lane1_data0(TRACK3_MM_lane1[0]),
    .TRACK3_MM_lane1_data1(TRACK3_MM_lane1[1]),
    .TRACK3_MM_lane1_data2(TRACK3_MM_lane1[2]),
    .TRACK3_MM_lane1_data3(TRACK3_MM_lane1[3]),
    
    .TRACK3_MM_lane2_data0(TRACK3_MM_lane2[0]),
    .TRACK3_MM_lane2_data1(TRACK3_MM_lane2[1]),
    .TRACK3_MM_lane2_data2(TRACK3_MM_lane2[2]),
    .TRACK3_MM_lane2_data3(TRACK3_MM_lane2[3]),
    
    .TRACK3_MM_lane3_data0(TRACK3_MM_lane3[0]),
    .TRACK3_MM_lane3_data1(TRACK3_MM_lane3[1]),
    .TRACK3_MM_lane3_data2(TRACK3_MM_lane3[2]),
    .TRACK3_MM_lane3_data3(TRACK3_MM_lane3[3]),
    
    .TRACK3_MM_lane4_data0(TRACK3_MM_lane4[0]),
    .TRACK3_MM_lane4_data1(TRACK3_MM_lane4[1]),
    .TRACK3_MM_lane4_data2(TRACK3_MM_lane4[2]),
    .TRACK3_MM_lane4_data3(TRACK3_MM_lane4[3]),
    
    .TRACK3_MM_lane5_data0(TRACK3_MM_lane5[0]),
    .TRACK3_MM_lane5_data1(TRACK3_MM_lane5[1]),
    .TRACK3_MM_lane5_data2(TRACK3_MM_lane5[2]),
    .TRACK3_MM_lane5_data3(TRACK3_MM_lane5[3]),
    
    .TRACK3_MM_lane6_data0(TRACK3_MM_lane6[0]),
    .TRACK3_MM_lane6_data1(TRACK3_MM_lane6[1]),
    .TRACK3_MM_lane6_data2(TRACK3_MM_lane6[2]),
    .TRACK3_MM_lane6_data3(TRACK3_MM_lane6[3]),
    
    .TRACK3_DTHETA_lane0_data0(TRACK3_DTHETA_lane0[0]),
    .TRACK3_DTHETA_lane0_data1(TRACK3_DTHETA_lane0[1]),
    .TRACK3_DTHETA_lane0_data2(TRACK3_DTHETA_lane0[2]),
    .TRACK3_DTHETA_lane0_data3(TRACK3_DTHETA_lane0[3]),
    
    .TRACK3_DTHETA_lane1_data0(TRACK3_DTHETA_lane1[0]),
    .TRACK3_DTHETA_lane1_data1(TRACK3_DTHETA_lane1[1]),
    .TRACK3_DTHETA_lane1_data2(TRACK3_DTHETA_lane1[2]),
    .TRACK3_DTHETA_lane1_data3(TRACK3_DTHETA_lane1[3]),
    
    .TRACK3_DTHETA_lane2_data0(TRACK3_DTHETA_lane2[0]),
    .TRACK3_DTHETA_lane2_data1(TRACK3_DTHETA_lane2[1]),
    .TRACK3_DTHETA_lane2_data2(TRACK3_DTHETA_lane2[2]),
    .TRACK3_DTHETA_lane2_data3(TRACK3_DTHETA_lane2[3]),
    
    .TRACK3_DTHETA_lane3_data0(TRACK3_DTHETA_lane3[0]),
    .TRACK3_DTHETA_lane3_data1(TRACK3_DTHETA_lane3[1]),
    .TRACK3_DTHETA_lane3_data2(TRACK3_DTHETA_lane3[2]),
    .TRACK3_DTHETA_lane3_data3(TRACK3_DTHETA_lane3[3]),
    
    .TRACK3_DTHETA_lane4_data0(TRACK3_DTHETA_lane4[0]),
    .TRACK3_DTHETA_lane4_data1(TRACK3_DTHETA_lane4[1]),
    .TRACK3_DTHETA_lane4_data2(TRACK3_DTHETA_lane4[2]),
    .TRACK3_DTHETA_lane4_data3(TRACK3_DTHETA_lane4[3]),
    
    .TRACK3_DTHETA_lane5_data0(TRACK3_DTHETA_lane5[0]),
    .TRACK3_DTHETA_lane5_data1(TRACK3_DTHETA_lane5[1]),
    .TRACK3_DTHETA_lane5_data2(TRACK3_DTHETA_lane5[2]),
    .TRACK3_DTHETA_lane5_data3(TRACK3_DTHETA_lane5[3]),
    
    .TRACK3_DTHETA_lane6_data0(TRACK3_DTHETA_lane6[0]),
    .TRACK3_DTHETA_lane6_data1(TRACK3_DTHETA_lane6[1]),
    .TRACK3_DTHETA_lane6_data2(TRACK3_DTHETA_lane6[2]),
    .TRACK3_DTHETA_lane6_data3(TRACK3_DTHETA_lane6[3]),
    
    .TRACK3_PHI_lane0_data0(TRACK3_PHI_lane0[0]),
    .TRACK3_PHI_lane0_data1(TRACK3_PHI_lane0[1]),
    .TRACK3_PHI_lane0_data2(TRACK3_PHI_lane0[2]),
    .TRACK3_PHI_lane0_data3(TRACK3_PHI_lane0[3]),
    
    .TRACK3_PHI_lane1_data0(TRACK3_PHI_lane1[0]),
    .TRACK3_PHI_lane1_data1(TRACK3_PHI_lane1[1]),
    .TRACK3_PHI_lane1_data2(TRACK3_PHI_lane1[2]),
    .TRACK3_PHI_lane1_data3(TRACK3_PHI_lane1[3]),
    
    .TRACK3_PHI_lane2_data0(TRACK3_PHI_lane2[0]),
    .TRACK3_PHI_lane2_data1(TRACK3_PHI_lane2[1]),
    .TRACK3_PHI_lane2_data2(TRACK3_PHI_lane2[2]),
    .TRACK3_PHI_lane2_data3(TRACK3_PHI_lane2[3]),
    
    .TRACK3_PHI_lane3_data0(TRACK3_PHI_lane3[0]),
    .TRACK3_PHI_lane3_data1(TRACK3_PHI_lane3[1]),
    .TRACK3_PHI_lane3_data2(TRACK3_PHI_lane3[2]),
    .TRACK3_PHI_lane3_data3(TRACK3_PHI_lane3[3]),
    
    .TRACK3_PHI_lane4_data0(TRACK3_PHI_lane4[0]),
    .TRACK3_PHI_lane4_data1(TRACK3_PHI_lane4[1]),
    .TRACK3_PHI_lane4_data2(TRACK3_PHI_lane4[2]),
    .TRACK3_PHI_lane4_data3(TRACK3_PHI_lane4[3]),
    
    .TRACK3_PHI_lane5_data0(TRACK3_PHI_lane5[0]),
    .TRACK3_PHI_lane5_data1(TRACK3_PHI_lane5[1]),
    .TRACK3_PHI_lane5_data2(TRACK3_PHI_lane5[2]),
    .TRACK3_PHI_lane5_data3(TRACK3_PHI_lane5[3]),
    
    .TRACK3_PHI_lane6_data0(TRACK3_PHI_lane6[0]),
    .TRACK3_PHI_lane6_data1(TRACK3_PHI_lane6[1]),
    .TRACK3_PHI_lane6_data2(TRACK3_PHI_lane6[2]),
    .TRACK3_PHI_lane6_data3(TRACK3_PHI_lane6[3]),
    
    .TRACK3_ETA_lane0_data0(TRACK3_ETA_lane0[0]),
    .TRACK3_ETA_lane0_data1(TRACK3_ETA_lane0[1]),
    .TRACK3_ETA_lane0_data2(TRACK3_ETA_lane0[2]),
    .TRACK3_ETA_lane0_data3(TRACK3_ETA_lane0[3]),
    
    .TRACK3_ETA_lane1_data0(TRACK3_ETA_lane1[0]),
    .TRACK3_ETA_lane1_data1(TRACK3_ETA_lane1[1]),
    .TRACK3_ETA_lane1_data2(TRACK3_ETA_lane1[2]),
    .TRACK3_ETA_lane1_data3(TRACK3_ETA_lane1[3]),
    
    .TRACK3_ETA_lane2_data0(TRACK3_ETA_lane2[0]),
    .TRACK3_ETA_lane2_data1(TRACK3_ETA_lane2[1]),
    .TRACK3_ETA_lane2_data2(TRACK3_ETA_lane2[2]),
    .TRACK3_ETA_lane2_data3(TRACK3_ETA_lane2[3]),
    
    .TRACK3_ETA_lane3_data0(TRACK3_ETA_lane3[0]),
    .TRACK3_ETA_lane3_data1(TRACK3_ETA_lane3[1]),
    .TRACK3_ETA_lane3_data2(TRACK3_ETA_lane3[2]),
    .TRACK3_ETA_lane3_data3(TRACK3_ETA_lane3[3]),
    
    .TRACK3_ETA_lane4_data0(TRACK3_ETA_lane4[0]),
    .TRACK3_ETA_lane4_data1(TRACK3_ETA_lane4[1]),
    .TRACK3_ETA_lane4_data2(TRACK3_ETA_lane4[2]),
    .TRACK3_ETA_lane4_data3(TRACK3_ETA_lane4[3]),
    
    .TRACK3_ETA_lane5_data0(TRACK3_ETA_lane5[0]),
    .TRACK3_ETA_lane5_data1(TRACK3_ETA_lane5[1]),
    .TRACK3_ETA_lane5_data2(TRACK3_ETA_lane5[2]),
    .TRACK3_ETA_lane5_data3(TRACK3_ETA_lane5[3]),
    
    .TRACK3_ETA_lane6_data0(TRACK3_ETA_lane6[0]),
    .TRACK3_ETA_lane6_data1(TRACK3_ETA_lane6[1]),
    .TRACK3_ETA_lane6_data2(TRACK3_ETA_lane6[2]),
    .TRACK3_ETA_lane6_data3(TRACK3_ETA_lane6[3]),
    
    .tmp0({track0_data[15:0]}),
    .tmp1({track1_data[7:0], track0_data[23:16]}),
    .tmp2({track1_data[23:8]}),
    .tmp3({track2_data[15:0]}),
    .tmp4({track3_data[7:0], track2_data[23:16]}),
    .tmp5({track3_data[23:8]}),
    .tmp6({BCID[11:0], EventID[3:0]})
);  


endmodule
