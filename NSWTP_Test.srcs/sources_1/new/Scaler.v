`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2016/09/01 17:21:02
// Design Name: 
// Module Name: Scaler
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Scaler(
    input wire CLK_in,
    input wire reset_in,
    input wire data_in,
    output reg [15:0] count_out
    );


always @(posedge CLK_in) begin
    if (reset_in) begin
        count_out <= 16'b0;
    end
    else begin
        if (data_in) begin
            count_out <= count_out + 16'b1;
        end
    end
end



endmodule
