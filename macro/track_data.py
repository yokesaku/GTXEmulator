import os
import sys
import csv
import re
import shutil
import glob
import csv
from time import sleep

sys.path.append("~/icepp/ATLAS/firmware/GTX_emulation/macro/")
from vme_addr import *

###########
# track data writer for GTX_emulator
# Input file whick looks like:
# ETA, 0, 4   // for the value of ETA for track0 is assigned to 4
# is requered

Addr_EMULATOR_ENABLLE='8d0'
Addr_EMULATOR_DATA_LENGTH='8d1'
Addr_EMULATOR_GTX_HEADER='8d2'
Addr_EMULATOR_RESET='8d3'

#ETA = [["" for i in range(10)] for j in range(4)]
#PHI = [["" for i in range(10)] for j in range(4)]
#DTHETA = [["" for i in range(10)] for j in range(4)]
#MM = [["" for i in range(10)] for j in range(4)]
#sTGC = [["" for i in range(10)] for j in range(4)]
#SPARE = [["" for i in range(10)] for j in range(4)]

args = sys.argv
if(len(args) < 4) :
    print "Usage: python track_data.py [board address] [lane] [file prefix]"
    sys.exit()

board = args[1]
lane = args[2]
lane_int = int(lane)
file_pre = args[3]

inputfile_name = file_pre + '_lane' + lane + '.csv'

print "lane :", lane
print "board:", board
print "input file : ", inputfile_name

inputfile = open(inputfile_name, 'r')
reader = csv.reader(inputfile)

for row in reader :
    data_type = row[0]
    track_num = int(row[1])
    data_count = int(row[2])
    data_value = int(row[3])

    sleep(0.001)
    if data_type == 'ETA' :
        command = 'vmeput /dev/vmedrv32d16 '
        address = '%s%s4 0x%s' %(board, ETA[track_num][lane_int][data_count], data_value)
        command += address
        #print command
        os.system(command)

    elif data_type == 'PHI' :
        command = 'vmeput /dev/vmedrv32d16 '
        address = '%s%s4 0x%s' %(board, PHI[track_num][lane_int][data_count], data_value)
        command += address
        #print command
        os.system(command)

    elif data_type == 'DTHETA' :
        command = 'vmeput /dev/vmedrv32d16 '
        address = '%s%s4 0x%s' %(board, DTHETA[track_num][lane_int][data_count], data_value)
        #command += address
        #print command

    elif data_type == 'MM' :
        command = 'vmeput /dev/vmedrv32d16 '
        address = '%s%s4 0x%s' %(board, MM[track_num][lane_int][data_count], data_value)
        command += address
        #print command
        os.system(command)

    elif data_type == 'sTGC' :
        command = 'vmeput /dev/vmedrv32d16 '
        address = '%s%s4 0x%s' %(board, sTGC[track_num][lane_int][data_count], data_value)
        command += address
        #print command
        os.system(command)

    elif data_type == 'SPARE' :
        command = 'vmeput /dev/vmedrv32d16 '
        address = '%s%s4 0x%s' %(board, SPARE[track_num][lane_int][data_count], data_value)
        command += address
        #print command
        os.system(command)

    else :
        print "Invalid data_type. Check the inputfile %s" %(inputfile_name)
        sys.exit()

