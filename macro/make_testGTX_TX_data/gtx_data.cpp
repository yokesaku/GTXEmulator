#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <sstream>

using namespace std;

int main(){
	const int DATA_TYPE_NUM = 6;
	const int DATA_LENGTH = 4;
	const int TRACK_NUM = 4;
	const int LANE_NUM = 7;

	vector<string> vs(DATA_TYPE_NUM);
	vs[0] = "SPARE";
	vs[1] = "sTGC";
	vs[2] = "MM";
	vs[3] = "DTHETA";
	vs[4] = "PHI";
	vs[5] = "ETA";

	for(int lane=0;lane<LANE_NUM;lane++){
		string output_filename = "test_";
		string suffix = ".csv";

		output_filename += "lane" + to_string(lane) + suffix;
		ofstream ofs(output_filename);

		for(int cnt=0; cnt<DATA_LENGTH; cnt++){
			for(int track=0;track<TRACK_NUM; track++){
				for(auto type : vs){
					ofs << type << "," << track << "," << cnt << ",";
					if(type == "ETA"){
						stringstream ss;
						ss << std::hex << cnt;
						ofs << ss.str();
					}else{
						ofs << "0";
					}
					ofs << endl;
				}
			}
		}
	}

	return 0;
}
