ETA = [[["" for i in range(4)] for j in range(7)] for k in range(4)]
SPARE = [[["" for i in range(4)] for j in range(7)] for k in range(4)]
PHI = [[["" for i in range(4)] for j in range(7)] for k in range(4)]
DTHETA = [[["" for i in range(4)] for j in range(7)] for k in range(4)]
MM = [[["" for i in range(4)] for j in range(7)] for k in range(4)]
sTGC = [[["" for i in range(4)] for j in range(7)] for k in range(4)]



SPARE[0][0][0] = 'c00'
SPARE[0][0][1] = 'c01'
SPARE[0][0][2] = 'c02'
SPARE[0][0][3] = 'c03'

SPARE[0][1][0] = 'c04'
SPARE[0][1][1] = 'c05'
SPARE[0][1][2] = 'c06'
SPARE[0][1][3] = 'c07'

SPARE[0][2][0] = 'c08'
SPARE[0][2][1] = 'c09'
SPARE[0][2][2] = 'c0a'
SPARE[0][2][3] = 'c0b'

SPARE[0][3][0] = 'c0c'
SPARE[0][3][1] = 'c0d'
SPARE[0][3][2] = 'c0e'
SPARE[0][3][3] = 'c0f'

SPARE[0][4][0] = 'c10'
SPARE[0][4][1] = 'c11'
SPARE[0][4][2] = 'c12'
SPARE[0][4][3] = 'c13'

SPARE[0][5][0] = 'c14'
SPARE[0][5][1] = 'c15'
SPARE[0][5][2] = 'c16'
SPARE[0][5][3] = 'c17'

SPARE[0][6][0] = 'c18'
SPARE[0][6][1] = 'c19'
SPARE[0][6][2] = 'c1a'
SPARE[0][6][3] = 'c1b'

sTGC[0][0][0] = 'c1c'
sTGC[0][0][1] = 'c1d'
sTGC[0][0][2] = 'c1e'
sTGC[0][0][3] = 'c1f'

sTGC[0][1][0] = 'c20'
sTGC[0][1][1] = 'c21'
sTGC[0][1][2] = 'c22'
sTGC[0][1][3] = 'c23'

sTGC[0][2][0] = 'c24'
sTGC[0][2][1] = 'c25'
sTGC[0][2][2] = 'c26'
sTGC[0][2][3] = 'c27'

sTGC[0][3][0] = 'c28'
sTGC[0][3][1] = 'c29'
sTGC[0][3][2] = 'c2a'
sTGC[0][3][3] = 'c2b'

sTGC[0][4][0] = 'c2c'
sTGC[0][4][1] = 'c2d'
sTGC[0][4][2] = 'c2e'
sTGC[0][4][3] = 'c2f'

sTGC[0][5][0] = 'c30'
sTGC[0][5][1] = 'c31'
sTGC[0][5][2] = 'c32'
sTGC[0][5][3] = 'c33'

sTGC[0][6][0] = 'c34'
sTGC[0][6][1] = 'c35'
sTGC[0][6][2] = 'c36'
sTGC[0][6][3] = 'c37'

MM[0][0][0] = 'c38'
MM[0][0][1] = 'c39'
MM[0][0][2] = 'c3a'
MM[0][0][3] = 'c3b'

MM[0][1][0] = 'c3c'
MM[0][1][1] = 'c3d'
MM[0][1][2] = 'c3e'
MM[0][1][3] = 'c3f'

MM[0][2][0] = 'c40'
MM[0][2][1] = 'c41'
MM[0][2][2] = 'c42'
MM[0][2][3] = 'c43'

MM[0][3][0] = 'c44'
MM[0][3][1] = 'c45'
MM[0][3][2] = 'c46'
MM[0][3][3] = 'c47'

MM[0][4][0] = 'c48'
MM[0][4][1] = 'c49'
MM[0][4][2] = 'c4a'
MM[0][4][3] = 'c4b'

MM[0][5][0] = 'c4c'
MM[0][5][1] = 'c4d'
MM[0][5][2] = 'c4e'
MM[0][5][3] = 'c4f'

MM[0][6][0] = 'c50'
MM[0][6][1] = 'c51'
MM[0][6][2] = 'c52'
MM[0][6][3] = 'c53'

DTHETA[0][0][0] = 'c54'
DTHETA[0][0][1] = 'c55'
DTHETA[0][0][2] = 'c56'
DTHETA[0][0][3] = 'c57'

DTHETA[0][1][0] = 'c58'
DTHETA[0][1][1] = 'c59'
DTHETA[0][1][2] = 'c5a'
DTHETA[0][1][3] = 'c5b'

DTHETA[0][2][0] = 'c5c'
DTHETA[0][2][1] = 'c5d'
DTHETA[0][2][2] = 'c5e'
DTHETA[0][2][3] = 'c5f'

DTHETA[0][3][0] = 'c60'
DTHETA[0][3][1] = 'c61'
DTHETA[0][3][2] = 'c62'
DTHETA[0][3][3] = 'c63'

DTHETA[0][4][0] = 'c64'
DTHETA[0][4][1] = 'c65'
DTHETA[0][4][2] = 'c66'
DTHETA[0][4][3] = 'c67'

DTHETA[0][5][0] = 'c68'
DTHETA[0][5][1] = 'c69'
DTHETA[0][5][2] = 'c6a'
DTHETA[0][5][3] = 'c6b'

DTHETA[0][6][0] = 'c6c'
DTHETA[0][6][1] = 'c6d'
DTHETA[0][6][2] = 'c6e'
DTHETA[0][6][3] = 'c6f'

PHI[0][0][0] = 'c70'
PHI[0][0][1] = 'c71'
PHI[0][0][2] = 'c72'
PHI[0][0][3] = 'c73'

PHI[0][1][0] = 'c74'
PHI[0][1][1] = 'c75'
PHI[0][1][2] = 'c76'
PHI[0][1][3] = 'c77'

PHI[0][2][0] = 'c78'
PHI[0][2][1] = 'c79'
PHI[0][2][2] = 'c7a'
PHI[0][2][3] = 'c7b'

PHI[0][3][0] = 'c7c'
PHI[0][3][1] = 'c7d'
PHI[0][3][2] = 'c7e'
PHI[0][3][3] = 'c7f'

PHI[0][4][0] = 'c80'
PHI[0][4][1] = 'c81'
PHI[0][4][2] = 'c82'
PHI[0][4][3] = 'c83'

PHI[0][5][0] = 'c84'
PHI[0][5][1] = 'c85'
PHI[0][5][2] = 'c86'
PHI[0][5][3] = 'c87'

PHI[0][6][0] = 'c88'
PHI[0][6][1] = 'c89'
PHI[0][6][2] = 'c8a'
PHI[0][6][3] = 'c8b'

ETA[0][0][0] = 'c8c'
ETA[0][0][1] = 'c8d'
ETA[0][0][2] = 'c8e'
ETA[0][0][3] = 'c8f'

ETA[0][1][0] = 'c90'
ETA[0][1][1] = 'c91'
ETA[0][1][2] = 'c92'
ETA[0][1][3] = 'c93'

ETA[0][2][0] = 'c94'
ETA[0][2][1] = 'c95'
ETA[0][2][2] = 'c96'
ETA[0][2][3] = 'c97'

ETA[0][3][0] = 'c98'
ETA[0][3][1] = 'c99'
ETA[0][3][2] = 'c9a'
ETA[0][3][3] = 'c9b'

ETA[0][4][0] = 'c9c'
ETA[0][4][1] = 'c9d'
ETA[0][4][2] = 'c9e'
ETA[0][4][3] = 'c9f'

ETA[0][5][0] = 'ca0'
ETA[0][5][1] = 'ca1'
ETA[0][5][2] = 'ca2'
ETA[0][5][3] = 'ca3'

ETA[0][6][0] = 'ca4'
ETA[0][6][1] = 'ca5'
ETA[0][6][2] = 'ca6'
ETA[0][6][3] = 'ca7'

SPARE[1][0][0] = 'ca8'
SPARE[1][0][1] = 'ca9'
SPARE[1][0][2] = 'caa'
SPARE[1][0][3] = 'cab'

SPARE[1][1][0] = 'cac'
SPARE[1][1][1] = 'cad'
SPARE[1][1][2] = 'cae'
SPARE[1][1][3] = 'caf'

SPARE[1][2][0] = 'cb0'
SPARE[1][2][1] = 'cb1'
SPARE[1][2][2] = 'cb2'
SPARE[1][2][3] = 'cb3'

SPARE[1][3][0] = 'cb4'
SPARE[1][3][1] = 'cb5'
SPARE[1][3][2] = 'cb6'
SPARE[1][3][3] = 'cb7'

SPARE[1][4][0] = 'cb8'
SPARE[1][4][1] = 'cb9'
SPARE[1][4][2] = 'cba'
SPARE[1][4][3] = 'cbb'

SPARE[1][5][0] = 'cbc'
SPARE[1][5][1] = 'cbd'
SPARE[1][5][2] = 'cbe'
SPARE[1][5][3] = 'cbf'

SPARE[1][6][0] = 'cc0'
SPARE[1][6][1] = 'cc1'
SPARE[1][6][2] = 'cc2'
SPARE[1][6][3] = 'cc3'

sTGC[1][0][0] = 'cc4'
sTGC[1][0][1] = 'cc5'
sTGC[1][0][2] = 'cc6'
sTGC[1][0][3] = 'cc7'

sTGC[1][1][0] = 'cc8'
sTGC[1][1][1] = 'cc9'
sTGC[1][1][2] = 'cca'
sTGC[1][1][3] = 'ccb'

sTGC[1][2][0] = 'ccc'
sTGC[1][2][1] = 'ccd'
sTGC[1][2][2] = 'cce'
sTGC[1][2][3] = 'ccf'

sTGC[1][3][0] = 'cd0'
sTGC[1][3][1] = 'cd1'
sTGC[1][3][2] = 'cd2'
sTGC[1][3][3] = 'cd3'

sTGC[1][4][0] = 'cd4'
sTGC[1][4][1] = 'cd5'
sTGC[1][4][2] = 'cd6'
sTGC[1][4][3] = 'cd7'

sTGC[1][5][0] = 'cd8'
sTGC[1][5][1] = 'cd9'
sTGC[1][5][2] = 'cda'
sTGC[1][5][3] = 'cdb'

sTGC[1][6][0] = 'cdc'
sTGC[1][6][1] = 'cdd'
sTGC[1][6][2] = 'cde'
sTGC[1][6][3] = 'cdf'

MM[1][0][0] = 'ce0'
MM[1][0][1] = 'ce1'
MM[1][0][2] = 'ce2'
MM[1][0][3] = 'ce3'

MM[1][1][0] = 'ce4'
MM[1][1][1] = 'ce5'
MM[1][1][2] = 'ce6'
MM[1][1][3] = 'ce7'

MM[1][2][0] = 'ce8'
MM[1][2][1] = 'ce9'
MM[1][2][2] = 'cea'
MM[1][2][3] = 'ceb'

MM[1][3][0] = 'cec'
MM[1][3][1] = 'ced'
MM[1][3][2] = 'cee'
MM[1][3][3] = 'cef'

MM[1][4][0] = 'cf0'
MM[1][4][1] = 'cf1'
MM[1][4][2] = 'cf2'
MM[1][4][3] = 'cf3'

MM[1][5][0] = 'cf4'
MM[1][5][1] = 'cf5'
MM[1][5][2] = 'cf6'
MM[1][5][3] = 'cf7'

MM[1][6][0] = 'cf8'
MM[1][6][1] = 'cf9'
MM[1][6][2] = 'cfa'
MM[1][6][3] = 'cfb'

DTHETA[1][0][0] = 'cfc'
DTHETA[1][0][1] = 'cfd'
DTHETA[1][0][2] = 'cfe'
DTHETA[1][0][3] = 'cff'

DTHETA[1][1][0] = 'd00'
DTHETA[1][1][1] = 'd01'
DTHETA[1][1][2] = 'd02'
DTHETA[1][1][3] = 'd03'

DTHETA[1][2][0] = 'd04'
DTHETA[1][2][1] = 'd05'
DTHETA[1][2][2] = 'd06'
DTHETA[1][2][3] = 'd07'

DTHETA[1][3][0] = 'd08'
DTHETA[1][3][1] = 'd09'
DTHETA[1][3][2] = 'd0a'
DTHETA[1][3][3] = 'd0b'

DTHETA[1][4][0] = 'd0c'
DTHETA[1][4][1] = 'd0d'
DTHETA[1][4][2] = 'd0e'
DTHETA[1][4][3] = 'd0f'

DTHETA[1][5][0] = 'd10'
DTHETA[1][5][1] = 'd11'
DTHETA[1][5][2] = 'd12'
DTHETA[1][5][3] = 'd13'

DTHETA[1][6][0] = 'd14'
DTHETA[1][6][1] = 'd15'
DTHETA[1][6][2] = 'd16'
DTHETA[1][6][3] = 'd17'

PHI[1][0][0] = 'd18'
PHI[1][0][1] = 'd19'
PHI[1][0][2] = 'd1a'
PHI[1][0][3] = 'd1b'

PHI[1][1][0] = 'd1c'
PHI[1][1][1] = 'd1d'
PHI[1][1][2] = 'd1e'
PHI[1][1][3] = 'd1f'

PHI[1][2][0] = 'd20'
PHI[1][2][1] = 'd21'
PHI[1][2][2] = 'd22'
PHI[1][2][3] = 'd23'

PHI[1][3][0] = 'd24'
PHI[1][3][1] = 'd25'
PHI[1][3][2] = 'd26'
PHI[1][3][3] = 'd27'

PHI[1][4][0] = 'd28'
PHI[1][4][1] = 'd29'
PHI[1][4][2] = 'd2a'
PHI[1][4][3] = 'd2b'

PHI[1][5][0] = 'd2c'
PHI[1][5][1] = 'd2d'
PHI[1][5][2] = 'd2e'
PHI[1][5][3] = 'd2f'

PHI[1][6][0] = 'd30'
PHI[1][6][1] = 'd31'
PHI[1][6][2] = 'd32'
PHI[1][6][3] = 'd33'

ETA[1][0][0] = 'd34'
ETA[1][0][1] = 'd35'
ETA[1][0][2] = 'd36'
ETA[1][0][3] = 'd37'

ETA[1][1][0] = 'd38'
ETA[1][1][1] = 'd39'
ETA[1][1][2] = 'd3a'
ETA[1][1][3] = 'd3b'

ETA[1][2][0] = 'd3c'
ETA[1][2][1] = 'd3d'
ETA[1][2][2] = 'd3e'
ETA[1][2][3] = 'd3f'

ETA[1][3][0] = 'd40'
ETA[1][3][1] = 'd41'
ETA[1][3][2] = 'd42'
ETA[1][3][3] = 'd43'

ETA[1][4][0] = 'd44'
ETA[1][4][1] = 'd45'
ETA[1][4][2] = 'd46'
ETA[1][4][3] = 'd47'

ETA[1][5][0] = 'd48'
ETA[1][5][1] = 'd49'
ETA[1][5][2] = 'd4a'
ETA[1][5][3] = 'd4b'

ETA[1][6][0] = 'd4c'
ETA[1][6][1] = 'd4d'
ETA[1][6][2] = 'd4e'
ETA[1][6][3] = 'd4f'

SPARE[2][0][0] = 'd50'
SPARE[2][0][1] = 'd51'
SPARE[2][0][2] = 'd52'
SPARE[2][0][3] = 'd53'

SPARE[2][1][0] = 'd54'
SPARE[2][1][1] = 'd55'
SPARE[2][1][2] = 'd56'
SPARE[2][1][3] = 'd57'

SPARE[2][2][0] = 'd58'
SPARE[2][2][1] = 'd59'
SPARE[2][2][2] = 'd5a'
SPARE[2][2][3] = 'd5b'

SPARE[2][3][0] = 'd5c'
SPARE[2][3][1] = 'd5d'
SPARE[2][3][2] = 'd5e'
SPARE[2][3][3] = 'd5f'

SPARE[2][4][0] = 'd60'
SPARE[2][4][1] = 'd61'
SPARE[2][4][2] = 'd62'
SPARE[2][4][3] = 'd63'

SPARE[2][5][0] = 'd64'
SPARE[2][5][1] = 'd65'
SPARE[2][5][2] = 'd66'
SPARE[2][5][3] = 'd67'

SPARE[2][6][0] = 'd68'
SPARE[2][6][1] = 'd69'
SPARE[2][6][2] = 'd6a'
SPARE[2][6][3] = 'd6b'

sTGC[2][0][0] = 'd6c'
sTGC[2][0][1] = 'd6d'
sTGC[2][0][2] = 'd6e'
sTGC[2][0][3] = 'd6f'

sTGC[2][1][0] = 'd70'
sTGC[2][1][1] = 'd71'
sTGC[2][1][2] = 'd72'
sTGC[2][1][3] = 'd73'

sTGC[2][2][0] = 'd74'
sTGC[2][2][1] = 'd75'
sTGC[2][2][2] = 'd76'
sTGC[2][2][3] = 'd77'

sTGC[2][3][0] = 'd78'
sTGC[2][3][1] = 'd79'
sTGC[2][3][2] = 'd7a'
sTGC[2][3][3] = 'd7b'

sTGC[2][4][0] = 'd7c'
sTGC[2][4][1] = 'd7d'
sTGC[2][4][2] = 'd7e'
sTGC[2][4][3] = 'd7f'

sTGC[2][5][0] = 'd80'
sTGC[2][5][1] = 'd81'
sTGC[2][5][2] = 'd82'
sTGC[2][5][3] = 'd83'

sTGC[2][6][0] = 'd84'
sTGC[2][6][1] = 'd85'
sTGC[2][6][2] = 'd86'
sTGC[2][6][3] = 'd87'

MM[2][0][0] = 'd88'
MM[2][0][1] = 'd89'
MM[2][0][2] = 'd8a'
MM[2][0][3] = 'd8b'

MM[2][1][0] = 'd8c'
MM[2][1][1] = 'd8d'
MM[2][1][2] = 'd8e'
MM[2][1][3] = 'd8f'

MM[2][2][0] = 'd90'
MM[2][2][1] = 'd91'
MM[2][2][2] = 'd92'
MM[2][2][3] = 'd93'

MM[2][3][0] = 'd94'
MM[2][3][1] = 'd95'
MM[2][3][2] = 'd96'
MM[2][3][3] = 'd97'

MM[2][4][0] = 'd98'
MM[2][4][1] = 'd99'
MM[2][4][2] = 'd9a'
MM[2][4][3] = 'd9b'

MM[2][5][0] = 'd9c'
MM[2][5][1] = 'd9d'
MM[2][5][2] = 'd9e'
MM[2][5][3] = 'd9f'

MM[2][6][0] = 'da0'
MM[2][6][1] = 'da1'
MM[2][6][2] = 'da2'
MM[2][6][3] = 'da3'

DTHETA[2][0][0] = 'da4'
DTHETA[2][0][1] = 'da5'
DTHETA[2][0][2] = 'da6'
DTHETA[2][0][3] = 'da7'

DTHETA[2][1][0] = 'da8'
DTHETA[2][1][1] = 'da9'
DTHETA[2][1][2] = 'daa'
DTHETA[2][1][3] = 'dab'

DTHETA[2][2][0] = 'dac'
DTHETA[2][2][1] = 'dad'
DTHETA[2][2][2] = 'dae'
DTHETA[2][2][3] = 'daf'

DTHETA[2][3][0] = 'db0'
DTHETA[2][3][1] = 'db1'
DTHETA[2][3][2] = 'db2'
DTHETA[2][3][3] = 'db3'

DTHETA[2][4][0] = 'db4'
DTHETA[2][4][1] = 'db5'
DTHETA[2][4][2] = 'db6'
DTHETA[2][4][3] = 'db7'

DTHETA[2][5][0] = 'db8'
DTHETA[2][5][1] = 'db9'
DTHETA[2][5][2] = 'dba'
DTHETA[2][5][3] = 'dbb'

DTHETA[2][6][0] = 'dbc'
DTHETA[2][6][1] = 'dbd'
DTHETA[2][6][2] = 'dbe'
DTHETA[2][6][3] = 'dbf'

PHI[2][0][0] = 'dc0'
PHI[2][0][1] = 'dc1'
PHI[2][0][2] = 'dc2'
PHI[2][0][3] = 'dc3'

PHI[2][1][0] = 'dc4'
PHI[2][1][1] = 'dc5'
PHI[2][1][2] = 'dc6'
PHI[2][1][3] = 'dc7'

PHI[2][2][0] = 'dc8'
PHI[2][2][1] = 'dc9'
PHI[2][2][2] = 'dca'
PHI[2][2][3] = 'dcb'

PHI[2][3][0] = 'dcc'
PHI[2][3][1] = 'dcd'
PHI[2][3][2] = 'dce'
PHI[2][3][3] = 'dcf'

PHI[2][4][0] = 'dd0'
PHI[2][4][1] = 'dd1'
PHI[2][4][2] = 'dd2'
PHI[2][4][3] = 'dd3'

PHI[2][5][0] = 'dd4'
PHI[2][5][1] = 'dd5'
PHI[2][5][2] = 'dd6'
PHI[2][5][3] = 'dd7'

PHI[2][6][0] = 'dd8'
PHI[2][6][1] = 'dd9'
PHI[2][6][2] = 'dda'
PHI[2][6][3] = 'ddb'

ETA[2][0][0] = 'ddc'
ETA[2][0][1] = 'ddd'
ETA[2][0][2] = 'dde'
ETA[2][0][3] = 'ddf'

ETA[2][1][0] = 'de0'
ETA[2][1][1] = 'de1'
ETA[2][1][2] = 'de2'
ETA[2][1][3] = 'de3'

ETA[2][2][0] = 'de4'
ETA[2][2][1] = 'de5'
ETA[2][2][2] = 'de6'
ETA[2][2][3] = 'de7'

ETA[2][3][0] = 'de8'
ETA[2][3][1] = 'de9'
ETA[2][3][2] = 'dea'
ETA[2][3][3] = 'deb'

ETA[2][4][0] = 'dec'
ETA[2][4][1] = 'ded'
ETA[2][4][2] = 'dee'
ETA[2][4][3] = 'def'

ETA[2][5][0] = 'df0'
ETA[2][5][1] = 'df1'
ETA[2][5][2] = 'df2'
ETA[2][5][3] = 'df3'

ETA[2][6][0] = 'df4'
ETA[2][6][1] = 'df5'
ETA[2][6][2] = 'df6'
ETA[2][6][3] = 'df7'

SPARE[3][0][0] = 'df8'
SPARE[3][0][1] = 'df9'
SPARE[3][0][2] = 'dfa'
SPARE[3][0][3] = 'dfb'

SPARE[3][1][0] = 'dfc'
SPARE[3][1][1] = 'dfd'
SPARE[3][1][2] = 'dfe'
SPARE[3][1][3] = 'dff'

SPARE[3][2][0] = 'e00'
SPARE[3][2][1] = 'e01'
SPARE[3][2][2] = 'e02'
SPARE[3][2][3] = 'e03'

SPARE[3][3][0] = 'e04'
SPARE[3][3][1] = 'e05'
SPARE[3][3][2] = 'e06'
SPARE[3][3][3] = 'e07'

SPARE[3][4][0] = 'e08'
SPARE[3][4][1] = 'e09'
SPARE[3][4][2] = 'e0a'
SPARE[3][4][3] = 'e0b'

SPARE[3][5][0] = 'e0c'
SPARE[3][5][1] = 'e0d'
SPARE[3][5][2] = 'e0e'
SPARE[3][5][3] = 'e0f'

SPARE[3][6][0] = 'e10'
SPARE[3][6][1] = 'e11'
SPARE[3][6][2] = 'e12'
SPARE[3][6][3] = 'e13'

sTGC[3][0][0] = 'e14'
sTGC[3][0][1] = 'e15'
sTGC[3][0][2] = 'e16'
sTGC[3][0][3] = 'e17'

sTGC[3][1][0] = 'e18'
sTGC[3][1][1] = 'e19'
sTGC[3][1][2] = 'e1a'
sTGC[3][1][3] = 'e1b'

sTGC[3][2][0] = 'e1c'
sTGC[3][2][1] = 'e1d'
sTGC[3][2][2] = 'e1e'
sTGC[3][2][3] = 'e1f'

sTGC[3][3][0] = 'e20'
sTGC[3][3][1] = 'e21'
sTGC[3][3][2] = 'e22'
sTGC[3][3][3] = 'e23'

sTGC[3][4][0] = 'e24'
sTGC[3][4][1] = 'e25'
sTGC[3][4][2] = 'e26'
sTGC[3][4][3] = 'e27'

sTGC[3][5][0] = 'e28'
sTGC[3][5][1] = 'e29'
sTGC[3][5][2] = 'e2a'
sTGC[3][5][3] = 'e2b'

sTGC[3][6][0] = 'e2c'
sTGC[3][6][1] = 'e2d'
sTGC[3][6][2] = 'e2e'
sTGC[3][6][3] = 'e2f'

MM[3][0][0] = 'e30'
MM[3][0][1] = 'e31'
MM[3][0][2] = 'e32'
MM[3][0][3] = 'e33'

MM[3][1][0] = 'e34'
MM[3][1][1] = 'e35'
MM[3][1][2] = 'e36'
MM[3][1][3] = 'e37'

MM[3][2][0] = 'e38'
MM[3][2][1] = 'e39'
MM[3][2][2] = 'e3a'
MM[3][2][3] = 'e3b'

MM[3][3][0] = 'e3c'
MM[3][3][1] = 'e3d'
MM[3][3][2] = 'e3e'
MM[3][3][3] = 'e3f'

MM[3][4][0] = 'e40'
MM[3][4][1] = 'e41'
MM[3][4][2] = 'e42'
MM[3][4][3] = 'e43'

MM[3][5][0] = 'e44'
MM[3][5][1] = 'e45'
MM[3][5][2] = 'e46'
MM[3][5][3] = 'e47'

MM[3][6][0] = 'e48'
MM[3][6][1] = 'e49'
MM[3][6][2] = 'e4a'
MM[3][6][3] = 'e4b'

DTHETA[3][0][0] = 'e4c'
DTHETA[3][0][1] = 'e4d'
DTHETA[3][0][2] = 'e4e'
DTHETA[3][0][3] = 'e4f'

DTHETA[3][1][0] = 'e50'
DTHETA[3][1][1] = 'e51'
DTHETA[3][1][2] = 'e52'
DTHETA[3][1][3] = 'e53'

DTHETA[3][2][0] = 'e54'
DTHETA[3][2][1] = 'e55'
DTHETA[3][2][2] = 'e56'
DTHETA[3][2][3] = 'e57'

DTHETA[3][3][0] = 'e58'
DTHETA[3][3][1] = 'e59'
DTHETA[3][3][2] = 'e5a'
DTHETA[3][3][3] = 'e5b'

DTHETA[3][4][0] = 'e5c'
DTHETA[3][4][1] = 'e5d'
DTHETA[3][4][2] = 'e5e'
DTHETA[3][4][3] = 'e5f'

DTHETA[3][5][0] = 'e60'
DTHETA[3][5][1] = 'e61'
DTHETA[3][5][2] = 'e62'
DTHETA[3][5][3] = 'e63'

DTHETA[3][6][0] = 'e64'
DTHETA[3][6][1] = 'e65'
DTHETA[3][6][2] = 'e66'
DTHETA[3][6][3] = 'e67'

PHI[3][0][0] = 'e68'
PHI[3][0][1] = 'e69'
PHI[3][0][2] = 'e6a'
PHI[3][0][3] = 'e6b'

PHI[3][1][0] = 'e6c'
PHI[3][1][1] = 'e6d'
PHI[3][1][2] = 'e6e'
PHI[3][1][3] = 'e6f'

PHI[3][2][0] = 'e70'
PHI[3][2][1] = 'e71'
PHI[3][2][2] = 'e72'
PHI[3][2][3] = 'e73'

PHI[3][3][0] = 'e74'
PHI[3][3][1] = 'e75'
PHI[3][3][2] = 'e76'
PHI[3][3][3] = 'e77'

PHI[3][4][0] = 'e78'
PHI[3][4][1] = 'e79'
PHI[3][4][2] = 'e7a'
PHI[3][4][3] = 'e7b'

PHI[3][5][0] = 'e7c'
PHI[3][5][1] = 'e7d'
PHI[3][5][2] = 'e7e'
PHI[3][5][3] = 'e7f'

PHI[3][6][0] = 'e80'
PHI[3][6][1] = 'e81'
PHI[3][6][2] = 'e82'
PHI[3][6][3] = 'e83'

ETA[3][0][0] = 'e84'
ETA[3][0][1] = 'e85'
ETA[3][0][2] = 'e86'
ETA[3][0][3] = 'e87'

ETA[3][1][0] = 'e88'
ETA[3][1][1] = 'e89'
ETA[3][1][2] = 'e8a'
ETA[3][1][3] = 'e8b'

ETA[3][2][0] = 'e8c'
ETA[3][2][1] = 'e8d'
ETA[3][2][2] = 'e8e'
ETA[3][2][3] = 'e8f'

ETA[3][3][0] = 'e90'
ETA[3][3][1] = 'e91'
ETA[3][3][2] = 'e92'
ETA[3][3][3] = 'e93'

ETA[3][4][0] = 'e94'
ETA[3][4][1] = 'e95'
ETA[3][4][2] = 'e96'
ETA[3][4][3] = 'e97'

ETA[3][5][0] = 'e98'
ETA[3][5][1] = 'e99'
ETA[3][5][2] = 'e9a'
ETA[3][5][3] = 'e9b'

ETA[3][6][0] = 'e9c'
ETA[3][6][1] = 'e9d'
ETA[3][6][2] = 'e9e'
ETA[3][6][3] = 'e9f'
