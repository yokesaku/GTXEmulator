#!/bin/bash

boardadd=$1
file_name=$2
for lane in `seq 0 6`
do
	python track_data.py ${boardadd} ${lane} ${file_name}
done
