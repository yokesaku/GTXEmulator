#!/bin/bash
# This script developed at NewSLFirmware-02-02-00.bit

Addr_EMULATOR_ENABLLE=8d0
Addr_EMULATOR_DATA_LENGTH=8d1
Addr_EMULATOR_GTX_HEADER=8d2
Addr_EMULATOR_RESET=8d3

Addr_TRACK0_ETA_lane0=c00
Addr_TRACK0_ETA_lane1=c01
Addr_TRACK0_ETA_lane2=c02
Addr_TRACK0_ETA_lane3=c03
Addr_TRACK0_ETA_lane4=c04
Addr_TRACK0_ETA_lane5=c05
Addr_TRACK0_ETA_lane6=c06
Addr_TRACK0_ETA_lane7=c07
Addr_TRACK0_ETA_lane8=c08
Addr_TRACK0_ETA_lane9=c09

Addr_TRACK0_PHI_lane0=c10
Addr_TRACK0_PHI_lane1=c11
Addr_TRACK0_PHI_lane2=c12
Addr_TRACK0_PHI_lane3=c13
Addr_TRACK0_PHI_lane4=c14
Addr_TRACK0_PHI_lane5=c15
Addr_TRACK0_PHI_lane6=c16
Addr_TRACK0_PHI_lane7=c17
Addr_TRACK0_PHI_lane8=c18
Addr_TRACK0_PHI_lane9=c19

Addr_TRACK0_DELTATHETA_lane0=c20
Addr_TRACK0_DELTATHETA_lane1=c21
Addr_TRACK0_DELTATHETA_lane2=c22
Addr_TRACK0_DELTATHETA_lane3=c23
Addr_TRACK0_DELTATHETA_lane4=c24
Addr_TRACK0_DELTATHETA_lane5=c25
Addr_TRACK0_DELTATHETA_lane6=c26
Addr_TRACK0_DELTATHETA_lane7=c27
Addr_TRACK0_DELTATHETA_lane8=c28
Addr_TRACK0_DELTATHETA_lane9=c29

Addr_TRACK0_MM_lane0=c30
Addr_TRACK0_MM_lane1=c31
Addr_TRACK0_MM_lane2=c32
Addr_TRACK0_MM_lane3=c33
Addr_TRACK0_MM_lane4=c34
Addr_TRACK0_MM_lane5=c35
Addr_TRACK0_MM_lane6=c36
Addr_TRACK0_MM_lane7=c37
Addr_TRACK0_MM_lane8=c38
Addr_TRACK0_MM_lane9=c39

Addr_TRACK0_sTGC_lane0=c40
Addr_TRACK0_sTGC_lane1=c41
Addr_TRACK0_sTGC_lane2=c42
Addr_TRACK0_sTGC_lane3=c43
Addr_TRACK0_sTGC_lane4=c44
Addr_TRACK0_sTGC_lane5=c45
Addr_TRACK0_sTGC_lane6=c46
Addr_TRACK0_sTGC_lane7=c47
Addr_TRACK0_sTGC_lane8=c48
Addr_TRACK0_sTGC_lane9=c49

Addr_TRACK0_SPARE_lane0=c50
Addr_TRACK0_SPARE_lane1=c51
Addr_TRACK0_SPARE_lane2=c52
Addr_TRACK0_SPARE_lane3=c53
Addr_TRACK0_SPARE_lane4=c54
Addr_TRACK0_SPARE_lane5=c55
Addr_TRACK0_SPARE_lane6=c56
Addr_TRACK0_SPARE_lane7=c57
Addr_TRACK0_SPARE_lane8=c58
Addr_TRACK0_SPARE_lane9=c59

Addr_TRACK1_ETA_lane0=c60
Addr_TRACK1_ETA_lane1=c61
Addr_TRACK1_ETA_lane2=c62
Addr_TRACK1_ETA_lane3=c63
Addr_TRACK1_ETA_lane4=c64
Addr_TRACK1_ETA_lane5=c65
Addr_TRACK1_ETA_lane6=c66
Addr_TRACK1_ETA_lane7=c67
Addr_TRACK1_ETA_lane8=c68
Addr_TRACK1_ETA_lane9=c69

Addr_TRACK1_PHI_lane0=c70
Addr_TRACK1_PHI_lane1=c71
Addr_TRACK1_PHI_lane2=c72
Addr_TRACK1_PHI_lane3=c73
Addr_TRACK1_PHI_lane4=c74
Addr_TRACK1_PHI_lane5=c75
Addr_TRACK1_PHI_lane6=c76
Addr_TRACK1_PHI_lane7=c77
Addr_TRACK1_PHI_lane8=c78
Addr_TRACK1_PHI_lane9=c79

Addr_TRACK1_DELTATHETA_lane0=c80
Addr_TRACK1_DELTATHETA_lane1=c81
Addr_TRACK1_DELTATHETA_lane2=c82
Addr_TRACK1_DELTATHETA_lane3=c83
Addr_TRACK1_DELTATHETA_lane4=c84
Addr_TRACK1_DELTATHETA_lane5=c85
Addr_TRACK1_DELTATHETA_lane6=c86
Addr_TRACK1_DELTATHETA_lane7=c87
Addr_TRACK1_DELTATHETA_lane8=c88
Addr_TRACK1_DELTATHETA_lane9=c89

Addr_TRACK1_MM_lane0=c90
Addr_TRACK1_MM_lane1=c91
Addr_TRACK1_MM_lane2=c92
Addr_TRACK1_MM_lane3=c93
Addr_TRACK1_MM_lane4=c94
Addr_TRACK1_MM_lane5=c95
Addr_TRACK1_MM_lane6=c96
Addr_TRACK1_MM_lane7=c97
Addr_TRACK1_MM_lane8=c98
Addr_TRACK1_MM_lane9=c99

Addr_TRACK1_sTGC_lane0=ca0
Addr_TRACK1_sTGC_lane1=ca1
Addr_TRACK1_sTGC_lane2=ca2
Addr_TRACK1_sTGC_lane3=ca3
Addr_TRACK1_sTGC_lane4=ca4
Addr_TRACK1_sTGC_lane5=ca5
Addr_TRACK1_sTGC_lane6=ca6
Addr_TRACK1_sTGC_lane7=ca7
Addr_TRACK1_sTGC_lane8=ca8
Addr_TRACK1_sTGC_lane9=ca9

Addr_TRACK1_SPARE_lane0=cb0
Addr_TRACK1_SPARE_lane1=cb1
Addr_TRACK1_SPARE_lane2=cb2
Addr_TRACK1_SPARE_lane3=cb3
Addr_TRACK1_SPARE_lane4=cb4
Addr_TRACK1_SPARE_lane5=cb5
Addr_TRACK1_SPARE_lane6=cb6
Addr_TRACK1_SPARE_lane7=cb7
Addr_TRACK1_SPARE_lane8=cb8
Addr_TRACK1_SPARE_lane9=cb9

Addr_TRACK2_ETA_lane0=cc0
Addr_TRACK2_ETA_lane1=cc1
Addr_TRACK2_ETA_lane2=cc2
Addr_TRACK2_ETA_lane3=cc3
Addr_TRACK2_ETA_lane4=cc4
Addr_TRACK2_ETA_lane5=cc5
Addr_TRACK2_ETA_lane6=cc6
Addr_TRACK2_ETA_lane7=cc7
Addr_TRACK2_ETA_lane8=cc8
Addr_TRACK2_ETA_lane9=cc9

Addr_TRACK2_PHI_lane0=cd0
Addr_TRACK2_PHI_lane1=cd1
Addr_TRACK2_PHI_lane2=cd2
Addr_TRACK2_PHI_lane3=cd3
Addr_TRACK2_PHI_lane4=cd4
Addr_TRACK2_PHI_lane5=cd5
Addr_TRACK2_PHI_lane6=cd6
Addr_TRACK2_PHI_lane7=cd7
Addr_TRACK2_PHI_lane8=cd8
Addr_TRACK2_PHI_lane9=cd9

Addr_TRACK2_DELTATHETA_lane0=ce0
Addr_TRACK2_DELTATHETA_lane1=ce1
Addr_TRACK2_DELTATHETA_lane2=ce2
Addr_TRACK2_DELTATHETA_lane3=ce3
Addr_TRACK2_DELTATHETA_lane4=ce4
Addr_TRACK2_DELTATHETA_lane5=ce5
Addr_TRACK2_DELTATHETA_lane6=ce6
Addr_TRACK2_DELTATHETA_lane7=ce7
Addr_TRACK2_DELTATHETA_lane8=ce8
Addr_TRACK2_DELTATHETA_lane9=ce9

Addr_TRACK2_MM_lane0=cf0
Addr_TRACK2_MM_lane1=cf1
Addr_TRACK2_MM_lane2=cf2
Addr_TRACK2_MM_lane3=cf3
Addr_TRACK2_MM_lane4=cf4
Addr_TRACK2_MM_lane5=cf5
Addr_TRACK2_MM_lane6=cf6
Addr_TRACK2_MM_lane7=cf7
Addr_TRACK2_MM_lane8=cf8
Addr_TRACK2_MM_lane9=cf9

Addr_TRACK2_sTGC_lane0=d00
Addr_TRACK2_sTGC_lane1=d01
Addr_TRACK2_sTGC_lane2=d02
Addr_TRACK2_sTGC_lane3=d03
Addr_TRACK2_sTGC_lane4=d04
Addr_TRACK2_sTGC_lane5=d05
Addr_TRACK2_sTGC_lane6=d06
Addr_TRACK2_sTGC_lane7=d07
Addr_TRACK2_sTGC_lane8=d08
Addr_TRACK2_sTGC_lane9=d09

Addr_TRACK2_SPARE_lane0=d10
Addr_TRACK2_SPARE_lane1=d11
Addr_TRACK2_SPARE_lane2=d12
Addr_TRACK2_SPARE_lane3=d13
Addr_TRACK2_SPARE_lane4=d14
Addr_TRACK2_SPARE_lane5=d15
Addr_TRACK2_SPARE_lane6=d16
Addr_TRACK2_SPARE_lane7=d17
Addr_TRACK2_SPARE_lane8=d18
Addr_TRACK2_SPARE_lane9=d19

Addr_TRACK3_ETA_lane0=d20
Addr_TRACK3_ETA_lane1=d21
Addr_TRACK3_ETA_lane2=d22
Addr_TRACK3_ETA_lane3=d23
Addr_TRACK3_ETA_lane4=d24
Addr_TRACK3_ETA_lane5=d25
Addr_TRACK3_ETA_lane6=d26
Addr_TRACK3_ETA_lane7=d27
Addr_TRACK3_ETA_lane8=d28
Addr_TRACK3_ETA_lane9=d29

Addr_TRACK3_PHI_lane0=d30
Addr_TRACK3_PHI_lane1=d31
Addr_TRACK3_PHI_lane2=d32
Addr_TRACK3_PHI_lane3=d33
Addr_TRACK3_PHI_lane4=d34
Addr_TRACK3_PHI_lane5=d35
Addr_TRACK3_PHI_lane6=d36
Addr_TRACK3_PHI_lane7=d37
Addr_TRACK3_PHI_lane8=d38
Addr_TRACK3_PHI_lane9=d39

Addr_TRACK3_DELTATHETA_lane0=d40
Addr_TRACK3_DELTATHETA_lane1=d41
Addr_TRACK3_DELTATHETA_lane2=d42
Addr_TRACK3_DELTATHETA_lane3=d43
Addr_TRACK3_DELTATHETA_lane4=d44
Addr_TRACK3_DELTATHETA_lane5=d45
Addr_TRACK3_DELTATHETA_lane6=d46
Addr_TRACK3_DELTATHETA_lane7=d47
Addr_TRACK3_DELTATHETA_lane8=d48
Addr_TRACK3_DELTATHETA_lane9=d49

Addr_TRACK3_MM_lane0=d50
Addr_TRACK3_MM_lane1=d51
Addr_TRACK3_MM_lane2=d52
Addr_TRACK3_MM_lane3=d53
Addr_TRACK3_MM_lane4=d54
Addr_TRACK3_MM_lane5=d55
Addr_TRACK3_MM_lane6=d56
Addr_TRACK3_MM_lane7=d57
Addr_TRACK3_MM_lane8=d58
Addr_TRACK3_MM_lane9=d59

Addr_TRACK3_sTGC_lane0=d60
Addr_TRACK3_sTGC_lane1=d61
Addr_TRACK3_sTGC_lane2=d62
Addr_TRACK3_sTGC_lane3=d63
Addr_TRACK3_sTGC_lane4=d64
Addr_TRACK3_sTGC_lane5=d65
Addr_TRACK3_sTGC_lane6=d66
Addr_TRACK3_sTGC_lane7=d67
Addr_TRACK3_sTGC_lane8=d68
Addr_TRACK3_sTGC_lane9=d69

Addr_TRACK3_SPARE_lane0=d70
Addr_TRACK3_SPARE_lane1=d71
Addr_TRACK3_SPARE_lane2=d72
Addr_TRACK3_SPARE_lane3=d73
Addr_TRACK3_SPARE_lane4=d74
Addr_TRACK3_SPARE_lane5=d75
Addr_TRACK3_SPARE_lane6=d76
Addr_TRACK3_SPARE_lane7=d77
Addr_TRACK3_SPARE_lane8=d78
Addr_TRACK3_SPARE_lane9=d79

echo "Board Address: " $1
###### Disable TrackOutputGenerator
vmeput 0x"$1"c004 0x0 
###### Eable TrackOutputGenerator
vmeput 0x"$1"c004 0x1 

###### Sector0
vmeput 0x"$1"c104 0x1  ## Track0 ROI
vmeput 0x"$1"c114 0x2  ## Track1 ROI
vmeput 0x"$1"c124 0x3  ## Track2 ROI
vmeput 0x"$1"c134 0x4  ## Track3 ROI
vmeput 0x"$1"c144 0xa  ## Track0 pT
vmeput 0x"$1"c154 0xb  ## Track1 pT
vmeput 0x"$1"c164 0xc  ## Track2 pT
vmeput 0x"$1"c174 0xd  ## Track3 pT
vmeput 0x"$1"c184 0x1  ## Track0 sign
vmeput 0x"$1"c194 0x1  ## Track1 sign
vmeput 0x"$1"c1a4 0x1  ## Track2 sign
vmeput 0x"$1"c1b4 0x1  ## Track3 sign
vmeput 0x"$1"c1c4 0x4  ## Track0 InnerCoincidenceFlag
vmeput 0x"$1"c1d4 0x5  ## Track1 InnerCoincidenceFlag
vmeput 0x"$1"c1e4 0x6  ## Track2 InnerCoincidenceFlag
vmeput 0x"$1"c1f4 0x7  ## Track3 InnerCoincidenceFlag
vmeput 0x"$1"c204 0x1  ## Global Flag Sector0

### Sector1
vmeput 0x"$1"c304 0x5  ## Track0 ROI
vmeput 0x"$1"c314 0x4  ## Track1 ROI
vmeput 0x"$1"c324 0x3  ## Track2 ROI
vmeput 0x"$1"c334 0x2 ## Track3 ROI
vmeput 0x"$1"c344 0xf  ## Track0 pT
vmeput 0x"$1"c354 0xe  ## Track1 pT
vmeput 0x"$1"c364 0xd  ## Track2 pT
vmeput 0x"$1"c374 0xc  ## Track3 pT
vmeput 0x"$1"c384 0x0  ## Track0 sign
vmeput 0x"$1"c394 0x0  ## Track1 sign
vmeput 0x"$1"c3a4 0x0  ## Track2 sign
vmeput 0x"$1"c3b4 0x0  ## Track3 sign
vmeput 0x"$1"c3c4 0x7  ## Track0 InnerCoincidenceFlag
vmeput 0x"$1"c3d4 0x6  ## Track1 InnerCoincidenceFlag
vmeput 0x"$1"c3e4 0x5  ## Track2 InnerCoincidenceFlag
vmeput 0x"$1"c3f4 0x4  ## Track3 InnerCoincidenceFlag
vmeput 0x"$1"c404 0x1  ## Global Flag 

###### Disable TrackOutputGenerator
#vmeput 0x"$1"c004 0x0 
