#!/bin/bash

bdaddr=$1
lane=$2
fpga=4

emuENB=${bdaddr}8d0${fpga}
emuLEN=${bdaddr}8d1${fpga}
emuHDR=${bdaddr}8d2${fpga}

tr0ETA=${bdaddr}8e0${fpga}
tr0PHI=${bdaddr}8e1${fpga}
tr0DTH=${bdaddr}8e2${fpga}
tr0_MM=${bdaddr}8e3${fpga}
tr0TGC=${bdaddr}8e4${fpga}
tr0SPR=${bdaddr}8e5${fpga}

tr1ETA=${bdaddr}8e6${fpga}
tr1PHI=${bdaddr}8e7${fpga}
tr1DTH=${bdaddr}8e8${fpga}
tr1_MM=${bdaddr}8e9${fpga}
tr1TGC=${bdaddr}8ea${fpga}
tr1SPR=${bdaddr}8eb${fpga}

tr2ETA=${bdaddr}8ec${fpga}
tr2PHI=${bdaddr}8ed${fpga}
tr2DTH=${bdaddr}8ee${fpga}
tr2_MM=${bdaddr}8ef${fpga}
tr2TGC=${bdaddr}8f0${fpga}
tr2SPR=${bdaddr}8f1${fpga}

tr3ETA=${bdaddr}8f2${fpga}
tr3PHI=${bdaddr}8f3${fpga}
tr3DTH=${bdaddr}8f4${fpga}
tr3_MM=${bdaddr}8f5${fpga}
tr3TGC=${bdaddr}8f6${fpga}
tr3SPR=${bdaddr}8f7${fpga}


monFIFO_0=${bdaddr}700${fpga}
monFIFO_1=${bdaddr}701${fpga}
monFIFO_2=${bdaddr}702${fpga}
monFIFO_3=${bdaddr}703${fpga}
monFIFO_4=${bdaddr}704${fpga}
monFIFO_5=${bdaddr}705${fpga}
monFIFO_6=${bdaddr}706${fpga}
monFIFO_7=${bdaddr}707${fpga}
monFIFO_8=${bdaddr}708${fpga}
monFIFO_9=${bdaddr}709${fpga}
monFIFO_10=${bdaddr}70a${fpga}
monFIFO_11=${bdaddr}70b${fpga}
monFIFO_12=${bdaddr}70c${fpga}
monFIFO_13=${bdaddr}70d${fpga}
monFIFO_14=${bdaddr}70e${fpga}
monFIFO_15=${bdaddr}70f${fpga}

monWR_ENB=${bdaddr}710${fpga}
monRD_ENB=${bdaddr}711${fpga}
mon_lane=${bdaddr}712${fpga}

FIFO_RST=${bdaddr}10a${fpga}
TX_LGC_RST=${bdaddr}10d${fpga}

# emulator length
vmeput /dev/vmedrv32d16 ${emuLEN} 0xFFFF
usleep 1

vmeput /dev/vmedrv32d16 ${emuHDR} 0xbcbc
usleep 1

# disable GTX emulator
vmeput /dev/vmedrv32d16 ${emuENB} 0x0
usleep 1

#reset TX_Logic
#not always needed
#vmeput /dev/vmedrv32d16 ${TX_LGC_RST} 0x1
#usleep 1

vmeput /dev/vmedrv32d16 $tr0ETA 0xa
usleep 1
vmeput /dev/vmedrv32d16 $tr0PHI 0x1
usleep 1
vmeput /dev/vmedrv32d16 $tr0DTH 0x0
usleep 1
vmeput /dev/vmedrv32d16 $tr0_MM 0x0
usleep 1
vmeput /dev/vmedrv32d16 $tr0TGC 0x0
usleep 1
vmeput /dev/vmedrv32d16 $tr0SPR 0x0
usleep 1


vmeput /dev/vmedrv32d16 $tr1ETA 0x1b
usleep 1
vmeput /dev/vmedrv32d16 $tr1PHI 0x2
usleep 1
vmeput /dev/vmedrv32d16 $tr1DTH 0x0
usleep 1
vmeput /dev/vmedrv32d16 $tr1_MM 0x0
usleep 1
vmeput /dev/vmedrv32d16 $tr1TGC 0x0
usleep 1
vmeput /dev/vmedrv32d16 $tr1SPR 0x0
usleep 1

vmeput /dev/vmedrv32d16 $tr2ETA 0x2c
usleep 1
vmeput /dev/vmedrv32d16 $tr2PHI 0x3
usleep 1
vmeput /dev/vmedrv32d16 $tr2DTH 0x0
usleep 1
vmeput /dev/vmedrv32d16 $tr2_MM 0x0
usleep 1
vmeput /dev/vmedrv32d16 $tr2TGC 0x0
usleep 1
vmeput /dev/vmedrv32d16 $tr2SPR 0x0
usleep 1

vmeput /dev/vmedrv32d16 $tr3ETA 0x3d
usleep 1
vmeput /dev/vmedrv32d16 $tr3PHI 0x4
usleep 1
vmeput /dev/vmedrv32d16 $tr3DTH 0x0
usleep 1
vmeput /dev/vmedrv32d16 $tr3_MM 0x0
usleep 1
vmeput /dev/vmedrv32d16 $tr3TGC 0x0
usleep 1
vmeput /dev/vmedrv32d16 $tr3SPR 0x0
usleep 1

#TX logic reset off
vmeput /dev/vmedrv32d16 ${TX_LGC_RST} 0x0
usleep 1
# enable GTX emulator
vmeput /dev/vmedrv32d16 ${emuENB} 0x1
usleep 1

#vmeput /dev/vmedrv32d16 ${mon_lane} 0x${lane}
#usleep 1
#
#vmeput /dev/vmedrv32d16 ${monWR_ENB} 0x0
#usleep 1
#
#vmeput /dev/vmedrv32d16 ${FIFO_RST} 0x1
#sleep 0.01
#vmeput /dev/vmedrv32d16 ${FIFO_RST} 0x0
#usleep 1
#
#vmeput /dev/vmedrv32d16 ${monWR_ENB} 0x1
#sleep 0.1
#vmeput /dev/vmedrv32d16 ${monWR_ENB} 0x0
#
##vmeput /dev/vmedrv32d16 ${monRD_ENB} 0x0
##usleep 1
#
#
##vmeput /dev/vmedrv32d16 ${monRD_ENB} 0x1
##usleep 1
#
#
#
#a=0
#num0=0; num1=0; num2=0; num3=0; num4=0; num5=0; num6=0; num7=0;
#num8=0; num9=0; numa=0; numb=0; numc=0; numd=0; nume=0; numf=0;
#
#while [ $a -lt 128 ]
#do
#      num0=`vmeget /dev/vmedrv32d16 ${monFIFO_0}` # | cut -c 7-10`
#      num1=`vmeget /dev/vmedrv32d16 ${monFIFO_1}` # | cut -c 7-10`
#      num2=`vmeget /dev/vmedrv32d16 ${monFIFO_2}` # | cut -c 7-10`
#      num3=`vmeget /dev/vmedrv32d16 ${monFIFO_3}` # | cut -c 7-10`
#      num4=`vmeget /dev/vmedrv32d16 ${monFIFO_4}` # | cut -c 7-10`
#      num5=`vmeget /dev/vmedrv32d16 ${monFIFO_5}` # | cut -c 7-10`
#      num6=`vmeget /dev/vmedrv32d16 ${monFIFO_6}` # | cut -c 7-10`
#      num7=`vmeget /dev/vmedrv32d16 ${monFIFO_7}` # | cut -c 7-10`
#      num8=`vmeget /dev/vmedrv32d16 ${monFIFO_8}` # | cut -c 7-10` 
#      num9=`vmeget /dev/vmedrv32d16 ${monFIFO_9}` # | cut -c 7-10`  
#      numa=`vmeget /dev/vmedrv32d16 ${monFIFO_10}` # | cut -c 7-10`  
#      numb=`vmeget /dev/vmedrv32d16 ${monFIFO_11}` # | cut -c 7-10`  
#      numc=`vmeget /dev/vmedrv32d16 ${monFIFO_12}` # | cut -c 7-10`  
#      numd=`vmeget /dev/vmedrv32d16 ${monFIFO_13}` # | cut -c 7-10`  
#      nume=`vmeget /dev/vmedrv32d16 ${monFIFO_14}` # | cut -c 7-10`  
#      numf=`vmeget /dev/vmedrv32d16 ${monFIFO_15}` # | cut -c 7-10`  
#
#      vmeput /dev/vmedrv32d16 ${monRD_ENB} 0x1 
#      result1=`vmeget /dev/vmedrv32d16 ${monRD_ENB} `
#      if [ "${result1}" = "0x00000000" ]; then
#          echo "Error: read command failed"
#      fi
#
#      vmeput /dev/vmedrv32d16 ${monRD_ENB} 0x0 
#      result1=`vmeget /dev/vmedrv32d16 ${monRD_ENB}`
#      if [ "${result1}" = "0x00000001" ]; then
#          echo "Error: read resume failed"
#      fi
#
#      echo "$numf $nume $numd $numc $numb $numa $num9 $num8 $num7 $num6 $num5 $num4 $num3 $num2 $num1 $num0"
#
#(( a++ ))
#done
#
