#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>
#include <iomanip>

using namespace std;

int main(){
	const int TRACK_NUM = 4; // track0 ~ track3
	const int LANE_NUM = 7; // lane0 ~ lane6
	const int DATA_TYPE_NUM = 6; // spare, sTGC, MM, dtheta, phi, eta
	const int DATA_NUM = 4; // num of data pattern

	const int LANE_disp = DATA_NUM;
	const int TYPE_disp = LANE_disp * LANE_NUM;
	const int TRACK_disp = TYPE_disp * DATA_TYPE_NUM;

	vector<string> vs(DATA_TYPE_NUM);
	vs[0] = "SPARE";
	vs[1] = "sTGC";
	vs[2] = "MM";
	vs[3] = "DTHETA";
	vs[4] = "PHI";
	vs[5] = "ETA";

	// for address definition
	for(int track=0; track<TRACK_NUM; track++){
		for(int type=0; type<DATA_TYPE_NUM; type++){
			for(int lane=0; lane<LANE_NUM; lane++){
				for(int data=0;data<DATA_NUM; data++){
					string command = "`define Addr_";
					command += "TRACK" + to_string(track) + "_";
					command += vs[type] + "_";
					command += "lane" + to_string(lane) + "_";
					command += to_string(data);
					int address = track * TRACK_disp + type * TYPE_disp + lane * LANE_disp + data + 3072;
					stringstream ss;
					ss << std::hex << address;
					cout << command << "     " << " 12'h" << ss.str() << endl;
				}
				cout << endl;
			}
		}
	}

	// for output registers
	for(int track=0; track<TRACK_NUM; track++){
		for(int type=0; type<DATA_TYPE_NUM; type++){
			for(int lane=0; lane<LANE_NUM; lane++){
				for(int data=0;data<DATA_NUM; data++){
					string command = "output reg ";
					if(vs[type] == "SPARE"){
					}else if(vs[type] == "sTGC"){
						command += "[1:0] ";
					}else if(vs[type] == "MM"){
						command += "[1:0] ";
					}else if(vs[type] == "DTHETA"){
						command += "[4:0] ";
					}else if(vs[type] == "PHI"){
						command += "[5:0] ";
					}else if(vs[type] == "ETA"){
						command += "[7:0] ";
					}else{
						cout << "ERRROR " << command << endl;
					}
					command += "TRACK" + to_string(track) + "_";
					command += vs[type] + "_";
					command += "lane" + to_string(lane) + "_";
					command += "data" + to_string(data) + ",";

					cout << command << endl;
				}
				cout << endl;
			}
		}
	}

	// for initial begin
	for(int track=0; track<TRACK_NUM; track++){
		for(int type=0; type<DATA_TYPE_NUM; type++){
			for(int lane=0; lane<LANE_NUM; lane++){
				for(int data=0;data<DATA_NUM; data++){
					string command;
					command += "TRACK" + to_string(track) + "_";
					command += vs[type] + "_";
					command += "lane" + to_string(lane) + "_";
					command += "data" + to_string(data) + " <= ";

					if(vs[type] == "SPARE"){
						command += "1'b0";
					}else if(vs[type] == "sTGC"){
						command += "2'b0";
					}else if(vs[type] == "MM"){
						command += "2'b0";
					}else if(vs[type] == "DTHETA"){
						command += "5'b0";
					}else if(vs[type] == "PHI"){
						command += "6'b0";
					}else if(vs[type] == "ETA"){
						command += "8'b0";
					}else{
						cout << "ERRROR " << command << endl;
					}
					command += ";";
					cout << command << endl;
				}
				cout << endl;
			}
		}
	}

	//for vme write
	for(int track=0; track<TRACK_NUM; track++){
		for(int type=0; type<DATA_TYPE_NUM; type++){
			for(int lane=0; lane<LANE_NUM; lane++){
				for(int data=0;data<DATA_NUM; data++){
					string command;
					command += "TRACK" + to_string(track) + "_";
					command += vs[type] + "_";
					command += "lane" + to_string(lane) + "_";
					command += "data" + to_string(data);

					if(vs[type] == "SPARE"){
					}else if(vs[type] == "sTGC"){
						command += "[1:0] ";
					}else if(vs[type] == "MM"){
						command += "[1:0] ";
					}else if(vs[type] == "DTHETA"){
						command += "[4:0] ";
					}else if(vs[type] == "PHI"){
						command += "[5:0] ";
					}else if(vs[type] == "ETA"){
						command += "[7:0] ";
					}else{
						cout << "ERRROR " << command << endl;
					}
					command += "<= VME_DIN";
					if(vs[type] == "SPARE"){
						command += "[0]";
					}else if(vs[type] == "sTGC"){
						command += "[1:0]";
					}else if(vs[type] == "MM"){
						command += "[1:0]";
					}else if(vs[type] == "DTHETA"){
						command += "[4:0]";
					}else if(vs[type] == "PHI"){
						command += "[5:0]";
					}else if(vs[type] == "ETA"){
						command += "[7:0]";
					}else{
						cout << "ERRROR " << command << endl;
					}
					command += ";";

					string address = "`Addr_";
					address += "TRACK" + to_string(track) + "_";
					address += vs[type] + "_";
					address += "lane" + to_string(lane) + "_";
					address += to_string(data);

					cout << address << " : begin" << endl
					     << "    " << command << endl
					     << "end" << endl;
				}
				cout << endl;
			}
		}
	}

	//for vme read
	for(int track=0; track<TRACK_NUM; track++){
		for(int type=0; type<DATA_TYPE_NUM; type++){
			for(int lane=0; lane<LANE_NUM; lane++){
				for(int data=0;data<DATA_NUM; data++){
					string command;
					command += "VME_DOUT[15:0] <= {";
					if(vs[type] == "SPARE"){
						command += "15'b0,"; 
					}else if(vs[type] == "sTGC"){
						command += "14'b0,"; 
					}else if(vs[type] == "MM"){
						command += "14'b0,"; 
					}else if(vs[type] == "DTHETA"){
						command += "11'b0,"; 
					}else if(vs[type] == "PHI"){
						command += "10'b0,"; 
					}else if(vs[type] == "ETA"){
						command += "8'b0,"; 
					}else{
						cout << "ERRROR " << command << endl;
					}
					command += " TRACK" + to_string(track) + "_";
					command += vs[type] + "_";
					command += "lane" + to_string(lane) + "_";
					command += "data" + to_string(data);

					if(vs[type] == "SPARE"){
					}else if(vs[type] == "sTGC"){
						command += "[1:0]";
					}else if(vs[type] == "MM"){
						command += "[1:0]";
					}else if(vs[type] == "DTHETA"){
						command += "[4:0]";
					}else if(vs[type] == "PHI"){
						command += "[5:0]";
					}else if(vs[type] == "ETA"){
						command += "[7:0]";
					}else{
						cout << "ERRROR " << command << endl;
					}
					command += "};";

					string address = "`Addr_";
					address += "TRACK" + to_string(track) + "_";
					address += vs[type] + "_";
					address += "lane" + to_string(lane) + "_";
					address += to_string(data);

					cout << address << " : begin" << endl
					     << "    " << command << endl
					     << "end" << endl;
				}
				cout << endl;
			}
		}
	}

	// for port connection
	for(int track=0; track<TRACK_NUM; track++){
		for(int type=0; type<DATA_TYPE_NUM; type++){
			for(int lane=0; lane<LANE_NUM; lane++){
				for(int data=0;data<DATA_NUM; data++){
					string out_port = ".";
					out_port += "TRACK" + to_string(track) + "_";
					out_port += vs[type] + "_";
					out_port += "lane" + to_string(lane) + "_";
					out_port += "data" + to_string(data);

					string in_port;
					in_port += "TRACK" + to_string(track) + "_";
					in_port += vs[type] + "_";
					in_port += "lane" + to_string(lane);
					in_port += "[" + to_string(data) + "]";
					
					cout << out_port << "(" << in_port << ")," << endl;
				}
				cout << endl;
			}
		}
	}
	
	// for address definition
	for(int track=0; track<TRACK_NUM; track++){
		for(int type=0; type<DATA_TYPE_NUM; type++){
			for(int lane=0; lane<LANE_NUM; lane++){
				for(int data=0;data<DATA_NUM; data++){
					int address = track * TRACK_disp + type * TYPE_disp + lane * LANE_disp + data + 3072;
					stringstream ss;
					ss << std::hex << address;
					string str_addr = ss.str();
					while(str_addr.length() < 3){
						str_addr = "0" + str_addr;
					}
					cout << vs[type] << "[" << track << "][" << lane << "][" << data << "] = '" << str_addr << "'" << endl;
				}
				cout << endl;
			}
		}
	}

	// for NewSLFirmware.v wire decleration
	for(int track=0; track<TRACK_NUM; track++){
		for(int type=0; type<DATA_TYPE_NUM; type++){
			for(int lane=0; lane<LANE_NUM; lane++){
					string command = "wire ";
					if(vs[type] == "SPARE"){
					}else if(vs[type] == "sTGC"){
						command += "[1:0]";
					}else if(vs[type] == "MM"){
						command += "[1:0]";
					}else if(vs[type] == "DTHETA"){
						command += "[4:0]";
					}else if(vs[type] == "PHI"){
						command += "[5:0]";
					}else if(vs[type] == "ETA"){
						command += "[7:0]";
					}else{
						cout << "ERRROR " << command << endl;
					}
					command += "  TRACK" + to_string(track) + "_" + vs[type] + "_" + "lane" + to_string(lane) + "[3:0]";
					cout << command << endl;
				}
			cout << endl;
		}
	}

	return 0;
}
